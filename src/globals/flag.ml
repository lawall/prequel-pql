(*
 * This file is part of Prequel, lincensed under the terms of the GPL v2.
 * See copyright.txt in the Prequel source code for more information.
 * The Prequel source code can be obtained at
 * https://github.com/prequel-pql/prequel
 *)

(*
 * This file is part of Coccinelle, lincensed under the terms of the GPL v2.
 * See copyright.txt in the Coccinelle source code for more information.
 * The Coccinelle source code can be obtained at http://coccinelle.lip6.fr
 *)

(* spatch version *)
let spatch = ref ""
let suffixes = ref [".c";".h"]

let sgrep_mode2 = ref false

let track_iso_usage = ref false

let cores = ref 0

let gitpath = ref "/var/linuxes/linux-next"
let reference_version = ref ""
let commits = ref ([] : string list)
let tmp = ref "/tmp/pi"
let debug = ref false
let top_output = ref false
let u3index = ref ""
let u0mindex = ref ""
let u0pindex = ref ""
let fileindex = ref ""

(*"Some" value is the path with respect to which the patch should be created*)
let patch = ref (None : string option)

let currentfile = ref (None : string option) (* file of current code *)
let currentfiles = ref ([] : string list) (* starting files of this run *)

let current_element = ref ""
let dir = ref ""

let defined_virtual_rules = ref ([] : string list)
let defined_virtual_env = ref ([] : (string*string) list)

let set_defined_virtual_rules s =
  match Str.split_delim (Str.regexp "=") s with
    [_] -> defined_virtual_rules := s :: !defined_virtual_rules
  | name::vl ->
      let vl = String.concat "=" vl in
      defined_virtual_env := (name,vl) :: !defined_virtual_env
  | _ -> failwith "nothing defined"

let c_plus_plus = ref false
let ibm = ref false

(* was in main *)
let include_headers = ref false

exception UnreadableFile of string

let cocci_attribute_names = ref ([] : string list)
let add_cocci_attribute_names s =
  if not (List.mem s !cocci_attribute_names)
  then cocci_attribute_names := s :: !cocci_attribute_names

type diff_type = LINE | WORD
let difftype = ref LINE

let all_at_once = ref false
let show_unmatched_lines = ref false
let show_unmatched_code = ref false

let clean_reference_version_name = ref ""

let setup_reference_version s =
  if Sys.file_exists s
  then reference_version := s
  else if Sys.file_exists !gitpath
  then
    begin
      let dir = Printf.sprintf "/tmp/linux%d" (Random.int 1000) in
      clean_reference_version_name := dir;
      (if Sys.file_exists dir then failwith "please clean /tmp");
      ignore
	(Sys.command
	   (Printf.sprintf "git clone -l -s -n %s %s" !gitpath dir));
      ignore
	(Sys.command
	   (Printf.sprintf "cd %s; git checkout %s" dir s));
      reference_version := dir
    end
  else failwith "--git option must precede --ref option"

let clean_reference_version _ =
  if !clean_reference_version_name <> ""
  then
    ignore
      (Sys.command
	 (Printf.sprintf "/bin/rm -rf %s"
	    !clean_reference_version_name))
