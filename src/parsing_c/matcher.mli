type tok = D of Lexer_c.tokens | B (* data and blank *)

val seq_match : Lexer_c.tokens list -> Lexer_c.tokens list ->
  tok list * tok list

val punct_weight : float ref
val kwd_weight : float ref
val id_weight : float ref
val cst_weight : float ref
val idmm_weight : float ref
val cstmm_weight : float ref
val blank_weight : float ref
val mismatch_weight : float ref
