module L = Lexer_c

type tok = D of L.tokens | B (* data and blank *)
type dir = L of L.tokens | U of L.tokens
| UL of L.tokens * L.tokens (* left, up, upper left *)

let punct_weight = ref 0.
let kwd_weight = ref 0.
let id_weight = ref 0.
let cst_weight = ref 0.
let idmm_weight = ref 0.
let cstmm_weight = ref 0.
let blank_weight = ref (-1.)
let mismatch_weight = ref (-3.)

let same (s1,h1,_) (s2,h2,_) =
  h1 = h2 && s1 = s2

let weight i j info =
  let res =
    match info with
      (D(L.Punct c1),D(L.Punct c2)) when same c1 c2 -> !punct_weight(*5*)
    | (D(L.Kwd c1),D(L.Kwd c2)) when same c1 c2 -> !kwd_weight(*4*)
    | (D(L.Ident c1),D(L.Ident c2)) when same c1 c2 -> !id_weight(*3*)
    | (D(L.Cst c1),D(L.Cst c2)) when same c1 c2 -> !cst_weight(*2*)
    | (D(L.Ident c1),D(L.Ident c2)) -> !idmm_weight(*1*)
    | (D(L.Cst c1),D(L.Cst c2)) -> !cstmm_weight(*1*)
    | (D(t),B) | (B,D(t)) -> !blank_weight(*-1*)
    | _ -> !mismatch_weight(*-3*) in
  let i = 1. +. float_of_int i in
  let j = 1. +. float_of_int j in
  let penalty = sqrt(1. -. ((min i j) /. (max i j))) in
  (* penalty is to disfavor matching same tokens at different places in
     the strings, not sure if this is the best strategy *)
  res -. penalty

let seq_match s1 s2 =
  let l1 = List.length s1 in
  let l2 = List.length s2 in
  let mat = Array.make_matrix (l1+1) (l2+1) (0.,UL(L.EOF,L.EOF)) in
  (* basis *)
  List.iteri
    (fun i e ->
      mat.(i+1).(0) <- (fst(mat.(i).(0)) +. weight i i (D(e),B),U(e)))
    s1;
  List.iteri
    (fun j e ->
      mat.(0).(j+1) <- (fst(mat.(0).(j)) +. weight j j (B,D(e)),L(e)))
    s2;
  (* rest *)
  List.iteri
    (fun i e1 ->
      List.iteri
	(fun j e2 ->
	  let ul = fst(mat.(i).(j)) +. weight i j (D(e1),D(e2)) in
	  let u = fst(mat.(i).(j+1)) +. weight i i (D(e1),B) in
	  let l = fst(mat.(i+1).(j)) +. weight j j (B,D(e2)) in
	  let mx = max (max ul u) l in
	  mat.(i+1).(j+1) <-
	    (if ul > u && ul > l
	    then (ul,UL(e1,e2))
	    else if l > ul && l > u
	    then (l,L(e2))
	    else (u,U(e1))))
	s2)
    s1;
  let rec rebuild acc1 acc2 i j =
    if i = 0 && j = 0
    then (acc1,acc2)
    else
      match snd(mat.(i).(j)) with
	UL(e1,e2) -> rebuild (D(e1)::acc1) (D(e2)::acc2) (i-1) (j-1)
      | U(e1) -> rebuild (D(e1)::acc1) (B::acc2) (i-1) j
      | L(e2) -> rebuild (B::acc1) (D(e2)::acc2) i (j-1) in
  let res = rebuild [] [] l1 l2 in
  flush stderr;
  res
