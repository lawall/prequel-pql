PARMAP=$(shell ocamlfind query parmap)

all: parse_c.ml lexer_c.mll
	ocamllex lexer_c.mll
	ocamlfind ocamlc -g -I ../commons -I ../commons/ocamlextra -I ../globals  -o parse_c str.cma unix.cma bigarray.cma nums.cma $(PARMAP)/parmap.cma -I $(PARMAP) ../commons/commons.cma ../globals/globals.cma lexer_c.ml matcher.ml get_data2.ml parse_c.ml


ifneq ($(MAKECMDGOALS),distclean)
include ../Makefile.config
-include ../Makefile.local
endif

TARGET=c_parser

OCAMLCFLAGS ?= -g
OPTFLAGS ?= -g

LEXER_SOURCES = lexer_c.mll

SOURCES = $(LEXER_SOURCES:.mll=.ml) parse_c.ml

LIBS=../commons/commons.cma ../globals/globals.cma
SYSLIBS = str.cma unix.cma bigarray.cma nums.cma $(PARMAP)/parmap.cma

INCLUDES = -I ../commons \
	   -I ../commons/ocamlextra \
	   -I ../globals \
	   -I $(PARMAP)

# The Caml compilers.
OCAMLCFLAGS ?= -g -dtypes
EXEC=$(TARGET).byte
EXEC=$(TARGET)
LIB=$(TARGET).cma
OPTLIB=$(LIB:.cma=.cmxa)

GENERATED= $(LEXER_SOURCES:.mll=.ml)
OBJS = $(SOURCES:.ml=.cmo)
OPTOBJS = $(OBJS:.cmo=.cmx)


all: $(LIB)
local: $(EXEC)

all.opt:
	@$(MAKE) $(OPTLIB) BUILD_OPT=yes

$(LIB): $(GENERATED) $(OBJS)
	$(OCAMLC) $(OCAMLCFLAGS) $(INCLUDES) -I $(MENHIRDIR) -a -o $(LIB) $(MENHIRMOD) $(OBJS)


$(OPTLIB): $(GENERATED) $(OPTOBJS)
	$(OCAMLOPT) $(OPTFLAGS) $(INCLUDES) -I $(MENHIRDIR) -a -o $(OPTLIB) $(MENHIROMOD) $(OPTOBJS)


$(EXEC): $(OBJS) main.cmo $(LIBS)
	$(OCAMLC) $(OCAMLCFLAGS) $(INCLUDES) -o $(EXEC) $(SYSLIBS) $(LIBS) $(OBJS) main.cmo

clean::
	rm -f $(LIB)
	rm -f $(OPTLIB) $(LIB:.cma=.a)
	rm -f $(TARGET)


.SUFFIXES:
.SUFFIXES: .ml .mli .cmo .cmi .cmx

.ml.cmo:
	$(OCAMLC) $(OCAMLCFLAGS) $(INCLUDES) -c $<

.mli.cmi:
	$(OCAMLC) $(OCAMLCFLAGS) $(INCLUDES) -c $<

.ml.cmx:
	$(OCAMLOPT) $(OPTFLAGS) $(INCLUDES) -c $<

$(LEXER_SOURCES:.mll=.ml) :	$(LEXER_SOURCES)
	$(OCAMLLEX) $(LEXER_SOURCES)

distclean:: clean
	if test -z "${KEEP_GENERATED}"; then \
		@echo cleaning generated parsers and lexers; \
		rm -f $(GENERATED); fi

# clean rule for others files
clean::
	rm -f *.cm[iox] *.o *.annot
	rm -f *~ .*~ #*#
	rm -f .depend

.PHONY: depend
.depend depend: $(GENERATED)
	$(OCAMLDEP) *.mli *.ml > .depend

lexer_cocci.ml: lexer_cocci.mll

ifneq ($(MAKECMDGOALS),clean)
ifneq ($(MAKECMDGOALS),distclean)
-include .depend
endif
endif
