let split_at_comma s =
  match Str.split (Str.regexp ",") s with
    [x] -> 1
  | [x;y] -> int_of_string y
  | _ -> failwith "bad @@ line"

let rec readn i = function
    0 -> []
  | n ->
      let l = input_line i in
      let l = String.sub l 1 (String.length l - 1) in
      l :: readn i (n-1)

let get_data file =
  let i = open_in file (*"test31_32"*) in
  let res = ref [] in
  let rec loop c =
    let l = input_line i in
    match Str.split_delim (Str.regexp " ") l with
      ["commit";c] -> loop c
    | "@@"::b::a::"@@"::_ ->
	let lenmin = split_at_comma b in
	let lenplu = split_at_comma a in
	(if lenmin > 0 && lenplu > 0 && (lenmin * lenplu <= 5000)
	then
	  let min = readn i lenmin in
	  let plu = readn i lenplu in
	  res := (c,b,a,min,plu) :: !res);
	loop c
    | _ -> loop c in
  try loop "" with End_of_file -> (close_in i; List.rev !res)
	  
