{
(* Yoann Padioleau
 *
 * Copyright (C) 2002, 2006, 2007, 2008, 2009, Ecole des Mines de Nantes
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * file license.txt for more details.
 *)

(*****************************************************************************)
(*
 * Warning: ocamllex uses side effects on lexbuf.
 * For instance one must do
 *
 *  let info = tokinfo lexbuf in
 *  TComment (info +> tok_add_s (comment lexbuf))
 *
 * rather than
 *
 *   TComment (tokinfo lexbuf +> tok_add_s (comment lexbuf))
 *
 * because of the "weird" order of evaluation of OCaml.
 *
 *
 *
 * note: can't use Lexer_parser._lexer_hint here to do different
 * things, because now we call the lexer to get all the tokens
 * (tokens_all), and then we parse. So we can't have the _lexer_hint
 * info here. We can have it only in parse_c. For the same reason, the
 * typedef handling here is now useless.
 *)
(*****************************************************************************)
(*****************************************************************************)


exception Lexical of string

type tokinfo = string * int * (int * int)
type tokens = EOF | Ident of tokinfo | Cst of tokinfo | Kwd of tokinfo
| Punct of tokinfo

let ident(s,i) = Ident(s,Hashtbl.hash s,i)
let cst(s,i) = Cst(s,Hashtbl.hash s,i)
let kwd(s,i) = Kwd(s,Hashtbl.hash s,i)
let punct(s,i) = Punct(s,Hashtbl.hash s,i)

let tok     lexbuf  = Lexing.lexeme lexbuf

let tokinfo lexbuf  =
  let start_pos = Lexing.lexeme_start_p lexbuf in
  let line = start_pos.Lexing.pos_lnum in
  let col = start_pos.Lexing.pos_cnum - start_pos.Lexing.pos_bol in
   (line,col)

let newline lexbuf =
   let lcp = lexbuf.Lexing.lex_curr_p in
   lexbuf.Lexing.lex_curr_p <- { lcp with
   Lexing.pos_lnum = lcp.Lexing.pos_lnum + 1;
   Lexing.pos_bol = lcp.Lexing.pos_cnum;
   }
 
let init lexbuf lnum =
   let lcp = lexbuf.Lexing.lex_curr_p in
   lexbuf.Lexing.lex_curr_p <- { lcp with
   Lexing.pos_lnum = lnum;
   Lexing.pos_bol = 0;
   }

(* opti: less convenient, but using a hash is faster than using a match *)
let keyword_table = [

  (* c: *)
  "void"; "char"; "short"; "int"; "long"; "float"; "double"; "size_t";
  "ssize_t"; "ptrdiff_t";

  "unsigned"; "signed";

  "auto"; "register"; "extern"; "static";

  "const"; "volatile";

  "struct"; "union"; "enum"; "typedef";

  "if"; "else"; "break"; "continue"; "switch"; "case"; "default"; "for";
  "do"; "while"; "return"; "goto";

  "sizeof";


  (* gccext: cppext: linuxext: synonyms *)
  "asm"; "__asm__"; "__asm";

  "inline"; "__inline__"; "__inline";

  "__attribute__"; "__attribute";

  "typeof"; "__typeof__"; "__typeof";

        (* found a lot in expanded code *)
  "__extension__";


  (* gccext: alias *)
  "__signed__";

  "__const__"; "__const";

  "__volatile__"; "__volatile";

  (* windowsext: *)
  "__declspec";

  "__stdcall"; "__cdecl"; "WINAPI"; "APIENTRY"; "CALLBACK";

  (* c99:  *)
  (* no just "restrict" ? maybe for backward compatibility they avoided
   * to use restrict which people may have used in their program already
   *)
  "__restrict"; "__restrict__"

 ]

}

(*****************************************************************************)
let letter = ['A'-'Z' 'a'-'z' '_']
let extended_letter = ['A'-'Z' 'a'-'z' '_' ':' '<' '>' '~'](*for c++, not used*)
let digit  = ['0'-'9']

let cplusplus_ident = (letter | '$') (letter | digit | '$') *
let cplusplus_ident_ext = (letter | '~' | '$') (letter | digit | '~' | '$') *

(* not used for the moment *)
let punctuation = ['!' '\"' '#' '%' '&' '\'' '(' ')' '*' '+' ',' '-' '.' '/' ':'
		   ';' '<' '=' '>' '?' '[' '\\' ']' '^' '{' '|' '}' '~']
let space = [' ' '\t' '\n' '\r' '\011' '\012' ]
let additionnal = [ ' ' '\b' '\t' '\011' '\n' '\r' '\007' ]
(* 7 = \a = bell in C. this is not the only char allowed !!
 * ex @ and $ ` are valid too
 *)

let cchar = (letter | digit | punctuation | additionnal)

let sp = [' ' '\t']+
let spopt = [' ' '\t']*

let dec = ['0'-'9']
let oct = ['0'-'7']
let hex = ['0'-'9' 'a'-'f' 'A'-'F']

let decimal = ('0' | (['1'-'9'] dec*))
let octal   = ['0']        oct+
let hexa    = ("0x" |"0X") hex+


let pent   = dec+
let pfract = dec+
let sign = ['-' '+']
let exp  = ['e''E'] sign? dec+
let real = pent exp | ((pent? '.' pfract | pent '.' pfract? ) exp?)
let ddecimal = ((pent? '.' pfract | pent '.' pfract? ))

let id = letter (letter | digit) *

(*****************************************************************************)
rule token = parse

  (* ----------------------------------------------------------------------- *)
  (* spacing/comments *)
  (* ----------------------------------------------------------------------- *)

  (* note: this lexer generates tokens for comments!! so can not give
   * this lexer as-is to the parsing function. The caller must preprocess
   * it, e.g. by using techniques like cur_tok ref in parse_c.ml.
   *
   * update: we now also generate a separate token for newlines, so now
   * the caller may also have to reagglomerate all those commentspace
   * tokens if it was assuming that spaces were agglomerate in a single
   * token.
   *)

  | ['\n']
      (* starting a new line; the newline character followed by whitespace *)
      { newline lexbuf; token lexbuf }
  | [' ' '\t' '\r' '\011' '\012' ]+
      { token lexbuf }
   | "/*" { comment lexbuf }


  (* C++ comment are allowed via gccext, but normally they are deleted by cpp.
   * So need this here only when dont call cpp before.
   * note that we don't keep the trailing \n; it will be in another token.
   *)
   | "//" [^'\r' '\n' '\011']*
      { token lexbuf }

  (* ----------------------------------------------------------------------- *)
  (* cpp *)
  (* ----------------------------------------------------------------------- *)

  (* old:
   *   | '#'		{ endline lexbuf} // should be line, and not endline
   *   and endline = parse  | '\n' 	{ token lexbuf}
   *                        |	_	{ endline lexbuf}
   *)

  (* less?:
   *  have found a # #else  in "newfile-2.6.c",  legal ?   and also a  #/* ...
   *    => just "#" -> token {lexbuf} (that is ignore)
   *  il y'a 1 #elif  sans rien  apres
   *  il y'a 1 #error sans rien  apres
   *  il y'a 2  mov dede, #xxx    qui genere du coup exn car
   *  entour� par des #if 0
   *  => make as for comment, call a comment_cpp that when #endif finish the
   *   comment and if other cpp stuff raise exn
   *  il y'a environ 10  #if(xxx)  ou le ( est coll� direct
   *  il y'a des include"" et include<
   *  il y'a 1 ` (derriere un #ifndef linux)
   *)



  (* ---------------------- *)
  (* misc *)
  (* ---------------------- *)

  (* bugfix: I want now to keep comments for the cComment study
   * so cant do:    sp [^'\n']+ '\n'
   * http://gcc.gnu.org/onlinedocs/gcc/Pragmas.html
   *)

  | "#" spopt "ident"   sp  [^'\n' '\r']* ('\n' | "\r\n")
  | "#" spopt "line"    sp  [^'\n' '\r']* ('\n' | "\r\n")
  | "#" spopt "error"   sp  [^'\n' '\r']* ('\n' | "\r\n")
  | "#" spopt "warning" sp  [^'\n' '\r']* ('\n' | "\r\n")
  | "#" spopt "abort"   sp  [^'\n' '\r']* ('\n' | "\r\n")
   { let info = tokinfo lexbuf in kwd (tok lexbuf, info) }

   | "#" [' ' '\t']* ('\n' | "\r\n")
   { let info = tokinfo lexbuf in kwd (tok lexbuf, info) }

  (* only after cpp, ex: # 1 "include/linux/module.h" 1 *)
   | "#" sp pent sp  '\"' [^ '\"']* '\"' (spopt pent)*  spopt ('\n' | "\r\n")
   { let info = tokinfo lexbuf in kwd (tok lexbuf, info) }

  (* ------------------------ *)
  (* #define, #undef, #pragma *)
  (* ------------------------ *)

  (* the rest of the lexing/parsing of define is done in fix_tokens_define
   * where we parse until a TCppEscapedNewline and generate a TDefEol
   *)
   | "#" [' ' '\t']* "define"
       { let info = tokinfo lexbuf in kwd (tok lexbuf, info) }

  (* note: in some cases can have stuff after the ident as in #undef XXX 50,
   * but I currently don't handle it cos I think it's bad code.
   *)
   | "#" [' ' '\t']* "undef"
       { let info = tokinfo lexbuf in kwd (tok lexbuf, info) }

  (* note: in some cases can have stuff after the ident as in #undef XXX 50,
   * but I currently don't handle it cos I think it's bad code.
   *)
   | ("#" [' ' '\t']* "pragma")
       { let info = tokinfo lexbuf in kwd (tok lexbuf, info) }

  (* ---------------------- *)
  (* #include *)
  (* ---------------------- *)

  (* The difference between a local "" and standard <> include is computed
   * later in parser_c.mly. So redo a little bit of lexing there; ugly but
   * simpler to generate a single token here.  *)
  | ("#" [' ''\t']* "include" [' ' '\t']*)
    ('\"' ([^ '\"']+) '\"' |
     '<' [^ '>']+ '>' |
      ['A'-'Z''_']+)
   { let info = tokinfo lexbuf in kwd (tok lexbuf, info) }
  (* gccext: found in glibc *)
  | ("#" [' ''\t']* "include_next" [' ' '\t']*)
    ('\"' ([^ '\"']+) '\"' |
     '<' [^ '>']+ '>' |
      ['A'-'Z''_']+)
   { let info = tokinfo lexbuf in kwd (tok lexbuf, info) }

  (* ---------------------- *)
  (* #ifdef *)
  (* ---------------------- *)

   | "#" [' ' '\t']* "ifdef"
       { let info = tokinfo lexbuf in kwd (tok lexbuf, info) }
   | "#" [' ' '\t']* "ifndef"
       { let info = tokinfo lexbuf in kwd (tok lexbuf, info) }
   | "#" [' ' '\t']* "if"
       { let info = tokinfo lexbuf in kwd (tok lexbuf, info) }
   | "#" [' ' '\t']* "elif"
       { let info = tokinfo lexbuf in kwd (tok lexbuf, info) }
   | "#" [' ' '\t']* "endif"
       { let info = tokinfo lexbuf in kwd (tok lexbuf, info) }

  (* ---------------------- *)
  (* #define body *)
  (* ---------------------- *)

  (* only in cpp directives normally *)
  | "\\" ('\n' | "\r\n") { let info = tokinfo lexbuf in kwd (tok lexbuf, info) }

  (* We must generate separate tokens for #, ## and extend the grammar.
   * Note there can be "elaborated" idents in many different places, in
   * expression but also in declaration, in function name. So having 3 tokens
   * for an ident does not work well with how we add info in
   * ast_c. Was easier to generate just one token, just one info,
   * even if have later to reanalyse those tokens and unsplit. But then,
   * handling C++ lead to having not just a string for ident but something
   * more complex. Also when we want to parse elaborated function headers
   * (e.g. void METH(foo)(int x)), we need anyway to go from a string
   * to something more. So having also for C something more than just
   * string for ident is natural.
   *
   * todo: our heuristics in parsing_hacks rely on TIdent. So maybe
   * an easier solution would be to augment the TIdent type such as
   *   TIdent of string * info * cpp_ident_additionnal_info
   *
   * old:
   * |  id   ([' ''\t']* "##" [' ''\t']* id)+
   *   { let info = tokinfo lexbuf in
   *     TIdent (tok lexbuf, info)
   *   }
   * |  "##" spopt id
   *   { let info = tokinfo lexbuf in
   *     TIdent (tok lexbuf, info)
   *   }
   *
   *)
  (* cppext: string concatenation of idents, also ##args for variadic macro. *)
  | "##" { let info = tokinfo lexbuf in kwd (tok lexbuf, info) }

  (* cppext: stringification.
   * bugfix: this case must be after the other cases such as #endif
   * otherwise take precedent.
   *)
  |  "#" spopt id
      { let info = tokinfo lexbuf in ident (tok lexbuf, info) }
  (* the ... next to id, e.g. arg..., works with ##, e.g. ##arg *)
  | (id "...")
      { let info = tokinfo lexbuf in ident (tok lexbuf, info) }

  (* ----------------------------------------------------------------------- *)
  (* C symbols *)
  (* ----------------------------------------------------------------------- *)
   (* stdC:
    ...   &&   -=   >=   ~   +   ;   ]
    <<=   &=   ->   >>   %   ,   <   ^
    >>=   *=   /=   ^=   &   -   =   {
    !=    ++   <<   |=   (   .   >   |
    %=    +=   <=   ||   )   /   ?   }
        --   ==   !    *   :   [
    recent addition:    <:  :>  <%  %>
    only at processing: %:  %:%: # ##
   *)


  | '[' {let info = tokinfo lexbuf in kwd(tok lexbuf, info) }
  | ']' {let info = tokinfo lexbuf in kwd(tok lexbuf, info) }
  | '(' {let info = tokinfo lexbuf in punct(tok lexbuf, info) }
  | ')' {let info = tokinfo lexbuf in punct(tok lexbuf, info) }
  | '{' {let info = tokinfo lexbuf in punct(tok lexbuf, info) }
  | '}' {let info = tokinfo lexbuf in punct(tok lexbuf, info) }

  | '+' {let info = tokinfo lexbuf in kwd(tok lexbuf, info) }
  | '*' {let info = tokinfo lexbuf in kwd(tok lexbuf, info) }
  | '-' {let info = tokinfo lexbuf in kwd(tok lexbuf, info) }
  | '/' {let info = tokinfo lexbuf in kwd(tok lexbuf, info) }
  | '%' {let info = tokinfo lexbuf in kwd(tok lexbuf, info) }
  | ">?" {let info = tokinfo lexbuf in kwd(tok lexbuf, info) }
  | "<?" {let info = tokinfo lexbuf in kwd(tok lexbuf, info) }

  | "++"{let info = tokinfo lexbuf in kwd(tok lexbuf, info) }
  | "--"{let info = tokinfo lexbuf in kwd(tok lexbuf, info) }

  | "="  {let info = tokinfo lexbuf in kwd(tok lexbuf, info) }

  | "-=" {let info = tokinfo lexbuf in kwd(tok lexbuf, info) }
  | "+=" {let info = tokinfo lexbuf in kwd(tok lexbuf, info) }
  | "*=" {let info = tokinfo lexbuf in kwd(tok lexbuf, info) }
  | "/=" {let info = tokinfo lexbuf in kwd(tok lexbuf, info) }
  | "%=" {let info = tokinfo lexbuf in kwd(tok lexbuf, info) }
  | "&=" {let info = tokinfo lexbuf in kwd(tok lexbuf, info) }
  | "|=" {let info = tokinfo lexbuf in kwd(tok lexbuf, info) }
  | "^=" {let info = tokinfo lexbuf in kwd(tok lexbuf, info) }
  | "<<=" {let info = tokinfo lexbuf in kwd(tok lexbuf, info) }
  | ">>=" {let info = tokinfo lexbuf in kwd(tok lexbuf, info) }
  | ">?=" {let info = tokinfo lexbuf in kwd(tok lexbuf, info) }
  | "<?=" {let info = tokinfo lexbuf in kwd(tok lexbuf, info) }

  | "==" {let info = tokinfo lexbuf in kwd(tok lexbuf, info) }
  | "!=" {let info = tokinfo lexbuf in kwd(tok lexbuf, info) }
  | ">=" {let info = tokinfo lexbuf in kwd(tok lexbuf, info) }
  | "<=" {let info = tokinfo lexbuf in kwd(tok lexbuf, info) }
  | "<"  {let info = tokinfo lexbuf in kwd(tok lexbuf, info) }
  | ">"  {let info = tokinfo lexbuf in kwd(tok lexbuf, info) }

  | "&&" {let info = tokinfo lexbuf in kwd(tok lexbuf, info) }
  | "||" {let info = tokinfo lexbuf in kwd(tok lexbuf, info) }
  | ">>" {let info = tokinfo lexbuf in kwd(tok lexbuf, info) }
  | "<<" {let info = tokinfo lexbuf in kwd(tok lexbuf, info) }
  | "&"  {let info = tokinfo lexbuf in kwd(tok lexbuf, info) }
  | "|"  {let info = tokinfo lexbuf in kwd(tok lexbuf, info) }
  | "^"  {let info = tokinfo lexbuf in kwd(tok lexbuf, info) }
  | "..." {let info = tokinfo lexbuf in kwd(tok lexbuf, info) }
  | "->" {let info = tokinfo lexbuf in kwd(tok lexbuf, info) }
  | '.'  {let info = tokinfo lexbuf in kwd(tok lexbuf, info) }
  | ','  {let info = tokinfo lexbuf in punct(tok lexbuf, info) }
  | ";"  {let info = tokinfo lexbuf in punct(tok lexbuf, info) }
  | "?"  {let info = tokinfo lexbuf in kwd(tok lexbuf, info) }
  | ":"  {let info = tokinfo lexbuf in kwd(tok lexbuf, info) }
  | "!"  {let info = tokinfo lexbuf in kwd(tok lexbuf, info) }
  | "~"  {let info = tokinfo lexbuf in kwd(tok lexbuf, info) }

  | "<:" {let info = tokinfo lexbuf in kwd(tok lexbuf, info) }
  | ":>" {let info = tokinfo lexbuf in kwd(tok lexbuf, info) }
  | "<%" {let info = tokinfo lexbuf in kwd(tok lexbuf, info) }
  | "%>" {let info = tokinfo lexbuf in kwd(tok lexbuf, info) }



  (* ----------------------------------------------------------------------- *)
  (* C keywords and ident *)
  (* ----------------------------------------------------------------------- *)

  (* StdC: must handle at least name of length > 509, but can
   * truncate to 31 when compare and truncate to 6 and even lowerise
   * in the external linkage phase
   *)
  | letter (letter | digit) *
      { let info = tokinfo lexbuf in
      let s = tok lexbuf in
      if List.mem s keyword_table
      then kwd(s,info)
      else ident(s,info) }
  | (cplusplus_ident "::")+ "operator new"
      { let info = tokinfo lexbuf in ident(tok lexbuf,info) }
  | cplusplus_ident { let info = tokinfo lexbuf in ident(tok lexbuf,info) }

  | cplusplus_ident
      ('<' "const "? cplusplus_ident_ext ("::" cplusplus_ident_ext) * '*'*
      (", " "const "? cplusplus_ident_ext ("::" cplusplus_ident_ext) * '*'* ) * '>') ?
    ("::~" cplusplus_ident
      ('<' "const "? cplusplus_ident_ext ("::" cplusplus_ident_ext) * '*'*
      (", " "const "? cplusplus_ident_ext ("::" cplusplus_ident_ext) * '*'* ) * '>') ?) +

      { let info = tokinfo lexbuf in ident(tok lexbuf,info) }
  | cplusplus_ident
      ('<' "const "? cplusplus_ident_ext ("::" cplusplus_ident_ext) * '*'*
      (", " "const "? cplusplus_ident_ext ("::" cplusplus_ident_ext) * '*'* ) * '>')

      { let info = tokinfo lexbuf in ident(tok lexbuf,info) }


  | cplusplus_ident
      ('<' "const "? cplusplus_ident_ext ("::" cplusplus_ident_ext) * '*'*
      (", " "const "? cplusplus_ident_ext ("::" cplusplus_ident_ext) * '*'* ) * '>') ?
    "::" cplusplus_ident
      ('<' "const "? cplusplus_ident_ext ("::" cplusplus_ident_ext) * '*'*
      (", " "const "? cplusplus_ident_ext ("::" cplusplus_ident_ext) * '*'* ) * '>') ?
    ("::" cplusplus_ident
      ('<' "const "? cplusplus_ident_ext ("::" cplusplus_ident_ext) * '*'*
      (", " "const "? cplusplus_ident_ext ("::" cplusplus_ident_ext) * '*'* ) * '>') ?) *

      { let info = tokinfo lexbuf in ident(tok lexbuf,info) }

   | "::" cplusplus_ident
      ('<' "const "? cplusplus_ident_ext ("::" cplusplus_ident_ext) * '*'*
      (", " "const "? cplusplus_ident_ext ("::" cplusplus_ident_ext) * '*'* ) * '>') ?
    ("::" cplusplus_ident
      ('<' "const "? cplusplus_ident_ext ("::" cplusplus_ident_ext) * '*'*
      (", " "const "? cplusplus_ident_ext ("::" cplusplus_ident_ext) * '*'* ) * '>') ?) *

      { let info = tokinfo lexbuf in ident(tok lexbuf,info) }

  (* ----------------------------------------------------------------------- *)
  (* C constant *)
  (* ----------------------------------------------------------------------- *)

  | "'"
      { let info = tokinfo lexbuf in cst(char lexbuf, info) }
  | '\"'
      { let info = tokinfo lexbuf in cst(string lexbuf, info) }
  (* wide character encoding, TODO L'toto' valid ? what is allowed ? *)
  | 'L' "'"
      { let info = tokinfo lexbuf in cst(char lexbuf, info) }
  | 'L' '\"'
      { let info = tokinfo lexbuf in cst(string lexbuf, info) }


  (* Take care of the order ? No because lex tries the longest match. The
   * strange diff between decimal and octal constant semantic is not
   * understood too by refman :) refman:11.1.4, and ritchie.
   *)

  | decimal
      { let info = tokinfo lexbuf in cst (tok lexbuf, info) }
  | hexa
      { let info = tokinfo lexbuf in cst (tok lexbuf, info) }
  | octal
      { let info = tokinfo lexbuf in cst (tok lexbuf, info) }
  | (decimal ['u' 'U'])
      { let info = tokinfo lexbuf in cst (tok lexbuf, info) }
  | (hexa ['u' 'U'])
      { let info = tokinfo lexbuf in cst (tok lexbuf, info) }
  | (octal ['u' 'U'])
      { let info = tokinfo lexbuf in cst (tok lexbuf, info) }
  | (decimal ['l' 'L'])
      { let info = tokinfo lexbuf in cst (tok lexbuf, info) }
  | (hexa ['l' 'L'])
      { let info = tokinfo lexbuf in cst (tok lexbuf, info) }
  | (octal ['l' 'L'])
      { let info = tokinfo lexbuf in cst (tok lexbuf, info) }
  | (((decimal | hexa | octal) ['l' 'L'] ['u' 'U'])
  | ((decimal | hexa | octal) ['u' 'U'] ['l' 'L']))
      { let info = tokinfo lexbuf in cst (tok lexbuf, info) }
  | (( decimal | hexa | octal) ['l' 'L'] ['l' 'L'])
      { let info = tokinfo lexbuf in cst (tok lexbuf, info) }
  | (( decimal | hexa | octal) ['u' 'U'] ['l' 'L'] ['l' 'L'])
      { let info = tokinfo lexbuf in cst (tok lexbuf, info) }

  | (real ['f' 'F']) { let info = tokinfo lexbuf in cst (tok lexbuf, info) }
  | (real ['l' 'L']) { let info = tokinfo lexbuf in cst (tok lexbuf, info) }
  | real             { let info = tokinfo lexbuf in cst (tok lexbuf, info) }
  (* How to make the following only available if !Flag.ibm *)
  | (ddecimal ['d' 'D'])
      { let info = tokinfo lexbuf in cst (tok lexbuf, info) }

  | ['0'] ['0'-'9']+
      { let info = tokinfo lexbuf in cst (tok lexbuf, info) }
  | ("0x" |"0X") ['0'-'9' 'a'-'z' 'A'-'Z']+
      { let info = tokinfo lexbuf in cst (tok lexbuf, info) }


 (* !!! to put after other rules !!! otherwise 0xff
  * will be parsed as an ident.
  *)
  | ['0'-'9']+ letter (letter | digit) *
      { let info = tokinfo lexbuf in ident (tok lexbuf, info) }

(* gccext: http://gcc.gnu.org/onlinedocs/gcc/Binary-constants.html *)
(*
 | "0b" ['0'-'1'] { TInt (((tok lexbuf)<!!>(??,??)) +> int_of_stringbits) }
 | ['0'-'1']+'b' { TInt (((tok lexbuf)<!!>(0,-2)) +> int_of_stringbits) }
*)


  (*------------------------------------------------------------------------ *)
  | eof { EOF }

  | _
      { let info = tokinfo lexbuf in ident (tok lexbuf, info) }



(*****************************************************************************)
and char = parse
  | "'"                                { "" } (* allow empty char *)
  | (_ as x)                           { String.make 1 x ^ restchars lexbuf }
  (* todo?: as for octal, do exception  beyond radix exception ? *)
  | (("\\" (oct | oct oct | oct oct oct)) as x     ) { x ^ restchars lexbuf }
  (* this rule must be after the one with octal, lex try first longest
   * and when \7  we want an octal, not an exn.
   *)
  | (("\\x" ((hex | hex hex))) as x           )      { x ^ restchars lexbuf }
  | (("\\" _)                  as x           )
	{ x ^ restchars lexbuf }
  | _
      { tok lexbuf ^ restchars lexbuf }
  | eof { "" }

and restchars = parse
  | "'"                                { "" }
  | "\n"                               { tok lexbuf }
  | (_ as x)                           { String.make 1 x ^ restchars lexbuf }
  (* todo?: as for octal, do exception  beyond radix exception ? *)
  | (("\\" (oct | oct oct | oct oct oct)) as x     ) { x ^ restchars lexbuf }
  (* this rule must be after the one with octal, lex try first longest
   * and when \7  we want an octal, not an exn.
   *)
  | (("\\x" ((hex | hex hex))) as x           )      { x ^ restchars lexbuf }
  | (("\\" _)                  as x           )      { x ^ restchars lexbuf }
  | _ { tok lexbuf ^ restchars lexbuf }
  | eof { "" }


(*****************************************************************************)

(* todo? factorise code with char ? but not same ending token so hard. *)
and string  = parse
  | '\"'                                      { "" }
  | '\n'                                      { newline lexbuf; string lexbuf }
  | (_ as x)
      { Common.string_of_char x^string lexbuf}
  | ("\\" (oct | oct oct | oct oct oct)) as x { x ^ string lexbuf }
  | ("\\x" (hex | hex hex)) as x              { x ^ string lexbuf }
  | ("\\" _) as x                      { x ^ string lexbuf }

  | eof { "" }

 (* Bug if add following code, cos match also the '"' that is needed
  * to finish the string, and so go until end of file.
  *)
 (*
  | [^ '\\']+
    { let cs = lexbuf +> tok +> list_of_string +> List.map Char.code in
      cs ++ string lexbuf
    }
  *)



(*****************************************************************************)

(* less: allow only char-'*' ? *)
and comment = parse
  | "*/"     { token lexbuf }
  | eof { EOF }
  | '\n' { newline lexbuf; comment lexbuf }
  | _ { comment lexbuf }



(*****************************************************************************)
