let cores = 20

let tokens_all_full lines =
  let lines = String.concat "\n" lines in
  let lexbuf = Lexing.from_string lines in
  let rec aux () =
    let result =
      try Lexer_c.token lexbuf
      with e -> Printf.eprintf "%s\n" lines; raise e in
    if result = Lexer_c.EOF
    then []
    else result :: aux() in
  aux ()

let verbose_print_one = function
    Lexer_c.Ident(tok,_,(ln,col)) ->
      Printf.sprintf "id: %s: %d: %d" tok ln col
  | Lexer_c.Cst(tok,_,(ln,col)) ->
      Printf.sprintf "cst: %s: %d: %d" tok ln col
  | Lexer_c.Kwd(tok,_,(ln,col)) ->
      Printf.sprintf "kwd: %s: %d: %d" tok ln col
  | Lexer_c.Punct(tok,_,(ln,col)) ->
      Printf.sprintf "punct: %s: %d: %d" tok ln col
  | Lexer_c.EOF -> ""

let print_one = function
    Lexer_c.Ident(tok,_,(ln,col)) -> tok
  | Lexer_c.Cst(tok,_,(ln,col)) -> tok
  | Lexer_c.Kwd(tok,_,(ln,col)) -> tok
  | Lexer_c.Punct(tok,_,(ln,col)) -> tok
  | Lexer_c.EOF -> ""

type ctx = C | P | M

let same e1 e2 =
  match (e1,e2) with
    (Matcher.D e1, Matcher.D e2) -> print_one e1 = print_one e2
  | _ -> e1 = e2

let simple_compare l1 l2 =
  List.fold_left2
    (fun prev e1 e2 ->
      let score =
	if same e1 e2
	then 2
	else if e1 = Matcher.B || e2 = Matcher.B
	then 0
	else
	  match (e1,e2) with
	    (Matcher.D(Lexer_c.Cst _),Matcher.D(Lexer_c.Cst _)) -> 1
	  | (Matcher.D(Lexer_c.Ident _),Matcher.D(Lexer_c.Ident _)) -> 1
	  | _ -> 0 in
      score + prev)
    0 l1 l2

let file = ref ""
let pattern = ref ""

let options = ["--pattern",Arg.Set_string pattern,"weight pattern";]
let anonymous s = file := s
let usage = ""

let _ =
  Random.self_init();
  Arg.parse (Arg.align options) anonymous usage;
  let lines = Get_data2.get_data !file in
  Printf.eprintf "finished parsing\n"; flush stderr;
  let rand _ = Random.int 7 in
  let perms =
    if !pattern = ""
    then
      let rec loop acc = function
	  0 -> acc
	| n ->
	    let config = (rand(),rand(),rand(),rand(),rand(),rand()) in
	    if List.mem config acc
	    then loop acc n
	    else loop (config::acc) (n-1) in
      loop [(5,4,3,2,1,1)] 500
    else
      let pieces = Str.split (Str.regexp ";") !pattern in
      List.map
	(function x ->
	  match List.map int_of_string(Str.split (Str.regexp " ") x) with
	    [m1;m2;m3;m4;m5;m6] -> (m1,m2,m3,m4,m5,m6)
	  | _ -> failwith "bad input")
	pieces in
  let tbl = Hashtbl.create 101 in
  let hashadd i info =
    let cell =
      try Hashtbl.find tbl i
      with Not_found ->
	let cell = ref [] in
	Hashtbl.add tbl i cell;
	cell in
    cell := info :: !cell in
  let all = List.length lines in
  let lines =
    Parmap.parmap ~ncores:cores ~chunksize:1
      (function (commit,b,a,min,plu) ->
	(commit,b,a,tokens_all_full min,tokens_all_full plu))
      (Parmap.L lines) in
  let prefix = !file ^ "_output" in
  let res =
    Parmap.parmapi ~ncores:cores ~chunksize:1
      ~init:(fun id -> Parmap.redirect ~path:prefix ~id)
      (fun i ((punct_wt,kwd_wt,id_wt,cst_wt,idmm_wt,cstmm_wt) as config) ->
	Printf.eprintf "round %d\n" i; flush stderr;
	Matcher.punct_weight := punct_wt;
	Matcher.kwd_weight := kwd_wt;
	Matcher.id_weight := id_wt;
	Matcher.cst_weight := cst_wt;
	Matcher.idmm_weight := idmm_wt;
	Matcher.cstmm_weight := cstmm_wt;
	let (score,_,acc) =
	  List.fold_left
	    (fun (score,i,acc) (commit,b,a,min,plu) ->
	      let res = Matcher.seq_match min plu in
	      let myscore = simple_compare (fst res) (snd res) in
	      (myscore + score,i+1,(i,(myscore,config))::acc))
	    (0,0,[]) lines in
	(score,config,acc))
      (Parmap.L perms) in
  Printf.eprintf "done with parmap\n"; flush stderr;
  let res =
    List.map
      (function (score,config,acc) ->
	List.iter (function (i,v) -> hashadd i v) acc;
	(score,config))
      res in
  Printf.eprintf "preparing results\n"; flush stderr;
  Hashtbl.iter
    (fun i info ->
      let (pscore,pconfigs) =
	List.fold_left
	  (fun (pscore,pconfigs) (score,config) ->
	    if score = pscore
	    then (pscore,config::pconfigs)
	    else if score > pscore
	    then (score,[config])
	    else (pscore,pconfigs))
	  (-1,[]) !info in
      let res = List.map (function c -> (pscore,c)) pconfigs in
      info := res)
    tbl;
  List.iter
    (function (score,((punct_wt,kwd_wt,id_wt,cst_wt,idmm_wt,cstmm_wt) as c)) ->
      let highest =
	Hashtbl.fold
	  (fun k v r -> if List.mem c (List.map snd !v) then r+1 else r)
	  tbl 0 in
      Printf.printf "%d %d %d %d %d %d: %d, highest %d/%d\n"
	punct_wt kwd_wt id_wt cst_wt idmm_wt cstmm_wt score highest all)
    (List.sort compare res)
