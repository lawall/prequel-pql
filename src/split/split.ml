(*
 * This file is part of Prequel, lincensed under the terms of the GPL v2.
 * See copyright.txt in the Prequel source code for more information.
 * The Prequel source code can be obtained at
 * https://github.com/prequel-pql/prequel
 *)

(* putting it all together... *)

module Ast0 = Ast0_cocci
module Ast  = Ast_cocci

(* Splitting a semantic patch *)

let split file for_constants =
  Flag_parsing_cocci.cocci_unparse := true;
  let (rules,virts) = Parser.process file in
  let categories_and_rules =
    Classify_rules.classify_on_relations rules in
  let categories_and_positions_and_rules =
    Insert_positions.insert_positions categories_and_rules for_constants in
  let categories_and_positions_and_rules =
    Drop_enders.process categories_and_positions_and_rules in
  let (positions,categories_and_rules) =
    List.split categories_and_positions_and_rules in
  let metas_and_ast_rules =
    Cleanup.cleanup categories_and_rules in
  Disjunctions.check_disjunctions positions (List.map fst categories_and_rules)
    metas_and_ast_rules;
  let rules_with_checks =
    if for_constants
    then ([],Add_checks.add_nothing metas_and_ast_rules)
    else
      Add_checks.add_checks positions (List.map fst categories_and_rules)
	metas_and_ast_rules in
  let rules_with_commits =
    Collect_rule_refs.collect_rule_refs rules_with_checks for_constants in
  Pretty_print_cocci.print_virts virts;
  List.iter
    (function (mvs,names,rule) -> Pretty_print_cocci.unparse mvs names rule)
    rules_with_commits

let split_for_code cocci_file = split cocci_file false
let split_for_constants cocci_file = split cocci_file true

(* --------------------------------------------------------------------- *)
(* Getting constants *)

let sp_of_file file star =
  let (rules, virt) = Parser.process file in
  List.map
    (function
	Ast0.ScriptRule (nm,lang,deps,mvs,scriptvars,code) ->
	  Ast.ScriptRule (nm,lang,deps,mvs,scriptvars,code)
      | Ast0.InitialScriptRule(a,b,c,d,e) -> Ast.InitialScriptRule (a,b,c,d,e)
      | Ast0.FinalScriptRule(a,b,c,d,e) -> Ast.FinalScriptRule (a,b,c,d,e)
      | Ast0.CocciRule((minus,mmetavars,infos),(plus,pmetavars),
		       imetavars,names,ty) ->
	  let (isos,drop_isos,deps,nm,quant) = infos in
	  (* there is no plus code at this point *)
	  Ast0toast.ast0toast nm deps drop_isos quant minus [] ty)
    rules

let get_constants coccifile =
  Flag_parsing_cocci.getconstants := true;
  let rules = sp_of_file coccifile false in
  let neg_pos = Free_vars.free_vars rules in
  Get_constants2.get_constants rules neg_pos
