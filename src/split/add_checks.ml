(*
 * This file is part of Prequel, lincensed under the terms of the GPL v2.
 * See copyright.txt in the Prequel source code for more information.
 * The Prequel source code can be obtained at
 * https://github.com/prequel-pql/prequel
 *)

(* Adding script rules after each rule to check on its minus and plus code.
These checks reference the functions in library.ml, which are copied into
the initialize rule. *)

module Ast = Ast_cocci
module V = Visitor_ast
module I = Insert_positions

type code_structure =
    And of code_structure list | Or of code_structure list
  | Pos of I.start_end I.pos
  | Opt of code_structure | True | False

let intersect l1 l2 = List.filter (function l1e -> List.mem l1e l2) l1
let subset l1 l2 = List.for_all (function x -> List.mem x l2) l1
let minus_set l1 l2 = List.filter (function l1e -> not (List.mem l1e l2)) l1

let norm = function
    And l -> And (List.sort compare l)
  | Or l  -> Or (List.sort compare l)
  | x -> x

let rec merge l1 l2 =
  match (l1,l2) with
    ([],l2) -> l2
  | (l1,[]) -> l1
  | (x::xs,y::ys) ->
      (match compare x y with
	-1 -> x::(merge xs l2)
      |	0  -> x::(merge xs ys)
      |	1  -> y::(merge l1 ys)
      |	_ -> failwith "not possible")

let rec insert x l = merge [x] l

let rec build_and x y =
  if x = y
  then x
  else
    match (x,y) with
      (True,x) | (x,True) -> x
    | (False,x) | (x,False) -> False
    | (And l1,And l2) -> And (merge l1 l2)
    | (x,Or l) when List.mem x l -> x
    | (Or l,x) when List.mem x l -> x
    | (Or l1,Or l2) when subset l1 l2 -> Or l1
    | (Or l1,Or l2) when subset l2 l1 -> Or l2
    | (Or l1,Or l2) when not ((intersect l1 l2) = []) ->
	let m1 = minus_set l1 l2 in
	let m2 = minus_set l2 l1 in
	let inner =
	  build_and
	    (List.fold_left build_or (List.hd m1) (List.tl m1))
	    (List.fold_left build_or (List.hd m2) (List.tl m2)) in
	List.fold_left build_or inner (intersect l1 l2)
    | (x,And l) | (And l,x) ->
	if List.mem x l
	then And l
	else
	  let others =
	    List.filter
	      (function
		  Or l -> not(List.mem x l)
		| _ -> true)
	      l in
	  And (insert x others)
    | (x,y) -> norm(And [x;y])

and build_or x y =
  if x = y
  then x
  else
    match (x,y) with
      (True,x) | (x,True) -> True
    | (False,x) | (x,False) -> x
    | (Or l1,Or l2) -> Or (merge l1 l2)
    | (x,And l) when List.mem x l -> x
    | (And l,x) when List.mem x l -> x
    | (And l1,And l2) when subset l1 l2 -> And l1
    | (And l1,And l2) when subset l2 l1 -> And l2
    | (And l1,And l2) when not ((intersect l1 l2) = []) ->
	let m1 = minus_set l1 l2 in
	let m2 = minus_set l2 l1 in
	let inner =
	  build_or
	    (List.fold_left build_and (List.hd m1) (List.tl m1))
	    (List.fold_left build_and (List.hd m2) (List.tl m2)) in
	List.fold_left build_and inner (intersect l1 l2)
    | (x,Or l) | (Or l,x) ->
	if List.mem x l
	then Or l
	else
	  let others =
	    List.filter
	      (function
		  And l -> not(List.mem x l)
		| _ -> true)
	      l in
	  Or (insert x others)
    | (x,y) -> norm(Or [x;y])

(* case for disj, only *)
let get_structure relevant body =
  let donothing r k e = k e in
  let option_default = True in
  (*let bad_default = False in  useful?*)
  let bind = build_and in
  let mcode _ x =
    List.fold_left bind True
      (List.map
	 (function Ast.MetaPos(name,constraints,_,keep,inh) ->
	   let name = Ast.unwrap_mcode name in
	   try Pos(List.assoc name relevant) with Not_found -> True)
	 (Ast.get_pos_var x)) in

  (* if one branch gives no information, then we have to take anything *)
  (* things with disj or nest, at least <... ...> case can't be under bind *)
  (* all cases for opt too *)
  let disj_union_all = List.fold_left build_or False in

  let ident r k i =
    match Ast.unwrap i with
      Ast.DisjId(ids) ->
	(* no bind (k i) here!!! would & combine *)
	disj_union_all (List.map r.V.combiner_ident ids)
    | Ast.OptIdent _ -> Opt(k i)
    | _ -> k i in

  (* no point to do anything special for records because glimpse is
     word-oriented *)
  let expression r k e =
    match Ast.unwrap e with
      Ast.DisjExpr(exps) ->
	disj_union_all (List.map r.V.combiner_expression exps)
    | Ast.NestExpr(starter,expr_dots,ender,wc,false) ->
	build_or (r.V.combiner_expression_dots expr_dots) False
    | Ast.NestExpr(starter,expr_dots,ender,wc,true) ->
	r.V.combiner_expression_dots expr_dots
    | Ast.OptExp _ -> Opt(k e)
    | _ -> k e in
  
  let fullType r k ft =
    match Ast.unwrap ft with
      Ast.DisjType(decls) ->
	disj_union_all (List.map r.V.combiner_fullType decls)
    | Ast.OptType _ -> Opt(k ft)
    | _ -> k ft in

  let declaration r k d =
    match Ast.unwrap d with
      Ast.DisjDecl(decls) ->
	disj_union_all (List.map r.V.combiner_declaration decls)
    | Ast.OptDecl _ -> Opt(k d)
    | _ -> k d in

  let initialiser r k i =
    match Ast.unwrap i with
      Ast.OptIni _ -> Opt(k i)
    | _ -> k i in

  let parameter r k p =
    match Ast.unwrap p with
      Ast.OptParam _ -> Opt(k p)
    | _ -> k p in

  (* Not used!  Need to extend visitor. *)
  (*let define_parameter r k p =
    match Ast.unwrap p with
      Ast.OptDParam _ -> Opt(k p)
    | _ -> k p in*)

  let case r k c =
    match Ast.unwrap c with
      Ast.OptCase _ -> Opt(k c)
    | _ -> k c in

  let rule_elem r k re =
    match Ast.unwrap re with
      Ast.DisjRuleElem(res) ->
	disj_union_all (List.map r.V.combiner_rule_elem res)
    | _ -> k re in

  let statement r k s =
    match Ast.unwrap s with
      Ast.Disj(stmt_dots) ->
	disj_union_all (List.map r.V.combiner_statement_dots stmt_dots)
    | Ast.Nest(starter,stmt_dots,ender,whn,false,_,_) ->
	build_or (r.V.combiner_statement_dots stmt_dots) False
    | Ast.Nest(starter,stmt_dots,ender,whn,true,_,_) ->
	r.V.combiner_statement_dots stmt_dots
    | Ast.OptStm _ -> Opt(k s)
    | _ -> k s in

  let op =
    V.combiner bind option_default
      mcode mcode mcode mcode mcode mcode mcode mcode mcode
      mcode mcode mcode mcode mcode
      donothing donothing donothing donothing donothing
      ident expression donothing donothing donothing donothing
      fullType donothing initialiser parameter declaration donothing
      rule_elem statement case donothing donothing in
  op.V.combiner_top_level body

(* TODO: define_parameter *)

(* ---------------------------------------------------------------------- *)

let get_position_names l =
  let get_var = function
      ((I.Concrete s,_,I.Concrete e) | (I.Meta s,_,I.Meta e)) ->
	if s = e then [s] else [s;e]
    | _ -> failwith "not possible" in
  let get_name = function
      I.Region r -> List.concat (List.map get_var r)
    | I.BeforeEmpty m -> [m]
    | I.AfterEmpty m -> [m]
    | I.AroundEmpty(m1,m2) -> if m1 = m2 then [m1] else [m1;m2] in
  List.concat
    (List.map
       (function
	   I.Shared(b,a) -> (get_name b) @ (get_name a)
	 | I.MUnshared r | I.PUnshared r -> List.concat (List.map get_var r))
       l)

let check_script_name nm = Printf.sprintf "check_%s" nm

let concmetavars = function
    (I.Concrete s,I.Concrete e) -> Printf.sprintf "(Concrete(%s,%s))" (snd s) (snd e)
  | (I.Meta s,I.Meta e) -> Printf.sprintf "(Meta(%s,%s))" (snd s) (snd e)
  | _ -> failwith "inconsistent start and end"
let unconcmetavar = function
    I.Concrete s -> snd s
  | I.Meta s -> snd s

let add_rule_checks positions nm body second_body =
  let script_name = check_script_name nm in
  let script_language = "ocaml" in
  let script_dep = Ast.Dep nm in
  let script_inh_vars =
    List.map
      (function position ->
	let script_meta_name = (Some (snd position), None) in
	let metavar = Ast.MetaScriptDecl(ref None, position) in
	let mvinit = Ast.MVInitPosList in
	(script_meta_name, position, metavar, mvinit))
      (get_position_names positions) in
  let script_new_vars = [(script_name,"found_hunks")] in
  let script_init_code = "let res = ref [] in" in
  let continue i =
    Printf.sprintf
      "match %s with
      None -> false
    | Some l -> (res := List.append l !res; true)" i in
  let script_start_code =
    let oneline = function
	I.MUnshared r ->
	  String.concat " &&\n"
	    (List.map
	       (function (m1,_,m2) ->
		 let name = unconcmetavar m1 in
		 let l1 =
		   if !Flag_split.uniform
		   then
		     Printf.sprintf
		       "let %s = check_in_same_hunk minus_hunks \"M\" %s %s in"
		       name name (unconcmetavar m2)
		   else
		     match (m1,m2) with
		       (I.Concrete (_,m1),I.Concrete (_,m2)) ->
			 Printf.sprintf
			   "let %s = check_in_same_hunk minus_hunks \"M\" %s %s in"
			   name m1 m2
		     | (I.Meta m1,I.Meta m2) when m1 = m2 ->
			 Printf.sprintf
			   "let %s = get_contained plus_hunks \"M\" %s in"
			   name name
		     | _ -> failwith "bad metavar combination" in
		 l1 ^ "\n" ^ (continue name))
	       r)
      | I.PUnshared r ->
	  String.concat " &&\n"
	    (List.map
	       (function (p1,_,p2) ->
		 let name = unconcmetavar p1 in
		 let l1 =
		   if !Flag_split.uniform
		   then
		     Printf.sprintf
		       "let %s = check_in_same_hunk plus_hunks \"P\" %s %s in"
		       name name (unconcmetavar p2)
		   else
		     match (p1,p2) with
		       (I.Concrete (_,p1),I.Concrete (_,p2)) ->
			 Printf.sprintf
			   "let %s = check_in_same_hunk plus_hunks \"P\" %s %s in"
			   name p1 p2
		     | (I.Meta p1,I.Meta p2) when p1 = p2 ->
			 Printf.sprintf
			   "let %s = get_contained plus_hunks \"P\" %s in"
			   name name
		     | _ -> failwith "bad metavar combination" in
		 l1 ^ "\n" ^ (continue name))
	       r)
      | I.Shared(b,a) ->
	  (* The challenge is that metavariables may encompass multiple
	     hunks, particularly when using word diff. *)
	  let name =
	    match b with
	      I.Region r -> let (m,_,_) = List.hd r in unconcmetavar m
	    | I.BeforeEmpty m -> snd m
	    | I.AfterEmpty m -> snd m
	    | I.AroundEmpty(m,_) -> snd m in
	  let pos = function
	      I.Region r ->
		Printf.sprintf "Region([%s])"
		  (String.concat ";"
		     (List.map (function (s,_,e) -> concmetavars(s,e)) r))
	    | I.BeforeEmpty m ->
		Printf.sprintf "BeforeEmpty(%s)" (snd m)
	    | I.AfterEmpty m ->
		Printf.sprintf "AfterEmpty(%s)" (snd m)
	    | I.AroundEmpty(m1,m2) ->
		Printf.sprintf "AroundEmpty(%s,%s)" (snd m1) (snd m2) in
	  let l1 =
	    Printf.sprintf "let %s = check_pair_in_same_hunk (%s) (%s) in"
	      name (pos b) (pos a) in
	  l1 ^ "\n" ^ (continue name) in
    let get_var = function
	(I.Concrete s,_,_)::_ -> s
      | (I.Meta s,_,_)::_ -> s
      | _ -> failwith "should not be empty" in
    let posenv =
      List.map
	(function a ->
	  match a with
	    I.Shared(b,_) ->
	      (match b with
		I.Region r -> (get_var r,a)
	      | I.BeforeEmpty s1 -> (s1,a)
	      | I.AfterEmpty s1 -> (s1,a)
	      | I.AroundEmpty(s1,_) -> (s1,a))
	  | I.MUnshared r -> (get_var r,a)
	  | I.PUnshared r -> (get_var r,a))
	positions in
    let rec interpret = function
	Pos p -> Printf.sprintf "(%s)" (oneline p)
      | And l ->
	  Printf.sprintf "(%s)"
	    (String.concat "\n&&\n" (List.map interpret l))
      | Or l ->
	  Printf.sprintf "(%s)"
	    (String.concat "\n||\n" (List.map interpret l))
      | Opt e ->
	  Printf.sprintf
	    "(let saved = !found_hunks in
	     let res = %s in
	     (if not res then found_hunks := saved);
	     true)" (interpret e)
      | True -> "true"
      | False -> "false" in
    let flag_line =
      Printf.sprintf "\nlet flag =\n%s in\n"
	(interpret (get_structure posenv body)) in
    let flag_line =
      match second_body with
	None -> flag_line
      | Some body ->
	  Printf.sprintf "%slet flag =\nflag &&\n%s in\n"
	    flag_line (interpret (get_structure posenv body)) in
    let result_line =
      "if not flag\nthen Coccilib.include_match false\nelse" in
    flag_line^result_line in
  let script_end_code =
    "found_hunks := make_ident (String.concat \",\" !res)\n" in
  let script_code =
    script_init_code ^ "\n" ^ script_start_code ^ "\n" ^
    script_end_code in
  [([],[],
    Ast.ScriptRule(script_name, script_language, script_dep, script_inh_vars,
		   script_new_vars, script_code))]

let get_name l =
  match List.rev l with
    (_, _, Ast.CocciRule (nm, (deps, drops, exists), x, _, _))::_ -> nm
  | _ -> failwith "unexpected rule"

let cleanup_positions positions category =
  let rename s (r,n) = (Printf.sprintf "%s_%s" s r, n) in
  let rename_elements s = function
      (I.Concrete s1,m1,I.Concrete e1) ->
	(I.Concrete(rename s s1),m1,I.Concrete(rename s e1))
    | (I.Meta s1,m1,I.Meta e1) ->
	(I.Meta(rename s s1),m1,I.Meta(rename s e1))
    | _ -> failwith "not possible" in
  match category with
    Classify_rules.Both ->
      List.map
	(function
	    I.Shared(b,a) ->
	      let one s = function
		  I.Region r -> I.Region (List.map (rename_elements s) r)
		| I.BeforeEmpty s1 -> I.BeforeEmpty(rename s s1)
		| I.AfterEmpty s1 -> I.AfterEmpty(rename s s1)
		| I.AroundEmpty(s1,s2) ->
		    I.AroundEmpty(rename s s1,rename s s2) in
	      I.Shared(one "before" b, one "after" a)
	  | I.MUnshared r ->
	      I.MUnshared (List.map (rename_elements "before") r)
	  | I.PUnshared r ->
	      I.PUnshared (List.map (rename_elements "after") r))
	positions
  | _ -> positions

(* ------------------------------------------------------------------------- *)
(* original positions, only want the ones in both before and after code *)

let original_local_positions = function
    [((before_metas,_,_) as before_rule);((after_metas,_,_) as after_rule)] ->
      let before_name = get_name [before_rule] in
      let after_name = get_name [after_rule] in
      let get_local_positions rulename metas =
	List.fold_left
	  (function prev ->
	    function
		Ast.MetaPosDecl((r,n),_) ->
		  if rulename = r then n::prev else prev
	      | _ -> prev)
	  [] metas in
      let before_pos_metas = get_local_positions before_name before_metas in
      let after_pos_metas = get_local_positions after_name after_metas in
      let common_metas = Common.inter_set before_pos_metas after_pos_metas in
      List.map (function n -> ((before_name,n),(after_name,n))) common_metas
  | _ -> []

let add_local_pos_rule_checks local_positions nm =
  let script_name = Printf.sprintf "local_check_%s" nm in
  let script_language = "ocaml" in
  let script_dep = Ast.Dep nm in
  let make_one_var ((r,n) as metaname) extra =
    let script_meta_name = (Some (n^extra), None) in
    let metavar = Ast.MetaScriptDecl(ref None, metaname) in
    let mvinit = Ast.MVInitPosList in
    (script_meta_name, metaname, metavar, mvinit) in
  let script_inh_vars =
    List.concat
      (List.map
	 (function (before_name,after_name) ->
	   [make_one_var before_name "_1";make_one_var after_name "_2"])
	 local_positions) in
  let script_new_vars = [] in
  let script_code =
    "if not (" ^
    (String.concat " &&\n"
      (List.map
	 (function ((_,n),_) ->
	   let before_name = n ^ "_1" in
	   let after_name = n ^ "_2" in
	   Printf.sprintf "check_point_correspondence %s %s"
	     before_name after_name)
	 local_positions)) ^ ")\nthen Coccilib.include_match false\n" in
  [([],[],
    Ast.ScriptRule(script_name, script_language, script_dep, script_inh_vars,
		   script_new_vars, script_code))]

(* ------------------------------------------------------------------------- *)
(* original fixed variables (name chosen by analogy to what is done for
   positions) *)

let original_fixed_variables = function
    [((before_metas,_,_) as before_rule);((after_metas,_,_) as after_rule)] ->
      let before_name = get_name [before_rule] in
      let after_name = get_name [after_rule] in
      let shared_fixed_variables =
	(* relies on that we got rid of the unshared ones in cleanup *)
	List.filter (function (r,_) -> r = before_name)
	  (Managed_fixed.get_fixed_decls before_metas) in
      Some (shared_fixed_variables,after_name)
  | _ -> None

let add_fixed_rule_checks (shared_fixed_variables,after_name) =
  List.map
    (function (r,n) ->
      let script_name = Printf.sprintf "check_fixed_%s_%s" r n in
      let script_language = "ocaml" in
      let script_inh_vars =
	let script_meta_name = (Some ("_"^(String.lowercase_ascii n)), None) in
	let metavar = Ast.MetaScriptDecl(ref None, (r,n)) in
	let mvinit = Ast.NoMVInit in
	[(script_meta_name, (r,n), metavar, mvinit)] in
      let script_new_vars = [] in
      let script_code = "Coccilib.include_match false\n" in
      ([],[],
       Ast.ScriptRule(script_name, script_language, Ast.AntiDep after_name,
		      script_inh_vars, script_new_vars, script_code)))
    shared_fixed_variables

let non_fixed_rule_check = function
    [((before_metas,_,_) as before_rule);((after_metas,_,_) as after_rule)] ->
      let before_name = get_name [before_rule] in
      let after_name = get_name [after_rule] in
      let script_name = Printf.sprintf "check_%s_%s" before_name after_name in
      let script_language = "ocaml" in
      let script_inh_vars = [] in
      let script_new_vars = [] in
      let script_code = "Coccilib.include_match false\n" in
      [([],[],
       Ast.ScriptRule(script_name, script_language,
		      Ast.AndDep(Ast.Dep before_name,Ast.AntiDep after_name),
		      script_inh_vars, script_new_vars, script_code))]
  | _ -> []

(* ------------------------------------------------------------------------- *)

let add_checks positions categories rules =
  let positions = List.map2 cleanup_positions positions categories in
  let rules_with_changes =
    List.concat
      (List.map2
	 (fun positions rules ->
	   if positions = []
	   then []
	   else [check_script_name(get_name rules)])
	 positions rules) in
  let newrules =
    List.concat
      (List.map2
	 (fun positions rules ->
	   match rules with
	     [] -> failwith "should have at least one rule"
	   | [(_,_,Ast.ScriptRule _)] -> [rules]
	   | (_,_,Ast.CocciRule(nm,info,[body],_,_))::_ ->
	       let nm = get_name rules in
	       let new_position_rules =
		 if positions = []
		 then []
		 else
		   let second_body =
		     match rules with
		       [_;(_,_,Ast.CocciRule(nm,info,[body],_,_))] ->
			 Some body
		     | [_] -> None
		     | _ -> failwith "too many rules" in
		   add_rule_checks positions nm body second_body in
	       let local_positions = original_local_positions rules in
	       let local_position_rules =
		 if local_positions = []
		 then []
		 else add_local_pos_rule_checks local_positions nm in
	       let fixed_variables = original_fixed_variables rules in
	       let both_rules =
		 (non_fixed_rule_check rules) @
		 (match fixed_variables with
		   None -> []
		 | Some infos -> add_fixed_rule_checks infos) in
	       (match List.rev rules with
		 last :: others ->
		   (List.map (function x -> [x]) (List.rev others)) @
		   [last ::
		     (both_rules @ local_position_rules @ new_position_rules)]
	       | _ -> failwith "not supporting multiple minirules")
	   | (_,_,Ast.InitialScriptRule _)::_ ->
	       failwith "not supporting initializer"
	   | (_,_,Ast.FinalScriptRule _)::_ ->
	       failwith "not supporting finalizer"
	   | (_,_,Ast.ScriptRule _)::_::_ ->
	       failwith "script rules should not merge with others"
	   | (_,_,Ast.CocciRule(nm,info,_,_,_))::_ ->
	       failwith "cocci rule should have exactly one body")
	 positions rules) in
  (rules_with_changes,newrules)

let add_nothing rules =
  let newrules =
    List.concat
      (List.map
	 (fun rules ->
	   match rules with
	     [(_,_,Ast.ScriptRule _)] -> [rules]
	   | _ ->
	       (match List.rev rules with
		 last :: others ->
		   (List.map (function x -> [x]) (List.rev others)) @
		   [[last]]
	       | _ -> failwith "not possible"))
	 rules) in
  newrules
