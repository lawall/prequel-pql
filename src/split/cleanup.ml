(*
 * This file is part of Prequel, lincensed under the terms of the GPL v2.
 * See copyright.txt in the Prequel source code for more information.
 * The Prequel source code can be obtained at
 * https://github.com/prequel-pql/prequel
 *)

(* This adds the file dependencies according to the rule classifications,
removes the minus and plus annotations, drops irrelevant rule fragments
according to the rule classifications, duplicates and renames Both rules,
and moves everything to the minus side.  This is the first half of the
Transformation section in the design document. *)

(* Change rule names to prepend before or after.
Update rule references accordingly.
Remove unneeded rules.
Linearize rules.
Convert to Ast_cocci. *)

module Ast0 = Ast0_cocci
module Ast = Ast_cocci

let fix_for_category nm str = function
    Classify_rules.Bot | Classify_rules.Before | Classify_rules.After -> nm
  | Classify_rules.Both -> Printf.sprintf "%s_%s" str nm

let rename env fixed_env str (r,nm) =
  let newrule =
    try List.assoc (r,nm) fixed_env
    with Not_found -> fix_for_category r str (List.assoc r env) in
  (newrule, nm)

let rename_code_and_metavars env fixed_env str code metavars deps =
  let renamer = rename env fixed_env str in
  let code = List.map (Cleanup_helper.rename_code renamer) code in
  let metavars = List.map (Cleanup_helper.rename_metavar renamer) metavars in
  let deps = Cleanup_helper.rename_deps env deps in
  (code, metavars, deps)

(* should be be doing something for faildeps? *)
let extend_dependency deps dep =
  match deps with
    Ast.NoDep -> dep
  | _ -> Ast.AndDep (dep, deps)

(* these dependencies are not longer needed, because they will be added back
by the file in dependencies *)
let rec drop_before_after = function
    Ast.Dep "before" | Ast.Dep "after" -> Ast.NoDep
  | Ast.AndDep(e1,e2) ->
      let e1 = drop_before_after e1 in
      let e2 = drop_before_after e2 in
      if e1 = Ast.NoDep then e2
      else if e2 = Ast.NoDep then e1
      else Ast.AndDep(e1,e2)
  | Ast.OrDep(e1,e2) ->
      let e1 = drop_before_after e1 in
      let e2 = drop_before_after e2 in
      if e1 = Ast.NoDep then e1
      else if e2 = Ast.NoDep then e2
      else Ast.OrDep(e1,e2)
  | dep -> dep

let cleanup rules =
  let env =
    List.fold_left
      (function prev ->
	function
	    (category,Ast0.CocciRule((_,_,(_,_,_,nm,_)),_,_,_,_)) ->
	      (nm,category) :: prev
	  | _ -> prev)
      [] rules in
  let (_,res) =
    List.fold_left
      (function (fixed_env,res) ->
	function
	    (Classify_rules.Before,
	     Ast0.CocciRule((minus,mmetavars,infos),_,imetavars,names,ty)) ->
	       let (isos,drop_isos,deps,nm,quant) = infos in
	       let deps = drop_before_after deps in
	       (* renaming *)
	       let mmetavars = mmetavars@imetavars in
	       let (minus,mmetavars,deps) =
		 rename_code_and_metavars env fixed_env "before"
		   minus mmetavars deps in
	       (* drop annotations *)
	       let minus = List.map Cleanup_helper.drop_annotations minus in
	       (* add file dependencies *)
	       let deps =
		 extend_dependency deps
		   (Ast.FileIn (!Flag.tmp ^ "/before")) in
	       (fixed_env,
		[(mmetavars, (* cocci doesn't have imetas here *)
		  names,
		  Ast0toast.ast0toast nm deps drop_isos quant minus [] ty)]
		::res)
	  | (Classify_rules.After,
	     Ast0.CocciRule((_,_,infos),(plus,pmetavars),imetavars,names,ty))->
	       let (isos,drop_isos,deps,nm,quant) = infos in
	       let deps = drop_before_after deps in
	       (* renaming *)
	       let pmetavars = pmetavars@imetavars in
	       let (plus,pmetavars,deps) =
		 rename_code_and_metavars env fixed_env "after"
		   plus pmetavars deps in
	       (* drop annotations *)
	       let plus = List.map Cleanup_helper.drop_annotations plus in
	       (* add file dependencies *)
	       let deps =
		 extend_dependency deps
		   (Ast.FileIn (!Flag.tmp ^ "/after")) in
	       (fixed_env,
		[(pmetavars,names,
		  Ast0toast.ast0toast nm deps drop_isos quant plus [] ty)]
		::res)
	  | (Classify_rules.Both,
	     Ast0.CocciRule((minus,mmetavars,infos),(plus,pmetavars),
			    imetavars,names,ty)) ->
	       let (isos,drop_isos,deps,nm,quant) = infos in
	       let deps = drop_before_after deps in
	       (* renaming *)
	       let mmetavars =
		 let available = mmetavars@imetavars in
		 let used = Free.free (fun _ -> true) minus in
		 List.filter
		   (function mv -> List.mem (Ast.get_meta_name mv) used)
		   available in
	       let mmetanames = List.map Ast.get_meta_name mmetavars in
	       let (minus,mmetavars,mdeps) =
		 rename_code_and_metavars env fixed_env "before"
		   minus mmetavars deps in
	       let mnm = fix_for_category nm "before" Classify_rules.Both in
	       let pmetavars =
		 let available = pmetavars@imetavars in
		 let used = Free.free (fun _ -> true) plus in
		 List.filter
		   (function mv -> List.mem (Ast.get_meta_name mv) used)
		   available in
	       let pmetanames = List.map Ast.get_meta_name pmetavars in
	       let (plus,pmetavars,pdeps) =
		 rename_code_and_metavars env fixed_env "after"
		   plus pmetavars deps in
	       let pnm = fix_for_category nm "after" Classify_rules.Both in
	       let (minus,mmetavars,plus,pmetavars,new_fixed_env) =
		 Managed_fixed.managed_fixed
		   nm mnm minus mmetavars pnm plus pmetavars in
	       let oneside_env =
		 let locals l = List.filter (function (r,n) -> r = nm) l in
		 let inonly l1 l2 =
		   List.filter (function x -> not (List.mem x l2)) l1 in
		 (List.map (function x -> (x,mnm))
		    (locals (inonly mmetanames pmetanames))) @
		 (List.map (function x -> (x,pnm))
		    (locals (inonly pmetanames mmetanames))) in
	       (* drop annotations *)
	       let minus = List.map Cleanup_helper.drop_annotations minus in
	       let plus = List.map Cleanup_helper.drop_annotations plus in
	       (* add file dependecies *)
	       let mdeps =
		 extend_dependency mdeps
		   (Ast.FileIn (!Flag.tmp ^ "/before")) in
	       let pdeps =
		 let pdeps = extend_dependency pdeps (Ast.Dep mnm) in
		 extend_dependency pdeps
		   (Ast.FileIn (!Flag.tmp ^ "/after")) in
	       (oneside_env@new_fixed_env@fixed_env,
		[(mmetavars,names,
		  Ast0toast.ast0toast mnm mdeps drop_isos quant minus [] ty);
		  (pmetavars,names,
		   Ast0toast.ast0toast pnm pdeps drop_isos quant plus [] ty)]
		::res)
	  | (Classify_rules.Bot,
	     Ast0.CocciRule((minus,mmetavars,infos),(plus,pmetavars),_,_,_)) ->
	       failwith "not possible"
	  | (cat,Ast0.ScriptRule (nm,lang,deps,mvs,scriptvars,code)) ->
	      (if not (scriptvars = [])
	      then failwith "not handling script generated metavars");
	      let deps = drop_before_after deps in
	      let deps = Cleanup_helper.rename_deps env deps in
	      let mvs =
		match cat with
		  Classify_rules.Before ->
		    let renamer = rename env fixed_env "before" in
		    let mvs =
		      List.map
			(function (localname,inhname,inhdecl,init) ->
			  let inhname = renamer inhname in
			  let inhdecl =
			    Cleanup_helper.rename_metavar renamer inhdecl in
			  (localname,inhname,inhdecl,init))
			mvs in
		    mvs
		| Classify_rules.After ->
		    let renamer = rename env fixed_env "after" in
		    let mvs =
		      List.map
			(function (localname,inhname,inhdecl,init) ->
			  let inhname = renamer inhname in
			  let inhdecl =
			    Cleanup_helper.rename_metavar renamer inhdecl in
			  (localname,inhname,inhdecl,init))
			mvs in
		    mvs
		| Classify_rules.Bot ->
		    List.map
		      (function a ->
			let (localname,((r,n) as inhname),inhdecl,init) = a in
			if r = "virtual"
			then a
			else
			  match List.assoc r env with
			    Classify_rules.Both ->
			      let fixed_env_info =
				try let _ = List.assoc (r,n) fixed_env in true
				with Not_found -> false in
			      if fixed_env_info
			      then
				(let renamer =
				  rename env fixed_env "notused" in
				let inhname = renamer inhname in
				let inhdecl =
				  Cleanup_helper.rename_metavar
				    renamer inhdecl in
				(localname,inhname,inhdecl,init))
			      else
				failwith
				  (Printf.sprintf
				     "%s.%s: which version to choose?"
				     r n)
			  | Classify_rules.Before ->
			      (* may need to rename constraints *)
			      let renamer = rename env fixed_env "before" in
			      let inhdecl =
				Cleanup_helper.rename_metavar renamer
				  inhdecl in
			      (localname,inhname,inhdecl,init)
			  | Classify_rules.After ->
			      (* may need to rename constraints *)
			      let renamer = rename env fixed_env "after" in
			      let inhdecl =
				Cleanup_helper.rename_metavar renamer
				  inhdecl in
			      (localname,inhname,inhdecl,init)
			  | Classify_rules.Bot ->
			      failwith "bot unexpected")
		      mvs
	      	| Classify_rules.Both -> failwith "not possible" in
	      (fixed_env,
	       [([],[],
		 Ast.ScriptRule (nm,lang,deps,mvs,scriptvars,code))]::res)
	  | (_,Ast0.InitialScriptRule(a,b,c,d,e)) ->
	      (fixed_env, [([],[],Ast.InitialScriptRule (a,b,c,d,e))]::res)
	  | (_,Ast0.FinalScriptRule(a,b,c,d,e)) ->
	      (fixed_env, [([],[],Ast.FinalScriptRule (a,b,c,d,e))]::res))
      ([],[]) rules in
  List.rev res
