(*
 * This file is part of Prequel, lincensed under the terms of the GPL v2.
 * See copyright.txt in the Prequel source code for more information.
 * The Prequel source code can be obtained at
 * https://github.com/prequel-pql/prequel
 *)

module Ast = Ast_cocci
module V = Visitor_ast

type pos = Shared of string | Unshared of string

let position_counter = ref 0
let positions = ref []
let current_rule = ref ""
let position_env = Hashtbl.create 101

(* ---------------------------------------------------------------------- *)

let get_lines =
  let bind = Common.union_set in
  let option_default = [] in
  let donothing r k e = k e in
  let mcode r mc = [Ast.get_mcode_line mc] in
  V.combiner bind option_default
    mcode mcode mcode mcode mcode mcode mcode mcode mcode mcode
    mcode mcode mcode mcode
    donothing donothing donothing donothing donothing
    donothing donothing donothing donothing donothing donothing
    donothing donothing donothing donothing donothing donothing
    donothing donothing donothing donothing donothing

let lines ll =
  List.fold_left
    (List.fold_left (fun prev cur -> Common.union_set cur prev)) []
    (List.map (List.map get_lines.V.combiner_anything) ll)

(* ---------------------------------------------------------------------- *)

let get_pos s builder =
  let c = !position_counter in
  position_counter := !position_counter + 1;
  let v = Printf.sprintf "%s__%d" s c in
  positions := builder v :: !positions;
  (v,Ast.MetaPos(Ast.make_mcode(!current_rule,v),[],Ast.ALL,
		 Type_cocci.Saved,false))

let minus_mcode ((x,info,mcodekind,metas) as mc) =
  match mcodekind with
    Ast.MINUS(pos,inst,adj,replacement) ->
      let plus_lines =
	match replacement with
	  Ast.NOREPLACEMENT -> []
	| Ast.REPLACEMENT(added,count) -> lines added in
      (match plus_lines with
	[] -> (x,info,mcodekind,
	       (snd(get_pos "p" (fun x -> Unshared x)))::metas)
      |	_ ->
	  let jpos = get_pos "j" (fun x -> Shared x) in
	  List.iter (fun x -> Hashtbl.add position_env x (fst jpos))
	    plus_lines;
	  (x,info,mcodekind,snd jpos::metas))
  | Ast.CONTEXT(pos,modif) -> mc
  | Ast.PLUS _ -> failwith "not possible"

let process_minus =
  let donothing r k e = k e in
  V.rebuilder
    minus_mcode minus_mcode minus_mcode minus_mcode minus_mcode minus_mcode
    minus_mcode minus_mcode minus_mcode minus_mcode minus_mcode minus_mcode
    minus_mcode minus_mcode
    donothing donothing donothing donothing donothing donothing donothing
    donothing donothing donothing donothing donothing donothing donothing
    donothing donothing donothing donothing donothing donothing donothing
    donothing

let plus_mcode ((x,info,mcodekind,metas) as mc) =
  match mcodekind with
    Ast.MINUS(pos,inst,adj,replacement) ->
      let ln = Ast.get_mcode_line mc in
      (try
	let jpos = Hashtbl.find position_env ln in
	(x,info,mcodekind,(snd(get_pos jpos (fun x -> Shared x)))::metas)
      with
	Not_found ->
	  (x,info,mcodekind,(snd(get_pos "p" (fun x -> Unshared x)))::metas))
  | Ast.CONTEXT(pos,modif) -> mc
  | Ast.PLUS _ -> failwith "not possible"

let process_plus =
  let donothing r k e = k e in
  V.rebuilder
    plus_mcode plus_mcode plus_mcode plus_mcode plus_mcode plus_mcode
    plus_mcode plus_mcode plus_mcode plus_mcode plus_mcode plus_mcode
    plus_mcode plus_mcode
    donothing donothing donothing donothing donothing donothing donothing
    donothing donothing donothing donothing donothing donothing donothing
    donothing donothing donothing donothing donothing donothing donothing
    donothing

let get_name (nm,_,_,_,_,_) =
  match nm with
    Some nm -> nm
  | None -> failwith "rule should have a name"

let insert_positions minus plus =
  List.map2
    (fun minus plus ->
      match (minus,plus) with
	(Ast.CocciRule(nm1, rule_info1, minirules1, isexp1, ruletype1),
	 Ast.CocciRule(nm2, rule_info2, minirules2, isexp2, ruletype2)) ->
	  position_counter := 0;
	  positions := [];
	  current_rule := nm1;
	  Hashtbl.clear position_env;
	  let minus =
	    Ast.CocciRule
	      (nm1, rule_info1,
	       (List.map process_minus.V.rebuilder_top_level minirules1),
	       isexp1, ruletype1) in
	  let minres = (!positions,minus) in
	  positions := [];
	  let plus =
	    Ast.CocciRule
	      (nm2, rule_info2,
	       (List.map process_plus.V.rebuilder_top_level minirules2),
	       isexp2, ruletype2) in
	  let plusres = (!positions,plus) in
	  (minres,plusres)
      |	_ -> (([],minus),([],plus)))
    minus plus
