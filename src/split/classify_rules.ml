(*
 * This file is part of Prequel, lincensed under the terms of the GPL v2.
 * See copyright.txt in the Prequel source code for more information.
 * The Prequel source code can be obtained at
 * https://github.com/prequel-pql/prequel
 *)

module Ast0 = Ast0_cocci
module Ast = Ast_cocci
module V0 = Visitor_ast0
module VT0 = Visitor_ast0_types

type category = Bot | Before | After | Both

let cat2c = function
    Bot -> "Bot"
  | Before -> "Before"
  | After -> "After"
  | Both -> "Both"

let lub a b =
  match (a,b) with
    (Bot,x) | (x,Bot) -> x
  | (Both,x) | (x,Both) -> Both
  | (Before,Before)-> Before
  | (After,After)-> After
  | (Before,After) | (After,Before) -> Both

let glb original lower =
  match (original,lower) with
    (_,Bot) -> Bot
  | (Both,x) -> x
  | _ -> original

(* --------------------------------------------------------------------------- *)

let classify_on_code rule =
  let donothing r k e = k e in
  let minus_mcode (x,arity,info,mcodekind,pos,adj) =
    match mcodekind with
      Ast0.MINUS _ -> if !Flag_split.backport then After else Before
    | Ast0.CONTEXT _ -> Bot
    | Ast0.PLUS _ -> After
    | Ast0.MIXED _ -> failwith "not possible" in
  let res =
    V0.flat_combiner lub Bot
      minus_mcode minus_mcode minus_mcode minus_mcode minus_mcode minus_mcode
      minus_mcode minus_mcode minus_mcode minus_mcode minus_mcode minus_mcode
      minus_mcode minus_mcode
      donothing donothing donothing donothing donothing donothing donothing
      donothing donothing donothing donothing donothing donothing donothing
      donothing donothing donothing donothing donothing donothing in
  List.fold_left lub Bot (List.map res.VT0.combiner_rec_top_level rule)

let rec analyze_dependencies = function
    Ast.Dep "before" -> Before
  | Ast.Dep "after" -> After
  | Ast.Dep _ -> Bot
  | Ast.AntiDep("before"|"after") ->
      failwith "negation not allowed on before/after"
  | Ast.AntiDep _ -> Bot
  | Ast.EverDep "before" | Ast.EverDep "after" ->
      failwith "ever not allowed with virtual rules"
  | Ast.EverDep _ -> Bot
  | Ast.NeverDep("before"|"after") ->
      failwith "negation not allowed on before/after"
  | Ast.NeverDep _ -> Bot
  | Ast.AndDep(e1,e2) ->
      let e1 = analyze_dependencies e1 in
      let e2 = analyze_dependencies e2 in
      if e1 = e2 then e1 else if e1 = Bot then e2 else if e2 = Bot then e1
      else failwith "inconsistent dependency annotation"
  | Ast.OrDep(e1,e2) ->
      let e1 = analyze_dependencies e1 in
      let e2 = analyze_dependencies e2 in
      (* Thus used to allow only cases where e1 = e2 or e1 or e2 is bot.  But I
	 have no recollection why.  And to handle position variables, we need
	 to be able to specify Both in some way *)
      lub e1 e2
  | Ast.FileIn _ | Ast.NotFileIn _ ->
      failwith
	"file dependencies require translation and not currently supported"
  | Ast.NoDep | Ast.FailDep -> Bot

let classify_on_dependencies dep minus plus =
  let category_of_rule_body =
    lub (classify_on_code minus) (classify_on_code plus) in
  let declared = analyze_dependencies dep in
  if declared = Bot
  then category_of_rule_body
  else if lub category_of_rule_body declared = declared
  then declared
  else failwith "inconsistent annotation"

(* This is only called on Bot rules, ie rules that have no transformations and
no specified constraints *)
let classify_on_constraints_from_children nm env denv category
    metavars (* locals *) =
  let depcat =
    try lub (List.assoc nm denv) category
    with Not_found -> category in
  List.fold_left
    (function category ->
      function meta ->
	let ((r,n) as metaname) = Ast.get_meta_name meta in
	let info_on_meta =
	  List.filter (function (nm,cat) -> nm = metaname) env in
	match info_on_meta with
	  [] -> category
	| [(_,v)] -> lub v category
	| _ -> failwith "only one binding allowed")
    depcat metavars

let check_inheriting_positions env category metavars =
  List.iter
    (function
	Ast.MetaPosDecl ((r,n) as metaname,constraints) ->
	  let info_on_meta =
	    List.filter (function (nm,cat) -> nm = metaname) env in
	  (match info_on_meta with
	    [] -> () (* never used after current rule *)
	  | [(_,v)] ->
	      if not (v = category)
	      then
		failwith
		  (Printf.sprintf
		     "position variable %s.%s can only inherit from %s" r n
		       "a rule of the same category")
	  | _ -> failwith "only one binding allowed")
      | _ -> ())
    metavars

let collect_local_features env category minus plus imetavars =
  let allmetas = List.concat (List.map Ast.get_all_meta_names imetavars) in
  let freem = Free.free (fun _ -> true) minus in
  let freep = Free.free (fun _ -> true) plus in
  List.fold_left
    (function env ->
      function ((r,n) as metaname) ->
	let (info_on_meta,rest) =
	  List.partition (function (nm,cat) -> nm = metaname) env in
	let use_category =
	  match (List.mem metaname freem,List.mem metaname freep) with
	    (true,true) -> Both
	  | (true,false) -> Before
	  | (false,true) -> After
	  | (false,false) -> Bot in
	let category = glb category use_category in
	match info_on_meta with
	  [] -> (metaname,category)::rest
	| [(nm,v)] -> (nm,lub v category)::rest
	| _ -> failwith "only one binding allowed")
    env allmetas

let collect_dependencies env category deps =
  let rec atoms_in_deps = function
      Ast.Dep d | Ast.AntiDep d | Ast.EverDep d | Ast.NeverDep d -> [d]
    | Ast.AndDep(e1,e2) ->
	Common.union_set (atoms_in_deps e1) (atoms_in_deps e2)
    | Ast.OrDep(e1,e2) ->
	Common.union_set (atoms_in_deps e1) (atoms_in_deps e2)
    | Ast.FileIn _ | Ast.NotFileIn _ ->
	failwith
	  "file dependencies require translation and not currently supported"
    | Ast.NoDep | Ast.FailDep -> [] in
  List.fold_left
    (fun env rdep ->
      let (info_on_rdep,rest) =
	List.partition (function (r,cat) -> r = rdep) env in
      match info_on_rdep with
	[] -> (rdep,category)::rest
      | [(nm,v)] -> (nm,lub v category)::rest
      | _ -> failwith "only one binding allowed")
    env (atoms_in_deps deps)

let check_for_parse_failure name str = function
    [code] ->
      (match Ast0.unwrap code with
	Ast0.FAILED e ->
	  Printf.eprintf "Parsing problem in %s fragment of rule %s:\n"
	    str name;
	  raise e
      | _ -> ())
  | l ->
      failwith
	(Printf.sprintf "not supporting multiple minirules: %s: %d"
	   name (List.length l))

let classify_rule env denv rule =
  match rule with
    Ast0.ScriptRule (_,_,dep,_,_,_) -> (env,denv,analyze_dependencies dep)
  | Ast0.InitialScriptRule (_,_,_,_,_)
  | Ast0.FinalScriptRule (_,_,_,_,_) -> (env,denv,Both)
  | Ast0.CocciRule ((minus,metavars,infos),(plus,_),imetavars,names,rt) ->
      let (iso, dropiso, deps, name, exists) = infos in
      let category =
	match classify_on_dependencies deps minus plus with
	  Bot -> classify_on_constraints_from_children name env denv Bot metavars
        | Before -> check_inheriting_positions env Before metavars; Before
        | After -> check_inheriting_positions env After metavars; After
	| c -> c in
      (match category with
	Bot -> ()
      | Before -> check_for_parse_failure name "minus" minus
      | After -> check_for_parse_failure name "plus" plus
      | Both ->
	  check_for_parse_failure name "minus" minus;
	  check_for_parse_failure name "plus" plus);
      (collect_local_features env category minus plus (metavars@imetavars),
       collect_dependencies denv category deps,category)

let classify_on_relations positions_and_rules =
  let rec loop env denv = function
      [] -> (env,denv,[])
    | rule::rules ->
	let (env,denv,code) = loop env denv rules in
	let (env,denv,category) = classify_rule env denv rule in
	(env, denv, (category,rule)::code) in
  let (_,_,res) = loop [] [] positions_and_rules in
  let res =
    List.filter
      (function
	  (Bot,Ast0.CocciRule((minus,mmetavars,infos),(plus,pmetavars),_,_,_)) ->
	    let (isos,drop_isos,deps,nm,quant) = infos in
	    Printf.eprintf "WARNING: rule %s is dead code\n" nm;
	    false
	| _ -> true)
      res in
  res
