(*
 * This file is part of Prequel, lincensed under the terms of the GPL v2.
 * See copyright.txt in the Prequel source code for more information.
 * The Prequel source code can be obtained at
 * https://github.com/prequel-pql/prequel
 *)

let backport = ref false (* invert backports, for paper *)

(* require complete modif of matched code, otherwise require only changes in
subterms of metavars *)
let uniform = ref true
