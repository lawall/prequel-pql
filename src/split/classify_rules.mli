(*
 * This file is part of Prequel, lincensed under the terms of the GPL v2.
 * See copyright.txt in the Prequel source code for more information.
 * The Prequel source code can be obtained at
 * https://github.com/prequel-pql/prequel
 *)

type category = Bot | Before | After | Both

val classify_on_relations :
  Ast0_cocci.parsed_rule list -> (category * Ast0_cocci.parsed_rule) list
