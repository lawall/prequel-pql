(*
 * This file is part of Prequel, lincensed under the terms of the GPL v2.
 * See copyright.txt in the Prequel source code for more information.
 * The Prequel source code can be obtained at
 * https://github.com/prequel-pql/prequel
 *)

val check_disjunctions :
    Insert_positions.start_end Insert_positions.pos list list ->
      Classify_rules.category list ->
      (Ast_cocci.metavar list * Ast_cocci.name list * Ast_cocci.rule)
	list list ->
	  unit
