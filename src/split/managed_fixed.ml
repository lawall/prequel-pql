(*
 * This file is part of Prequel, lincensed under the terms of the GPL v2.
 * See copyright.txt in the Prequel source code for more information.
 * The Prequel source code can be obtained at
 * https://github.com/prequel-pql/prequel
 *)

module Ast = Ast_cocci
module Ast0 = Ast0_cocci

let get_fixed_decls metavars =
  List.fold_left
    (fun prev d ->
      match d with
	Ast.MetaMetaDecl _ -> failwith "should be removed"
      | Ast.MetaIdDecl(flex,nm,_)
      | Ast.MetaTypeDecl(flex,nm)
      | Ast.MetaInitDecl(flex,nm)
      | Ast.MetaInitListDecl(flex,nm,_)
      | Ast.MetaListlenDecl(flex,nm)
      | Ast.MetaParamDecl(flex,nm)
      | Ast.MetaBinaryOperatorDecl(flex,nm,_)
      | Ast.MetaAssignmentOperatorDecl(flex,nm,_)
      | Ast.MetaParamListDecl(flex,nm,_)
      | Ast.MetaConstDecl(flex,nm,_)
      | Ast.MetaErrDecl(flex,nm)
      | Ast.MetaExpDecl(flex,nm,_)
      | Ast.MetaIdExpDecl(flex,nm,_)
      | Ast.MetaLocalIdExpDecl(flex,nm,_)
      | Ast.MetaGlobalIdExpDecl(flex,nm,_)
      | Ast.MetaExpListDecl(flex,nm,_)
      | Ast.MetaDeclDecl(flex,nm)
      | Ast.MetaFieldDecl(flex,nm)
      | Ast.MetaFieldListDecl(flex,nm,_)
      | Ast.MetaStmDecl(flex,nm)
      | Ast.MetaStmListDecl(flex,nm,_)
      | Ast.MetaFuncDecl(flex,nm)
      | Ast.MetaLocalFuncDecl(flex,nm)
      | Ast.MetaFmtDecl(flex,nm)
      | Ast.MetaFragListDecl(flex,nm,_)
      | Ast.MetaDeclarerDecl(flex,nm)
      | Ast.MetaIteratorDecl(flex,nm) ->
	  if flex = Ast.NOFLEX then nm::prev else prev
      | Ast.MetaFreshIdDecl(nm,_)
      | Ast.MetaPosDecl(nm,_)
      | Ast.MetaAnalysisDecl(_,nm) -> prev
      | Ast.MetaScriptDecl _ -> failwith "not a cocci decl")
    [] metavars


let managed_fixed name mname minus mmetavars pname plus pmetavars =
  let fixed = get_fixed_decls mmetavars in
  let fixed = List.filter (function (r,_) -> r = mname) fixed in
  let fixed_names = List.map snd fixed in
  let fixedp rulename (r,n) = rulename = r && List.mem n fixed_names in
  let mused_fixed = List.map snd (Free.free (fixedp mname) minus) in
  let pused_fixed = List.map snd (Free.free (fixedp pname) plus) in
  let todo = Common.inter_set mused_fixed pused_fixed in
  let onlyplus =
    List.filter (function x -> not (List.mem x mused_fixed)) pused_fixed in
  let needed used decl =
    let nm = snd (Ast.get_meta_name decl) in
    not (List.mem nm fixed_names) || List.mem nm used in
  let mmetavars = List.filter (needed mused_fixed) mmetavars in
  let pmetavars = List.filter (needed pused_fixed) pmetavars in
  let env ((r,n) as x) =
    if r = pname && List.mem n todo
    then (mname,n)
    else x in
  let pmetavars =
    List.map (function decl -> Cleanup_helper.rename_metavar env decl)
      pmetavars in
  let plus = List.map (Cleanup_helper.rename_code env) plus in
  (minus,mmetavars,plus,pmetavars,
   (* here metavars have already been renamed, but in future rules they will
      have their original rule names, so need to replace the mmetavars rule
      name by that *)
   (List.map (fun x -> ((name,x),mname)) mused_fixed) @
   (List.map (fun x -> ((name,x),pname)) onlyplus))
