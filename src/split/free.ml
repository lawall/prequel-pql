(*
 * This file is part of Prequel, lincensed under the terms of the GPL v2.
 * See copyright.txt in the Prequel source code for more information.
 * The Prequel source code can be obtained at
 * https://github.com/prequel-pql/prequel
 *)

module Ast = Ast_cocci
module Ast0 = Ast0_cocci
module V0 = Visitor_ast0
module VT0 = Visitor_ast0_types
module TC = Type_cocci

(* Like in free_vars.ml with include_constraints = true, but that is not
exported *)
let free filter =
  let bind x y = Common.union_set x y in
  let option_default = [] in

  let donothing recursor k e = k e in (* just combine in the normal way *)

  let metaid (x,_,_,_,_,_) = if filter x then [x] else [] in

  let astfvident recursor k i =
    bind (k i)
      (match Ast0.unwrap i with
        Ast0.MetaId(name,idconstraint,_,_)
      | Ast0.MetaFunc(name,idconstraint,_)
      | Ast0.MetaLocalFunc(name,idconstraint,_) ->
	  let metas =
	    match idconstraint with
	      Ast.IdNegIdSet (_,metas) -> List.filter filter metas
	    | Ast.IdPosIdSet (_,metas) -> List.filter filter metas
	    | _ -> [] in
	  bind (List.rev metas) (metaid name)
      | _ -> option_default) in

  let rec type_collect res = function
      TC.ConstVol(_,ty) | TC.Pointer(ty) | TC.FunctionPointer(ty)
    | TC.Array(ty) -> type_collect res ty
    | TC.EnumName(TC.MV(tyname,_,_)) ->
	bind [tyname] res
    | TC.StructUnionName(_,TC.MV(tyname,_,_)) ->
	bind [tyname] res
    | TC.MetaType(tyname,_,_) ->
	bind [tyname] res
    | TC.Decimal(e1,e2) ->
	let e2mv = function TC.MV(mv,_,_) -> [mv] | _ -> [] in
	bind (e2mv e1) (e2mv e2)
    | TC.SignedT(_,Some ty) -> type_collect res ty
    | ty -> res in

  let astfvexpr recursor k e =
    bind (k e)
      (match Ast0.unwrap e with
        Ast0.MetaExpr(name,constraints,Some type_list,_,_) ->
          let types = List.fold_left type_collect option_default type_list in
          let extra =
            match constraints with
              Ast0.SubExpCstrt l -> l
            | _ -> [] in
          bind extra (bind (metaid name) types)
      | Ast0.MetaErr(name,constraints,_)
      | Ast0.MetaExpr(name,constraints,_,_,_) ->
          let extra =
              match constraints with
                Ast0.SubExpCstrt l -> l
              | _ -> [] in
          bind extra (metaid name)
      | Ast0.MetaExprList(name,Ast0.MetaListLen (lenname),_) ->
          bind (metaid name) (metaid lenname)
      | Ast0.MetaExprList(name,_,_) -> (metaid name)
      |	_ -> option_default) in

  let astfvfrag recursor k ft =
    bind (k ft)
      (match Ast0.unwrap ft with
        Ast0.MetaFormatList(pct,name,
			   Ast0.MetaListLen (lenname)) ->
            (metaid name) @ (metaid lenname)
      |	Ast0.MetaFormatList(pct,name,_) -> metaid name
      |	_ -> option_default) in

(*  let astfvfmt recursor k ft =
    bind (k ft)
      (match Ast0.unwrap ft with
        Ast0.MetaFormat(name,_) -> metaid name
      |	_ -> option_default) in *)

  let astfvassignop recursor k aop =
    bind (k aop)
      (match Ast0.unwrap aop with
	Ast0.MetaAssign(name,_,_) -> metaid name
      | _ -> option_default) in

  let astfvbinaryop recursor k bop =
    bind (k bop)
      (match Ast0.unwrap bop with
	Ast0.MetaBinary(name,_,_) -> metaid name
      | _ -> option_default) in

  let astfvdecls recursor k d =
    bind (k d)
      (match Ast0.unwrap d with
        Ast0.MetaDecl(name,_)
      | Ast0.MetaField(name,_) -> metaid name
      |	Ast0.MetaFieldList
	  (name,Ast0.MetaListLen(lenname),_) ->
            (metaid name) @ (metaid lenname)
      |	Ast0.MetaFieldList(name,_,_) -> metaid name
      |	_ -> option_default) in

  let astfvtypeC recursor k ty =
    bind (k ty)
      (match Ast0.unwrap ty with
        Ast0.MetaType(name,_) -> metaid name
      |	_ -> option_default) in

  let astfvinit recursor k ty =
    bind (k ty)
      (match Ast0.unwrap ty with
        Ast0.MetaInit(name,_) -> metaid name
      |	Ast0.MetaInitList
	  (name,Ast0.MetaListLen(lenname),_) ->
            (metaid name) @ (metaid lenname)
      |	Ast0.MetaInitList(name,_,_) -> metaid name
      |	_ -> option_default) in

  let astfvparam recursor k p =
    bind (k p)
      (match Ast0.unwrap p with
        Ast0.MetaParam(name,_) -> metaid name
      |	Ast0.MetaParamList
	  (name,Ast0.MetaListLen(lenname),_) ->
            (metaid name) @ (metaid lenname)
      |	Ast0.MetaParamList(name,_,_) -> metaid name
      |	_ -> option_default) in

  let astfvstm recursor k re =
    bind (k re)
      (Common.nub
         (match Ast0.unwrap re with
           Ast0.MetaStmt(name,_) -> metaid name
	 | Ast0.MetaStmtList(name,Ast0.MetaListLen(lenname),_) ->
	     (metaid name) @ (metaid lenname)
         | Ast0.MetaStmtList(name,_,_) -> metaid name
         | _ -> option_default)) in

  let mcode mc =
    List.fold_left bind option_default
      (List.map
	 (function
	     Ast0.MetaPosTag(Ast0.MetaPos(name,constraints,_)) ->
	       bind (metaid name)
		 (List.fold_left bind option_default
		    (List.map
		       (function
			   Ast.PosNegSet l -> List.filter filter l
			 | Ast.PosScript(name,lang,params,body) ->
			     List.filter filter (List.map fst params))
		       constraints))
	   | _ -> failwith "expect pos")
	 (Ast0.get_pos mc)) in

  let fn =
    V0.flat_combiner bind option_default
      mcode mcode mcode mcode mcode mcode mcode mcode mcode mcode mcode
      mcode mcode mcode
      donothing donothing donothing donothing donothing donothing donothing
      astfvident astfvexpr astfvassignop astfvbinaryop astfvtypeC
      astfvinit astfvparam astfvdecls astfvstm donothing donothing
      astfvfrag (* why only frag? *) donothing in
  function t ->
    List.fold_left bind option_default
      (List.map fn.VT0.combiner_rec_top_level t)
