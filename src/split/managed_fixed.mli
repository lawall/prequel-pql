(*
 * This file is part of Prequel, lincensed under the terms of the GPL v2.
 * See copyright.txt in the Prequel source code for more information.
 * The Prequel source code can be obtained at
 * https://github.com/prequel-pql/prequel
 *)

val get_fixed_decls : Ast_cocci.metavar list -> Ast_cocci.meta_name list

val managed_fixed :
    string (* original name *) ->
      string -> Ast0_cocci.rule -> Ast_cocci.metavar list -> (* - info *)
	string -> Ast0_cocci.rule -> Ast_cocci.metavar list -> (* + info *)
	  (Ast0_cocci.rule * Ast_cocci.metavar list *
	     Ast0_cocci.rule * Ast_cocci.metavar list *
	     (Ast_cocci.meta_name * string) list)
