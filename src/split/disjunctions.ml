(*
 * This file is part of Prequel, lincensed under the terms of the GPL v2.
 * See copyright.txt in the Prequel source code for more information.
 * The Prequel source code can be obtained at
 * https://github.com/prequel-pql/prequel
 *)

(* We can't really support disjunctions with -+ modifications.
See formal.tex for a counterexample and discussion. *)

module Ast = Ast_cocci
module V = Visitor_ast
module IP = Insert_positions

type modif = Modif | NoModif | Any

let same = function (Any,_) | (_,Any) -> true | (x,y) -> x = y

let check_for_modifs two_sided one_sided =
  let donothing r k e = k e in
  let option_default = Any in
  let bind x y =
    if same (x,y)
    then x
    else failwith "mix of context code and modif not allowed in Both rules" in

  let mcode _ x =
    List.fold_left
      (function prev ->
	(function Ast.MetaPos(name,constraints,_,keep,inh) ->
	  let name = Ast.unwrap_mcode name in
	  if List.mem name two_sided 
	  then
	    failwith "no modifications allowed in disjunctions of Both rules"
	  else if List.mem name one_sided
	  then bind Modif prev
	  else bind NoModif prev))
      Any (Ast.get_pos_var x) in

  V.combiner bind option_default
    mcode mcode mcode mcode mcode mcode mcode mcode mcode
    mcode mcode mcode mcode mcode
    donothing donothing donothing donothing donothing
    donothing donothing donothing donothing donothing donothing
    donothing donothing donothing donothing donothing donothing
    donothing donothing donothing donothing donothing

let all_same l =
  match l with
    [] -> ()
  | x::xs ->
      if List.for_all (function a -> same(a,x)) l
      then ()
      else failwith "all branches have to have same modif in Both"

(* case for disj, only *)
let modif_in_disj two_sided one_sided body =
  let donothing r k e = k e in
  let option_default = () in
  let bind x y = () in
  let mcode _ x = () in
  let cfm = check_for_modifs two_sided one_sided in

  let ident r k i =
    match Ast.unwrap i with
      Ast.DisjId(ids) -> all_same (List.map cfm.V.combiner_ident ids)
    | _ -> k i in
  
  let expression r k e =
    match Ast.unwrap e with
      Ast.DisjExpr(exps) -> all_same (List.map cfm.V.combiner_expression exps)
    | _ -> k e in
  
  let fullType r k ft =
    match Ast.unwrap ft with
      Ast.DisjType(decls) -> all_same (List.map cfm.V.combiner_fullType decls)
    | _ -> k ft in

  let declaration r k d =
    match Ast.unwrap d with
      Ast.DisjDecl(decls) ->
	all_same (List.map cfm.V.combiner_declaration decls)
    | _ -> k d in

  let rule_elem r k re =
    match Ast.unwrap re with
      Ast.DisjRuleElem(res) -> all_same (List.map cfm.V.combiner_rule_elem res)
    | _ -> k re in

  let statement r k s =
    match Ast.unwrap s with
      Ast.Disj(stmt_dots) ->
	all_same (List.map cfm.V.combiner_statement_dots stmt_dots)
    | _ -> k s in

  let op =
    V.combiner bind option_default
      mcode mcode mcode mcode mcode mcode mcode mcode mcode
      mcode mcode mcode mcode mcode
      donothing donothing donothing donothing donothing
      ident expression donothing donothing donothing donothing
      fullType donothing donothing donothing declaration donothing
      rule_elem statement donothing donothing donothing in
  op.V.combiner_top_level body

(* why is this not done in cleanup? *)
let cleanup_positions positions category =
  let rename s (r,n) = (Printf.sprintf "%s_%s" s r, n) in
  let se_rename s = function
      IP.Concrete (r,n) -> IP.Concrete(Printf.sprintf "%s_%s" s r, n)
    | IP.Meta (r,n) -> IP.Meta(Printf.sprintf "%s_%s" s r, n) in
  let rename_start_end_list s r =
    List.map (function (s1,m1,e1) -> (se_rename s s1,m1,se_rename s e1)) r in
  match category with
    Classify_rules.Both ->
      let rename_start_end s = function
	  IP.Region r -> IP.Region (rename_start_end_list s r)
	| IP.BeforeEmpty m1 -> IP.BeforeEmpty(rename s m1)
	| IP.AfterEmpty m1 -> IP.AfterEmpty(rename s m1)
	| IP.AroundEmpty(m1,m2) -> IP.AroundEmpty(rename s m1,rename s m2) in
      List.map
	(function
	    IP.Shared(m,p) ->
	      IP.Shared(rename_start_end "before" m,rename_start_end "after" p)
	  | IP.MUnshared r -> IP.MUnshared(rename_start_end_list "before" r)
	  | IP.PUnshared r -> IP.PUnshared(rename_start_end_list "after" r))
	positions
  | _ -> positions

let check_disjunctions positions categories rules =
  let positions = List.map2 cleanup_positions positions categories in
  List.iter2
    (fun positions (category,rules) ->
      if category = Classify_rules.Both
      then
	let coccis =
	  List.fold_left
	    (function prev ->
	      function
		  (_,_,Ast.CocciRule(nm,info,[body],_,_)) ->
		    body :: prev
		| _ -> prev)
	    [] rules in
	let undo = function IP.Concrete s -> s | IP.Meta s -> s in
	let (two_sided,one_sided) =
	  let get_all l =
	    List.fold_left
	      (fun prev (s,m,e) ->
		(undo s)::(undo e)::(List.map undo m)@prev)
	      [] l in
	  let allinfo = function
	      IP.Region r -> get_all r
	    | IP.BeforeEmpty m -> [m]
	    | IP.AfterEmpty m -> [m]
	    | IP.AroundEmpty(m1,m2) -> [m1;m2] in
	  List.fold_left
	    (fun (two_sided,one_sided) a ->
	      match a with
		IP.Shared(b,a) ->
		  ((allinfo b)@(allinfo a)@two_sided,one_sided)
	      | IP.MUnshared r -> (two_sided,(get_all r)@one_sided)
	      | IP.PUnshared r -> (two_sided,(get_all r)@one_sided))
	    ([],[]) positions in
	List.iter (function cocci -> modif_in_disj two_sided one_sided cocci)
	  coccis
      else ())
    positions (List.combine categories rules)
