(*
 * This file is part of Prequel, lincensed under the terms of the GPL v2.
 * See copyright.txt in the Prequel source code for more information.
 * The Prequel source code can be obtained at
 * https://github.com/prequel-pql/prequel
 *)

let code =

"type repl = REPLACEMENT | ADDREMOVE

let get_minus (_,mstarter,mscol,mender,mecol,pstarter,pscol,pender,pecol) =
  (mstarter,mscol,mender,mecol)

let get_plus (_,mstarter,mscol,mender,mecol,pstarter,pscol,pender,pecol) =
  (pstarter,pscol,pender,pecol)

let minus_hunks = (Hashtbl.create 101,get_minus)
let plus_hunks = (Hashtbl.create 101,get_plus)
let hunks_to_lines = Hashtbl.create 101

type data = string (*file*) * (int * int * int * int) (*start/end ln/col*) *
      int (*hunknum*)
type tree = Node of data * tree * tree | Leaf

let minus_tree = ref Leaf
let plus_tree = ref Leaf

type 'a tokty = Meta of 'a | Concrete of 'a

type start_end =
    Region of (Coccilib.pos list * Coccilib.pos list) tokty list
  | BeforeEmpty of Coccilib.pos list
  | AfterEmpty of Coccilib.pos list
  | AroundEmpty of Coccilib.pos list * Coccilib.pos list

(* ----------------------------------------------------------------------- *)

let rec iota n end_line =
  if n = end_line then [n] else n :: iota (n+1) end_line

let rec split n = function
    [] -> ([],[])
  | (x::xs) as all ->
      if n = 0
      then ([],all)
      else
        let (f,r) = split (n-1) xs in
        (x::f,r)

let rec build n = function
    [] -> Leaf
  | [x] -> Node(x,Leaf,Leaf)
  | l ->
      let n2 = n / 2 in
      let (f,r) = split n2 l in
      match r with
        [] -> failwith \"not possible1\"
      | r::rs ->
          let rslen = if n2 * 2 = n then n2-1 else n2 in
          Node (r, build n2 f, build rslen rs)

let mktree l = build (List.length l) (List.sort compare l)

let search file line col tree =
  let rec search tree parent =
    match tree with
      Node((fl,(s,scol,e,ecol),n),lc,rc) ->
        if file = fl &&
	  (s < line || (s = line && scol <= col)) &&
	  (line < e || (line = e && col <= ecol))
        then tree
        else
          if fl > file || (fl = file && (s > line || (s = line && col < scol)))
          then search lc parent
          else search rc tree
    | Leaf -> parent in
  match search tree Leaf with
    Node((fl,(s,scol,e,ecol),n),_,_) when file = fl -> Some n
  | _ -> None

(* ----------------------------------------------------------------------- *)

let ios = int_of_string

let fromto n1 n2 =
  let rec loop n1 = if n1 > n2 then [] else n1 :: loop (n1+1) in
  loop n1

let _parse_hunk_data = (* executed during initialize *)
  let i = open_in hunk_file in
  let mhunks = ref [] in
  let phunks = ref [] in
  let hashadd tbl k v =
    let cell =
      try Hashtbl.find tbl k
      with Not_found ->
	let cell = ref [] in
	Hashtbl.add tbl k cell;
	cell in
    cell := v :: !cell in
  let rec loop _ =
    let l = input_line i in
    (match Str.split (Str.regexp \"|\") l with
    | [\"Hunk\";n;ty;file;mstarter;mscol;mender;mecol;
	  pstarter;pscol;pender;pecol] ->
	let n = ios n in
	let ty = if ty = \"R\" then REPLACEMENT else ADDREMOVE in
	let mstarter = ios mstarter in
	let mscol = ios mscol in
	let mender = ios mender in
	let mecol = ios mecol in
	let pstarter = ios pstarter in
	let pscol = ios pscol in
	let pender = ios pender in
	let pecol = ios pecol in
	let infos =
	  (ty,mstarter,mscol,mender,mecol,pstarter,pscol,pender,pecol) in
	Hashtbl.add hunks_to_lines n (file,infos);
	mhunks := (file,(mstarter,mscol,mender,mecol),n) :: !mhunks;
	phunks := (file,(pstarter,pscol,pender,pecol),n) :: !phunks;
	(if mstarter < mender || (mstarter = mender && mscol < mecol)
	then
	  List.iter (fun l -> hashadd (fst minus_hunks) (file,l) (n,infos))
	    (fromto mstarter mender));
	(if pstarter < pender || (pstarter = pender && pscol < pecol)
	then
	  List.iter (fun l -> hashadd (fst plus_hunks) (file,l) (n,infos))
	    (fromto pstarter pender))
    | _ -> failwith (Printf.sprintf \"bad hunk record: %s\" l));
    loop() in
  try loop()
  with End_of_file ->
    begin
      minus_tree := mktree !mhunks;
      plus_tree := mktree !phunks
    end

(* ----------------------------------------------------------------------- *)

let hunknum = fst

let before line1 col1 line2 col2 =
  line1 < line2 || (line1 = line2 && col1 <= col2)

let find_in_hunks (table,getter) file line col =
  let possibilities = Hashtbl.find table (file,line) in
  List.find
    (function (n,infos) ->
      let (sline,scol,eline,ecol) = getter infos in
      before sline scol line col && before line col eline ecol)
    !possibilities

let find_in_between_hunks (table,getter) file
    start_line start_col end_line end_col =
  let lines = iota start_line end_line in
  let possibilities =
    Common.nub
      (List.concat
	 (List.map
	    (function line ->
	      try !(Hashtbl.find table (file,line)) with Not_found -> [])
	    lines)) in
  List.filter
    (function (n,infos) ->
      let (sline,scol,eline,ecol) = getter infos in
      (before sline scol start_line start_col &&
       before start_line start_col eline ecol) ||
       (before sline scol end_line end_col &&
	before end_line end_col eline ecol) ||
	(before start_line start_col sline scol &&
	 before eline ecol end_line end_col))
    possibilities

(* ----------------------------------------------------------------------- *)

let start_record _ =
  flush stdout; Printf.printf \"start record\\n\"; flush stdout
let end_record _ =
  flush stdout; Printf.printf \"end record\\n\"; flush stdout

let normalize_points compare_by_line starter ender =
  let (starter,ender) = (* probably not safe in obscure cases *)
    if List.length starter = List.length ender
    then (starter,ender)
    else
      match (starter,ender) with (* not sure this is useful *)
	([starter],_) -> (List.map (function _ -> starter) ender, ender)
      |	(_,[ender]) -> (starter, List.map (function _ -> ender) starter)
      | _ ->
	  failwith
	    \"not able to accomodate position lists of different lengths\" in
  let starter = List.sort compare_by_line starter in
  let ender = List.sort compare_by_line ender in
  (starter,ender)

let string_of_hunk (k,f,h,l,c,el,ec) =
  Printf.sprintf \"%s:%s:%d:%d:%d\" k f h l el (* drop columns for line % *)

let thefile = ref \"\"
let get_file key p =
  let drop_parmap fl =
    let res =
      String.concat \"/\" (List.tl (Str.split (Str.regexp \"/\") fl)) in
    (if not (res = !thefile) then thefile := res);
    res in
  let fl = p.Coccilib.file in
  match key with
      \"M\" ->
	(try drop_parmap(List.nth (Str.split (Str.regexp \"/before/\") fl) 1)
	with e -> !thefile (*normally file is standard.h, hope for the best*))
    | \"P\" ->
        (try drop_parmap(List.nth (Str.split (Str.regexp \"/after/\") fl) 1)
         with e -> !thefile)
    | _ -> failwith \"bad key\"

let get_contained table key starter =
  if starter = [] (* can at least occur in -+ case *)
  then None
  else
  try
    Some
      (List.map string_of_hunk
	 (Common.nub
	    (List.concat
               (List.map
		  (fun starter ->
		    let fl = get_file key starter in
		    let start_line = starter.Coccilib.line in
		    let start_col = starter.Coccilib.col in
		    let end_line = starter.Coccilib.line_end in
		    let end_col = starter.Coccilib.col_end in
		    let lines = iota start_line end_line in
		    let hunks =
		      find_in_between_hunks table fl
			start_line start_col end_line end_col in
		    (* need the lines matches by each hunk, to ultimately get
		       get the matched line percentage *)
		    let hunks =
		      List.sort compare
			(List.concat
			   (List.map
			      (function line ->
				List.concat
				  (List.map
				     (function (hunk,infos) ->
				       let (sline,scol,eline,ecol) =
					 (snd table) infos in
				       if sline <= line && eline >= line
				       then [(line,hunk)]
				       else [])
				  hunks))
			      lines)) in
		    if hunks = []
		    then raise Not_found
		    else
		      let rec merge = function
			  [] -> failwith \"not possible\"
			| [(line,num)] -> [((line,line),num)]
			| (line,num) :: rest ->
			    match merge rest with
			      [] -> failwith \"not possible\"
			    | ((_,line1),num1)::rest when num = num1 ->
				((line,line1),num1) :: rest
			    | rest -> ((line,line),num)::rest in
		      let infos = merge hunks in
		      match infos with
			[] -> failwith \"not possible\"
		      | _ ->
			  List.map
			    (function ((start_line,end_line),start_hunk) ->
			      (* columns do not matter, so put 0 *)
			      (key,fl,start_hunk,start_line,0,end_line,0))
			    infos)
		  starter))))
  with Not_found -> None

let do_check_in_same_hunk table key region =
  let res =
    List.map
      (function starter_ender ->
	let (is_concrete,starter,ender) =
	  match starter_ender with
	    Concrete(starter,ender) -> (true,starter,ender)
	  | Meta(starter,ender) -> (false,starter,ender) in
	if starter = [] || ender = [] (* can at least occur in -+ case *)
	then None
	else
	  let compare_by_line p1 p2 =
	    let l1 = p1.Coccilib.line in
	    let l2 = p2.Coccilib.line in
	    if l1 = l2
	    then compare p1.Coccilib.col p2.Coccilib.col
	    else compare l1 l2 in
	  let (starter,ender) = normalize_points compare_by_line starter ender in
	  try
	    Some
	      (Common.nub
		 (List.concat
		    (List.map2
		       (fun starter ender ->
			 let fl = get_file key starter in
			 let start_line = starter.Coccilib.line in
			 let start_col = starter.Coccilib.col in
			 let end_line = ender.Coccilib.line_end in
			 let end_col = ender.Coccilib.col_end in
			 let hunks =
			   Common.nub
			     (List.map hunknum
				(find_in_between_hunks table fl
				   start_line start_col end_line end_col)) in
			 if not is_concrete || List.length hunks = 1
			 then
			   List.map
			     (fun h ->
			       (key,fl,h,
				start_line,start_col,end_line,end_col))
			     hunks
			 else raise Not_found)
		       starter ender)))
	  with Not_found -> None)
      region in
  if List.exists (fun x -> x = None) res
  then None
  else
    Some
      (List.fold_left
	 (fun prev ->
	   function
	       Some l -> List.append l prev
	     | None -> failwith \"not possible\")
	 [] res)

let check_in_same_hunk table key starter ender =
  match do_check_in_same_hunk table key [Concrete(starter,ender)] with
    Some l -> Some (List.map string_of_hunk l)
  | None -> None

let in_or_before hunk point choose =
  let end_line = point.Coccilib.line_end in
  let end_col = point.Coccilib.col_end in
  try
    let (_,(_,_,_,mender,mecol,_,_,pender,pecol)) =
      Hashtbl.find hunks_to_lines hunk in
    let ender = choose mender pender in
    end_line < ender || (end_line = ender && end_col <= choose mecol pecol)
  with Not_found -> false

let in_or_after hunk point choose =
  let line = point.Coccilib.line in
  let col = point.Coccilib.col in
  try
    let (_,(_,mstarter,mscol,_,_,pstarter,pscol,_,_)) =
      Hashtbl.find hunks_to_lines hunk in
    let starter = choose mstarter pstarter in
    line > starter || (line = starter && col >= choose mscol pscol)
  with Not_found -> false

let empty_check hunks other dir choose =
  match hunks with
    None -> None
  | Some hunks ->
      if List.length hunks = List.length other
      then
	try
	  Some
	    (Common.nub
	       (List.concat
		  (List.map2
		     (fun ((_,_,h,_,_,_,_) as hunk) p ->
		       if dir h p choose then [hunk] else raise Not_found)
		     hunks other)))
	with Not_found -> None
      else (Printf.eprintf \"range-empty: lengths must be the same\"; None)

let do_check_pair_in_same_hunk = function
    (Region(mstarter_mender),Region(pstarter_pender)) ->
      let mhunks = do_check_in_same_hunk minus_hunks \"M\" mstarter_mender in
      let phunks = do_check_in_same_hunk plus_hunks \"P\" pstarter_pender in
      (match (mhunks,phunks) with
	  (None,_) | (_,None) -> None
	| (Some mhunks,Some phunks) ->
	    (* not sure this is good enough... *)
	    let res =
	      try Some(normalize_points compare mhunks phunks)
	      with Failure _ -> None in
	    match res with
	      None -> None
	    | Some(mhunks,phunks) ->
		try
		  Some
		    (Common.nub
		       (List.concat
			  (List.map2
			     (fun
			       ((_,_,mh,_,_,_,_) as mhunk)
				 ((_,_,ph,_,_,_,_) as phunk) ->
				   if mh = ph
				   then [mhunk;phunk]
				   else raise Not_found)
			     mhunks phunks)))
		with Not_found -> None)
  | (Region(mstarter_mender),BeforeEmpty m) ->
      let mhunks = do_check_in_same_hunk minus_hunks \"M\" mstarter_mender in
      empty_check mhunks m in_or_before (fun m p -> p)
  | (Region(mstarter_mender),AfterEmpty m) ->
      let mhunks = do_check_in_same_hunk minus_hunks \"M\" mstarter_mender in
      empty_check mhunks m in_or_after (fun m p -> p)
  | (Region(mstarter_mender),AroundEmpty(m1,m2)) ->
      let mhunks = do_check_in_same_hunk minus_hunks \"M\" mstarter_mender in
      let res1 = empty_check mhunks m1 in_or_before (fun m p -> p) in
      let res2 = empty_check mhunks m2 in_or_after (fun m p -> p) in
      (match (res1,res2) with
	(Some(h1),Some(h2)) -> Some(Common.nub(List.append h1 h2))
      | _ -> None)
  | (BeforeEmpty m,Region(pstarter_pender)) ->
      let phunks = do_check_in_same_hunk plus_hunks \"P\" pstarter_pender in
      empty_check phunks m in_or_before (fun m p -> m)
  | (AfterEmpty m,Region(pstarter_pender)) ->
      let phunks = do_check_in_same_hunk plus_hunks \"P\" pstarter_pender in
      empty_check phunks m in_or_after (fun m p -> m)
  | (AroundEmpty(m1,m2),Region(pstarter_pender)) ->
      let phunks = do_check_in_same_hunk plus_hunks \"P\" pstarter_pender in
      let res1 = empty_check phunks m1 in_or_before (fun m p -> m) in
      let res2 = empty_check phunks m2 in_or_after (fun m p -> m) in
      (match (res1,res2) with
	(Some(h1),Some(h2)) -> Some(Common.nub(List.append h1 h2))
      | _ -> None)
  | _ -> failwith \"not possible2\"

let check_pair_in_same_hunk minus plus =
  match do_check_pair_in_same_hunk (minus, plus) with
    Some l -> Some (List.map string_of_hunk l)
  | None -> None

let inside line col getter hunk =
  let (line_start,col_start,line_end,col_end) = getter (snd hunk) in
  (line > line_start || (line = line_start && col >= col_start)) &&
  (line < line_end || (line = line_end && col <= col_end))

let check_in_a_hunk(key,(table,getter),p) =
  let fl = get_file key (List.hd p) in
  List.exists
    (function p ->
      let start_line = p.Coccilib.line in
      let start_col = p.Coccilib.col in
      try
	let hunks = Hashtbl.find table (fl,start_line) in
	let _ = List.find (inside start_line start_col getter) !hunks in true
      with Not_found -> false)
    p

let check_intersects_a_hunk(key,(table,getter),p) =
  let fl = get_file key (List.hd p) in
  List.exists
    (function p ->
      let start_line = p.Coccilib.line in
      let start_col = p.Coccilib.col in
      let end_line = p.Coccilib.line_end in
      let end_col = p.Coccilib.col_end in
      let options =
	find_in_between_hunks (table,getter) fl
	  start_line start_col end_line end_col in
      not (options = []))
    p

let check_in_a_replacement_hunk(key,(table,getter),p) =
  let fl = get_file key (List.hd p) in
  List.exists
    (function p ->
      let start_line = p.Coccilib.line in
      try
	let hunks = Hashtbl.find table (fl,start_line) in
	let start_col = p.Coccilib.col in
	let (_,(ty,_,_,_,_,_,_,_,_)) =
	  List.find (inside start_line start_col getter) !hunks in
	ty = REPLACEMENT
      with Not_found -> false)
    p

let check_intersects_a_replacement_hunk(key,(table,getter),p) =
  let fl = get_file key (List.hd p) in
  List.exists
    (function p ->
      let start_line = p.Coccilib.line in
      let start_col = p.Coccilib.col in
      let end_line = p.Coccilib.line_end in
      let end_col = p.Coccilib.col_end in
      let options =
	find_in_between_hunks (table,getter) fl
	  start_line start_col end_line end_col in
      List.exists
	(function (_,(ty,_,_,_,_,_,_,_,_)) ->
	  ty = REPLACEMENT)
	options)
    p

(* pretty inefficient for the moment... *)
let find_pre_hunk tbl file line =
  let rec loop = function
      0 -> None
    | n ->
	let attempt =
	  try Some (Hashtbl.find tbl (file,line))
	  with Not_found -> None in
	match attempt with
	  Some hunk -> Some hunk
	| None -> loop (n-1) in
  loop line

(* Only concerned about success and failure, so just return boolean *)
let do_check_point_correspondence file bef_line bef_col aft_line aft_col =
  let bef_hunk =
    try Some(find_in_hunks minus_hunks file bef_line bef_col)
    with Not_found -> None in
  let aft_hunk =
    try Some(find_in_hunks plus_hunks file aft_line aft_col)
    with Not_found -> None in
  match (bef_hunk,aft_hunk) with
    (Some _,None) | (None,Some _) -> false
  | (Some bef_hunk, Some aft_hunk) -> fst bef_hunk = fst aft_hunk
  | (None,None) ->
      let bef_pre_hunk = search file bef_line bef_col !minus_tree in
      match bef_pre_hunk with
	None ->
	  let aft_pre_hunk = search file aft_line aft_col !plus_tree in
	  (match aft_pre_hunk with
	    None -> bef_line = aft_line
	  | Some _ -> false)
      | Some bef_hunk ->
	  let (_,(_,_,_,_,_,_,_,pender,pecol)) as hi =
	    Hashtbl.find hunks_to_lines bef_hunk in
	  let hi = (* hunk_file must be ordered! *)
	    try
	      let (fl,(_,_,_,_,_,pstarter,pscol,_,_)) =
		Hashtbl.find hunks_to_lines (bef_hunk+1) in
	      if file = fl &&
		(pender < aft_line ||
		(pender = aft_line && pecol < aft_col)) &&
		(aft_line < pstarter ||
		(aft_line = pstarter && aft_col < pscol))
	      then Some hi
	      else None
	    with Not_found -> None in
	  match hi with
	    Some (_,(_,mstarter,mscol,mender,mecol,
		     pstarter,pscol,pender,pecol)) ->
	      if mender < bef_line
	      then bef_col = aft_col && (bef_line - mender + pender) = aft_line
	      else bef_line = aft_line && (bef_col - mecol + pecol) = aft_col
	  | None -> false

let check_point_correspondence before after =
  (List.length before) = (List.length after) &&
  List.for_all2
   (fun bef aft ->
     let file =
       List.nth (Str.split (Str.regexp \"/before/\") bef.Coccilib.file) 1 in
     (do_check_point_correspondence file
	bef.Coccilib.line bef.Coccilib.col
	aft.Coccilib.line aft.Coccilib.col) &&
     (do_check_point_correspondence file
	bef.Coccilib.line_end bef.Coccilib.col_end
	aft.Coccilib.line_end aft.Coccilib.col_end))
  (List.sort compare before) (List.sort compare after)
"
