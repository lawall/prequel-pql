(*
 * This file is part of Prequel, lincensed under the terms of the GPL v2.
 * See copyright.txt in the Prequel source code for more information.
 * The Prequel source code can be obtained at
 * https://github.com/prequel-pql/prequel
 *)

module Ast0 = Ast0_cocci

let get_ender f = function
    [rule] -> (* not supporting multiple minirules *)
      (match Ast0.unwrap rule with
	Ast0.CODE sl ->
	  (match f (Ast0.unwrap sl) with
	    fst::rest ->
	      (match Ast0.unwrap fst with
		Ast0.Dots(_,whens)
		when
		  List.for_all
		    (function Ast0.WhenModifier _-> true | _ -> false)
		    whens ->
		      Some
			([Ast0.rewrap rule
			     (Ast0.CODE(Ast0.rewrap sl (f rest)))])
	      | _ -> None)
	  | _ -> None)
      | _ -> None)
  | _ -> None

let get_left_ender = get_ender (function x -> x)
let get_right_ender = get_ender List.rev

let drop_enders minus plus =
  let (minus,plus) =
    let left_mender = get_left_ender minus in
    let left_pender = get_left_ender plus in
    match (left_mender,left_pender) with
      (Some shorter, None) -> (shorter,plus)
    | (None, Some shorter) -> (minus,shorter)
    | _ -> (minus,plus) in
  let (minus,plus) =
    let right_mender = get_right_ender minus in
    let right_pender = get_right_ender plus in
    match (right_mender,right_pender) with
      (Some shorter, None) -> (shorter,plus)
    | (None, Some shorter) -> (minus,shorter)
    | _ -> (minus,plus) in
  (minus,plus)

let process rules =
  List.map
    (function
	(pos,(cat,Ast0.CocciRule((minus,mmetavars,infos),(plus,pmetavars),
				 imetavars,names,ty))) ->
	let (minus,plus) = drop_enders minus plus in
	(pos,(cat,Ast0.CocciRule((minus,mmetavars,infos),(plus,pmetavars),
				 imetavars,names,ty)))
      | r -> r)
    rules
