(*
 * This file is part of Prequel, lincensed under the terms of the GPL v2.
 * See copyright.txt in the Prequel source code for more information.
 * The Prequel source code can be obtained at
 * https://github.com/prequel-pql/prequel
 *)

module Ast0 = Ast0_cocci
module Ast = Ast_cocci
module V0 = Visitor_ast0
module VT0 = Visitor_ast0_types

(* ----------------------------------------------------------------------- *)
(* Renaming metavar decls *)

let get_option f = function
    None -> None
  | Some x -> Some (f x)

let rec rename_metavar rename = function
    Ast.MetaMetaDecl(flex,nm) -> Ast.MetaMetaDecl(flex,rename nm)
  | Ast.MetaIdDecl(flex,nm,cstr) ->
      Ast.MetaIdDecl(flex,rename nm,rename_idcstr rename cstr)
  | Ast.MetaFreshIdDecl(nm,seed) ->
      Ast.MetaFreshIdDecl(rename nm,rename_seed rename seed)
  | Ast.MetaTypeDecl(flex,nm) -> Ast.MetaTypeDecl(flex,rename nm)
  | Ast.MetaInitDecl(flex,nm) -> Ast.MetaInitDecl(flex,rename nm)
  | Ast.MetaInitListDecl(flex,nm,nm1) ->
      Ast.MetaInitListDecl(flex,rename nm,rename_list_len rename nm1)
  | Ast.MetaListlenDecl(flex,nm) -> Ast.MetaListlenDecl(flex,rename nm)
  | Ast.MetaParamDecl(flex,nm) -> Ast.MetaParamDecl(flex,rename nm)
  | Ast.MetaParamListDecl(flex,nm,nm1) ->
      Ast.MetaParamListDecl(flex,rename nm,rename_list_len rename nm1)
  | Ast.MetaBinaryOperatorDecl(flex,nm,cstr) ->
      Ast.MetaBinaryOperatorDecl(flex,rename nm,cstr)
  | Ast.MetaAssignmentOperatorDecl(flex,nm,cstr) ->
      Ast.MetaAssignmentOperatorDecl(flex,rename nm,cstr)
  | Ast.MetaConstDecl(flex,nm,ty) ->
      Ast.MetaConstDecl(flex,rename nm,
			get_option (List.map (rename_type rename)) ty)
  | Ast.MetaErrDecl(flex,nm) -> Ast.MetaErrDecl(flex,rename nm)
  | Ast.MetaExpDecl(flex,nm,ty) ->
      Ast.MetaExpDecl(flex,rename nm,
		      get_option (List.map (rename_type rename)) ty)
  | Ast.MetaIdExpDecl(flex,nm,ty) ->
      Ast.MetaIdExpDecl(flex,rename nm,
			get_option (List.map (rename_type rename)) ty)
  | Ast.MetaLocalIdExpDecl(flex,nm,ty) ->
      Ast.MetaLocalIdExpDecl(flex,rename nm,
			     get_option
			       (List.map (rename_type rename)) ty)
  | Ast.MetaGlobalIdExpDecl(flex,nm,ty) ->
      Ast.MetaGlobalIdExpDecl(flex,rename nm,
			      get_option
				(List.map (rename_type rename)) ty)
  | Ast.MetaExpListDecl(flex,nm,nm1) ->
      Ast.MetaExpListDecl(flex,rename nm,rename_list_len rename nm1)
  | Ast.MetaDeclDecl(flex,nm) -> Ast.MetaDeclDecl(flex,rename nm)
  | Ast.MetaFieldDecl(flex,nm) -> Ast.MetaFieldDecl(flex,rename nm)
  | Ast.MetaFieldListDecl(flex,nm,nm1) ->
      Ast.MetaFieldListDecl(flex,rename nm,rename_list_len rename nm1)
  | Ast.MetaStmDecl(flex,nm) -> Ast.MetaStmDecl(flex,rename nm)
  | Ast.MetaStmListDecl(flex,nm,nm1) ->
      Ast.MetaStmListDecl(flex,rename nm,rename_list_len rename nm1)
  | Ast.MetaFuncDecl(flex,nm) -> Ast.MetaFuncDecl(flex,rename nm)
  | Ast.MetaLocalFuncDecl(flex,nm) -> Ast.MetaLocalFuncDecl(flex,rename nm)
  | Ast.MetaPosDecl(nm,constraints) ->
      let constraints =
	List.map
	  (function
	      Ast.PosNegSet l -> Ast.PosNegSet(List.map rename l)
	    | Ast.PosScript(name,lang,params,body) ->
		let params =
		  List.map
		    (function (nm,mv) -> (rename nm,rename_metavar rename mv))
		    params in
		Ast.PosScript(name,lang,params,body))
	  constraints in
      Ast.MetaPosDecl(rename nm,constraints)
  | Ast.MetaFmtDecl(flex,nm) -> Ast.MetaFmtDecl(flex,rename nm)
  | Ast.MetaFragListDecl(flex,nm,nm1) ->
      Ast.MetaFragListDecl(flex,rename nm,rename_list_len rename nm1)
  | Ast.MetaAnalysisDecl(code,nm) -> Ast.MetaAnalysisDecl(code,rename nm)
  | Ast.MetaDeclarerDecl(flex,nm) -> Ast.MetaDeclarerDecl(flex,rename nm)
  | Ast.MetaIteratorDecl(flex,nm) -> Ast.MetaIteratorDecl(flex,rename nm)
  | Ast.MetaScriptDecl(flex,nm) -> Ast.MetaScriptDecl(flex,rename nm)

and rename_idcstr rename cstr =
  match cstr with
    Ast.IdNoConstraint -> cstr
  | Ast.IdPosIdSet(s,mv) -> Ast.IdPosIdSet(s,List.map rename mv)
  | Ast.IdNegIdSet(s,mv) -> Ast.IdNegIdSet(s,List.map rename mv)
  | Ast.IdRegExpConstraint re -> cstr

and rename_seed rename seed =
  match seed with
    Ast.NoVal -> seed
  | Ast.StringSeed _ -> seed
  | Ast.ListSeed(l) ->
      Ast.ListSeed(List.map (rename_seed_elem rename) l)

and rename_seed_elem rename seed_elem =
  match seed_elem with
    Ast.SeedString _ -> seed_elem
  | Ast.SeedId(nm) -> Ast.SeedId(rename nm)

and rename_type rename ty =
  match ty with
    Type_cocci.ConstVol(cv,ty) ->
      Type_cocci.ConstVol(cv,rename_type rename ty)
  | Type_cocci.BaseType _ -> ty
  | Type_cocci.SignedT(sgn,ty) ->
      Type_cocci.SignedT(sgn,get_option (rename_type rename) ty)
  | Type_cocci.Pointer(ty) ->
      Type_cocci.Pointer(rename_type rename ty)
  | Type_cocci.FunctionPointer(ty) ->
      Type_cocci.FunctionPointer(rename_type rename ty)
  | Type_cocci.Array(ty) ->
      Type_cocci.Array(rename_type rename ty)
  | Type_cocci.Decimal(e1,e2) ->
      Type_cocci.Decimal(rename_tyname rename e1,
			 rename_tyname rename e2)
  | Type_cocci.EnumName(name) ->
      Type_cocci.EnumName(rename_tyname rename name)
  | Type_cocci.StructUnionName(kind,name) ->
      Type_cocci.StructUnionName(kind,rename_tyname rename name)
  | Type_cocci.TypeName(name) -> ty
  | Type_cocci.MetaType(nm,keep,inherited) ->
      Type_cocci.MetaType(rename nm,keep,inherited)
  | Type_cocci.Unknown -> ty

and rename_tyname rename name =
  match name with
    Type_cocci.MV(nm,kb,inh) -> Type_cocci.MV(rename nm,kb,inh)
  | _ -> name

and rename_list_len rename name =
  match name with
    Ast.MetaLen(nm,flex) -> Ast.MetaLen (rename nm,flex)
  | Ast.CstLen _ -> name
  | Ast.AnyLen -> name

(* ----------------------------------------------------------------------- *)
(* Rename dependencies *)

let rename_rule env rule rebuilder =
  if List.mem rule !Flag.defined_virtual_rules
  then rebuilder rule
  else
    match List.assoc rule env with
      Classify_rules.Bot -> failwith "depend on dead code"
    | Classify_rules.Before | Classify_rules.After -> rebuilder rule
    | Classify_rules.Both ->
	Ast.AndDep(rebuilder ("before_"^rule),rebuilder ("after_"^rule))

let rec rename_deps env = function
    Ast.Dep(rule) -> rename_rule env rule (fun x -> Ast.Dep x)
  | Ast.AntiDep(rule) -> rename_rule env rule (fun x -> Ast.AntiDep x)
  | Ast.EverDep(rule) -> rename_rule env rule (fun x -> Ast.EverDep x)
  | Ast.NeverDep(rule) -> rename_rule env rule (fun x -> Ast.NeverDep x)
  | Ast.AndDep(d1,d2) -> Ast.AndDep(rename_deps env d1,rename_deps env d2)
  | Ast.OrDep(d1,d2) -> Ast.OrDep(rename_deps env d1,rename_deps env d2)
  | dep -> dep

(* ----------------------------------------------------------------------- *)
(* Rename code *)

let rename_listlen r name =
  match name with
    Ast0.MetaListLen nm -> Ast0.MetaListLen (r.VT0.rebuilder_rec_meta_mcode nm)
  | Ast0.CstListLen _ -> name
  | Ast0.AnyListLen -> name

let rename_constraints rename r cstr =
  match cstr with
    Ast0.NoConstraint -> cstr
  | Ast0.NotIdCstrt _ -> cstr
  | Ast0.NotExpCstrt l ->
      Ast0.NotExpCstrt(List.map r.VT0.rebuilder_rec_expression l)
  | Ast0.SubExpCstrt l -> Ast0.SubExpCstrt (List.map rename l)

(* Need cases for type, listlen, constraints, positions *)
let rec do_rename_code rename =
  let metamcode (nm,arity,info,mc,pos,adj) =
    let pos =
      List.map (do_rename_code rename).VT0.rebuilder_rec_anything !pos in
    (rename nm,arity,info,mc,ref pos,adj) in

  let othermcode (a,arity,info,mc,pos,adj) =
    let pos =
      List.map (do_rename_code rename).VT0.rebuilder_rec_anything !pos in
    (a,arity,info,mc,ref pos,adj) in

  let expression r k e =
    let e = k e in
    match Ast0.unwrap e with
      Ast0.MetaErr(name,constraints,u) ->
	let constraints = rename_constraints rename r constraints in
	Ast0.rewrap e (Ast0.MetaErr(name,constraints,u))
    | Ast0.MetaExpr(name,constraints,ty,form,u) ->
	let constraints = rename_constraints rename r constraints in
	let ty = get_option (List.map (rename_type rename)) ty in
	Ast0.rewrap e (Ast0.MetaExpr(name,constraints,ty,form,u))
    | Ast0.MetaExprList(name,lenname,pure) ->
	let lenname = rename_listlen r lenname in
	Ast0.rewrap e (Ast0.MetaExprList(name,lenname,pure))
    | _ -> e in

  let string_fragment r k f =
    let f = k f in
    match Ast0.unwrap f with
      Ast0.MetaFormatList(pct,nm,lenname) ->
	let lenname = rename_listlen r lenname in
	Ast0.rewrap f (Ast0.MetaFormatList(pct,nm,lenname))
    | _ -> f in

  let declaration r k d =
    let d = k d in
    match Ast0.unwrap d with
      Ast0.MetaFieldList(nm,lenname,pure) ->
	let lenname = rename_listlen r lenname in
	Ast0.rewrap d (Ast0.MetaFieldList(nm,lenname,pure))
    | _ -> d in

  let initialiser r k i =
    let i = k i in
    match Ast0.unwrap i with
      Ast0.MetaInitList(nm,lenname,pure) ->
	let lenname = rename_listlen r lenname in
	Ast0.rewrap i (Ast0.MetaInitList(nm,lenname,pure))
    | _ -> i in

  let parameterTypeDef r k p =
    let p = k p in
    match Ast0.unwrap p with
      Ast0.MetaParamList(nm,lenname,pure) ->
	let lenname = rename_listlen r lenname in
	Ast0.rewrap p (Ast0.MetaParamList(nm,lenname,pure))
    | _ -> p in

  let statement r k s =
    let s = k s in
    match Ast0.unwrap s with
      Ast0.MetaStmtList(nm,lenname,pure) ->
	let lenname = rename_listlen r lenname in
	Ast0.rewrap s (Ast0.MetaStmtList(nm,lenname,pure))
    | _ -> s in

  let donothing r k e = k e in

  V0.flat_rebuilder
    metamcode othermcode othermcode othermcode othermcode othermcode
    othermcode othermcode othermcode othermcode othermcode othermcode
    othermcode othermcode
    donothing donothing donothing donothing donothing donothing donothing
    donothing expression donothing donothing donothing initialiser
    parameterTypeDef declaration statement donothing donothing
    string_fragment donothing

let rename_code rename =
  (do_rename_code rename).VT0.rebuilder_rec_top_level

(* ----------------------------------------------------------------------- *)

let drop_annotations =
  let mcode (nm,arity,info,mc,pos,adj) =
    (nm,arity,info,Ast0.context_befaft(),pos,adj) in
  let donothing r k e = k e in
  let res =
    V0.flat_rebuilder
      mcode mcode mcode mcode mcode mcode mcode mcode mcode mcode mcode mcode
      mcode mcode
      donothing donothing donothing donothing donothing donothing donothing
      donothing donothing donothing donothing donothing donothing donothing
      donothing donothing donothing donothing donothing donothing in
  res.VT0.rebuilder_rec_top_level

(* ----------------------------------------------------------------------- *)
