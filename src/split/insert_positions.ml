(*
 * This file is part of Prequel, lincensed under the terms of the GPL v2.
 * See copyright.txt in the Prequel source code for more information.
 * The Prequel source code can be obtained at
 * https://github.com/prequel-pql/prequel
 *)

(* Code for positions: m is for removed only code, j is for a token that
is replaced, c is for a context token that is added to, p is for added
only code. *)

module Ast0 = Ast0_cocci
module Ast = Ast_cocci
module V0 = Visitor_ast0
module VT0 = Visitor_ast0_types

type 'a tokty = Meta of 'a | Concrete of 'a

(* In the following, there are one or two names.  If there is one, there is
only one token.  If there are more than one, then there are two, and the
first is the start and the second is the end. *)
type start_end =
    Ast.meta_name tokty * Ast.meta_name tokty list * Ast.meta_name tokty
type 'start_end start_end_points =
    BeforeEmpty of Ast.meta_name
  | AfterEmpty of Ast.meta_name
  | AroundEmpty of Ast.meta_name * Ast.meta_name
  | Region of 'start_end list
type 'start_end pos =
    Shared of 'start_end start_end_points * 'start_end start_end_points
  | MUnshared of 'start_end list | PUnshared of 'start_end list

let position_counter = ref 0
let positions = ref []
let current_rule = ref ""

(* ---------------------------------------------------------------------- *)
(* Collect all modified tokens *)

let get_modified_tokens ismodified rule =
  let bind = (@) in
  let option_default = [] in
  let donothing r k e = k e in
  let rewrap_mcode (_,arity,info,mcodekind,pos,adj) x =
    let pos = ref !pos in
    (x,arity,info,mcodekind,pos,adj) in
  let mcode mc fn =
    let mcodekind = Ast0.get_mcode_mcodekind mc in 
    let get_tok mc =
      [fn(Ast0.get_mcode_logline mc,Ast0.get_pos_ref mc,mcodekind,None,
	  (function x -> rewrap_mcode mc x))] in
    match mcodekind with
      Ast0.MINUS _ | Ast0.PLUS _ -> if ismodified then get_tok mc else []
    | _ -> if not ismodified then get_tok mc else [] in
  let string_mcode mc =
    let mcodekind = Ast0.get_mcode_mcodekind mc in 
    let get_tok mc =
      [Concrete(Ast0.get_mcode_logline mc,Ast0.get_pos_ref mc,mcodekind,
		Some(Ast0.unwrap_mcode mc),rewrap_mcode mc)] in
    match mcodekind with
      Ast0.MINUS _ | Ast0.PLUS _ -> if ismodified then get_tok mc else []
    | _ -> if not ismodified then get_tok mc else [] in
  let meta x = Meta x in
  (* rules for everything that can match nothing. putting a position
     variable on something that matches nothing is a problem, so just skip
     it *)
  (* also rules for all metavariables that may match multiple toks *)
  let expression r k e =
    match Ast0.unwrap e with
      Ast0.MetaExprList(nm,Ast0.CstListLen n,_) when n > 0 -> mcode nm meta
    | Ast0.MetaExprList _ when not ismodified -> []
    | Ast0.MetaExprList(nm,_,_) | Ast0.MetaExpr(nm,_,_,_,_)
    | Ast0.MetaErr(nm,_,_) -> mcode nm meta
    | _ -> k e in
  let string_fragment r k f =
    match Ast0.unwrap f with
      Ast0.MetaFormatList(_,nm,_) -> mcode nm meta
    | _ -> k f in
  let ty r k t =
    match Ast0.unwrap t with
      Ast0.MetaType(nm,_) -> mcode nm meta
    | _ -> k t in
  let declaration r k d =
    match Ast0.unwrap d with
      Ast0.MetaFieldList(nm,Ast0.CstListLen n,_) when n > 0 -> mcode nm meta
    | Ast0.MetaFieldList _ when not ismodified -> []
    | Ast0.MetaFieldList(nm,_,_) | Ast0.MetaDecl(nm,_)
    | Ast0.MetaField(nm,_) -> mcode nm meta
    | _ -> k d in
  let initialiser r k i =
    match Ast0.unwrap i with
      Ast0.MetaInitList(nm,Ast0.CstListLen n,_) when n > 0 -> mcode nm meta
    | Ast0.MetaInitList _ when not ismodified -> []
    | Ast0.MetaInitList(nm,_,_) | Ast0.MetaInit(nm,_) -> mcode nm meta
    | _ -> k i in
  let parameter r k p =
    match Ast0.unwrap p with
      Ast0.MetaParamList(nm,Ast0.CstListLen n,_) when n > 0 -> mcode nm meta
    | Ast0.MetaParamList _ when not ismodified -> []
    | Ast0.MetaParamList(nm,_,_) | Ast0.MetaParam(nm,_) -> mcode nm meta
    | _ -> k p in
  let statement r k s =
    match Ast0.unwrap s with
      Ast0.MetaStmtList(nm,Ast0.CstListLen n,_) when n > 0 -> mcode nm meta
    | Ast0.MetaStmtList _ when not ismodified -> []
    | Ast0.MetaStmtList(nm,_,_) | Ast0.MetaStmt(nm,_) -> mcode nm meta
    | _ -> k s in
  let mcode mc = mcode mc (fun x -> Concrete x) in
  let res =
    V0.flat_combiner bind option_default
      mcode string_mcode mcode mcode mcode mcode mcode mcode mcode mcode
      mcode mcode mcode mcode
      donothing donothing donothing donothing donothing donothing donothing
      donothing expression donothing donothing ty initialiser
      parameter declaration statement donothing donothing string_fragment
      donothing in
  match rule with
    [rule] -> res.VT0.combiner_rec_top_level rule
  | [] -> []
  | l ->
      failwith
	("only one minirule per rule supported: "^
	 (string_of_int(List.length l)))

let get_modified_tokens_for_rule minus plus =
  let get_log_line = function
      Meta(ll,_,_,_,_) -> ll
    | Concrete(ll,_,_,_,_) -> ll in
  let rec merge l1 l2 =
    match (l1,l2) with
      ([],_) -> l2
    | (_,[]) -> l1
    | (a::l1,b::l2) ->
	let ll1 = get_log_line a in
	let ll2 = get_log_line b in
	if ll1 = ll2
	then a :: b :: merge l1 l2
	else if ll1 < ll2
	then a :: merge l1 (b::l2)
	else b :: merge (a::l1) l2 in
  merge (get_modified_tokens true minus) (get_modified_tokens true plus)

let get_tokens_for_context l =
  List.map
    (function
	Meta(ll,pcell,mc,str,reb) -> (ll,pcell,mc,str,reb)
      | Concrete(ll,pcell,mc,str,reb) -> (ll,pcell,mc,str,reb))
    (get_modified_tokens false l)

let chunkify_tokens tokens for_constants =
  let get_log_line = function
      Meta(ll,_,_,_,_) -> ll
    | Concrete(ll,_,_,_,_) -> ll in
  let rec loop = function
      [] -> []
    | tok :: rest ->
	let ll = get_log_line tok in
	(match loop rest with
	  ((tok1::irest)::rest) as allrest ->
	    let ll1 = get_log_line tok1 in
	    if ll == ll1 || ll+1 = ll1
	    then (tok::tok1::irest)::rest
	    else [tok]::allrest
	| [] -> [[tok]]
	| _ -> failwith "not possible") in
  let rec inner = function
      [] -> failwith "not possible"
    | [Meta(ll,pcell,mc,str,reb)] ->
	[Meta[(ll,pcell,mc,str,reb)]]
    | [Concrete(ll,pcell,mc,str,reb)] ->
	[Concrete[(ll,pcell,mc,str,reb)]]
    | Meta(ll,pcell,mc,str,reb)::rest ->
	Meta[(ll,pcell,mc,str,reb)]::inner rest
    | Concrete(ll,pcell,mc,str,reb)::rest ->
	match inner rest with
	  Concrete l :: rest -> Concrete ((ll,pcell,mc,str,reb)::l) :: rest
	| rest -> Concrete[(ll,pcell,mc,str,reb)]::rest in
  if !Flag_split.uniform || for_constants
  then
    List.map
      (function l ->
	[Concrete (List.map (function Meta i | Concrete i -> i) l)])
      (loop tokens)
  else List.map inner (loop tokens)

type repl = ONLY | REPL

let build_pos_constraints key table var for_constants =
  if for_constants
  then []
  else
    match var with
      Concrete (r,n) ->
	[Ast.PosScript
	    (Printf.sprintf "%s_%s" r n,"ocaml",[],
	     Printf.sprintf "check_in_a_hunk(\"%s\",%s,%s)" key table n)]
    | Meta (r,n) ->
	[Ast.PosScript
	    (Printf.sprintf "%s_%s" r n,"ocaml",[],
	     Printf.sprintf "check_intersects_a_hunk(\"%s\",%s,%s)" key table
	       n)]

let build_repl_constraints key table var for_constants =
  if for_constants
  then []
  else
    match var with
      Concrete (r,n) ->
	[Ast.PosScript
	    (Printf.sprintf "%s_%s" r n,"ocaml",[],
	     Printf.sprintf "check_in_a_replacement_hunk(\"%s\",%s,%s)"
	       key table n)]
    | Meta (r,n) ->
	[Ast.PosScript
	    (Printf.sprintf "%s_%s" r n,"ocaml",[],
	     Printf.sprintf "check_intersects_a_replacement_hunk(\"%s\",%s,%s)"
	       key table n)]

let build_pos_decl key table for_constants constraints x =
  let unwrap = function Meta(r,n) | Concrete(r,n) -> (r,n) in
  match constraints with
  | ONLY ->
      Ast.MetaPosDecl(unwrap x,
		      build_pos_constraints key table x for_constants)
  | REPL->
      Ast.MetaPosDecl(unwrap x,
		      build_repl_constraints key table x for_constants)

let build_unc_pos_decl key table for_constants x =
  Ast.MetaPosDecl(x,[])

(* probably not super efficient... start from the beginning each time *)
let get_just_before model l =
  let (startll,_,_,_,_) = List.hd model in
  let rec loop prev res = function
      [] -> res
    | ((ll1,_,_,str,_) as a)::rest ->
	if ll1 < startll
	then
	  if ll1 >= prev
	  then loop ll1 (if str = Some "..." then None else Some a) rest
	  else loop prev res rest
	else res in
  loop (-1) None l

let get_just_after model l =
  let (endll,_,_,_,_) = List.hd (List.rev model) in
  let rec loop = function
      [] -> None
    | ((ll1,_,_,str,_) as a)::rest ->
	if ll1 > endll
	then if str = Some "..." then None else Some a
	else loop rest in
  loop l

let add_positions cat chunks current_rule for_constants minus plus =
  Common.mapi
    (fun i chunk ->
      let (msubchunks,psubchunks) =
	List.fold_left
	  (function (msubchunks,psubchunks) ->
	    function
		Meta ([(_,_,Ast0.PLUS _,_,_)] as l) ->
		  if !Flag_split.backport
		  then (Meta l :: msubchunks,psubchunks)
		  else (msubchunks,Meta l :: psubchunks)
	      | Meta ([(_,_,Ast0.MINUS _,_,_)] as l) ->
		  if !Flag_split.backport
		  then (msubchunks,Meta l :: psubchunks)
		  else (Meta l :: msubchunks,psubchunks)
	      | Meta _ -> failwith "not possible"
	      | Concrete l ->
		  let (mtoks,ptoks) =
		    if !Flag_split.backport
		    then
		      List.partition
			(function (_,_,Ast0.PLUS _,_,_) -> true | _ -> false)
			l
		    else
		      List.partition
			(function (_,_,Ast0.MINUS _,_,_) -> true | _ -> false)
			l in
		  match (mtoks,ptoks) with
		    ([],[]) -> failwith "not possible"
		  | (m,[]) -> (Concrete m :: msubchunks,psubchunks)
		  | ([],p) -> (msubchunks,Concrete p :: psubchunks)
		  | (m,p) ->
		      (Concrete m :: msubchunks,Concrete p :: psubchunks))
	  ([],[]) chunk in
      let check_start_dots fn = function
	  Concrete l ->
	    (match fn l with
	      (_,_,_,Some "...",_)::_ -> failwith "start/end dots not allowed"
	    | _ -> ())
	| _ -> () in
      List.iter (check_start_dots (fun x -> x)) msubchunks;
      List.iter (check_start_dots List.rev) msubchunks;
      List.iter (check_start_dots (fun x -> x)) psubchunks;
      List.iter (check_start_dots List.rev) psubchunks;
      let mtokens = msubchunks in
      let ptokens = psubchunks in
      let upd_start char n (_,pos,_,_,reb) key_table subchunk =
	let name =
	  (current_rule,Printf.sprintf "%s__%d__%d__%d" char i subchunk n) in
	let posvar = Ast0.MetaPosTag(Ast0.MetaPos(reb name,[],Ast.PER)) in
	pos := posvar :: !pos;
	name in
      let rec first_non_comma for_constants = function
	(* can't put a position on a comma, because it might not
	   actually match anything, if the comma is part of an
	   argument or parameter list and we have the first or
	   last argument or parameter *)
	  (* also a problem for #include, because SmPL lexer lexes
	     the include with the subsequent file indicator - not
	     good for word diff, ok for line diff *)
	  [] -> failwith "bad list"
	| [x] -> x
	| ((_,_,_,str,_) as x)::xs ->
	    if (not for_constants && str = Some ",") || str = Some "#include"
	    then first_non_comma for_constants xs
	    else x in
      let upd_start_end char l key table subchunk =
	let toks = function
	  [start] ->
	    let key_table = Some(key,table) in
	    let start = upd_start char 1 start key_table subchunk in
	    (start,[],start)
	| l ->
	    let start = first_non_comma for_constants l in
	    let finish = first_non_comma for_constants (List.rev l) in
	    let middle =
	      if not for_constants
	      then []
	      else
		match List.tl l with
		  [] -> []
		| r -> List.rev(List.tl(List.rev r)) in
	    let key_table = Some(key,table) in
	    (upd_start char 1 start key_table subchunk,
	     Common.mapi (fun i x -> upd_start char (i+3) x None subchunk)
	       middle,
	     upd_start char 2 finish key_table subchunk) in
	match l with
	  Concrete t ->
	    let (l,m,r) = toks t in
	    (Concrete l, List.map (fun x -> Concrete x) m, Concrete r)
	| Meta t ->
	    let (l,m,r) = toks t in
	    (Meta l, List.map (fun x -> Meta x) m, Meta r) in
      let upd_all_start_end char l key table =
	List.mapi (fun i l -> upd_start_end char l key table i) l in
      let get_context model context =
	let model = (* don't need to differentiate Concrete and Meta *)
	  List.concat (* want endpoints *)
	    (List.map (function Concrete l | Meta l -> l) model) in
	let bef = get_just_before model context in
	let aft = get_just_after model context in
	match (bef,aft) with
	  (Some bef,Some aft) ->
	    Some(AroundEmpty(upd_start "c" 0 bef None 0,
			     upd_start "c" 1 aft None 0))
	| (None,Some aft) ->
	    Some(AfterEmpty(upd_start "c" 0 aft None 0))
	| (Some bef,None) ->
	    Some(BeforeEmpty(upd_start "c" 0 bef None 0))
	| (None,None) -> None in
      (match (mtokens,ptokens) with
	([],[]) -> failwith "not possible"
      | (_,[]) ->
	  let default _ =
	    MUnshared(upd_all_start_end "m" mtokens "M" "minus_hunks") in
	  if cat = Classify_rules.Both && not for_constants
	  then
	    (match get_context mtokens plus with
	      Some c ->
		let r = upd_all_start_end "m" mtokens "M" "minus_hunks" in
		Shared(Region r,c)
	    | None -> default())
	  else default()
      | ([],_) ->
	  let default _ =
	    PUnshared(upd_all_start_end "p" ptokens "P" "plus_hunks") in
	  if cat = Classify_rules.Both && not for_constants
	  then
	    (match get_context ptokens minus with
	      Some c ->
		let r = upd_all_start_end "p" ptokens "P" "plus_hunks" in
		Shared(c,Region r)
	    | None -> default())
	  else default()
      | _ ->
	  let r1 = upd_all_start_end "jm" mtokens "M" "minus_hunks" in
	  let r2 = upd_all_start_end "jp" ptokens "P" "plus_hunks" in
	  Shared(Region r1,Region r2)))
    chunks

let insert_positions rules for_constants =
  List.map
    (function
      (cat,Ast0.CocciRule((minus,mmetavars,infos),(plus,pmetavars),
			  imetavars,names,ty)) ->
	let (iso, dropiso, deps, name, exists) = infos in
	let tokens = get_modified_tokens_for_rule minus plus in
	let chunks = chunkify_tokens tokens for_constants in
	let minus_tokens =
	  if cat = Classify_rules.Both && not for_constants
	  then get_tokens_for_context minus
	  else [] (* not used *) in
	let plus_tokens =
	  if cat = Classify_rules.Both && not for_constants
	  then get_tokens_for_context plus
	  else [] (* not used *) in
	let positions_for_rule =
	  add_positions cat chunks name for_constants
	    minus_tokens plus_tokens in
	let (in_hunk_minus_only_positions,in_hunk_minus_repl_positions) =
	  List.fold_left
	    (function (only,repl) ->
	      function
		  Shared (Region r,Region _) ->
		    (only,
		     List.fold_left
		       (fun repl (m1,m2,m3) ->
			 if m1 = m3 then m1::m2@repl else m1::m2@m3::repl)
		       repl r)
		| MUnshared r | Shared (Region r,_) ->
		    (List.fold_left
		       (fun only (m1,m2,m3) ->
			 if m1 = m3 then m1::m2@only else m1::m2@m3::only)
		       only r,repl)
		| _ -> (only,repl))
	    ([],[]) positions_for_rule in
	let unconstrained_minus_positions =
	  List.fold_left
	    (function prev ->
	      function
		  Shared(BeforeEmpty m1,_) | Shared(AfterEmpty m1,_) ->
		    m1::prev
		| Shared(AroundEmpty(m1,m2),_) -> m1::m2::prev
		| _ -> prev)
	    [] positions_for_rule in
	let (in_hunk_plus_only_positions,in_hunk_plus_repl_positions) =
	  List.fold_left
	    (function (only,repl) ->
	      function
		  Shared(Region _,Region r) ->
		    (only,
		     List.fold_left
		       (fun repl (p1,p2,p3) ->
			 if p1 = p3 then p1::p2@repl else p1::p2@p3::repl)
		       repl r)
		| PUnshared r | Shared(_,Region r) ->
		    (List.fold_left
		       (fun only (p1,p2,p3) ->
			 if p1 = p3 then p1::p2@only else p1::p2@p3::only)
		       only r,repl)
		| _ -> (only,repl))
	    ([],[]) positions_for_rule in
	let unconstrained_plus_positions =
	  List.fold_left
	    (function prev ->
	      function
		  Shared(_,BeforeEmpty p1) | Shared(_,AfterEmpty p1) ->
		    p1::prev
		| Shared(_,AroundEmpty(m1,m2)) -> m1::m2::prev
		| _ -> prev)
	    [] positions_for_rule in
	let mmetavars =
	  (List.map (build_pos_decl "M" "minus_hunks" for_constants ONLY)
	     in_hunk_minus_only_positions) @
	  (List.map (build_pos_decl "M" "minus_hunks" for_constants REPL)
	     in_hunk_minus_repl_positions) @
	  (List.map (build_unc_pos_decl "M" "minus_hunks" for_constants)
	     unconstrained_minus_positions) @
	  mmetavars in
	let pmetavars =
	  (List.map (build_pos_decl "P" "plus_hunks" for_constants ONLY)
	     in_hunk_plus_only_positions) @
	  (List.map (build_pos_decl "P" "plus_hunks" for_constants REPL)
	     in_hunk_plus_repl_positions) @
	  (List.map (build_unc_pos_decl "P" "plus_hunks" for_constants)
	     unconstrained_plus_positions) @
	  pmetavars in
	(positions_for_rule,
	 (cat,Ast0.CocciRule((minus,mmetavars,infos),(plus,pmetavars),
			     imetavars,names,ty)))
      | (cat,Ast0.ScriptRule(a,b,c,d,fv,e)) ->
	  ([],(cat,Ast0.ScriptRule(a,b,c,d,fv,e)))
      | (cat,Ast0.InitialScriptRule(a,b,c,d,e)) ->
	  ([],(cat,Ast0.InitialScriptRule(a,b,c,d,e)))
      | (cat,Ast0.FinalScriptRule(a,b,c,d,e)) ->
	  ([],(cat,Ast0.FinalScriptRule(a,b,c,d,e))))
    rules
