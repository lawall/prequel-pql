(*
 * This file is part of Prequel, lincensed under the terms of the GPL v2.
 * See copyright.txt in the Prequel source code for more information.
 * The Prequel source code can be obtained at
 * https://github.com/prequel-pql/prequel
 *)

val process :
    (Insert_positions.start_end Insert_positions.pos list *
       (Classify_rules.category * Ast0_cocci.parsed_rule)) list ->
	 (Insert_positions.start_end Insert_positions.pos list *
	    (Classify_rules.category * Ast0_cocci.parsed_rule)) list
