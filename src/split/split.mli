(*
 * This file is part of Prequel, lincensed under the terms of the GPL v2.
 * See copyright.txt in the Prequel source code for more information.
 * The Prequel source code can be obtained at
 * https://github.com/prequel-pql/prequel
 *)

val split_for_code : string -> unit
val split_for_constants : string -> unit

val get_constants :
    string ->
      (int *
	 ((Get_constants2.cases * Get_constants2.combine) *
	    (Get_constants2.cases * Get_constants2.combine))) (*idutils*) list
