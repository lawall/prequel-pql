(*
 * This file is part of Prequel, lincensed under the terms of the GPL v2.
 * See copyright.txt in the Prequel source code for more information.
 * The Prequel source code can be obtained at
 * https://github.com/prequel-pql/prequel
 *)

(* For each rule, collect the other rules that it relies on.  And then find
the rules that no one depends on.  At this point, the rules should be
linearized - no more point to combine the Both rules. *)

module Ast = Ast_cocci
module TC = Type_cocci

let rec analyze_depends_on_dependencies = function
    Ast.Dep r -> ([],[r])
  | Ast.AntiDep r -> ([r],[])
  | Ast.EverDep r -> (* diff than dep? *)
      failwith "not supporting ever dependencies for the moment"
  | Ast.NeverDep r -> (* diff than antidep? *)
      failwith "not supporting never dependencies for the moment"
  | Ast.AndDep(d1,d2) ->
      let (neg1,pos1) = analyze_depends_on_dependencies d1 in
      let (neg2,pos2) = analyze_depends_on_dependencies d2 in
      (Common.union_set neg1 neg2, Common.union_set pos1 pos2)
  | Ast.OrDep(d1,d2) ->
      failwith "not supporting or dependencies for the moment"
  | Ast.FileIn _  | Ast.NotFileIn _ | Ast.NoDep | Ast.FailDep -> ([],[])

(* ---------------------------------------------------------------------- *)

let get_pos_neg_metanames = function
    Ast.MetaIdDecl(flex,nm,cstr) ->
      let (neg,pos) =
	match cstr with
	  Ast.IdPosIdSet(_,names) -> ([], names)
	| Ast.IdNegIdSet(_,names) -> (names, [])
	| _ -> ([],[]) in
      (neg,nm::pos)
  | Ast.MetaPosDecl(nm,constraints) ->
      List.fold_left
	(function (neg,pos) ->
	  function
	      Ast.PosNegSet l -> (Common.union_set l neg,pos)
	    | Ast.PosScript(_,_,params,_) ->
		let l = List.map fst params in
		(neg,Common.union_set l pos))
	([],[nm]) constraints
  | decl -> ([], Ast.get_all_meta_names decl) (* all positives *)

let analyze_metavars_dependencies rulename mv =
  let get_rules =
    List.fold_left
      (fun prev (r,n) ->
	if r = rulename || r = "virtual"
	then prev
	else if not (List.mem r prev)
	then r :: prev
	else prev)
      [] in
  let (neg,pos) = get_pos_neg_metanames mv in
  (get_rules neg,get_rules pos)

(* ---------------------------------------------------------------------- *)

let normalize nm rule =
  (* if something depends on before, then consider that it also depends
     on after.  After always depends on before, so nothing additional needed
     in that case. *)
   match Str.split (Str.regexp "_") rule with
     "before"::r ->
       let other = "after_"^(String.concat "_" r) in
       if nm = other
       then [rule]
       else [rule;other]
   | _ -> [rule]

let normalize_all nm prev l =
  List.fold_left (fun prev par -> Common.union_set (normalize nm par) prev)
    prev l

let rec collect_refs rules =
  let union = Common.union_set in
  let get_parents_and_pos_ancestors nm deps metavars env =
    let (neg,pos) = analyze_depends_on_dependencies deps in
    let metadeps = List.map (analyze_metavars_dependencies nm) metavars in
    let parents =
      List.fold_left
	(fun prev (negmeta,posmeta) -> union negmeta (union posmeta prev))
	(union pos neg) metadeps in
    let parents = normalize_all nm [] parents in
    let posparents =
      List.fold_left
	(fun prev (_negmeta,posmeta) -> normalize_all nm prev posmeta)
	pos metadeps in
    let posancestors =
      List.fold_left union posparents
	(List.map (* fails for virt rules *)
	   (fun rule -> try snd(List.assoc rule env) with Not_found -> [])
	   posparents) in
    (parents,posancestors) in
  let rec loop env = function
      [] -> env
    | (metavars,names,(Ast.CocciRule (nm, (deps, drops, exists), x, _, _)))
      :: rest ->
	let entry = get_parents_and_pos_ancestors nm deps metavars env in
	loop ((nm,entry)::env) rest
    | (_,_,(Ast.ScriptRule (nm, lang, deps, mvs, scriptvars, code))) :: rest ->
	let metavars = List.map (function (_,_,mv,_) -> mv) mvs in
	let entry = get_parents_and_pos_ancestors nm deps metavars env in
	loop ((nm,entry)::env) rest
    | _::rest -> loop env rest in
  loop [] rules

(* Env maps a rule to the ones it depends on.  The reverse environment maps
a rule to the rules that depend on it.  Leaves are rules that no one
depends on. *)
let find_leaves env =
  let tbl = Hashtbl.create 101 in
  let hashadd k v =
    if not (k = v)
    then
      begin
	let cell =
	  try Hashtbl.find tbl k
	  with Not_found ->
	    failwith (Printf.sprintf "find_leaves: %s not in env" k) in
	if not (List.mem v !cell) then cell := v :: !cell
      end in
  List.iter (function (rule,_) -> Hashtbl.add tbl rule (ref [])) env;
  List.iter
    (function (rule,dependlist) ->
      List.iter (function dep -> hashadd dep rule) (fst dependlist))
    env;
  Hashtbl.fold (fun k v rest -> if !v = [] then k :: rest else rest) tbl []

(* ---------------------------------------------------------------------- *)

let collect_rule_refs (rules_with_changes,rules) for_constants =
  let env = collect_refs (List.map List.hd rules) in (* tl is script, if any *)
  let leaf_rules = find_leaves env in
  let checked_rules =
    (* collect all coccis; either checked directly by script in the tail,
       or before rule is checked by script on after rule; assume bot rules
       are gone *)
    List.fold_left
      (function prev ->
	function
	    ((metavars,names,
	      (Ast.CocciRule(nm,(deps,drops,exists),x,_,_)))::_) ->
	      nm::prev
	  | _ -> prev)
      [] rules in
  let init_rule =
    if for_constants
    then []
    else
      let meta_name = ("virtual","hunk_file") in
      let hunk_file =
	((Some "hunk_file",None),meta_name,
	 Ast.MetaIdDecl(Ast.NOFLEX, meta_name, Ast.IdNoConstraint),
	 Ast.NoMVInit) in
      [([],[],
	Ast.InitialScriptRule
	  ("__init__","ocaml",Ast.NoDep,[hunk_file],
	   if !Flag.difftype = Flag.LINE
	   then Library.code
	   else Library_word.code))] in
  init_rule @
  (List.concat
    (List.map
       (function
	   (((metavars,names,
	      ((Ast.CocciRule (nm, _, _, _, _)) |
	      Ast.ScriptRule (nm,_,_,_,_,_)))::checkrule)
	      as rule)
	   when List.mem nm leaf_rules ->
		let parents = nm :: snd(List.assoc nm env) in
		let parents =
		  List.filter (function nm -> List.mem nm checked_rules)
		    parents in
		let script_name = Printf.sprintf "commit_%s" nm in
		let script_language = "ocaml" in
		let script_deps =
		  if for_constants
		  then Ast.Dep nm
		  else Ast.NoDep in
		let script_inh_vars =
		  if for_constants
		  then []
		  else
		    List.concat
		      (List.map
			 (function parent ->
			   let check_rule =
			     Add_checks.check_script_name parent in
			   if List.mem check_rule rules_with_changes
			   then
			     let script_meta_name =
			       (Some (Printf.sprintf "hunks_for_%s" parent),
				None) in
			     let meta_name = (check_rule,"found_hunks") in
			     let metavar =
			       Ast.MetaScriptDecl(ref None, meta_name) in
			     [(script_meta_name, meta_name, metavar,
			       Ast.NoMVInit)]
			   else [])
			 parents) in
		let script_new_vars = [] in
		let script_code _ =
		  String.concat "\n"
		    ((Printf.sprintf "start_record();") ::
		     (List.map
			(function (script_meta_name, _, _, _) ->
			  let nm =
			    match fst script_meta_name with
			      Some nm -> nm
			    | None -> failwith "not possible" in
			  Printf.sprintf "Printf.printf \"%%s\\n\" %s;" nm)
			script_inh_vars) @
		     [Printf.sprintf "end_record()\n"]) in
		let script_code =
		  if for_constants then "()\n" else script_code() in
		rule@
		  [([],[],
		   Ast.ScriptRule(script_name, script_language, script_deps,
				  script_inh_vars, script_new_vars,
				  script_code))]
	 | rule -> rule)
       rules))

