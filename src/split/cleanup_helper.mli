(*
 * This file is part of Prequel, lincensed under the terms of the GPL v2.
 * See copyright.txt in the Prequel source code for more information.
 * The Prequel source code can be obtained at
 * https://github.com/prequel-pql/prequel
 *)

val rename_metavar :
    (Ast_cocci.meta_name -> Ast_cocci.meta_name) ->
      Ast_cocci.metavar -> Ast_cocci.metavar

val rename_deps :
    (string * Classify_rules.category) list ->
      Ast_cocci.dependency -> Ast_cocci.dependency

val rename_code :
    (Ast_cocci.meta_name -> Ast_cocci.meta_name) ->
      Ast0_cocci.top_level -> Ast0_cocci.top_level

val drop_annotations : Ast0_cocci.top_level -> Ast0_cocci.top_level
