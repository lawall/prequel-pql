(*
 * This file is part of Prequel, lincensed under the terms of the GPL v2.
 * See copyright.txt in the Prequel source code for more information.
 * The Prequel source code can be obtained at
 * https://github.com/prequel-pql/prequel
 *)

type 'a tokty = Meta of 'a | Concrete of 'a

type start_end =
    Ast_cocci.meta_name tokty * Ast_cocci.meta_name tokty list *
      Ast_cocci.meta_name tokty
type 'start_end start_end_points =
    BeforeEmpty of Ast_cocci.meta_name
  | AfterEmpty of Ast_cocci.meta_name
  | AroundEmpty of Ast_cocci.meta_name * Ast_cocci.meta_name
  | Region of 'start_end list
type 'start_end pos =
    Shared of 'start_end start_end_points * 'start_end start_end_points
  | MUnshared of 'start_end list | PUnshared of 'start_end list

val insert_positions :
    (Classify_rules.category * Ast0_cocci.parsed_rule) list -> bool ->
      (start_end pos list *
	 (Classify_rules.category * Ast0_cocci.parsed_rule)) list
