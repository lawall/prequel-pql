(*
 * This file is part of Prequel, lincensed under the terms of the GPL v2.
 * See copyright.txt in the Prequel source code for more information.
 * The Prequel source code can be obtained at
 * https://github.com/prequel-pql/prequel
 *)

val free : (Ast_cocci.meta_name -> bool) -> Ast0_cocci.top_level list ->
  Ast_cocci.meta_name list
