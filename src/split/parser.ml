(*
 * This file is part of Prequel, lincensed under the terms of the GPL v2.
 * See copyright.txt in the Prequel source code for more information.
 * The Prequel source code can be obtained at
 * https://github.com/prequel-pql/prequel
 *)

module Ast0 = Ast0_cocci
module Ast = Ast_cocci

let check_for_failure minus plus =
  match (minus,plus) with
    (m::_,p::_) -> (* not safe for multiple minirules *)
      (match (Ast0.unwrap m,Ast0.unwrap p) with
	  (Ast0.FAILED e1, Ast0.FAILED e2) ->
	    Printf.eprintf "Double failure:\n%s\n" (Dumper.dump e1);
	    raise e2
      | _ -> ())
  | _ -> ()

let rec notfail failures dep =
  match dep with
    Ast.Dep d | Ast.EverDep d -> if List.mem d failures then None else Some dep
  | Ast.AntiDep _ | Ast.NeverDep _ -> Some dep
  | Ast.AndDep(e1,e2) ->
      (match (notfail failures e1,notfail failures e2) with
	(Some e1,Some e2) -> Some dep
      | _ -> None)
  | Ast.OrDep(e1,e2) ->
      (match (notfail failures e1,notfail failures e2) with
	(Some e1,Some e2) -> Some dep
      | (Some e1,_) -> Some e1
      | (_,Some e2) -> Some e2
      | _ -> None)
  | Ast.FileIn _ | Ast.NotFileIn _ -> Some dep
  | Ast.NoDep -> Some dep
  | Ast.FailDep -> None

exception Backport

let process file =
  Parse_aux.contains_string_constant := false;
  Adjacency.reinit();
  let implicit_rule_names = ["before";"after"] in
  Flag.defined_virtual_rules :=
    implicit_rule_names @ !Flag.defined_virtual_rules;
  let (_iso_files, rules, virt, _metas) =
    Parse_cocci.parse file implicit_rule_names in
  (List.rev
     (snd
    (List.fold_left
       (function (failures,prev) ->
	 function
             Ast0.ScriptRule (nm,b,dep,fv,d,e) ->
	       let fvparents =
		 List.fold_left
		   (fun prev (_,(r,_),_,init) ->
		     (* The case of a failed rule + and initializer
			might end up causing problems - deal with it
			when the need arises... *)
		     if List.mem r failures && init = Ast.NoMVInit
		     then false
		     else prev)
		   true fv in
	       if fvparents
	       then
		 match notfail failures dep with
		   Some dep ->
		     (failures,Ast0.ScriptRule (nm,b,dep,fv,d,e) :: prev)
		 | None -> (nm::failures,prev)
	       else (nm::failures,prev)
	   | Ast0.InitialScriptRule(nm,b,dep,d,e) ->
	       (match notfail failures dep with
		 Some dep ->
		   (failures, Ast0.InitialScriptRule (nm,b,dep,d,e) :: prev)
	       | None -> (nm::failures,prev))
	   | Ast0.FinalScriptRule (nm,b,dep,d,e) ->
	       (match notfail failures dep with
		 Some dep ->
		   (failures, Ast0.FinalScriptRule (nm,b,dep,d,e) :: prev)
	       | None -> (nm::failures,prev))
	   | Ast0.CocciRule
	       ((minus, metavarsm,
		 (iso, dropiso, dependencies, rule_name, exists)),
		(plus, metavars),imetavars,names,ruletype) ->
		  (match notfail failures dependencies with
		    Some dependencies ->
		      let (minus,plus) =
			if !Flag_split.backport
			then ([Ast0.wrap (Ast0.FAILED Backport)],
			      minus)
			else (minus,plus) in
		      let minus = Test_exps.process minus in
		      let plus = Test_exps.process plus in
		      let minus = Compute_lines.compute_lines false minus in
		      let plus = Compute_lines.compute_lines false plus in
		      let minus = Arity.minus_arity minus in
		      let plus = Arity.minus_arity plus in
		      (* only actually useful for get_constants, which only
			 has minus code... *)
		      let minus = Adjacency.compute_adjacency minus in
		      let plus = Adjacency.compute_adjacency plus in
		      Type_infer.type_infer minus;
		      Type_infer.type_infer plus;
		      check_for_failure minus plus;
		      (failures,
		       Ast0.CocciRule
			 ((minus, metavarsm,
			   (iso, dropiso, dependencies, rule_name, exists)),
			  (plus, metavars),imetavars,names,ruletype) :: prev)
		  | None -> (rule_name::failures,prev)))
       ([],[]) rules)),
   virt)
