(*
 * This file is part of Prequel, lincensed under the terms of the GPL v2.
 * See copyright.txt in the Prequel source code for more information.
 * The Prequel source code can be obtained at
 * https://github.com/prequel-pql/prequel
 *)

(* Idea:
Copy the original semantic patch, remove the minus lines and keep the
plus lines, but replace the + with -.
We can use the result of this directly for the after case. *)

let only_whitespace s =
  let blank_string = Str.regexp "^[ '\t']*$" in
  Str.string_match blank_string s 0

let drop_front s = String.sub s 1 ((String.length s)-1)

type line = Minus of string | Plus of string | Context of string

let get_metas ls =
  let rec loop = function
      [] -> ([],[])
    | l::rest ->
	match Str.split_delim (Str.regexp "[ \t]*@@[ \t]*") l with
	  ["";""] -> (["@@"],rest)
	| _ ->
	    let (front,rest) = loop rest in (l::front,rest) in
  loop ls

let split_at_at l =
  let rec loop = function
      [] -> ([],[])
    | l::ls ->
	match Str.split_delim (Str.regexp "[ \t]*@[ \t]*") l with
	  ""::infos::aft::_ ->
	    let (metas,ls) = get_metas ls in
	    let (front,rest) = loop ls in
	    ([],(l::metas,front)::rest)
	| _ ->
	    let (front,rest) = loop ls in
	    Printf.printf "front %s rest %s\n"
	      (Dumper.dump front) (Dumper.dump rest);
	    match Str.split_delim (Str.regexp "[ \t]*virtual[ \t]*") l with
	      ""::_ ->
		if List.for_all (fun x -> x = Minus "") front
		then ([],([l],front)::rest)
		else failwith ("bad virtual "^(Dumper.dump front))
	    | _ ->
		let l =
		  if l = ""
		  then Minus ""
		  else
		    (match String.get l 0 with
		      '+' -> Plus l
		    | '-' -> Minus l
		    | _ ->
			if only_whitespace l then Minus "" else Context l) in
		(l::front,rest) in
  snd(loop l)

let splitpatch file plus_file =
  let i = Common.cmd_to_list(Printf.sprintf "cat %s" file) in
  let o = open_out plus_file in
  let rules = split_at_at i in
  List.iter
    (function (header,body) ->
      let keep_rule =
	body = [] ||
	List.for_all (function Minus s -> true | _ -> false) body in
      if keep_rule
      then
	begin
	  List.iter (function x -> Printf.fprintf o "%s\n" x) header;
	  List.iter
	    (function
		Plus s -> Printf.fprintf o "-%s\n" (drop_front s)
	      | Context s -> Printf.fprintf o "%s\n" s
	      | _ -> Printf.fprintf o "\n")
	    body
	end)
    rules;
  close_out o
