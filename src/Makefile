# This file is part of Prequel, lincensed under the terms of the GPL v2.
# See copyright.txt in the Prequel source code for more information.
# The Prequel source code can be obtained at
# https://github.com/prequel-pql/prequel

ifneq ($(MAKECMDGOALS),distclean)
include  Makefile.config
-include Makefile.local
endif

SOURCES = flag_cocci.ml main.ml
EXEC=prequel
OPTEXEC=$(EXEC).opt

OBJS = $(SOURCES:.ml=.cmo)
OPTOBJS = $(SOURCES:.ml=.cmx)
LOCALINCLUDESET = commons commons/ocamlextra globals parsing_c \
parsing_cocci split select
INCLUDESET = $(PARMAP) $(MENHIRLIB) $(LOCALINCLUDESET)
LOCALINCLUDES=$(LOCALINCLUDESET:%=-I %)
INCLUDES=$(INCLUDESET:%=-I %)

FLAGSLIBS    = -cclib -lparmap_stubs
OPTFLAGSLIBS = -cclib -lparmap_stubs

SYSLIBS=str.cma unix.cma bigarray.cma nums.cma $(PARMAP)/parmap.cma $(MENHIRLIB)/menhirLib.cma
OPTSYSLIBS=$(SYSLIBS:.cma=.cmxa)

LIBS=commons/commons.cma globals/globals.cma parsing_c/parser_c.cma \
     parsing_cocci/cocci_parser.cma split/split.cma select/select.cma
OPTLIBS=$(LIBS:.cma=.cmxa)

SUBDIRS=commons globals parsing_c parsing_cocci split select

all: rec $(EXEC)
opt: rec.opt $(OPTEXEC)

native: rec.opt $(OPTEXEC)

rec:
	set -e; for i in $(SUBDIRS); do $(MAKE) -C $$i all; done 

rec.opt:
	set -e; for i in $(SUBDIRS); do $(MAKE) -C $$i all.opt; done 

$(EXEC): $(OBJS) $(LIBS)
	$(OCAMLC) -o $(EXEC) $(INCLUDES) $(FLAGSLIBS) $(SYSLIBS) $(LIBS) \
	$(OBJS)


$(OPTEXEC): $(OPTOBJS) $(OPTLIBS)
	$(OCAMLOPT) -o $(OPTEXEC) $(INCLUDES) $(OPTFLAGSLIBS) $(OPTSYSLIBS) \
	$(OPTLIBS) $(OPTOBJS)


.SUFFIXES:
.SUFFIXES: .ml .mli .cmo .cmi .cmx

.ml.cmo:
	$(OCAMLC) $(INCLUDES) -c $<

.mli.cmi:
	$(OCAMLC) $(INCLUDES) -c $<

.ml.cmx:
	$(OCAMLOPT) $(INCLUDES) -c $<

# clean rule for others files
clean::
	set -e; for i in $(SUBDIRS); do $(MAKE) -C $$i clean; done 
	rm -f *.cm[iox] *.o 
	rm -f *~ .*~ #*# 
	rm -f .depend $(EXEC)

alldepend:
	set -e; for i in $(SUBDIRS); do $(MAKE) -C $$i depend; done

depend: alldepend
	$(OCAMLDEP) $(LOCALINCLUDES) *.mli *.ml > .depend


.depend:
	$(OCAMLDEP) $(LOCALINCLUDES) *.mli *.ml > .depend

include .depend
