# This file is part of Prequel, lincensed under the terms of the GPL v2.
# See copyright.txt in the Prequel source code for more information.
# The Prequel source code can be obtained at
# https://github.com/prequel-pql/prequel

ifneq ($(MAKECMDGOALS),distclean)
include ../Makefile.config
-include ../Makefile.local
endif

##############################################################################
# Variables
##############################################################################
TARGET=select

OCAMLCFLAGS ?= -g
OPTFLAGS ?= -g

SRC=setup.ml get_data.ml select_commits.ml run_cocci.ml check_results.ml

LOCALINCLUDEDIRS = ../commons \
../commons/ocamlextra ../globals ../parsing_c ../parsing_cocci ../split
INCLUDEDIRS = $(PARMAP) $(LOCALINCLUDEDIRS)

##############################################################################
# Generic variables
##############################################################################

LOCALINCLUDES=$(LOCALINCLUDEDIRS:%=-I %)
INCLUDES=$(INCLUDEDIRS:%=-I %)
OCAMLC_CMD=$(OCAMLC) $(OCAMLCFLAGS) $(INCLUDES)
OCAMLOPT_CMD=$(OCAMLOPT) $(OPTFLAGS) $(INCLUDES)
OCAMLDEP_CMD=$(OCAMLDEP) $(LOCALINCLUDES)
OCAMLMKTOP_CMD=$(OCAMLMKTOP) -g -custom $(INCLUDES)

OBJS= $(SRC:.ml=.cmo)
OPTOBJS= $(SRC:.ml=.cmx)

##############################################################################
# Top rules
##############################################################################
all: $(TARGET).cma

all.opt:
	@$(MAKE) $(TARGET).cmxa BUILD_OPT=yes

$(TARGET).cma: $(OBJS)
	$(OCAMLC_CMD) -a -o $(TARGET).cma $(OBJS)

$(TARGET).cmxa: $(OPTOBJS)
	$(OCAMLOPT_CMD) -a -o $(TARGET).cmxa $(OPTOBJS)

##############################################################################
# Developer rules
##############################################################################
.SUFFIXES: .ml .mli .cmo .cmi .cmx

.ml.cmo:
	$(OCAMLC_CMD) -c $<
.mli.cmi:
	$(OCAMLC_CMD) -c $<
.ml.cmx:
	$(OCAMLOPT_CMD) -c $<

.ml.mldepend:
	$(OCAMLC_CMD) -i $<

clean:
	rm -f *.cm[ioxa] *.o *.a *.cmxa *.annot
	rm -f *~ .*~ gmon.out #*#
	rm -f .depend

distclean: clean

.PHONY: depend
.depend depend:
	$(OCAMLDEP_CMD) *.mli *.ml > .depend

ifneq ($(MAKECMDGOALS),clean)
ifneq ($(MAKECMDGOALS),distclean)
-include .depend
endif
endif
