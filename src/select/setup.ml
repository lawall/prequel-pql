(*
 * This file is part of Prequel, lincensed under the terms of the GPL v2.
 * See copyright.txt in the Prequel source code for more information.
 * The Prequel source code can be obtained at
 * https://github.com/prequel-pql/prequel
 *)

let global_setup _ =
  let _ = Sys.command (Printf.sprintf "/bin/rm -rf %s" !Flag.tmp) in
  let _ = Sys.command (Printf.sprintf "mkdir -p %s" !Flag.tmp) in
  if !Flag.spatch = ""
  then
    match Common.cmd_to_list "which spatch.opt" with
      [] -> "spatch"
    | _ -> "spatch.opt"
  else !Flag.spatch

(* ---------------------------------------------------------------------- *)

let do_split sp split_file =
  let nm = Filename.chop_extension (Filename.basename sp) in
  let nm =
     match split_file with
      None -> Printf.sprintf "%s/%s.cocci" !Flag.tmp nm
    | Some sf -> sf in
  let cnm =
    let pieces = List.rev (Str.split_delim (Str.regexp "/") nm) in
    (String.concat "/" (List.rev(List.tl pieces)))^"/constants_"^
    (List.hd pieces) in
  let o = open_out nm in
  Format.set_formatter_out_channel o;
  Split.split_for_code sp;
  close_out o;
  Format.set_formatter_out_channel stdout;
  let o = open_out cnm in
  Format.set_formatter_out_channel o;
  Split.split_for_constants sp;
  close_out o;
  (nm,cnm)
