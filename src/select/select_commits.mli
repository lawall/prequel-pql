(*
 * This file is part of Prequel, lincensed under the terms of the GPL v2.
 * See copyright.txt in the Prequel source code for more information.
 * The Prequel source code can be obtained at
 * https://github.com/prequel-pql/prequel
 *)

val maxpatches : int ref
val reducepatches : int ref
val all_patches : bool ref

type file_table =
    (string, (* file table *)
     ((((int * int) * (int * int) * (int * int) * (int * int)) * int) list *
	(int * int) *
	((int * string) list * (int * string) list) *
	(int list ref * int list ref) * (int list ref * int list ref)) ref)
      Hashtbl.t

type commit = string * string list option

val get_requirements : string -> string -> string -> (int * commit) list

val setup : commit -> string -> string -> string -> file_table -> float
