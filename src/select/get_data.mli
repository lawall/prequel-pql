val get_data :
    string (* commit *) ->
    (string, (* file table *)
     ((((int * int) * (int * int) * (int * int) * (int * int)) * int) list *
	(int * int) *
	((int * string) list * (int * string) list) *
	(int list ref * int list ref) * (int list ref * int list ref)) ref)
      Hashtbl.t ->
	string list -> int ref -> string list option -> out_channel ->
	  (string*string) list
