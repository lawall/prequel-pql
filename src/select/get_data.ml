let empty_line s =
  (Str.string_match (Str.regexp "[ \t]*$") s 0)

let split_at_comma s =
  match Str.split (Str.regexp ",") s with
    [x] -> 1
  | [x;y] -> int_of_string y
  | _ -> failwith "bad @@ line"

let rec readn n = function
    [] -> ([],[])
  | ((x::xs) as l) ->
      if n = 0
      then ([],l)
      else
	let (first,rest) = readn (n-1) xs in
	(* drop first char, which is - or + *)
	let x = String.sub x 1 (String.length x - 1) in
	(x::first,rest)

let rec iota start mx =
  if mx < start
  then []
  else start :: iota (start+1) mx

let normalize s =
  match Str.split (Str.regexp ",") (String.sub s 1 (String.length s - 1)) with
    [n] -> let n = int_of_string n in ((n,n),1)
  | [n;l] -> let n = int_of_string n in let l = int_of_string l in ((n,n+l-1),l)
  |  _ -> failwith "bad lines"

let getinfos hunk_data =
  let ocode =
    List.concat
      (List.map
	 (function (oldn,newn,minlines,plslines) ->
	   (* empty lines would count for pct, so drop them *)
	   List.filter (fun (_,x) -> not (empty_line x))
	     (List.combine (iota (fst oldn) (snd oldn)) minlines))
	 hunk_data) in
  let ncode =
    List.concat
      (List.map
	 (function (oldn,newn,minlines,plslines) ->
	   List.filter (fun (_,x)  -> not (empty_line x))
	     (List.combine (iota (fst newn) (snd newn)) plslines))
	 hunk_data) in
  (List.length ocode,List.length ncode,ocode,ncode)

let linediff res hunknum o =
  List.iter
    (function (file,cell,hunk_data) ->
      let hunks =
	List.map
	  (function ((oldst,olde),(newst,newe),minlines,plslines) ->
	    let ty =
	      match (olde < oldst,newe < newst) with
		(true,true) -> failwith "not possible"
	      | (true,false) -> "P" (* plus only *)
	      | (false,true) -> "M" (* minus only *)
	      | (false,false) -> "R" (* replacement *) in
	    Printf.fprintf o "Hunk|%d|%s|%s|%d|%d|%d|%d\n"
	      !hunknum ty (snd file) oldst olde newst newe;
	    let h = (((oldst,0),(olde,0),(newst,0),(newe,0)),!hunknum) in
      	    hunknum := !hunknum + 1;
	    h)
	  hunk_data in
      let (olines,nlines,ocode,ncode) = getinfos hunk_data in
      let (_,_,_,foundhunks,foundlines) = !cell in
      cell := (hunks,(olines,nlines),(ocode,ncode),foundhunks,foundlines))
    res

let tokens_all_full lines start =
  let lines = String.concat "\n" lines in
  let lexbuf = Lexing.from_string lines in
  Lexer_c.init lexbuf start;
  let rec aux () =
    let result =
      try Lexer_c.token lexbuf
      with e -> Printf.eprintf "%s\n" lines; raise e in
    if result = Lexer_c.EOF
    then []
    else result :: aux() in
  aux ()

type preinfo = int * int
type info = preinfo * string
type ctx = C of info * info | MP of (preinfo * info) * (preinfo * info)

let identify_hunks oldstart newstart (oldt,newt) =
  let get_line = function
      Lexer_c.Ident(tok,_,lncol)
    | Lexer_c.Cst(tok,_,lncol)
    | Lexer_c.Kwd(tok,_,lncol)
    | Lexer_c.Punct(tok,_,lncol) -> (lncol,tok)
    | Lexer_c.EOF -> ((-1,-1),"eof") in
  let rec loop ctx oldt newt =
    match (oldt,newt) with
      ([],[]) ->
	(match ctx with
	  C(mtok,ptok) -> []
	| MP( (mstart,((mln,mcol),mtok)),(pstart,((pln,pcol),ptok)) ) ->
	    [(mstart,(mln,mcol+String.length mtok),
	      pstart,(pln,pcol+String.length ptok))])
    | (x::xs,y::ys) ->
	(match (x,y) with
	  (Matcher.B,Matcher.D tok) -> (* added code *)
	    let newctx =
	      match ctx with
		C(((mln,mcol),mtok),_) ->
		  let mcol = mcol + String.length mtok in
		  let ((pln,pcol),ptok) = get_line tok in
		  MP( ((mln,mcol),((mln,mcol),"")),
		      ((pln,pcol),((pln,pcol),ptok)) )
	      | MP((mstart,mend),(pstart,_)) ->
		  MP((mstart,mend),(pstart,get_line tok)) in
	    loop newctx xs ys
	| (Matcher.D tok,Matcher.B) -> (* removed code *)
	    let newctx =
	      match ctx with
		C(_,((pln,pcol),ptok)) ->
		  let pcol = pcol + String.length ptok in
		  let ((mln,mcol),mtok) = get_line tok in
		  MP( ((mln,mcol),((mln,mcol),mtok)),
		      ((pln,pcol),((pln,pcol),"")) )
	      | MP((mstart,_),(pstart,pend)) ->
		  MP((mstart,get_line tok),(pstart,pend)) in
	    loop newctx xs ys
	| (Matcher.D tok1,Matcher.D tok2) -> (* replaced code *)
	    let (_,x) as xl = get_line tok1 in
	    let (_,y) as yl = get_line tok2 in
	    if x = y
	    then (* context code *)
	      let res =
		match ctx with
		  C _ -> []
		| MP((mstart,((mln,mcol),mtok)),(pstart,((pln,pcol),ptok))) ->
		    [(mstart,(mln,mcol+String.length mtok),
		      pstart,(pln,pcol+String.length ptok))] in
	      res @ loop (C(xl,yl)) xs ys
	    else (* replacement *)
	      let nc =
		match ctx with
		  C _ ->
		    let ((mln,mcol),mtok) = get_line tok1 in
		    let ((pln,pcol),ptok) = get_line tok2 in
		    MP( ((mln,mcol),((mln,mcol),mtok)),
                        ((pln,pcol),((pln,pcol),ptok)) )
		| MP((mstart,_),(pstart,_)) ->
		    MP((mstart,get_line tok1),(pstart,get_line tok2)) in
	      loop nc xs ys
	| _ -> failwith "not possible")
    | _ -> failwith "not possible" in
  loop (C(((oldstart,0),""),((newstart,0),""))) oldt newt

let word_diff_parameters = (6.,5.,6.,6.,2.,3.)

let worddiff res hunknum o =
  List.iter
    (function (file,cell,hunk_data) ->
      let hunks =
	List.concat
	  (List.map
	     (function (oldn,newn,minlines,plslines) ->
	       match (snd oldn < fst oldn,snd newn < fst newn) with
		 (true,true) -> failwith "both can't be empty"
	       | (true,false) ->
		   [((fst oldn,0),(fst oldn,0),(fst newn,0),
		    (snd newn,String.length (List.hd (List.rev plslines))))]
	       | (false,true) ->
		   [((fst oldn,0),
		    (snd oldn,String.length (List.hd (List.rev minlines))),
		    (fst newn,0),(fst newn,0))]
	       | (false,false) ->
		   let oldtoks = tokens_all_full minlines (fst oldn) in
		   let newtoks = tokens_all_full plslines (fst newn) in
		   let res = Matcher.seq_match oldtoks newtoks in
		   identify_hunks (fst oldn) (fst newn) res)
	     hunk_data) in
      let hunks =
	List.filter
	  (function ((ols,ocs),(ole,oce),(nls,ncs),(nle,nce)) ->
	    (* empty can happen for eg a commit that only adds a comment *)
	    not (ols = ole && ocs = oce && nls = nle && ncs = nce))
	  hunks in
      let modified_lines getter =
	List.length
	  (Common.nub
	     (List.fold_left
		(fun prev h ->
		  let ((ols,ocs),(ole,oce)) = getter h in
		  if ols < ole
		  then iota ols ole @ prev
		  else if ocs < oce
		  then ols :: prev
		  else prev)
		[] hunks)) in
      let modified_olines =
	modified_lines
	  (fun (starter,ender,_,_) -> (starter,ender)) in
      let modified_nlines =
	modified_lines
	  (fun (_,_,starter,ender) -> (starter,ender)) in
      let hunks =
	List.map
	  (function (((ols,ocs),(ole,oce),(nls,ncs),(nle,nce)) as info) ->
	    let res = (info,!hunknum) in
	    let ty =
	      match (ols = ole && ocs = oce, nls = nle && ncs = nce) with
		(true,true) -> failwith "not possible"
	      | (true,false) -> "P" (* plus only *)
	      | (false,true) -> "M" (* minus only *)
	      | (false,false) -> "R" (* replacement *) in
	    Printf.fprintf o "Hunk|%d|%s|%s|%d|%d|%d|%d|%d|%d|%d|%d\n"
	      !hunknum ty (snd file) ols ocs ole oce nls ncs nle nce;
      	    hunknum := !hunknum + 1;
	    res)
	  hunks in
      let (olines,nlines,ocode,ncode) = getinfos hunk_data in
      let (_,_,_,foundhunks,foundlines) = !cell in
      cell :=
	(hunks,(modified_olines,modified_nlines),(ocode,ncode),
	 foundhunks,foundlines))
    res

let hashfind tbl key =
  try Hashtbl.find tbl key
  with Not_found ->
    let cell = ref ([], (0, 0), ([], []), (ref [],ref []), (ref [],ref [])) in
    Hashtbl.add tbl key cell;
    cell

let get_data commit file_tbl lines hunknum valid_files o =
  let check_file file =
    match valid_files with
      None -> true
    | Some l -> List.mem file l in
  let get_cell_and_info (_,file) = hashfind file_tbl file in
  let rec read_to_file = function
      [] -> []
    | cur::rest ->
	match Str.split_delim (Str.regexp " ") cur with
          [_;"/dev/null"] -> read_to_file(snd(read_code(List.tl rest)))
        | ["---";minfile] ->
            (match Str.split_delim (Str.regexp " ") (List.hd rest) with
              [_;"/dev/null"] -> read_to_file(snd(read_code(List.tl rest)))
            | ["+++";plusfile] ->
		let minfile = String.sub minfile 2 (String.length minfile - 2) in
		let plusfile = String.sub plusfile 2 (String.length plusfile - 2) in
		let rest = List.tl rest in
		if (List.exists (Filename.check_suffix plusfile) !Flag.suffixes) &&
		   check_file plusfile &&
		   (* only allows changes, not new files or removed files *)
		   (List.exists (Filename.check_suffix minfile) !Flag.suffixes) &&
		   check_file minfile
		then
		  let (code,rest) = read_code rest in
		  if code = []
		  then read_to_file rest
		  else
		    let file = (minfile,plusfile) in
		    (file,(get_cell_and_info file),code) :: read_to_file rest
		else read_to_file rest
	    | _ ->
		failwith
		  (Printf.sprintf "get_data: %s bad diff: %s" commit
		     (List.hd rest)))
	| _ -> read_to_file rest
  and read_code = function
      [] -> ([],[])
    | cur::rest ->
	match Str.split (Str.regexp " ") cur with
	  "@@"::oldn::newn::"@@"::_ ->
	    let (oldn,olen) = normalize oldn in
	    let (newn,nlen) = normalize newn in
	    let (minlines,rest) = readn olen rest in
	    let (plslines,rest) = readn nlen rest in
	    let (res,rest) = read_code rest in
	    let empty l = String.trim l = "" in
	    if List.for_all empty minlines && List.for_all empty plslines
	    then (res,rest) (* ignore changes on comments *)
	    else ((oldn,newn,minlines,plslines) :: res, rest)
	| "diff"::_ -> ([],cur::rest)
	| _ -> read_code rest in
  let res = read_to_file lines in
  let files = List.map (function (f,_,_) -> f) res in
  (match !Flag.difftype with
    Flag.LINE -> linediff res hunknum o
  | Flag.WORD ->
      let (punct_wt,kwd_wt,id_wt,cst_wt,idmm_wt,cstmm_wt) =
	word_diff_parameters in
      Matcher.punct_weight := punct_wt;
      Matcher.kwd_weight := kwd_wt;
      Matcher.id_weight := id_wt;
      Matcher.cst_weight := cst_wt;
      Matcher.idmm_weight := idmm_wt;
      Matcher.cstmm_weight := cstmm_wt;
      worddiff res hunknum o);
  (* remove files for which there are no changes.
     relevant for word diff, which reduces the number of changed lines *)
  List.filter
    (function (_,file) -> (* key is plus file only *)
      let cell = Hashtbl.find file_tbl file in
      match !cell with
	(_,(0,0),_,_,_) ->
	  Hashtbl.remove file_tbl file;
	  false
      | _ -> true)
  files
