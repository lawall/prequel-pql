(*
 * This file is part of Prequel, lincensed under the terms of the GPL v2.
 * See copyright.txt in the Prequel source code for more information.
 * The Prequel source code can be obtained at
 * https://github.com/prequel-pql/prequel
 *)

module Cst = Get_constants2

let maxpatches = ref 10000
let reducepatches = ref 2000
let all_patches = ref false

type befaft = Before of Cst.combine | After of Cst.combine

type file_table =
    (string, (* file table *)
     ((((int * int) * (int * int) * (int * int) * (int * int)) * int) list *
	(int * int) *
	((int * string) list * (int * string) list) *
	(int list ref * int list ref) * (int list ref * int list ref)) ref)
      Hashtbl.t

type commit = string * string list option

(* ------------------------------------------------------------------------ *)
(* preparing for word diff *)

let union l1 l2 =
  List.fold_left
    (fun prev cur -> if not (List.mem cur prev) then cur::prev else prev)
    l2 l1

let minus l1 l2 = List.filter (function x -> not (List.mem x l2)) l1

let bad = ["$";"^";"\\";".";"*";"+";"?";"[";"]"]
let fix_special = function
    Cst.Tok s | Cst.Kwd s -> ("\\b"^s^"\\b",s)
  | Cst.Op s ->
      let l = Str.split (Str.regexp "") s in
      let fixed =
	String.concat ""
	  (List.map
	     (function x ->
	       if List.mem x bad
	       then "\\"^x
	       else x)
	     l) in
      (fixed,s)

let parfilter f l =
  List.fold_left
    (function prev ->
      function
	  (_,(_,[])) -> prev
	| (i,(c,f)) -> (i,(c,Some f))::prev)
    []
    (if !Flag.cores = 1
    then List.fold_left (fun prev x -> f x @ prev) [] l
    else
      List.fold_left (fun prev x -> x @ prev) []
	(Parmap.parmap f ~ncores:!Flag.cores ~chunksize:1 (Parmap.L l)))

let collect_atoms l = List.fold_left union [] l

let modified_file = function
    [] -> failwith "no diff info"
  | l::_ ->
      match Str.split (Str.regexp " ") l with
	"new"::"file"::_ -> false
      | "deleted"::"file"::_ -> false
      | _ -> true

let fix_entry = function
      Cst.Modified(color,_,Some x) ->
	let (re_op,x) = fix_special x in
	((Cst.ModifiedCase,color,true),re_op,x)
    | Cst.Modified(color,_,None) -> (* a hack, boolean represents None *)
	((Cst.ModifiedCase,color,false),"","")
    | Cst.Unmodified(color,_,x) ->
	let (re_op,x) = fix_special x in
	((Cst.UnmodifiedCase,color,true),re_op,x)
    | _ -> failwith "bad formula"

let contains_in_lines needs_u3 formula (i,(commit,valid_files)) lines =
  let check_file file =
    match valid_files with
      None -> true
    | Some l -> List.mem file l in
  let split_by_files side =
    let rec loop inhunk drop = function
	[] -> ([],[])
      | x::xs ->
	  (match Str.split (Str.regexp " ") x with
	    "diff"::rest ->
	      let file = List.hd (List.rev rest) in
	      let file = String.sub file 2 (String.length file - 2) in
	      if modified_file xs && check_file file
	      then
		let (restlines,restinfo) = loop false false xs in
		([],(file,restlines)::restinfo)
	      else loop false true xs (* skip to next file *)
	  | _ when (not inhunk) || drop ->
	      let inhunk = try String.get x 0 = '@' with _ -> false in
	      loop inhunk drop xs
	  | _ ->
	      let (restlines,restinfo) = loop inhunk false xs in
	      let start_ok = try String.get x 0 = side with _ -> false in
	      if start_ok
	      then (x::restlines,restinfo)
	      else (restlines,restinfo)) in
    snd (loop false false lines) in
  let minus_infos = split_by_files '-' in
  let plus_infos = split_by_files '+' in
  let ctx_infos =
    if needs_u3
    then split_by_files ' '
    else List.map (function (file,_) -> (file,[])) plus_infos in
  let interpret_clause c (ba,re) l =
    let check _ =
      try let _ = Str.search_forward re l 0 in true
      with Not_found -> false in
    match (c,ba) with
      ('-',(_,Cst.Before,true)) | ('+',(_,Cst.After,true)) -> check()
    | (' ',(Cst.UnmodifiedCase,_,true)) -> check()
    | _ -> false in
  let matches ((um,ba,hastok) as info,x,_) mlines plines clines =
    (not hastok && ba = Cst.Before && not (mlines = [])) ||
    (not hastok && ba = Cst.After  && not (plines = [])) ||
    (let word = (info,Str.regexp x) in
    (hastok && ba = Cst.Before &&
     List.exists (interpret_clause '-' word) mlines) ||
     (hastok && ba = Cst.After  &&
      List.exists (interpret_clause '+' word) plines) ||
      (hastok && um = Cst.UnmodifiedCase &&
       List.exists (interpret_clause ' ' word) clines)) in
  let interesting_files =
    List.fold_left2
      (fun prev ((mfile,mlines),(pfile,plines)) (cfile,clines) ->
	(if not (mfile = pfile)
	then failwith "different files");
	let tbl = Hashtbl.create 101 in
	let rec loop = function
	    Cst.And xs -> List.for_all loop xs
	  | Cst.Or xs -> List.exists loop xs
	  | Cst.False -> false
	  | Cst.True -> true
	  | entry ->
	      let (_,_,key) as tok = fix_entry entry in
	      (try Hashtbl.find tbl key
	      with Not_found ->
		let res = matches tok mlines plines clines in
		Hashtbl.add tbl key res;
		res) in
	if loop formula then mfile :: prev else prev)
      [] (List.combine minus_infos plus_infos) ctx_infos in
  let interesting_files =
    if !Flag.all_at_once
    (* could instead update the above code to find the formula
       across all ofthe files in the patch.  Currently require that
       a single file contain enough words to possibly trigger the patch *)
    then
      match interesting_files with
	[] -> []
      | _->
	  (* if we are doing all at once, keep all files in the patch *)
	  List.map (fun (mfile,mlines) -> mfile) minus_infos
    else interesting_files in
  (i,(commit,interesting_files))

let contains_in needs_u3 formula commitsl =
  let uarg = if needs_u3 then "-U3" else "-U0" in
  let commits =
    List.map (function (i,(commit,valid_files)) -> commit) commitsl in
  let lines =
    Printf.sprintf "git show --no-merges --diff-filter=M -b -w --format=\"commit %%H\" %s %s"
      uarg (String.concat " " commits) in
  let lines = Common.cmd_to_list lines in
  let rec split acc = function
      [] -> (match acc with [] -> [] | _ -> [List.rev acc])
    | x::xs ->
	if not (x="") && String.get x 0 = 'c'
	then (* should be commit, but col 0 is enough *)
	  match acc with [] -> split [] xs | _ -> (List.rev acc) :: split [] xs
	else split (x :: acc) xs in
  List.fold_left2
    (fun prev (i,(commit,valid_files)) lines ->
      let res =
	contains_in_lines needs_u3 formula (i,(commit,valid_files)) lines in
      match res with
	(_,(_,[])) -> prev
      | _ -> res::prev)
    [] commitsl (split [] lines)

(* ---------------------------------------------------------------------- *)
(* reading commits *)
(* The use of sets is good for efficiency when managing large numbers of
commits, but it means that we can't keep track of file names for the File
cases.  As a hack, just keep all of the files that have ever motivated
the collection of a given commit.  This will turn intersection into union for
these file names, but it is safe and should be faster than taking all of the
files associated with all of the commits *)
module StringSet = Set.Make(String)

let idutils_get_all commits formula dir =
  let valid_commits = Hashtbl.create 101 in
  Printf.eprintf "starting commits %d %s %s\n" (List.length commits)
    (Dumper.dump(fst (List.hd commits)))
    (Dumper.dump(snd (List.hd commits)));
  List.iter (function (x,i) -> Hashtbl.add valid_commits x i) commits;
  let memo_table = Hashtbl.create 101 in
  let file_table = Hashtbl.create 101 in
  let file_commit_table = Hashtbl.create 101 in
  let final_file_table = Hashtbl.create 101 in
  let (lid,grep_for_ref) =
    if !Flag.reference_version = ""
    then (false,"") (* not used *)
    else
      let lidref =
	Filename.concat !Flag.reference_version ".id-utils.index" in
      if Sys.file_exists lidref
      then (true,Printf.sprintf "lid -f %s -S newline" lidref)
      else
	(false,
	 if Sys.file_exists (Filename.concat !Flag.reference_version ".git")
	 then "git grep -w"
	 else "grep -r -w") in
  let refdir =
    if dir = ""
    then !Flag.reference_version
    else Filename.concat !Flag.reference_version dir in
  let commits = List.fold_left (fun prev (s,i) -> s::prev) [] commits in
  let list2stringset l =
    List.fold_left (fun p c -> StringSet.add c p) StringSet.empty l in
  let lid_to_commits index keys queries =
    (* batch these in groups of 25 to reduce shell creation time *)
    let cmd =
      Printf.sprintf "lid -f %s %s -S newline" index
	(String.concat " " (List.map (function t -> "-l "^t) queries)) in
    let index = List.map2 (fun k q -> (q,k)) keys queries in
    let files = Common.cmd_to_list cmd in
    let files =
      let rec loop cur = function
	  [] -> []
	| x::xs ->
	    match Str.split (Str.regexp "[ \t]+") x with
	      [key;res] -> loop (List.assoc key index) (res::xs)
	    | [_] ->
		(match loop cur xs with
		  (k,l)::rest ->
		    if k = cur
		    then (k,x::l)::rest
		    else (cur,[x])::(k,l)::rest
		| [] -> [(cur,[x])])
	    | _-> failwith "not possible" in
      loop (List.hd keys) files in
    let res =
      List.map
	(function (k,x) ->
	  Printf.eprintf "%s: %d\n" k (List.length x);
	  (k,
	   List.map (fun x -> Filename.chop_extension (Filename.basename x))
	     x))
	files in
    let res =
      List.map
	(function (k,x) ->
	  Printf.eprintf "%s: %d\n" k (List.length x);
	  (k,
	   List.filter
	     (function x ->
	       try let _ = Hashtbl.find valid_commits x in true
	       with Not_found -> false)
	     x))
	res in
    List.map (function (k,x) ->
	  Printf.eprintf "%s: %d\n" k (List.length x);
      (k,list2stringset x)) res in
  let memoize tok sign =
    try Hashtbl.find memo_table (tok,sign)
    with Not_found ->
      let t =
	match tok with
	  Cst.Tok t | Cst.Kwd t -> t
	| Cst.Op _ -> failwith "not possible" in
      let index =
	match sign with
	  Some Cst.Before -> !Flag.u0mindex
	| Some Cst.After -> !Flag.u0pindex
	| None -> !Flag.u3index in
      let res =
	match lid_to_commits index [t] [t] with
	  [] -> (* no result at all *) list2stringset []
	| [(_,res)] -> res
	| _ -> failwith "multiple results not possible with only one key" in
      Hashtbl.add memo_table (tok,sign) res;
      res in
  let memo_file1 tok =
    try Hashtbl.find file_table tok
    with Not_found ->
      let cmd =
	if lid (* don't bother with cd if using index *)
	then Printf.sprintf "%s -l %s" grep_for_ref tok
	else Printf.sprintf "cd %s; %s -l %s" refdir grep_for_ref tok in
      let relevant_files = Common.cmd_to_list cmd in
      let refver =
	let lastchar =
	  String.get !Flag.reference_version
	    (String.length !Flag.reference_version - 1) in
	if lid && not (lastchar = '/')
	then !Flag.reference_version ^ "/"
	else !Flag.reference_version in
      let relevant_files =
	match relevant_files with
	  x::xs ->
	    (match Str.split (Str.regexp "[ \t]+") x with
	      [t;res] when tok = t && lid ->
		List.map
		  (function x ->
		    List.hd (Str.split (Str.regexp_string refver) x))
		  (res::xs)
	    | [x] when not lid -> x::xs
	    | _ -> failwith "bad relevant files")
	| [] -> [] in
      let relevant_files =
	List.filter
	  (function x ->
	    List.exists (Filename.check_suffix x) !Flag.suffixes)
	  relevant_files in
      let res = list2stringset relevant_files in
      Hashtbl.add file_table tok res;
      res in
  let memo_file2 files =
    let relevant_files = StringSet.fold (fun x rest -> x :: rest) files [] in
    let relevant_files =
      let group_size = 25 in
      Common.group_by_n group_size relevant_files in
    let res =
      Parmap.parmapi ~ncores:!Flag.cores ~chunksize:100
	(fun i ofiles ->
	  let (processed,others) =
	    List.fold_left
	      (fun (processed,others) ofile ->
		try ((ofile,Hashtbl.find file_commit_table ofile)::processed,
		     others)
		with Not_found -> (processed,ofile::others))
	      ([],[]) ofiles in
	  if others = []
	  then processed
	  else
	    let encodedothers =
	      List.map
		(function ofile ->
		  let file =
		    String.concat "QQ" (Str.split (Str.regexp "/") ofile) in
		  let file =
		    String.concat "FF" (Str.split (Str.regexp "-") file) in
		  String.concat "XX" (Str.split (Str.regexp_string ".") file))
		others in
	    let res = lid_to_commits !Flag.fileindex others encodedothers in
	    res @ processed)
	(Parmap.L relevant_files) in
    let res = List.concat res in
    List.iter
      (function (file,commits) ->
	if not (Hashtbl.mem file_commit_table file)
	then Hashtbl.add file_commit_table file commits)
      res;
    let res =
      match res with
	[] -> StringSet.empty
      | x::xs ->
	  List.fold_left (fun prev x -> StringSet.union (snd x) prev)
	    (snd x) xs in
    res in
  let rec loop = function
      Cst.And ((x::xs) as l) ->
	let (files,others) = (* optim, because file checks are slow *)
	  List.partition (function Cst.File _ -> true | _ -> false) l in
	let toks =
	  List.map (function Cst.File t -> t | _ -> failwith "bad file")
	    files in
	let toks = List.sort compare toks in
	(try
	  List.fold_left (fun p x -> StringSet.inter (loop x) p)
	    (Hashtbl.find final_file_table toks) others
	with Not_found ->
	  match toks with
	    t1::((_::_) as rest) ->
	      let files =
		List.fold_left (fun p x -> StringSet.inter (memo_file1 x) p)
		  (memo_file1 t1) rest in
	      let res = memo_file2 files in
	      Hashtbl.add final_file_table toks res;
	      (match others with
		[] -> res
	      | _ ->
		  List.fold_left (fun p x -> StringSet.inter (loop x) p)
		    res others)
	  | _ ->
	      List.fold_left (fun p x -> StringSet.inter (loop x) p)
		(loop x) xs)
    | Cst.Or (x::xs) ->
	List.fold_left (fun p x -> StringSet.union (loop x) p) (loop x) xs
    | Cst.And _ | Cst.Or _ -> failwith "and and or should not be empty"
    | Cst.False -> StringSet.empty
    | Cst.True ->
	List.fold_left (fun p c -> StringSet.add c p) StringSet.empty commits
    | Cst.Modified(sign,_,Some tok) -> memoize tok (Some sign)
    | Cst.Unmodified(_,_,tok) -> memoize tok None
    | Cst.File tok ->
	(try Hashtbl.find final_file_table [tok]
	with Not_found ->
	  let files = memo_file1 tok in
	  let res = memo_file2 files in
	  Hashtbl.add final_file_table [tok] res;
	  res)
    | _ -> failwith "unexpected formula" in
  (* indices mean nothing in this case *)
  let info = loop formula in
  StringSet.fold
    (fun x rest -> (Hashtbl.find valid_commits x,(x,None))::rest)
    info []

let id_interpret needs_u3 all_commits formula rebuilt_formula dir =
  (* The formula has the form (a v b) & (c v d), ie a cnf.*)
  (* For each conjunct, run git grep to get the current usage count *)
  let resm =
    let commitsl = idutils_get_all all_commits formula dir in
    Printf.eprintf "from lid: %d\n" (List.length commitsl);
    if rebuilt_formula = Cst.True (* true, only File elements *)
    then commitsl (* take everything *)
    else
      begin
	let home = Sys.getcwd() in
	Sys.chdir !Flag.gitpath;
        let commitsl = Common.group_by_n 5 commitsl in
	let res = parfilter (contains_in needs_u3 rebuilt_formula) commitsl in
	Sys.chdir home;
	res
      end in
  Printf.eprintf "%s: %d entries\n" (Cst.dep2c formula) (List.length resm);
  resm

(* ------------------------------------------------------------------------ *)

let limit n l =
  if n > 0 && List.length l > n
  then
    begin
      Printf.eprintf "limiting %d commits to %d\n" (List.length l) n;
      flush stderr;
      let rec loop n acc = function
	  x::xs -> if n = 0 then acc else loop (n-1) (x::acc) xs
	| _ -> failwith "should not happen" in
      List.rev(loop n [] l)
    end
  else Common.mapi (fun i (_,x) -> (i,x)) (List.sort compare l)

let get_requirements sp dir gitargs =
  let tstart = Unix.gettimeofday() in
  let c =
    List.fold_left
      (fun prev range ->
	let cmd =
	  Printf.sprintf
	    "cd %s;git log --abbrev=12 --no-merges --pretty=format:%%h %s %s %s"
	    !Flag.gitpath gitargs range (if dir = "" then "" else "-- "^dir) in
	let c = Common.cmd_to_list cmd in
	c@prev)
      [] !Flag.commits in
  let all_commits =
    snd(List.fold_left (fun (i,prev) s -> (i+1,(s,i) :: prev)) (0,[]) c) in
  let istart = Unix.gettimeofday() in
  let options = Split.get_constants sp in
  let rec loop acc = function
      [] -> limit !maxpatches acc
    | (id,((ce,elements),(ci,idelements) (* for idutils, no ops/files *)))
      ::rest ->
	let rec dfe = function (*No information for supporting the File case*)
	    Cst.And l ->
	      List.fold_left (fun prev x -> Cst.build_and (dfe x) prev)
		Cst.True l
	  | Cst.Or l ->
	      List.fold_left (fun prev x -> Cst.build_or (dfe x) prev)
		Cst.False l
	  | Cst.File _ -> Cst.True
	  | x -> x in
	let drop_file_elements = dfe elements in
	let id_drop_file_elements =
	  if !Flag.reference_version = ""
	  then dfe idelements
	  else idelements in
	let needs_u3 = ce = Cst.UnmodifiedCase in
	let commitsl =
	  id_interpret needs_u3 all_commits id_drop_file_elements
	    drop_file_elements dir in
	Printf.eprintf "%d: lowest: commits %d\n" id (List.length commitsl);
	if List.length commitsl > !reducepatches
	then
          let res = loop commitsl rest in
          (* if we get nothing from the more restricted case, then go with
             what we have *)
          if res = [] then limit !maxpatches commitsl else res
	else limit !maxpatches commitsl in
  let res =
    if !all_patches
    then Common.mapi (fun i x -> (i,(x,None))) c
    else loop [] options in
  let tend = Unix.gettimeofday() in
  Printf.eprintf "commit selection time %0.2f (%0.2f for git)\n"
    (tend -. tstart) (istart -. tstart);
  flush stderr;
  res

(* ---------------------------------------------------------------------- *)

let decomment lines =
  let rec inloop acc = function
      [] -> List.rev acc
    | ""::ls -> inloop (""::acc) ls
    | l::ls ->
	(match String.get l 0 with
	  '@' -> outloop (l :: acc) ls
	| fst ->
	    (match Str.bounded_split_delim (Str.regexp_string "*/") l 2 with
	      [before;after] ->
		let (lres,cont) = parse_rest after in
		cont ((Printf.sprintf "%c%s" fst lres) :: acc) ls
	    | [inside] -> inloop (Printf.sprintf "%c" fst :: acc) ls
	    | _ -> failwith "not possible"))
  and outloop acc = function
      [] -> List.rev acc
    | ""::ls -> outloop ("" :: acc) ls
    | l::ls ->
	(match String.get l 0 with
	  '@' -> outloop (l :: acc) ls
	| _ ->
	    let (lres,cont) = parse_rest l in
	    cont (lres :: acc) ls)
  and comsplit com l =
    match Str.bounded_split_delim (Str.regexp_string com) l 2 with
      [before;after] -> (before,Some after)
    | [x] -> (x,None)
    | [] -> ("",None)
    | _ -> failwith "not possible"
  and parse_rest l =
    let (bef_com1,after_com1) = comsplit "//" l in
    let (before,after) = comsplit "/*" l in
    if String.length bef_com1 < String.length before
    then (bef_com1,outloop)
    else
      match after with
	None -> (l,outloop)
      | Some after ->
	  (match Str.bounded_split_delim (Str.regexp_string "*/") after 2 with
	    [middle;after] ->
	      let (lres,cont) = parse_rest after in
	      (before^lres,cont)
	  | _ -> (before,inloop)) in
  outloop [] lines

(* skips file introductions and file removals, only takes .c or .h files *)
let setup (commit,valid_files) beforedir afterdir hunk_file file_tbl =
  let _ = Sys.command (Printf.sprintf "/bin/rm -rf %s %s" beforedir afterdir) in
  let _ = Sys.command (Printf.sprintf "mkdir -p %s %s" beforedir afterdir) in
  let patchlines =
    Common.cmd_to_list
      (Printf.sprintf
	 "cd %s; git show --format=\"%%n\" %s -m --ignore-blank-lines -w -U0"
	 !Flag.gitpath commit) in
  let patchlines = decomment patchlines in
  let hunknum = ref 0 in
  let o = open_out hunk_file in
  let tstart = Unix.gettimeofday() in
  let files =
    Get_data.get_data commit file_tbl patchlines hunknum valid_files o in
  let tend = Unix.gettimeofday() in
  close_out o;
  let home = Sys.getcwd() in
  Sys.chdir !Flag.gitpath;
  let files = Common.group_by_n 10 files in
  List.iter
    (function files ->
      let commands =
	List.map
	  (function (minfile,plusfile) ->
	    (* files only use the plus file, which is in
	       the file_tbl *)
	    let dir = Filename.dirname plusfile in
	    let c1 =
	      Printf.sprintf "mkdir -p %s/%s %s/%s"
		beforedir dir afterdir dir in
	    let c2 =
              Printf.sprintf "git show %s^:%s > %s/%s"
		commit minfile beforedir plusfile in
	    let c3 =
              Printf.sprintf "git show %s:%s > %s/%s"
		commit plusfile afterdir plusfile in
	    Printf.sprintf "%s ; %s ; %s" c1 c2 c3)
	  files in
      let _ = Sys.command (Printf.sprintf "%s" (String.concat ";" commands)) in
      ())
    files;
  Sys.chdir home;
  tend -. tstart
