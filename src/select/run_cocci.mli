(*
 * This file is part of Prequel, lincensed under the terms of the GPL v2.
 * See copyright.txt in the Prequel source code for more information.
 * The Prequel source code can be obtained at
 * https://github.com/prequel-pql/prequel
 *)

val run_cocci : string -> string -> int -> Select_commits.commit ->
  string -> string -> string -> Select_commits.file_table -> string -> int ->
    (string list * float)

