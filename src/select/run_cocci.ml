(*
 * This file is part of Prequel, lincensed under the terms of the GPL v2.
 * See copyright.txt in the Prequel source code for more information.
 * The Prequel source code can be obtained at
 * https://github.com/prequel-pql/prequel
 *)

let dodebug i commit allcommits =
  if !Flag.debug
  then
    begin
      Printf.eprintf "%s (%d/%d)\n" commit (i+1) allcommits;
      flush stderr
    end

let cmd_to_list s =
(*  (if !Flag.debug then (Printf.eprintf "%s\n" s; flush stderr));*)
  Common.cmd_to_list s

let union l1 l2 =
  List.fold_left
    (fun prev cur -> if List.mem cur prev then prev else cur :: prev)
    l2 l1

let iota a b =
  let rec loop n = if n > b then [] else n::loop (n+1) in
  loop a

type matcher =
    { unshared_positions : (string * int * int) list;
      shared_positions  : (string * string * int * int) list;
      post_shared_positions  : (string * string * int * int) list;
      metavars : (string * string) list }

let run_cocci spatch sp i (commit,valid_files) hunk_file
    beforedir afterdir file_tbl cocciargs allcommits =
  dodebug i commit allcommits;
  let find_pattern =
    let points s = String.length s = 2 && String.get s 0 = '.'in
    if List.for_all points !Flag.suffixes
    then
      Printf.sprintf "\"*.[%s]\""
	(String.concat ""
	   (List.map (fun x -> Printf.sprintf "%c" (String.get x 1))
	      !Flag.suffixes))
    else
      match !Flag.suffixes with
	[suff] -> Printf.sprintf "\"*%s\"" suff
      | _ -> failwith "too many suffixes" in
  let before_files =
    cmd_to_list (Printf.sprintf "find %s -name %s" beforedir find_pattern) in
  let after_files =
    cmd_to_list (Printf.sprintf "find %s -name %s" afterdir find_pattern) in
  let before_files = List.sort compare before_files in
  let after_files = List.sort compare after_files in
  (if not(List.length before_files = List.length after_files)
  then failwith "unequal number of files");
  let commit_file = Printf.sprintf "%s/commit" beforedir in
  let file_groups = Printf.sprintf "%s/file_groups" beforedir in
  let test_script = Printf.sprintf "%s/test_script" beforedir in
  let o = open_out commit_file in
  (* handy to have the commit available *)
  Printf.fprintf o "%s\n" commit;
  close_out o;
  let o = open_out file_groups in
  let check_file file =
    match valid_files with None -> true | Some x -> List.mem file x in
  List.iter2
    (fun bef aft ->
      let befpost = List.nth (Str.split (Str.regexp "/before/") bef) 1 in
      let aftpost = List.nth (Str.split (Str.regexp "/after/") aft) 1 in
      let befpost = (* parmap numbers *)
	String.concat "/" (List.tl (Str.split (Str.regexp "/") befpost)) in
      let aftpost =
	String.concat "/" (List.tl (Str.split (Str.regexp "/") aftpost)) in
      (if not (befpost = aftpost)
      then
	failwith
	  (Printf.sprintf "files don't match up: %s %s" befpost aftpost));
      if check_file befpost
      then
	begin
	  Printf.fprintf o "%s\n%s\n" bef aft;
	  if not(!Flag.all_at_once)
	  then Printf.fprintf o "\n"
	end)
    before_files after_files;
  close_out o;
  let quiet = if !Flag.debug then "" else "--very-quiet" in
  let cmd =
    (* try to avoid expanding macros, which gives inconsistent line numbers *)
    Printf.sprintf
      "%s --disable-multi-pass --include-headers-for-types --timeout 40 %s --cocci-file %s --file-groups %s -D hunk_file=%s -D commit=%s %s"
      spatch quiet sp file_groups hunk_file commit cocciargs in
  let o = open_out test_script in
  Printf.fprintf o "%s $*\n" cmd;
  close_out o;
  let tstart = Unix.gettimeofday() in
  let (result,stat) = Common.cmd_to_list_and_status cmd in
  let tend = Unix.gettimeofday() in
  (if not (stat = Unix.WEXITED 0)
  then Printf.eprintf "cocci failure on %s\n" commit);
  (if !Flag.debug
  then (flush stderr; Printf.eprintf "time: %0.2f\n" (tend -. tstart)));
(*(if !Flag.debug
   then List.iter (function x -> Printf.eprintf "%s\n" x) result);*)
  let rec collect_loop inside = function
      [] -> ([],[])
    | line::rest ->
	match line with
	  "start record" -> collect_loop true rest
	| "end record" ->
	    let (output,records) = collect_loop false rest in
	    (output,[]::records)
	| _ ->
	    if inside
	    then
	      match collect_loop inside rest with
		(output,l1::rest) -> (output,(line::l1)::rest)
	      | _ -> failwith "missing end record"
	    else
	      let (output,records) = collect_loop inside rest in
	      (line::output,records) in
  let (output,checked_info) = collect_loop false result in
  let checked_info = List.map (String.concat ",") checked_info in
  let add_if_needed x cell =
    if not (List.mem x !cell) then cell := x :: !cell in 
  List.iter
    (function record ->
      List.iter
	(function hunk_and_line ->
	  match Str.split (Str.regexp ":") hunk_and_line with
	    [key;file;hunk;line;endline] ->
	      let hunk = int_of_string hunk in
	      let line = int_of_string line in
	      let endline = int_of_string endline in
	      let (_,_,_,
		   (foundohunks,foundnhunks),(foundolines,foundnlines)) =
		!(Hashtbl.find file_tbl file) in
	      let (foundhunks,foundlines) =
		match key with
		  "M" -> (foundohunks,foundolines)
		| "P" -> (foundnhunks,foundnlines)
		| _ -> failwith "bad key" in
	      add_if_needed hunk foundhunks;
	      List.iter (function line -> add_if_needed line foundlines)
		(iota line endline)	    
	  | _ -> failwith ("bad info: "^hunk_and_line))
	(Str.split (Str.regexp ",") record))
    checked_info; (* dont seem to care about record boundaries? *)
  (output,tend -. tstart)
