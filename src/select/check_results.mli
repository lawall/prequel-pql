(*
 * This file is part of Prequel, lincensed under the terms of the GPL v2.
 * See copyright.txt in the Prequel source code for more information.
 * The Prequel source code can be obtained at
 * https://github.com/prequel-pql/prequel
 *)

val pctmin : int ref
val pctplus : int ref
val pct : int ref
val pcthunk : int ref

val upd_hunks : unit -> unit
val upd_old   : unit -> unit
val upd_new   : unit -> unit
val upd_all   : unit -> unit

val message : string ref

val check_results :
    Select_commits.commit -> Select_commits.file_table -> string list ->
      (Select_commits.commit * string list * (int * int) list *
	 int * string list *
	 (string * (int*string) list * (int*string) list) list) option

val print_top_results : int -> int option ->
    ((Select_commits.commit * string list * (int * int) list *
	int * string list *
	(string * (int*string) list * (int*string) list) list) * int) list ->
      string -> unit

val call_spinfer : int -> string ->
    ((Select_commits.commit * string list * (int * int) list *
	int * string list *
	(string * (int*string) list * (int*string) list) list) * int) list ->
    int option -> unit

val ignore : string list ref

val gitgcompare : string -> unit
val gitscompare : string -> unit
type git = Git of string | NoGit
val git_compare_type : git ref
