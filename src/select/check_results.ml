(*
 * This file is part of Prequel, lincensed under the terms of the GPL v2.
 * See copyright.txt in the Prequel source code for more information.
 * The Prequel source code can be obtained at
 * https://github.com/prequel-pql/prequel
 *)

let pctmin = ref 0
let pctplus = ref 0
let pct = ref 0
let pcthunk = ref 0

let hunk_key = ref 1
let oline_key = ref 2
let nline_key = ref 3
let allline_key = ref 4

let key_ctr = ref (-4)

let upd cell = let c = !key_ctr in key_ctr := c + 1; cell := c
let upd_hunks _ = upd hunk_key
let upd_old _ = upd oline_key
let upd_new _ = upd nline_key
let upd_all _ = upd allline_key

let message = ref ""

type git = Git of string | NoGit

let git_compare_type = ref NoGit
let git_compare_string = ref ""

let gitgcompare s = git_compare_type := Git "-G"; git_compare_string := s
let gitscompare s =
  git_compare_type := Git "--pickaxe-regex -S"; git_compare_string := s

let nub l =
  List.fold_left
    (fun prev cur -> if List.mem cur prev then prev else cur::prev)
    [] l

let unmatched_words foundlines code =
  let code =
    List.fold_left
      (fun prev (n,c) -> if List.mem n foundlines then prev else c::prev)
      [] code in
  let wordre = Str.regexp "[a-zA-Z_][a-zA-Z0-9_]*" in
  let get_words line =
    let words = Str.split (Str.regexp "\\b") line in
    let rec loop = function
	[] -> []
      | word::paren::rest when String.get paren 0 = '(' ->
	  if Str.string_match wordre word 0
	  then word :: loop rest
	  else loop rest
      | "->"::word::rest
      | "."::word::rest ->
	  if Str.string_match wordre word 0
	  then word :: loop rest
	  else loop rest
      | _::rest -> loop rest in
    loop words in
  List.fold_left
    (fun (unique,all) code ->
      let words = get_words code in
      (Common.union_set (nub words) unique,
       words@all))
    ([],[]) code

let check_results commit file_tbl output =
  let (total_hunks,total_olines,total_nlines,
       found_hunks,found_olines,found_nlines,
       unique_unmatched_words,all_unmatched_words,unmatched_code) =
    Hashtbl.fold
      (fun file cell (total_hunks,total_olines,total_nlines,
		      found_hunks,found_olines,found_nlines,uuw,auw,ucode) ->
	let (hunks,(olines,nlines),(ocode,ncode),
	     ((foundohunks : int list ref),(foundnhunks : int list ref)),
	     (foundolines,foundnlines)) =
	  !cell in
	let (ounique,ounmatched_words) = unmatched_words !foundolines ocode in
	let (nunique,nunmatched_words) = unmatched_words !foundnlines ncode in
	let uuw = Common.union_set ounique (Common.union_set nunique uuw) in
	let auw =
	  (List.length ounmatched_words) + (List.length nunmatched_words) +
	    auw in
	let unmatched_code =
	  if !Flag.show_unmatched_lines || !Flag.show_unmatched_code
	  then
	    let unmatched_ocode =
	      List.filter
		(function (ln,_) -> not (List.mem ln !foundolines))
		ocode in
	    let unmatched_ncode =
	      List.filter
		(function (ln,_) -> not (List.mem ln !foundnlines))
		ncode in
	    if unmatched_ocode = [] && unmatched_ncode = []
	    then []
	    else [(file,unmatched_ocode,unmatched_ncode)]
	  else [] in
	(total_hunks + List.length hunks,
	 total_olines + olines,
	 total_nlines + nlines,
	 found_hunks +
	   (List.length
	      (nub (Common.union_set !foundohunks !foundnhunks))),
	 found_olines + List.length !foundolines,
	 found_nlines + List.length !foundnlines,
	 uuw,auw,unmatched_code@ucode))
      file_tbl (0, 0, 0, 0, 0, 0, [], 0, []) in
  let xpct a b = if b > 0 then (100 * a) / b else 100 in
  let hunkpct = xpct found_hunks total_hunks in
  let olinespct = xpct found_olines total_olines in
  let nlinespct = xpct found_nlines total_nlines in
  let uniquepct =
    xpct (List.length unique_unmatched_words) all_unmatched_words in
  let alllinespct =
    xpct (found_olines+found_nlines) (total_olines+total_nlines) in
  (*Printf.eprintf "%s: hunkpct: %d (%d/%d) pcthunk: %d, olinespct: %d (%d/%d) pctmin: %d, nlinespct: %d (%d/%d) pctplus: %d, alllinespct: %d (%d/%d) pct: %d\n"
    (Dumper.dump commit)
    hunkpct found_hunks total_hunks !pcthunk olinespct found_olines total_olines !pctmin nlinespct found_nlines total_nlines !pctplus
    alllinespct (found_olines+found_nlines) (total_olines+total_nlines) !pct; flush stderr;*)
  if hunkpct > 0 && (* must match at least one hunk *)
    hunkpct >= !pcthunk && olinespct >= !pctmin && nlinespct >= !pctplus &&
    alllinespct >= !pct
  then
    Some (commit,output,
	  List.sort compare
	    [(!hunk_key,hunkpct);(!oline_key,olinespct);
	      (!nline_key,nlinespct);(!allline_key,alllinespct)],
	  uniquepct,unique_unmatched_words,unmatched_code)
  else None

(* ---------------------------------------------------------------------- *)
(* top result processing *)

let sim l1 l2 =
  let both = Common.inter_set l1 l2 in
  (float_of_int (List.length both)) /.
  ((sqrt(float_of_int(List.length l1))) *.
     (sqrt(float_of_int(List.length l2))))

let compare_results ((_,_,entry1,up1,_,_),_) ((_,_,entry2,up2,_,_),_) =
  match (entry1,entry2) with
    (((_,d1)::_), ((_,d2)::_)) ->
      let r1 = compare d2 d1 in
      if r1 = 0
      then compare up1 up2
      else r1
  | _ -> failwith "not possible"

let take_top n all_results =
  let rec outer seen n = function
      [] -> []
    | (((_,output,((_,d)::_),_,_,_),_) as x)::xs ->
	if n = 1
	then
	  if !Flag.top_output
	  then
	    if List.mem output seen
	    then inner seen d xs
	    else x :: (inner (output :: seen) d xs)
	  else x::(inner seen d xs)
	else
	  if !Flag.top_output
	  then
	    if List.mem output seen
	    then outer seen n xs
	    else x::(outer (output :: seen) (n-1) xs)
	  else x::(outer seen (n-1) xs)
    | _ -> failwith "invalid entry"
  and inner seen d = function
      [] -> []
    | (((_,output,((_,d1)::_),_,_,_),_) as x)::xs when d1 = d ->
	if !Flag.top_output
	then
	  if List.mem output seen
	  then inner seen d xs
	  else x :: inner (output::seen) d xs
	else x::(inner seen d xs)
    | _ -> [] in
  if n = 0
  then []
  else outer [] n all_results

let take_first_output all_results =
  let rec loop = function
      [] -> []
    | (((_,output,_,_,_,_),_) as x) :: xs ->
	let xs =
	  List.filter
	    (function ((_,output1,_,_,_,_),_) -> not (output = output1))
	    xs in
	x :: loop xs in
  loop all_results

let ignore = ref []

let drop_ignored_files lines =
  if !ignore = []
  then (false,lines)
  else
    let something_ignored = ref false in
    let rec loop acc = function
        [] -> List.rev acc
      | x::xs ->
          match Str.split (Str.regexp " ") x with
            ["diff";"--git";f1;f2] ->
              if List.mem f1 !ignore
              then
                begin
                  something_ignored := true;
                  loop ("IGNORED"::x::acc) (skip xs)
                end
              else loop (x :: acc) xs
          | _ -> loop (x :: acc) xs
    and skip = function
        [] -> []
      | x::xs ->
          match Str.split (Str.regexp " ") x with
            ["diff";"--git";f1;f2] -> x::xs
          | _ -> skip xs in
    let res = loop [] lines in
    (!something_ignored,res)

type line = Commit of string | Diff | IDiff

let structured_diff_file lines wanted =
  let add acc = function (_,0) -> acc | i -> i :: acc in
  let rec loop keep allacc flag acc wanted = function
      [] -> List.rev (add allacc (flag,acc))
    | x::xs ->
	  match Str.split (Str.regexp " ") x with
	    ["commit";id] ->
	      if wanted = [id]
	      then List.rev (add (add allacc (flag,acc)) (Commit id,1))
	      else
		let wanted = List.filter (fun x -> not(x=id)) wanted in
		loop true (add allacc (flag,acc)) (Commit id) 1 wanted xs
	  | ["diff";"--git";f1;f2] ->
	      if List.mem f1 !ignore
	      then loop false (add allacc (flag,acc)) IDiff 1 wanted xs
	      else loop true (add allacc (flag,acc)) Diff 1 wanted xs
	  | _ ->
	      loop keep allacc flag (if keep then 1+acc else acc) wanted xs in
  loop true [] IDiff 0 wanted lines

let drop_git_ignored_files lines allresults =
  let structured = structured_diff_file lines allresults in
  let rec loop prior_commits prior_lines prior_diff_lines record = function
      [] -> record
    | ((Commit c),ct)::xs ->
	let rec iloop = function
	    [] -> Common.Right []
	  | (IDiff,ct)::xs -> iloop xs
	  | (((Commit _),ct)::_) as xs -> Common.Left xs
	  | l -> Common.Right l in
	(match iloop xs with
	  Common.Left l ->
	    loop prior_commits prior_lines prior_diff_lines record l
	| Common.Right l ->
	    loop (1 + prior_commits) (ct + prior_lines) prior_diff_lines
	      ((c,(prior_commits,prior_lines,prior_diff_lines)) :: record) l)
    | (Diff,ct)::xs ->
	loop prior_commits (ct + prior_lines) (ct + prior_diff_lines)
	  record xs
    | (IDiff,ct)::xs ->
	loop prior_commits prior_lines prior_diff_lines record xs in
  loop 0 0 0 [] structured

let print_unmatched o unmatched_code =
  let printer_code str lines =
    if not(lines = [])
    then
      begin
	Printf.fprintf o "%s\n" str;
	List.iter (fun (ln,cd) -> Printf.fprintf o "%d: %s\n" ln cd)
	  lines
      end in
  let printer_lines str lines =
    if not(lines = [])
    then
      Printf.fprintf o "%s: %s\n" str
	(String.concat " " (List.map (fun (ln,_) -> string_of_int ln) lines)) in
  if !Flag.show_unmatched_code
  then
    List.iter
      (fun (file,unmatched_olines,unmatched_nlines) ->
	Printf.fprintf o "%s\n" file;
	printer_code "Expected removed lines" unmatched_olines;
	printer_code "Expected added lines" unmatched_nlines)
      unmatched_code
  else if !Flag.show_unmatched_lines
  then
    List.iter
      (fun (file,unmatched_olines,unmatched_nlines) ->
	Printf.fprintf o "%s\n" file;
	printer_lines "Expected removed lines" unmatched_olines;
	printer_lines "Expected added lines" unmatched_nlines)
      unmatched_code
  else failwith "should not reach here"

let print_top_results all_commits top all_results output_file =
  let all_results = List.sort compare_results all_results in
  let all_results =
    match top with
      None ->
	if !Flag.top_output
	then take_first_output all_results
	else all_results
    | Some n -> take_top n all_results in
  let all_results =
    List.map
      (function
	  ((((commit,_),_,((_,d)::_),_,uniquewords,unmatched_code),ind) as a) ->
	    (*let others =
	      List.filter
		(function
		    (((commit1,_),_,((_,d1)::_),_,uniquewords1,_),_) ->
		      not (commit = commit1) && d1 >= d
		  | _ -> failwith "not possible")
		all_results in
	    let other_words =
	      List.map
		(function (((commit1,_),_,_,uniquepct1,uniquewords1,_),_) ->
		  uniquewords1)
		others in*)
	    (a,(*List.fold_left (fun p x -> max p (sim uniquewords x))
	       0.0 other_words*) 0.0)
	| _ -> failwith "not possible")
      all_results in
  let record =
    match !git_compare_type with
      NoGit -> []
    | Git arg ->
	let args = "--diff-filter=M --no-merges --abbrev-commit" in
	let gitlist =
	  List.fold_left
	    (fun prev range ->
	      let cmd =
		Printf.sprintf "cd %s; git log -p %s '\\b%s\\b' %s %s"
		  !Flag.gitpath arg !git_compare_string args range in
	      Printf.eprintf "cmd: %s\n" cmd;
	      let tstart = Unix.gettimeofday() in
	      let gitlist = Common.cmd_to_list cmd in
	      let tend = Unix.gettimeofday() in
	      Printf.eprintf "git time: %0.2f\n" (tend -. tstart);
	      gitlist@prev)
	    [] !Flag.commits in
	let commits =
	  List.map (function ((((commit,_),output,entry,_,_,_),ind),_) -> commit)
	    all_results in
	drop_git_ignored_files gitlist commits in
  List.iter
    (fun ((((commit,_),output,entry,uniquepct,uniquewords,unmatched_code),ind),sim) ->
      match entry with
	((_,d)::_) ->
	  Printf.printf "%s: %d%% (%d/%d) " commit d ind all_commits;
	  (try
	    (let (prior_commits,prior_lines,prior_diff_lines) =
	      List.assoc commit record in
	    Printf.printf " prior: commits: %d, lines: %d, diff_lines %d"
	      prior_commits prior_lines prior_diff_lines)
	  with Not_found -> ());
	  Printf.printf "\n";
	  List.iter (function x -> Printf.printf "%s\n" x) output
      | _ -> failwith "bad entry")
    all_results;
  if not (output_file = "")
  then
    begin
      let o = open_out output_file in
      let home = Sys.getcwd() in
      (if not (!message = "") then Printf.fprintf o "%s\n" !message);
      Sys.chdir !Flag.gitpath;
      List.iter
	(fun ((((commit,_),output,entry,uniquepct,uniquewords,unmatched_code),ind),sim) ->
	  match entry with
	    ((_,d)::_) ->
	      let lines =
		Common.cmd_to_list (Printf.sprintf "git show %s" commit) in
	      let (something_ignored,lines) = drop_ignored_files lines in
(*	      Printf.fprintf o "* %s: %d%% %d%% (%d/%d)%s"
		commit d uniquepct ind all_commits
		(if something_ignored then " something ignored" else "");*)
	      Printf.fprintf o "* %s: %d%% (%d/%d)%s"
		commit d ind all_commits
		(if something_ignored then " something ignored" else "");
	      (try
		(let (prior_commits,prior_lines,prior_diff_lines) =
		  List.assoc commit record in
		Printf.fprintf o
		  " prior_commits %d, prior_lines %d, prior_diff_lines %d"
		  prior_commits prior_lines prior_diff_lines)
	      with Not_found -> ());
	      (match output with
		[] -> Printf.fprintf o "\n"
	      | [x] when unmatched_code = [] ->
		  Printf.fprintf o ": %s\n" x
	      | x::xs ->
		  Printf.fprintf o ": %s\n" x;
		  (if not(xs=[])
		  then
		    (Printf.fprintf o "** Text\n";
		     List.iter (function x -> Printf.fprintf o "%s\n" x) output));
		  (if not(unmatched_code=[])
		  then
		    (Printf.fprintf o "** Unmatched code\n";
		     print_unmatched o unmatched_code));
		  Printf.fprintf o "** Patch\n");
	      List.iter (function x -> Printf.fprintf o "%s\n" x) lines
	  | _ -> failwith "bad entry")
	all_results;
      Sys.chdir home;
      close_out o
    end

let call_spinfer spinfer_threshold spinfer_args (all_results : (('a * 'b * 'c * 'd * 'e * 'f) * int) list) top =
  let cmd commits spinfer_args =
    Printf.sprintf "spinfer -g %s %s -o %s" !Flag.gitpath
      (String.concat " "
	 (List.map (function x -> Printf.sprintf "-c %s" x) commits))
      spinfer_args in
  let first_round =
    let topn =
      List.filter
	(fun (((commit,_),output,entry,_,_,_),ind) ->
	  match entry with ((_,d)::_) -> d >= spinfer_threshold | _ -> false)
	all_results in
    List.map (fun (((commit,_),output,entry,_,_,_),ind) -> commit) topn in
  let cmd1 = cmd first_round ("1_"^spinfer_args) in
  Printf.eprintf "%s\n" cmd1; flush stderr;
  let _ = Sys.command cmd1 in
  if spinfer_threshold > 0
  then
    let second_round =
      let topn =
	match top with
	  None -> all_results
	| Some n -> take_top n all_results in
      List.map (fun (((commit,_),output,entry,_,_,_),ind) -> commit) topn in
    let _ = Sys.command (cmd second_round ("2_"^spinfer_args)) in
    ()
