(*
 * This file is part of Prequel, lincensed under the terms of the GPL v2.
 * See copyright.txt in the Prequel source code for more information.
 * The Prequel source code can be obtained at
 * https://github.com/prequel-pql/prequel
 *)

let cocciargs = ref ""
let gitargs = ref ""
let sp = ref ""
let split_file = ref None
let success = 0

let beforedir = ref ""
let afterdir = ref ""

let spinfer_threshold = ref 0
let spinfer_args = ref ""

(* ---------------------------------------------------------------------- *)

let top = ref None
let requirements_only = ref false
let dir = ref ""
let output_file = ref ""

let set_output_file s =
  if not (Filename.check_suffix s ".porg")
  then failwith "output extension must be porg, see prequel/scripts/porg.el"
  else output_file := s

let get_definitions s =
  let rec loop = function
      [] | [_] -> []
    | "-D"::s::rest -> s::loop rest
    | x::xs -> loop xs in
  loop (Str.split (Str.regexp "[ \t]+") s)

let options =
  [ "--git", Arg.Set_string Flag.gitpath, "  path to git repository";
    "--ref", Arg.String Flag.setup_reference_version,
    "  path or commit id of reference version";
    "--all-patches", Arg.Set Select_commits.all_patches,
    "  do no filtering";
    "--dir", Arg.Set_string dir, "  subdir of interest";
    "--sp", Arg.Set_string sp, "  semantic patch";
    "--commits", Arg.String (function s -> Flag.commits := s :: !Flag.commits),
    "  commmit range";
    "--cores", Arg.Set_int Flag.cores, "  cores available";
    "--tmp", Arg.Set_string Flag.tmp, "  temporary directory";
    "--split-file", Arg.String (fun sf -> split_file := Some sf),
    "  path name of split file";
    "--spatch", Arg.Set_string Flag.spatch, "  spatch version";
    "--suffix", Arg.String (fun s -> Flag.suffixes := [s]),
      "  suffix of relevant files";
    "--cocciargs", Arg.String (fun s -> cocciargs := !cocciargs ^ " " ^ s),
      "  arguments for spatch";
    "--gitargs", Arg.String (fun s -> gitargs := !gitargs ^ " " ^ s),
      "  arguments for git";
    "--requirements-only", Arg.Set requirements_only,
    "  only show stats on selected commits and files";
    "--cocci-file", Arg.Set_string sp, "  semantic patch";

    "--pct-minus", Arg.Set_int Check_results.pctmin,
    "  % of removed lines matched";
    "--pct-plus", Arg.Set_int Check_results.pctplus,
    "  % of added lines matched";
    "--pct", Arg.Set_int Check_results.pct,
    "  % of modified lines matched";
    "--pct-hunk", Arg.Set_int Check_results.pcthunk,
    "  % of hunks matched";
    "--hunks", Arg.Unit Check_results.upd_hunks, "  priority for hunks";
    "--old-lines", Arg.Unit Check_results.upd_old, "  priority for old lines";
    "--new-lines", Arg.Unit Check_results.upd_new, "  priority for new lines";
    "--all-lines", Arg.Unit Check_results.upd_all, "  priority for all lines";
    "--top", Arg.Int (fun n -> top := Some n), "  limit results to the best";
    "--top-output", Arg.Set Flag.top_output,
    "  give the best commit for each output sequence";

    "--debug", Arg.Set Flag.debug, "  debugging output";
    "--maxpatches", Arg.Set_int Select_commits.maxpatches,
    "  max number of patches to consider (0 for no limit, default 10 000)";
    "--reducepatches", Arg.Set_int Select_commits.reducepatches,
    "  threshold at which to try a formula with a greater FP risk (default 2000)";
    "--backport", Arg.Set Flag_split.backport,
    "  drop +, make - code +, to reverse backport";
    "-D", Arg.String (fun s -> cocciargs := !cocciargs ^ " -D " ^ s),
    "  indicate that a virtual rule should be considered to be matched";
    "--u3index", Arg.Set_string Flag.u3index, "-U3 idutils index";
    "--u0mindex", Arg.Set_string Flag.u0mindex,
    "-U0 idutils index (minus lines)";
    "--u0pindex", Arg.Set_string Flag.u0pindex,
    "-U0 idutils index (plus lines)";
    "--fileindex", Arg.Set_string Flag.fileindex, "idutils file index";
    "--spinfer-threshold", Arg.Set_int spinfer_threshold,
    "minimum pct for first try with spinfer";
    "--spinfer-args", Arg.Set_string spinfer_args,
    "arguments for spinfer";
    "-o", Arg.String set_output_file, "patch output file (org mode)";
    "--ignore",
    Arg.String
      (fun s -> Check_results.ignore := ("a/"^s) :: !Check_results.ignore),
    "files to ignore";
    "--gitgcompare",  Arg.String Check_results.gitgcompare,
    "compare with git log -G";
    "--gitscompare",  Arg.String Check_results.gitscompare,
    "compare with git log -S";
    "--msg",  Arg.Set_string Check_results.message,
    "message for porg file";
    "--line-diff", Arg.Unit (fun _ -> Flag.difftype := Flag.LINE),
    "line diff for identifying hunks";
    "--word-diff", Arg.Unit (fun _ -> Flag.difftype := Flag.WORD),
    "word diff for identifying hunks";
    "--uniform", Arg.Unit (fun _ -> Flag_split.uniform := true),
    "metavars must match a single hunk";
    "--nonuniform", Arg.Unit (fun _ -> Flag_split.uniform := false),
    "metavars can span hunks";
    "--all-at-once", Arg.Set Flag.all_at_once,
    "all files in a commit considered at once, regardless of whether change relevant";
    "--show-unmatched-lines", Arg.Set Flag.show_unmatched_lines,
    "show unmatched line numbers";
    "--show-unmatched-code", Arg.Set Flag.show_unmatched_code,
    "show unmatched line numbers and the relevant code";
  ]

let anonymous s =
  if Filename.check_suffix s ".cocci"
  then sp := s
  else
    match Str.split (Str.regexp_string "..") s with
      [starter;ender] -> Flag.commits := s :: !Flag.commits
    | _ -> Flag.gitpath := s

let usage = ""

let _ =
  Random.self_init();
  let path = Filename.dirname(Array.get Sys.argv 0) in
  Flag.u3index := Printf.sprintf "%s/../indices/linux_idutils_U3.index" path;
  Flag.u0mindex :=
    Printf.sprintf "%s/../indices/linux_idutils_minus_U0.index" path;
  Flag.u0pindex :=
    Printf.sprintf "%s/../indices/linux_idutils_plus_U0.index" path;
  Flag.fileindex :=
    Printf.sprintf "%s/../indices/linux_idutils_files.index" path;
  Arg.parse (Arg.align options) anonymous usage;
  (* for benchmarking - do not commit
  Flag.cores := 1;
  cocciargs := "--temp-files /dev/shm " ^ !cocciargs;
  Flag.tmp := "/dev/shm/julia";
  end for benchmarking - do not commit *)
  (* hack to allow concurrency for git cases *)
  (if not (!Check_results.git_compare_type = Check_results.NoGit)
  then Flag.tmp := Filename.temp_file "prequel" "t1" (*
  else
    (spinfer_threshold := 33;
     spinfer_args :=
       Printf.sprintf "%s_spi.cocci" (Filename.chop_extension !sp))*));
  List.iter Flag.set_defined_virtual_rules (get_definitions !cocciargs);
  beforedir := Printf.sprintf "%s/before" !Flag.tmp;
  afterdir := Printf.sprintf "%s/after" !Flag.tmp;
  let spatch = Setup.global_setup() in
  (if !Flag.commits = [] then failwith "coccipatch: commit range required");
  (if !sp = "" then failwith "coccipatch: semantic patch required");
  let (sp,csp) = Setup.do_split !sp !split_file in
  let commits = Select_commits.get_requirements csp !dir !gitargs in
  let allcommits = List.length commits in
  Printf.eprintf "commits %d files %d\n" allcommits
      (List.fold_left (+) 0
	 (List.map (function (_,(_,Some x)) -> List.length x | _ -> 0)
	    commits)); flush stderr;
  if !requirements_only
  then
    let start = if !cocciargs = "" then "" else !cocciargs ^ ": " in
    Printf.printf "%scommits %d files %d\n" start allcommits
      (List.fold_left (+) 0
	 (List.map (function (_,(_,Some x)) -> List.length x | _ -> 0)
	    commits))
  else
    (let all_results =
      let index = ref 1 in
      let mapi f l =
	if !Flag.cores = 1
	then Common.mapi f l
	else
	  let record_num i = index := i in
	  Parmap.parmapi ~init:record_num ~ncores:!Flag.cores ~chunksize:1 f
	    (Parmap.L l) in
      let restart dir =
	let _ =
	  Sys.command (Printf.sprintf "/bin/rm -rf %s; mkdir -p %s" dir dir) in
	dir in
      mapi
	(fun i (ind,commit) ->
	  let index = if !Flag.debug then i else !index in
	  let beforedir = restart(Printf.sprintf "%s/%d" !beforedir index) in
	  let afterdir = restart(Printf.sprintf "%s/%d" !afterdir index) in
	  let hunk_file = Printf.sprintf "%s/hunk_file" beforedir in
	  let file_tbl = Hashtbl.create 101 in
	  let tm =
	    Select_commits.setup commit beforedir afterdir hunk_file
	      file_tbl in
	  if Hashtbl.length file_tbl = 0 (* no relevant files *)
	  then (None,ind,tm,0.0)
	  else
	    let (output,runtime) =
	      Run_cocci.run_cocci spatch sp ind commit hunk_file
		beforedir afterdir file_tbl !cocciargs allcommits in
	    (Check_results.check_results commit file_tbl output,ind,
	     tm,runtime))
	commits in
    let third (c,i,t,r) = t in
    let fourth (c,i,t,r) = r in
    let setup_time = List.fold_left (+.) 0.0 (List.map third all_results) in
    let spatch_time = List.fold_left (+.) 0.0 (List.map fourth all_results) in
    Printf.eprintf "setup time: %0.2f\n" setup_time;
    Printf.eprintf "spatch time: %0.2f\n" spatch_time;
    let final_results =
      List.fold_left
	(function prev ->
	  function (None,_,_,_) -> prev | ((Some x),i,_,_) -> (x,i+1) :: prev)
	[] all_results in
    Check_results.print_top_results allcommits !top final_results !output_file;
    if not (!spinfer_threshold = 0) || not (!spinfer_args = "")
    then
      Check_results.call_spinfer !spinfer_threshold !spinfer_args final_results
	!top);
  Flag.clean_reference_version()
