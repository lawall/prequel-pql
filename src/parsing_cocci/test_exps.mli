(*
 * This file is part of Prequel, lincensed under the terms of the GPL v2.
 * See copyright.txt in the Prequel source code for more information.
 * The Prequel source code can be obtained at
 * https://github.com/prequel-pql/prequel
 *)

val process : Ast0_cocci.rule -> Ast0_cocci.rule

val process_anything : Ast0_cocci.anything -> Ast0_cocci.anything
