(*
 * This file is part of Prequel, lincensed under the terms of the GPL v2.
 * See copyright.txt in the Prequel source code for more information.
 * The Prequel source code can be obtained at
 * https://github.com/prequel-pql/prequel
 *)

val plus :
    Ast_cocci.rule ->
      (Ast_cocci.anything * int * int * int * int) list list list
