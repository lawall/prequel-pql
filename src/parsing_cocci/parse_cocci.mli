(*
 * This file is part of Prequel, lincensed under the terms of the GPL v2.
 * See copyright.txt in the Prequel source code for more information.
 * The Prequel source code can be obtained at
 * https://github.com/prequel-pql/prequel
 *)

exception Bad_virt of string

val parse : string -> string list (* implicit virtual rules *) ->
    (string, string) Common.either Common.set (* iso files *) *
    Ast0_cocci.parsed_rule list (* rules *) *
    string list (* virtuals *) *
    Ast_cocci.metavar list (* metavariables *)
