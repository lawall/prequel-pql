(*
 * This file is part of Prequel, lincensed under the terms of the GPL v2.
 * See copyright.txt in the Prequel source code for more information.
 * The Prequel source code can be obtained at
 * https://github.com/prequel-pql/prequel
 *)

(* For each rule return the list of variables that are used after it.
Also augment various parts of each rule with unitary, inherited, and freshness
informations *)

(* metavar decls should be better integrated into computations of free
variables in plus code *)

module Ast = Ast_cocci
module V = Visitor_ast
module TC = Type_cocci

(* position variables that appear as a constraint on another position variable.
a position variable also cannot appear both positively and negatively in a
single rule. *)

let get_neg_pos_list rule =
  let donothing r k e = k e in
  let bind (p1,np1) (p2,np2) =
    (Common.union_set p1 p2, Common.union_set np1 np2) in
  let option_default = ([],[]) in
  let metaid (x,_,_,_) = x in
  let get_neg_pos_constraints constraints =
    List.concat
      (List.map
	 (function
	     Ast.PosNegSet l -> l
	   | Ast.PosScript _ -> [])
	 constraints) in
  let mcode r mc =
    List.fold_left
      (function (a,b) ->
	(function
	    Ast.MetaPos(name,constraints,Ast.PER,_,_) ->
	      let constraint_vars = get_neg_pos_constraints constraints in
	      ((metaid name)::a,constraint_vars@b)
	  | Ast.MetaPos(name,constraints,Ast.ALL,_,_) ->
	      let constraint_vars = get_neg_pos_constraints constraints in
	      ((metaid name)::a,constraint_vars@b)))
      option_default (Ast.get_pos_var mc) in
  let v =
    V.combiner bind option_default
    mcode mcode mcode mcode mcode mcode mcode mcode mcode mcode mcode mcode
    mcode mcode
    donothing donothing donothing donothing donothing donothing donothing
    donothing donothing donothing donothing donothing donothing donothing
    donothing donothing donothing donothing donothing donothing
    donothing donothing in
  match rule with
    Ast.CocciRule(_,_,minirules,_,_) ->
      List.map
	(function toplevel ->
	  let (positions,neg_positions) = v.V.combiner_top_level toplevel in
	  (if List.exists (function p -> List.mem p neg_positions) positions
	  then
	    failwith
	      "a variable cannot be used both as a position and a constraint");
	  neg_positions)
	minirules
  | Ast.ScriptRule _ | Ast.InitialScriptRule _ | Ast.FinalScriptRule _ ->
      (*no negated positions*) []

let free_vars rules = List.map get_neg_pos_list rules
