(*
 * This file is part of Prequel, lincensed under the terms of the GPL v2.
 * See copyright.txt in the Prequel source code for more information.
 * The Prequel source code can be obtained at
 * https://github.com/prequel-pql/prequel
 *)

(*
ModifiedCase: there are modified tokens to search for
UnmodifiedCase: there are unmodified tokens to search for near modified ones
FileModifiedCase: there are unmodified tokens to search for somewhere, and
the file is modified.
FileCase: there are unmodified tokens to search for somewhere, but the file is
not modified.
*)

type cases = ModifiedCase | UnmodifiedCase

type color = Before | After

type tok =
    Op of string (* frontier as is*) | Tok of string (* frontier is \b *)
  | Kwd of string (* frontier is \b, not worth promoting to File *)
type combine =
    And of combine list | Or of combine list | Alpha of combine | False | True
  | Modified of color * Ast_cocci.info * tok option
  | Unmodified of color * Ast_cocci.info * tok
  | File of string

val dep2c : combine -> string

val build_and : combine -> combine -> combine
val build_or  : combine -> combine -> combine

(* The result is a big or of the ways we can get some information out of the
rule.  Each disjunct is a dnf. *)
val get_constants :
    Ast_cocci.rule list ->
      (((Ast_cocci.meta_name list) list) list) (*negated pos vars*) ->
	(int * ((cases * combine) * (cases * combine))) list
