(*
 * This file is part of Prequel, lincensed under the terms of the GPL v2.
 * See copyright.txt in the Prequel source code for more information.
 * The Prequel source code can be obtained at
 * https://github.com/prequel-pql/prequel
 *)

val compute_lines : bool -> Ast0_cocci.rule -> Ast0_cocci.rule

val compute_statement_dots_lines : bool ->
    Ast0_cocci.statement Ast0_cocci.dots ->
      Ast0_cocci.statement Ast0_cocci.dots

val compute_statement_lines :
    bool -> Ast0_cocci.statement -> Ast0_cocci.statement
