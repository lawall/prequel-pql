(*
 * This file is part of Prequel, lincensed under the terms of the GPL v2.
 * See copyright.txt in the Prequel source code for more information.
 * The Prequel source code can be obtained at
 * https://github.com/prequel-pql/prequel
 *)

(* Unlike the normal get_constants, this one indicates whether the constants
are minus *)

module Ast = Ast_cocci
module V = Visitor_ast
module TC = Type_cocci

type cases = ModifiedCase | UnmodifiedCase

let prequeldep = ["m";"jm";"p";"jp"]

(* Issues:

1.  If a rule X depends on a rule Y (in a positive way), then we can ignore
    the constants in X.

2.  If a rule X contains a metavariable that is not under a disjunction and
    that is inherited from rule Y, then we can ignore the constants in X.

3.  If a rule contains a constant x in + code then subsequent rules that
    have it in - or context should not include it in their list of required
    constants.
*)

(* This doesn't do the . -> trick of get_constants for record fields, as
    that does not fit well with the recursive structure.  It was not clear
    that that was completely safe either, although eg putting a newline
    after the . or -> is probably unusual. *)

(* ----------------------------------------------------------------------- *)
(* This phase collects everything.  One can then filter out what it not
wanted *)

(* True means nothing was found
   False should never drift to the top, it is the neutral element of or
   and an or is never empty *)
(* Within a run, the color is always the same.  Useful for using build_and etc
afterwards on hybrid terms. *)
type color = Before | After
type tok =
    Op of string (* frontier as is*) | Tok of string (* frontier is \b *)
  | Kwd of string (* frontier is \b, not worth promoting to File *)
type combine =
    And of combine list | Or of combine list | Alpha of combine | False | True
  | Modified of color * Ast.info * tok option
  | Unmodified of color * Ast.info * tok
  | File of string

let rec dep2c = function
    And l -> Printf.sprintf "(%s)" (String.concat "&" (List.map dep2c l))
  | Or l -> Printf.sprintf "(%s)" (String.concat "|" (List.map dep2c l))
  | Alpha x -> Printf.sprintf "alpha(%s)" (dep2c x)
  | False -> "false"
  | True -> "true"
  | Modified (_,info,None) -> Printf.sprintf "M?-%d" info.Ast.adjacency
  | Modified (_,info,Some x) -> "M-"^(match x with Tok s | Kwd s | Op s -> s)
  | Unmodified (_,info,x)    ->
      let t = match x with Tok s | Kwd s | Op s -> s in
      Printf.sprintf "U-%d-%s" info.Ast.adjacency t
  | File s -> "F-"^s

let case2c = function
    ModifiedCase -> "ModifiedCase"
  | UnmodifiedCase -> "UnmodifiedCase"

let norm = function
    And l -> And (List.sort compare l)
  | Or l  -> Or  (List.sort compare l)
  | x -> x

let rec merge l1 l2 =
  match (l1,l2) with
    ([],l2) -> l2
  | (l1,[]) -> l1
  | (x::xs,y::ys) ->
      (match compare x y with
	-1 -> x::(merge xs l2)
      |	0  -> x::(merge xs ys)
      |	1  -> y::(merge l1 ys)
      |	_ -> failwith "not possible")

let intersect l1 l2 = List.filter (function l1e -> List.mem l1e l2) l1
let subset l1 l2 = List.for_all (function x -> List.mem x l2) l1
let minus_set l1 l2 = List.filter (function l1e -> not (List.mem l1e l2)) l1

let rec insert x l = merge [x] l

let better x y = (* x >= y *)
  x = y ||
  (match (x,y) with
    (Modified(_,info1,Some s1),Modified(_,info2,None)) ->
      info1.Ast.adjacency = info2.Ast.adjacency
  | _ -> false)

let worse x y = (* x <= y *)
  x = y ||
  (match (x,y) with
    (Modified(_,info1,None),Modified(_,info2,Some s1)) ->
      info1.Ast.adjacency = info2.Ast.adjacency
  | _ -> false)

let subset_better l1 l2 =
  List.for_all (function x -> List.exists (better x) l2) l1

let subset_worse l1 l2 =
  List.for_all (function x -> List.exists (worse x) l2) l1

let rec build_and x y =
  if better x y
  then x
  else
  if better y x
  then y
  else
    let and_attempt l x =
      let attempt =
	let rec loop acc = function
	    [] -> None
	  | y::ys ->
	      match build_and x y with
		And _ -> loop (y::acc) ys
	      | res -> Some (res,(List.rev acc)@ys) in
	loop [] l in
      (match attempt with
	Some (res,others) -> insert res others
      | None -> insert x l) in
    match (x,y) with
      (Alpha x1,Alpha x2) -> Alpha(build_or x1 x2)
    | (Alpha _,x) | (x,Alpha _) -> x
    | (False,x) | (x,False) -> False
    | (True,x) | (x,True) -> x
    | (x,Or l) when List.exists (better x) l -> x
    | (Or l,x) when List.exists (better x) l -> x
    | (Or l1,Or l2) when subset_better l1 l2 -> Or l1
    | (Or l1,Or l2) when subset_better l2 l1 -> Or l2
    | (Or l1,Or l2) when not ((intersect l1 l2) = []) ->
	let m1 = minus_set l1 l2 in
	let m2 = minus_set l2 l1 in
	let inner =
	  build_and
	    (List.fold_left build_or (List.hd m1) (List.tl m1))
	    (List.fold_left build_or (List.hd m2) (List.tl m2)) in
	List.fold_left build_or inner (intersect l1 l2)
    | (And l1,And l2) -> And(List.fold_left and_attempt l1 l2)
    | (x,And l) | (And l,x) -> And(and_attempt l x)
    | (x,y) -> norm(And [x;y])

and build_or x y =
  if worse x y
  then x
  else
  if worse y x
  then y
  else
    let or_attempt l x =
      let attempt =
	let rec loop acc = function
	    [] -> None
	  | y::ys ->
	      match build_or x y with
		Or _ -> loop (y::acc) ys
	      | res -> Some (res,(List.rev acc)@ys) in
	loop [] l in
      (match attempt with
	Some (res,others) -> insert res others
      | None -> insert x l) in
    match (x,y) with
      (Alpha x1,Alpha x2) -> Alpha(build_or x1 x2)
    | (Alpha x1,x2) | (x1,Alpha x2) -> Alpha(build_or x1 x2)
    | (True,x) | (x,True) -> True
    | (False,x) | (x,False) -> x
    | (x,And l) when List.exists (worse x) l -> x
    | (And l,x) when List.exists (worse x) l -> x
    | (And l1,And l2) when subset_worse l1 l2 -> And l1
    | (And l1,And l2) when subset_worse l2 l1 -> And l2
    | (And l1,And l2) when not ((intersect l1 l2) = []) ->
	let m1 = minus_set l1 l2 in
	let m2 = minus_set l2 l1 in
	let inner =
	  build_or
	    (List.fold_left build_and (List.hd m1) (List.tl m1))
	    (List.fold_left build_and (List.hd m2) (List.tl m2)) in
	List.fold_left build_and inner (intersect l1 l2)
    | (Or l1,Or l2) -> Or(List.fold_left or_attempt l1 l2)
    | (x,Or l) | (Or l,x) -> Or(or_attempt l x)
    | (x,y) -> norm(Or [x;y])

type impt = SameModified | NotModified | Ignored

let important = function
    [] -> NotModified
  | pos ->
      let infos =
	List.map
	  (function
	      Ast.MetaPos(nm,_,_,_,false) ->
		let (_,nm) = Ast.unwrap_mcode nm in
		(match Str.split (Str.regexp "__") nm with
		  front::_::_ ->
		    if List.mem front prequeldep (* very unsafe *)
		    then SameModified
		    else Ignored
		| _ -> NotModified)
	    | _ -> NotModified)
	  pos in
      if List.mem SameModified infos then SameModified
      else if List.mem NotModified infos then NotModified
      else Ignored

let constants color (_,info,_,pos) x =
  match important pos with
    SameModified -> Modified(color,info,Some x)
  | NotModified -> Unmodified(color,info,x)
  | Ignored -> True

let get_name_constants self env nm =
  if self = nm
  then True (* not inherited *)
  else
    let names =
      let expanded =
	match Str.split (Str.regexp "_") nm with
	  "before"::rest -> [nm;String.concat "_" ("after"::rest)]
	| "after"::rest  -> [nm;String.concat "_" ("before"::rest)]
	| _ -> [nm] in
      if List.mem self expanded (* not super efficient... *)
      then [nm] (* don't want to expand if in bef/aft rules themselves *)
      else expanded in
    (try
      let infos = List.map (function nm -> List.assoc nm env) names in
      List.fold_left build_and (List.hd infos) (List.tl infos)
    with Not_found -> False)

let do_get_constants color self env neg_pos =
  let donothing r k e = k e in
  let option_default = True in
  let bad_default = False in
  let bind = build_and in
  let inherited ((nm1,_) as x) =
    (* ignore virtuals, can never match *)
    if nm1 = "virtual" then bad_default
    (* perhaps inherited, but value not required, so no constraints *)
    else if List.mem x neg_pos then option_default
    else get_name_constants self env nm1 in
  let minherited name = inherited (Ast.unwrap_mcode name) in
  let mcode r ((_,info,_,pos) as x) =
    List.fold_left bind
      (if important pos = SameModified
      then Modified(color,info,None)
      else option_default)
      (List.map
	 (function Ast.MetaPos(name,constraints,_,keep,inh) -> minherited name)
	 (Ast.get_pos_var x)) in

  let rec type_collect res = function
      TC.ConstVol(_,ty) | TC.Pointer(ty) | TC.FunctionPointer(ty)
    | TC.Array(ty) -> type_collect res ty
    | TC.Decimal _ -> build_or res (File "decimal")
    | TC.MetaType(tyname,_,_) ->
	build_or res (inherited tyname)
    | TC.TypeName(s) -> build_or res (File s)
    | TC.EnumName(TC.Name s) -> build_or res (File s)
    | TC.StructUnionName(_,TC.Name s) -> build_or res (File s)
    | ty -> option_default in

  (* if one branch gives no information, then we have to take anything *)
  (* things with disj or nest, at least <... ...> case can't be under bind *)
  let disj_union_all = List.fold_left build_or False in

  let ident r k i =
    match Ast.unwrap i with
      Ast.Id(name) ->
	bind (k i)
	  (match Ast.unwrap_mcode name with
	    "NULL" -> constants color name (Kwd "NULL")
	  | nm -> constants color name (Tok nm))
    | Ast.MetaId(name,Ast.IdPosIdSet(strs,mids),_,_)
    | Ast.MetaFunc(name,Ast.IdPosIdSet(strs,mids),_,_)
    | Ast.MetaLocalFunc(name,Ast.IdPosIdSet(strs,mids),_,_) ->
	let cur =
	  build_or
	    (disj_union_all
	       (List.map (fun x -> constants color name (Tok x)) strs))
	    (disj_union_all (List.map inherited mids)) in
	bind (k i) (bind (minherited name) cur)
    | Ast.MetaId(name,_,_,_) | Ast.MetaFunc(name,_,_,_)
    | Ast.MetaLocalFunc(name,_,_,_) ->
	bind (k i) (minherited name)
    | Ast.DisjId(ids) ->
	(* no bind (k i) here!!! would & combine *)
	disj_union_all (List.map r.V.combiner_ident ids)
    | _ -> k i in

  (* no point to do anything special for records because glimpse is
     word-oriented *)
  let expression r k e =
    match Ast.unwrap e with
      Ast.DisjExpr(exps) ->
	disj_union_all (List.map r.V.combiner_expression exps)
    | Ast.NestExpr(starter,expr_dots,ender,wc,false) ->
	Alpha (r.V.combiner_expression_dots expr_dots)
    | Ast.NestExpr(starter,expr_dots,ender,wc,true) ->
	r.V.combiner_expression_dots expr_dots
    | _ ->
	bind (k e)
	  (match Ast.unwrap e with
	    Ast.Constant(const) ->
	      (match Ast.unwrap_mcode const with
		Ast.Int s ->
		  if List.mem s ["0";"0x0";"1";"-1"]
		  then option_default (* too common to stand alone *)
		  else constants color const (Kwd s)
	      | _ -> option_default)
	  | Ast.MetaErr(name,_,_,_) | Ast.MetaExpr(name,_,_,None,_,_) ->
	      minherited name
	  | Ast.MetaExpr(name,_,_,Some type_list,_,_) ->
	      (* if types is empty, then can't match... None is empty case *)
	      let types = List.fold_left type_collect bad_default type_list in
	      bind (minherited name) types
	  | Ast.MetaExprList(name,Ast.MetaListLen (lenname,_,_),_,_) ->
	      bind (minherited name) (minherited lenname)
	  | Ast.MetaExprList(name,_,_,_) -> minherited name
	  | Ast.SizeOfExpr(sizeof,exp) ->
	      constants color sizeof (Kwd "sizeof")
	  | Ast.SizeOfType(sizeof,lp,ty,rp) ->
	      constants color sizeof (Kwd "sizeof")
	  | Ast.Postfix(exp,op) | Ast.Infix(exp,op) ->
	      let fixops op =
		match Ast.unwrap_mcode op with
		  Ast.Dec -> "--"
		| Ast.Inc -> "++" in
	      constants color op (Op (fixops op))
	  | Ast.Unary(exp,uop) ->
	      let uops op =
		match Ast.unwrap_mcode op with
		  Ast.GetRef -> "&"
		| Ast.GetRefLabel -> "&&"
		| Ast.DeRef -> "*"
		| Ast.UnPlus -> "+"
		| Ast.UnMinus -> "-"
		| Ast.Tilde -> "~"
		| Ast.Not -> "!" in
	      constants color uop (Op (uops uop))
	  | Ast.Binary(exp,bop,exp2)
	  | Ast.Nested(exp,bop,exp2) ->
	  (* could also do assignments *)
	      let arithops op =
		match Ast.unwrap_mcode op with
		  Ast.Plus -> "+"
		| Ast.Minus -> "-"
		| Ast.Mul -> "*"
		| Ast.Div -> "/"
		| Ast.Min -> "<?"
		| Ast.Max -> ">?"
		| Ast.Mod -> "%"
		| Ast.DecLeft -> "<<"
		| Ast.DecRight -> ">>"
		| Ast.And -> "&"
		| Ast.Or -> "|"
		| Ast.Xor -> "^" in
	      let logops op =
		match Ast.unwrap_mcode op with
		  Ast.Inf -> "<"
		| Ast.Sup -> ">"
		| Ast.InfEq -> "<="
		| Ast.SupEq -> ">="
		| Ast.Eq -> "=="
		| Ast.NotEq -> "!="
		| Ast.AndLog -> "&&"
		| Ast.OrLog -> "||" in
	      let rec bloop mcode (*mcode of metavar*) bop =
		match Ast.unwrap bop with
		  Ast.Arith(aop) ->
		    constants color mcode (Op (arithops aop))
		| Ast.Logical(lop) ->
		    constants color mcode (Op (logops lop))
		| Ast.MetaBinary(metavar,Ast.BinaryOpNoConstraint,_,_) ->
		    option_default
		| Ast.MetaBinary(metavar,Ast.BinaryOpInSet l,_,_) ->
		    disj_union_all (List.map (bloop mcode) l) in
	      (match Ast.unwrap bop with
		Ast.Arith(aop) ->
		  constants color aop (Op (arithops aop))
	      | Ast.Logical(lop) ->
		  constants color lop (Op (logops lop))
	      | Ast.MetaBinary(metavar,Ast.BinaryOpNoConstraint,_,_) ->
		  option_default
	      | Ast.MetaBinary(metavar,Ast.BinaryOpInSet l,_,_) ->
		  disj_union_all (List.map (bloop metavar) l))
	  | _ -> option_default) in
  
  (* cases for metavariables *)
  let string_fragment r k ft =
    bind (k ft) (* no disj *)
      (match Ast.unwrap ft with
	Ast.MetaFormatList(pct,name,Ast.MetaListLen (lenname,_,_),_,_) ->
	  bind (minherited name) (minherited lenname)
      | Ast.MetaFormatList(pct,name,_,_,_) -> minherited name
      | _ -> option_default) in
  
  let string_format r k ft =
    bind (k ft)
      (match Ast.unwrap ft with
	Ast.MetaFormat(name,_,_,_) -> minherited name
      | _ -> option_default) in
  
  let fullType r k ft =
    match Ast.unwrap ft with
      Ast.DisjType(decls) ->
	disj_union_all (List.map r.V.combiner_fullType decls)
    | _ -> k ft in
  
  let typeC r k ty =
    bind (k ty)
      (match Ast.unwrap ty with
	Ast.BaseType(ty1,strings) ->
	  List.fold_left
	    (fun prev x ->
	      build_and (constants color x (Kwd (Ast.unwrap_mcode x))) prev)
	    True strings
      | Ast.TypeName(name) ->
	  constants color name (Tok (Ast.unwrap_mcode name))
      | Ast.MetaType(name,_,_) -> minherited name
      | Ast.Pointer(ty,pt) -> constants color pt (Op "*")
      | _ -> option_default) in

  let declaration r k d =
    match Ast.unwrap d with
      Ast.MetaDecl(name,_,_) | Ast.MetaField(name,_,_) ->
	bind (k d) (minherited name)
    | Ast.MetaFieldList(name,Ast.MetaListLen(lenname,_,_),_,_) ->
	bind (k d) (bind (minherited name) (minherited lenname))
    | Ast.DisjDecl(decls) ->
	disj_union_all (List.map r.V.combiner_declaration decls)
    | _ -> k d in

  let parameter r k p =
    bind (k p)
      (match Ast.unwrap p with
	Ast.MetaParam(name,_,_) -> minherited name
      | Ast.MetaParamList(name,Ast.MetaListLen(lenname,_,_),_,_) ->
	  bind (minherited name) (minherited lenname)
      | Ast.MetaParamList(name,_,_,_) -> minherited name
      | _ -> option_default) in

  let rule_elem r k re =
    match Ast.unwrap re with
      Ast.DisjRuleElem(res) ->
	disj_union_all (List.map r.V.combiner_rule_elem res)
    | _ ->
	bind (k re)
	  (match Ast.unwrap re with
	    Ast.MetaStmtList(name,Ast.MetaListLen (lenname,_,_),_,_) ->
	      bind (minherited name) (minherited lenname)
	  | Ast.MetaRuleElem(name,_,_) | Ast.MetaStmt(name,_,_,_)
	  | Ast.MetaStmtList(name,_,_,_) -> minherited name
	  | Ast.IfHeader(iff,lp,exp,rp) -> constants color iff (Kwd "if")
	  | Ast.Else(els) -> constants color els (Kwd "else")
	  | Ast.WhileHeader(whl,lp,exp,rp) ->
	      constants color whl (Kwd "while")
	  | Ast.WhileTail(whl,lp,exp,rp,sem) ->
	      constants color whl (Kwd "while")
	  | Ast.ForHeader(fr,lp,first,e2,sem2,e3,rp) ->
	      constants color fr (Kwd "for")
	  | Ast.SwitchHeader(switch,lp,exp,rp) ->
	      constants color switch (Kwd "switch")
	  | Ast.Break(br,sem) -> constants color br (Kwd "break")
	  | Ast.Continue(cont,sem) ->
	      constants color cont (Kwd "continue")
	  | Ast.Goto(g,i,_) -> constants color g (Kwd "goto")
	  | Ast.Default(def,colon) -> constants color def (Kwd "default")
	  | Ast.Include(inc,s) ->
	      (match Ast.unwrap_mcode s with
		Ast.AnyInc -> True
	      | Ast.Local l | Ast.NonLocal l ->
		  let strings =
		    List.fold_left
		      (function prev ->
			function
			(* just take the last thing, probably the most
			   specific.  everything is necessary anyway.
			   Drop extension, which poses problems for prequel
			   parser *)
			    Ast.IncPath path ->
			      let path =
				try Filename.chop_extension path
				with Invalid_argument _ -> path in
			      (constants color s (Tok path)) :: prev
			  | Ast.IncDots -> prev)
		      [] l in
		  (match strings with
		    [] -> True
		  | x::xs -> List.fold_left bind x xs))
	  | Ast.Pragma(prg,id,body) -> constants color prg (Kwd "pragma")
	  | _ -> option_default) in

  let statement r k s =
    match Ast.unwrap s with
      Ast.Disj(stmt_dots) ->
	disj_union_all (List.map r.V.combiner_statement_dots stmt_dots)
    | Ast.Nest(starter,stmt_dots,ender,whn,false,_,_) ->
	Alpha (r.V.combiner_statement_dots stmt_dots)
    | Ast.Nest(starter,stmt_dots,ender,whn,true,_,_) ->
	r.V.combiner_statement_dots stmt_dots
    | _ -> k s in

  V.combiner bind option_default
    mcode mcode mcode mcode mcode mcode mcode mcode mcode
    mcode mcode mcode mcode mcode
    donothing donothing donothing donothing donothing
    ident expression string_fragment string_format donothing donothing
    fullType typeC donothing parameter declaration donothing
    rule_elem statement donothing donothing donothing

(* ------------------------------------------------------------------------ *)

(* true means the rule should be analyzed, false means it should be ignored *)
let rec dependencies self env = function
    Ast.Dep s -> get_name_constants self env s
  | Ast.AntiDep s -> True (* not sure *)
  | Ast.EverDep s -> get_name_constants self env s
  | Ast.NeverDep s -> True (* not sure *)
  | Ast.AndDep (d1,d2) ->
      build_and (dependencies self env d1) (dependencies self env d2)
  | Ast.OrDep (d1,d2) ->
      build_or (dependencies self env d1) (dependencies self env d2)
  | Ast.FileIn _ | Ast.NotFileIn _ | Ast.NoDep -> True
  | Ast.FailDep -> False

let get_color dep =
  let rec loop = function
      Ast.Dep s      -> []
    | Ast.AntiDep s  -> []
    | Ast.EverDep s  -> []
    | Ast.NeverDep s -> []
    | Ast.AndDep (d1,d2) -> loop d1 @ loop d2
    | Ast.OrDep (d1,d2)  -> []
    | Ast.FileIn fl ->
	(match Filename.basename fl with
	  "before" -> [Before]
	| "after" -> [After]
	| _ -> [])
    | Ast.NotFileIn _ | Ast.NoDep | Ast.FailDep -> [] in
  match loop dep with
    [x] -> x
  | _ -> failwith "no file information found"

(* ------------------------------------------------------------------------ *)

let all_context =
  let bind x y = x && y in
  let option_default = true in

  let donothing recursor k e = k e in

  let process_mcodekind = function
      Ast.CONTEXT(_,Ast.NOTHING) -> true
    | _ -> false in

  let mcode r e = process_mcodekind (Ast.get_mcodekind e) in

  let end_info (_,_,_,mc) = process_mcodekind mc in

  let initialiser r k e =
    match Ast.unwrap e with
      Ast.StrInitList(all_minus,_,_,_,_) ->
	not all_minus && k e
    | _ -> k e in

  let annotated_decl decl =
    match Ast.unwrap decl with
      Ast.DElem(bef,_,_) -> bef
    | _ -> failwith "not possible" in

  let rule_elem r k e =
    match Ast.unwrap e with
      Ast.FunHeader(bef,_,_,_,_,_,_,_) -> bind (process_mcodekind bef) (k e)
    | Ast.Decl decl ->
	bind (process_mcodekind (annotated_decl decl)) (k e)
    | Ast.ForHeader(fr,lp,Ast.ForDecl(decl),e2,sem2,e3,rp) ->
	bind (process_mcodekind (annotated_decl decl)) (k e)
    | _ -> k e in

  let statement r k e =
    match Ast.unwrap e with
      Ast.IfThen(_,_,ei) | Ast.IfThenElse(_,_,_,_,ei)
    | Ast.While(_,_,ei)  | Ast.For(_,_,ei)
    | Ast.Iterator(_,_,ei) | Ast.FunDecl(_,_,_,_,ei) ->
	bind (k e) (end_info ei)
    | _ -> k e in

  V.combiner bind option_default
    mcode mcode mcode mcode mcode mcode mcode mcode mcode
    mcode mcode mcode mcode mcode
    donothing donothing donothing donothing donothing donothing donothing
    donothing donothing donothing donothing donothing donothing
    initialiser donothing donothing donothing rule_elem statement
    donothing donothing donothing

(* ------------------------------------------------------------------------ *)

(* The whole "in_plus" idea is flawed.  If something is added in one rule and
matched in a later one, we want to include files that originally contain
the thing, so no point to keep track of what is added by earlier rules.
The situation is something like a -> b v (b & c).  We don't actually need
both b and c, but if we don't have b, then the only way that we can get it is
fro the first rule matching, in which case the formula is already true. *)
let rule_fn nm tls env neg_pos color =
  (* tls seems like it is supposed to relate to multiple minirules.  If we
     were to actually allow that, then the following could be inefficient,
     because it could run sat on the same rule name (x) more than once. *)
  List.fold_left
    (function rest_info ->
      function (cur,neg_pos) ->
	let minuses =
	  let getter = do_get_constants color nm env neg_pos in
	  getter.V.combiner_top_level cur in
	(* the following is for eg -foo(2) +foo(x) then in another rule
	   -foo(10); don't want to consider that foo is guaranteed to be
	   created by the rule.  not sure this works completely: what if foo is
	   in both - and +, but in an or, so the cases aren't related?
	   not sure this whole thing is a good idea.  how do we know that
	   something that is only in plus is really freshly created? *)
	(*let was_bot = minuses = True in*)
	(* perhaps it should be build_and here?  we don't really have multiple
	   minirules anymore anyway. *)
	match minuses with
	  True -> True
	| x -> build_or x rest_info)
    False (List.combine tls neg_pos)

let debug_deps nm deps res =
  if !Flag_parsing_cocci.debug_parse_cocci
  then
    begin
      Printf.fprintf stderr "Rule: %s\n" nm;
      Printf.fprintf stderr "Dependecies: %s\n"
	(Common.format_to_string
	   (function _ -> Pretty_print_cocci.dep true deps));
      Printf.fprintf stderr "Result: %s\n\n" (dep2c res)
    end

let run rules neg_pos_vars =
  let build_named_or nm x y =  (nm,x)::y in
  let (info,_) =
    List.fold_left
      (function (rest_info,env) ->
        function
	    (Ast.ScriptRule (nm,_,deps,mv,_,_),_) ->
	      let extra_deps =
		List.fold_left
		  (function prev ->
		    function
			(_,("virtual",_),_,_) -> prev
                      | (_,(rule,_),_,Ast.NoMVInit) ->
                          Ast.AndDep (Ast.Dep rule,prev)
                      | (_,(rule,_),_,_) ->
                          (* default initializer, so no dependency *)
                          prev)
		  deps mv in
	      let dependencies = dependencies nm env extra_deps in
	      debug_deps nm extra_deps dependencies;
	      let env = (nm,dependencies) :: env in
	      (match dependencies with
		False -> (rest_info, env)
	      | _ ->
		  (match Str.split (Str.regexp "_") nm with
		    "commit"::_ ->
		      (build_named_or nm dependencies rest_info, env)
		  | _ -> (rest_info, env)))
          | (Ast.InitialScriptRule (_,_,deps,_,_),_)
	  | (Ast.FinalScriptRule (_,_,deps,_,_),_) ->
	      (* initialize and finalize dependencies are irrelevant to
		 get_constants *)
	      (* only possible metavariables are virtual *)
	      (rest_info,env)
          | (Ast.CocciRule (nm,(dep,_,_),cur,_,_),neg_pos_vars) ->
	      let color = get_color dep in
	      let dependencies = dependencies nm env dep in
	      debug_deps nm dep dependencies;
	      (match dependencies with
		False -> (rest_info,env)
	      | dependencies ->
		  let cur_info = rule_fn nm cur env neg_pos_vars color in
		  let re_cur_info = build_and dependencies cur_info in
		  (rest_info,(nm,re_cur_info)::env)))
      ([],[])
      (List.combine (rules : Ast.rule list) neg_pos_vars) in
  List.map
    (function (nm,info) ->
      let res = match info with Alpha(x) -> x | _ -> info in
      (nm,res))
    info

(* ----------------------------------------------------------------------- *)
(* Ensure that something can be done.  Produce a characterization of what
information is needed.  Dnf (ie, (a & b) v (c & d)) is good for this
because it makes apparent what the cases are, and what is needed in each
case. *)

let union l1 l2 =
  List.fold_left
    (fun prev cur -> if not (List.mem cur prev) then cur::prev else prev)
    l2 l1

let optor l1 (*potential adds*) l2 (*existing elements*) =
  let subset l1 l2 = List.for_all (fun x -> List.mem x l2) l1 in
  let (l2,acc) =
    List.fold_left
      (function (l2,acc) ->
	function x ->
          if List.exists (subset x) l2
          then (l2,acc) (* prefer existing clause *)
          else
            let l2 = List.filter (function y -> not (subset y x)) l2 in
            (l2,x::acc))
      (l2,[]) l1 in
  acc@l2

let dnf2c l =
  Printf.sprintf "[%s]"
    (String.concat " "
       (List.map
	  (function x ->
	    Printf.sprintf "[%s]" (String.concat " " (List.map dep2c x)))
	  l))

let dnf f =
  let opt_union_set longer shorter =
    (* (A v B) & (A v B v C) = A v B *)
    (* tries to be efficient by not updating prv, so optimize is still
       needed *)
    optor shorter longer in
  let rec dnf = function
      (Modified _) as tok -> [[tok]]
    | (Unmodified _) as tok -> [[tok]]
    | (File _) as tok -> [[tok]]
    | Or l -> List.fold_left opt_union_set [] (List.map dnf l)
    | And l ->
        let l = List.map dnf l in
        (match l with
          fst::rest ->
            List.fold_left
              (function prev ->
                function cur ->
                  List.fold_left opt_union_set []
                    (List.map (fun x -> List.map (union x) prev) cur))
              fst rest
        | [] -> failwith "or can't be empty")
    | True -> [[]] (* not sure *)
    | False -> [] (* not sure *)
    | c -> failwith (Printf.sprintf "unexpected constraint") in
  dnf f

(* Would like to have something in between Some and None.  For example, 0
can't stand on its own, but could be beneficial in combination with
something else.  But if we just have a modified 0, it woud be helpful to
allow unmodified things to attach to it.  TODO.  For now 0 is just
ignored.  This would be relevant for NULL also. *)

let minimize clauses strict =
  let modified = function
      Modified(_,_,_) -> true
    | _ -> false in
  let important = function
      Modified(_,_,Some x) when strict -> true
    | _ -> false in
  let unimportant = function
      Unmodified(_,_,_) -> true
    | x -> important x in
  let is_unmodified = function
      Unmodified(_,_,_) -> true
    | _ -> false in
  let mkfile = function
      Unmodified(color,info,Tok token) -> File token (* color not needed *)
    | Unmodified(color,info,_) -> True (* not worth grepping for *)
    | File x -> File x
    | x -> failwith (Printf.sprintf "not possible: %s" (dep2c x)) in
  let unmodified_to_file strict l =
    let available =
      List.fold_left
	(function prev ->
	  function
	      Modified(_,info,_) -> info.Ast.adjacency :: prev
	    | _ -> prev)
	[] l in
    let (close,far) =
      List.partition
	(function
	    Unmodified(_,info,_) -> List.mem info.Ast.adjacency available
	  | x -> true)
	l in
    if strict
    then close
    else (List.map mkfile far) @ close in
  let keep_closest l =
    match List.filter important l with
      [] ->
	let l = unmodified_to_file strict l in
	let unimportant = List.filter unimportant l in
	let modified = List.filter modified l in
	let contains_modif =
	  List.fold_left
	    (function prev ->
	      function
		  Modified(_,info,_) ->
		    (info.Ast.adjacency,info.Ast.line) :: prev
		| _ -> prev)
	    [] modified in
	let unimportant =
	  List.fold_left
	    (function unimportant ->
	      function
		  (Unmodified(color,info,token) as me) ->
		    let my_hunk = info.Ast.adjacency in
		    let my_line = info.Ast.line in
		    let same_hunk =
		      List.filter (function (x,_) -> x = my_hunk)
			contains_modif in
		    let (existing,others) =
		      List.partition (function (x,_,_) -> x = my_hunk)
			unimportant in
		    let closest l =
		      List.fold_left
			(function cdiff ->
			  function (_,modif_line) ->
			    let diff = abs(my_line - modif_line) in
			    if cdiff < 0 || (cdiff > 0 && diff < cdiff)
			    then diff
			    else cdiff)
			(-1) l in
		    (match (same_hunk,existing) with
		      ([],_) -> unimportant
		    | (l,[]) ->
			let closest = closest l in
			if closest >= 0
			then (my_hunk,closest,me)::others
			else others
		    | (l,[(_,dist,x)]) ->
			let closest = closest l in
			if closest >= 0 && closest < dist
			then (my_hunk,closest,me)::others
			else existing@others
		    | _ -> failwith "only one existing per hunk")
		| _ -> unimportant (*should not happen, can be True*))
	    [] unimportant in
	let modified =
	  List.filter
	    (function Modified(_,_,Some _) | File _ -> not strict | _ -> false)
	    l in
	modified @ (List.map (function (_,_,me) -> me) unimportant)
    | l -> l (*just keep the important ones*) in
  let noinfo = { Ast.line = 0; Ast.column = 0; Ast.adjacency = 0;
		 Ast.strbef = []; Ast.straft = []; Ast.whitespace = "" } in
  let clauses =
    List.fold_left
      (function prev ->
	function clause ->
	  let c =
	    List.fold_left
	      (function prev ->
		function
		    Modified(color,info,token) ->
		      let n = Modified(color,noinfo,token) in
		      if List.mem n prev then prev else n :: prev
		  | Unmodified(color,info,token) ->
		      let n = Unmodified(color,noinfo,token) in
		      if List.mem n prev then prev else n :: prev
		  | File x -> File x :: prev
		  | _ -> failwith "not possible")
	      [] (keep_closest clause) in
	  if List.mem c prev then prev else c :: prev)
      [] clauses in
  let contains_unmodified = List.exists (List.exists is_unmodified) clauses in
  let case = if contains_unmodified then UnmodifiedCase else ModifiedCase in
  (case,clauses)

let rebuild (case,dnf) =
  let res =
    List.fold_left
     (fun outer cur ->
       let cur =
	 List.fold_left (fun inner cur -> build_and cur inner) True cur in
       build_or cur outer)
     False dnf in
  (case,res)

let finalize l =
  let rec finalize_one = function
      Alpha(x) -> finalize_one x
    | x -> x in
  let unop = function (* idutils can't search for ops *)
      Modified(color,info,Some(Op _))
    | Unmodified(color,info,Op _) -> True
    | x -> x in
  let unfile(case,clauses) =
    (case, List.map (List.map (function File tok -> True | x -> x)) clauses) in
  let all =
    List.fold_left (fun prev cur -> build_or (finalize_one (snd cur)) prev)
      False l in
  (if !Flag.debug
  then (Printf.eprintf "before minimize: %s\n" (dep2c all); flush stderr));
  let dnf = dnf all in
  let strict =
    (rebuild (unfile (minimize dnf true)),
     rebuild (unfile (minimize (List.map (List.map unop) dnf) true))) in
  let d1 = minimize dnf false in
  let d2 = minimize (List.map (List.map unop) dnf) false in
  let nonstrict = (rebuild (unfile d1),rebuild (unfile d2)) in
  let very_nonstrict = (rebuild d1,rebuild d2) in
  (if !Flag.debug
  then
    begin
      Printf.eprintf "strict %s %s\n"
	(dep2c (snd (fst strict))) (dep2c (snd (snd strict)));
      Printf.eprintf "nonstrict %s %s\n"
	(dep2c (snd (fst nonstrict))) (dep2c (snd (snd nonstrict)));
      Printf.eprintf "very nonstrict %s %s\n"
	(dep2c (snd (fst very_nonstrict))) (dep2c (snd (snd very_nonstrict)))
    end);
  match (strict=nonstrict,nonstrict=very_nonstrict) with
    (true,true) -> [(1,strict)]
  | (true,false) -> [(1,strict);(3,very_nonstrict)]
  | (false,true) -> [(1,strict);(2,nonstrict)]
  | _ -> [(1,strict);(2,nonstrict);(3,very_nonstrict)]

(* ----------------------------------------------------------------------- *)

let get_constants rules neg_pos_vars =
  finalize (run rules neg_pos_vars)
