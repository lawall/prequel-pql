(*
 * This file is part of Prequel, lincensed under the terms of the GPL v2.
 * See copyright.txt in the Prequel source code for more information.
 * The Prequel source code can be obtained at
 * https://github.com/prequel-pql/prequel
 *)

(* exports everything, used only by parser_cocci_menhir.mly *)
module Ast0 = Ast0_cocci
module Ast = Ast_cocci

let contains_string_constant = ref false

(* types for metavariable tokens *)
type info = Ast.meta_name * Ast0.pure * Data.clt
type midinfo =
    Ast.meta_name * Data.iconstraints * Ast.seed * Ast0.pure * Data.clt
type idinfo = Ast.meta_name * Data.iconstraints * Ast0.pure * Data.clt
type assignOpinfo =
    Ast.meta_name * Ast0_cocci.assignOpconstraint * Ast0.pure * Data.clt
type binaryOpinfo =
    Ast.meta_name * Ast0_cocci.binaryOpconstraint * Ast0.pure * Data.clt
type expinfo = Ast.meta_name * Data.econstraints * Ast0.pure * Data.clt
type tyinfo = Ast.meta_name * Ast0.typeC list * Ast0.pure * Data.clt
type list_info = Ast.meta_name * Ast.list_len * Ast0.pure * Data.clt
type typed_expinfo =
    Ast.meta_name * Data.econstraints * Ast0.pure *
      Type_cocci.typeC list option * Data.clt
type pos_info = Ast.meta_name * Data.pconstraints * Ast.meta_collect * Data.clt

let get_option fn = function
    None -> None
  | Some x -> Some (fn x)

let make_info line logical_line logical_line_end offset col strbef straft 
    isSymbol ws =
  let new_pos_info =
    {Ast0.line_start = line; Ast0.line_end = line;
      Ast0.logical_start = logical_line; Ast0.logical_end = logical_line_end;
      Ast0.column = col; Ast0.offset = offset; } in
  { Ast0.pos_info = new_pos_info; Ast0.whitespace = ws;
    Ast0.attachable_start = true; Ast0.attachable_end = true;
    Ast0.mcode_start = []; Ast0.mcode_end = [];
    Ast0.strings_before = strbef; Ast0.strings_after = straft;
    Ast0.isSymbolIdent = isSymbol; }

let clt2info (_,line,logical_line,logical_line_end,offset,col,
              strbef,straft,pos,ws) =
 make_info line logical_line logical_line_end offset col strbef straft false ws

let drop_bef (arity,line,lline,llineend,offset,col,strbef,straft,pos,ws) =
  (arity,line,lline,llineend,offset,col,[],straft,pos,ws)

let drop_aft (arity,line,lline,llineend,offset,col,strbef,straft,pos,ws) =
  (arity,line,lline,llineend,offset,col,strbef,[],pos,ws)

(* used for #define, to put aft on ident/( *)
let get_aft (arity,line,lline,llineen,offset,col,strbef,straft,pos,ws) = straft

let set_aft aft
  (arity,line,lline,llineend,offset,col,strbef,_,pos,ws) =
  (arity,line,lline,llineend,offset,col,strbef,aft,pos,ws)

let drop_pos
  (arity,line,lline,llineend,offset,col,strbef,straft,pos,ws) =
  (arity,line,lline,llineend,offset,col,strbef,straft,[],ws)

let clt2mcode_ext str isSymbol = function
    (Data.MINUS,line,lline,llineend,offset,col,strbef,straft,pos,ws) ->
      (str,Ast0.NONE,
       make_info line lline llineend offset col strbef straft isSymbol ws,
       Ast0.MINUS(ref(Ast.NOREPLACEMENT,Ast0.default_token_info)),ref pos,-1)
  | (Data.OPTMINUS,line,lline,llineend,offset,col,strbef,straft,pos,ws)->
      (str,Ast0.OPT,
       make_info line lline llineend offset col strbef straft isSymbol ws,
       Ast0.MINUS(ref(Ast.NOREPLACEMENT,Ast0.default_token_info)),ref pos,-1)
  | (Data.PLUS,line,lline,llineend,offset,col,strbef,straft,pos,ws)        ->
      (str,Ast0.NONE,
       make_info line lline llineend offset col strbef straft isSymbol ws,
       Ast0.PLUS(Ast.ONE),ref pos,-1)
  | (Data.PLUSPLUS,line,lline,llineend,offset,col,strbef,straft,pos,ws)    ->
      (str,Ast0.NONE,
       make_info line lline llineend offset col strbef straft isSymbol ws,
       Ast0.PLUS(Ast.MANY),ref pos,-1)
  | (Data.CONTEXT,line,lline,llineend,offset,col,strbef,straft,pos,ws)     ->
      (str,Ast0.NONE,
       make_info line lline llineend offset col strbef straft isSymbol ws,
       Ast0.CONTEXT(ref(Ast.NOTHING,
			Ast0.default_token_info,Ast0.default_token_info)),
       ref pos,-1)
  | (Data.OPT,line,lline,llineend,offset,col,strbef,straft,pos,ws)         ->
      (str,Ast0.OPT,
       make_info line lline llineend offset col strbef straft isSymbol ws,
       Ast0.CONTEXT(ref(Ast.NOTHING,
			Ast0.default_token_info,Ast0.default_token_info)),
       ref pos,-1)

let clt2mcode name clt = clt2mcode_ext name false clt
let id2name   (name, clt) = name
let id2clt    (name, clt) = clt
let id2mcode  (name, clt) = clt2mcode name clt
let sym2mcode (name, clt) = clt2mcode_ext name true clt

let mkdots str (dot,whencode) =
  match str with
    "..." -> Ast0.wrap(Ast0.Dots(clt2mcode str dot, whencode))
  | _ -> failwith "cannot happen"

let mkedots str (dot,whencode) =
  match str with
    "..." -> Ast0.wrap(Ast0.Edots(clt2mcode str dot, whencode))
  | _ -> failwith "cannot happen"

let mkdpdots str dot =
  match str with
    "..." -> Ast0.wrap(Ast0.DPdots(clt2mcode str dot))
  | _ -> failwith "cannot happen"

let mkidots str (dot,whencode) =
  match str with
    "..." -> Ast0.wrap(Ast0.Idots(clt2mcode str dot, whencode))
  | _ -> failwith "cannot happen"

let mkddots str (dot,whencode) =
  match (str,whencode) with
    ("...",None) -> Ast0.wrap(Ast0.Ddots(clt2mcode str dot, None))
  | ("...",Some [w]) -> Ast0.wrap(Ast0.Ddots(clt2mcode str dot, Some w))
  | _ -> failwith "cannot happen"

let mkddots_one str (dot,whencode) =
  match str with
    "..." -> Ast0.wrap(Ast0.Ddots(clt2mcode str dot, whencode))
  | _ -> failwith "cannot happen"

let mkpdots str dot =
  match str with
    "..." -> Ast0.wrap(Ast0.Pdots(clt2mcode str dot))
  | _ -> failwith "cannot happen"

let arith_op ast_op left op right =
  let op' = Ast0.wrap (Ast0.Arith (clt2mcode ast_op op)) in  
  Ast0.wrap (Ast0.Binary(left, op', right))

let logic_op ast_op left op right =
  let op' = Ast0.wrap (Ast0.Logical (clt2mcode ast_op op)) in  
  Ast0.wrap (Ast0.Binary(left, op', right))

let make_cv cv ty =
  match cv with None -> ty | Some x -> Ast0.wrap (Ast0.ConstVol(x,ty))

let top_dots l = Ast0.wrap l

(* here the offset is that of the first in the sequence of *s, not that of
each * individually *)
let pointerify ty m =
  List.fold_left
    (function inner ->
      function cur ->
	Ast0.wrap(Ast0.Pointer(inner,clt2mcode "*" cur)))
    ty m

let ty_pointerify ty m =
  List.fold_left
    (function inner -> function cur -> Type_cocci.Pointer(inner))
    ty m

let arrayify ty ar =
  List.fold_right
    (function (l,i,r) ->
      function rest ->
	Ast0.wrap (Ast0.Array(rest,clt2mcode "[" l,i,clt2mcode "]" r)))
    ar ty

(* Left is <=>, Right is =>.  Collect <=>s. *)
(* The parser should have done this, with precedences.  But whatever... *)
let iso_adjust first_fn fn first rest =
  let rec loop = function
      [] -> [[]]
    | (Common.Left x)::rest ->
	(match loop rest with
	  front::after -> (fn x::front)::after
	| _ -> failwith "not possible")
    | (Common.Right x)::rest ->
	(match loop rest with
	  front::after -> []::(fn x::front)::after
	| _ -> failwith "not possible") in
  match loop rest with
    front::after -> (first_fn first::front)::after
  | _ -> failwith "not possible"

let lookup rule name =
  try
    let info = Hashtbl.find Data.all_metadecls rule in
    List.find (function mv -> Ast.get_meta_name mv = (rule,name)) info
  with
    Not_found ->
      raise
	(Semantic_cocci.Semantic("bad rule "^rule^" or bad variable "^name))

let meta_lookup rule name v =
  match lookup rule name with
    Ast.MetaScriptDecl(cell,_) ->
      (match !cell with
	Some x -> x
      | None -> cell := Some v; v)
  | res -> res

let check_meta_tyopt type_irrelevant v =
  let fail name =
    raise
      (Semantic_cocci.Semantic
	 ("incompatible inheritance declaration "^name)) in
  match v with
    Ast.MetaMetaDecl(flex,(rule,name)) ->
      (match meta_lookup rule name v with
	Ast.MetaMetaDecl(flex1,_) when flex = flex1 -> ()
      | _ -> fail name)
  | Ast.MetaIdDecl(flex,(rule,name),constraints) ->
      (* constraints can be different than at previous declaration *)
      (match meta_lookup rule name v with
	Ast.MetaIdDecl(flex1,_,_) when flex = flex1 -> ()
      | _ -> fail name)
  | Ast.MetaTypeDecl(flex,(rule,name)) ->
      (match meta_lookup rule name v with
	Ast.MetaTypeDecl(flex1,_) when flex = flex1 -> ()
      | _ -> fail name)
  | Ast.MetaInitDecl(flex,(rule,name)) ->
      (match meta_lookup rule name v with
	Ast.MetaInitDecl(flex1,_) when flex = flex1 -> ()
      | _ -> fail name)
  | Ast.MetaInitListDecl(flex,(rule,name),len) ->
      (match meta_lookup rule name v with
	Ast.MetaInitListDecl(flex1,_,_) when flex = flex1 -> ()
      | _ -> fail name)
  | Ast.MetaListlenDecl(flex,(rule,name)) ->
      (match meta_lookup rule name v with
	Ast.MetaListlenDecl(flex1,_) when flex = flex1 -> ()
      | _ -> fail name)
  | Ast.MetaParamDecl(flex,(rule,name)) ->
      (match meta_lookup rule name v with
	Ast.MetaParamDecl(flex1,_) when flex = flex1 -> ()
      | _ -> fail name)
  | Ast.MetaParamListDecl(flex,(rule,name),len) ->
      (match meta_lookup rule name v with
	Ast.MetaParamListDecl(flex1,_,_) when flex = flex1 -> ()
      | _ -> fail name)
  | Ast.MetaBinaryOperatorDecl(flex,(rule,name),cstr) ->
      (match meta_lookup rule name v with
	Ast.MetaBinaryOperatorDecl(flex1,_,_) when flex = flex1 -> ()
      | _ -> fail name)
  | Ast.MetaAssignmentOperatorDecl(flex,(rule,name),cstr) ->
      (match meta_lookup rule name v with
	Ast.MetaAssignmentOperatorDecl(flex1,_,_) when flex = flex1 -> ()
      | _ -> fail name)
  | Ast.MetaConstDecl(flex,(rule,name),ty) ->
      (match meta_lookup rule name v with
	Ast.MetaConstDecl(flex1,_,ty1)
	when (type_irrelevant || ty = ty1) && flex = flex1 -> ()
      | _ -> fail name)
  | Ast.MetaErrDecl(flex,(rule,name)) ->
      (match meta_lookup rule name v with
	Ast.MetaErrDecl(flex1,_) when flex = flex1 -> ()
      | _ -> fail name)
  | Ast.MetaExpDecl(flex,(rule,name),ty) ->
      (match meta_lookup rule name v with
	Ast.MetaExpDecl(flex1,_,ty1)
	when (type_irrelevant || ty = ty1) && flex = flex1 -> ()
      | _ -> fail name)
  | Ast.MetaIdExpDecl(flex,(rule,name),ty) ->
      (match meta_lookup rule name v with
	Ast.MetaIdExpDecl(flex1,_,ty1)
	when (type_irrelevant || ty = ty1) && flex = flex1 -> ()
      | _ -> fail name)
  | Ast.MetaLocalIdExpDecl(flex,(rule,name),ty) ->
      (match meta_lookup rule name v with
	Ast.MetaLocalIdExpDecl(flex1,_,ty1)
	when (type_irrelevant || ty = ty1) && flex = flex1 -> ()
      | _ -> fail name)
  | Ast.MetaGlobalIdExpDecl(flex,(rule,name),ty) ->
      (match meta_lookup rule name v with
	Ast.MetaGlobalIdExpDecl(flex1,_,ty1)
	when (type_irrelevant || ty = ty1) && flex = flex1 -> ()
      | _ -> fail name)
  | Ast.MetaExpListDecl(flex,(rule,name),len_name) ->
      (match meta_lookup rule name v with
	Ast.MetaExpListDecl(flex1,_,_) when flex = flex1 -> ()
      | _ -> fail name)
  | Ast.MetaDeclDecl(flex,(rule,name)) ->
      (match meta_lookup rule name v with
	Ast.MetaDeclDecl(flex1,_) when flex = flex1 -> ()
      | _ -> fail name)
  | Ast.MetaFieldDecl(flex,(rule,name)) ->
      (match meta_lookup rule name v with
	Ast.MetaFieldDecl(flex1,_) when flex = flex1 -> ()
      | _ -> fail name)
  | Ast.MetaFieldListDecl(flex,(rule,name),len_name) ->
      (match meta_lookup rule name v with
	Ast.MetaFieldListDecl(flex1,_,_) when flex = flex1 -> ()
      | _ -> fail name)
  | Ast.MetaStmDecl(flex,(rule,name)) ->
      (match meta_lookup rule name v with
	Ast.MetaStmDecl(flex1,_) when flex = flex1 -> ()
      | _ -> fail name)
  | Ast.MetaStmListDecl(flex,(rule,name),len_name) ->
      (match meta_lookup rule name v with
	Ast.MetaStmListDecl(flex1,_,_) when flex = flex1 -> ()
      | _ -> fail name)
  | Ast.MetaFuncDecl(flex,(rule,name)) ->
      (match meta_lookup rule name v with
	Ast.MetaFuncDecl(flex1,_) when flex = flex1 -> ()
      | _ -> fail name)
  | Ast.MetaLocalFuncDecl(flex,(rule,name)) ->
      (match meta_lookup rule name v with
	Ast.MetaLocalFuncDecl(flex1,_) when flex = flex1 -> ()
      | _ -> fail name)
  | Ast.MetaPosDecl((rule,name),constraints) ->
      (match meta_lookup rule name v with
	Ast.MetaPosDecl _ -> ()
      | _ -> fail name)
  | Ast.MetaFmtDecl(flex,(rule,name)) ->
      (match meta_lookup rule name v with
	Ast.MetaFmtDecl(flex1,_) when flex = flex1 -> ()
      | _ -> fail name)
  | Ast.MetaFragListDecl(flex,(rule,name),len) ->
      (match meta_lookup rule name v with
	Ast.MetaFragListDecl(flex1,_,_) when flex = flex1 -> ()
      | _ -> fail name)
  | Ast.MetaAnalysisDecl(analyzer,(rule,name)) ->
      (match meta_lookup rule name v with
	Ast.MetaAnalysisDecl(analyzer1,_) ->
	  if analyzer = analyzer1
	  then ()
	  else fail name
      | _ -> fail name)
  | Ast.MetaDeclarerDecl(flex,(rule,name)) ->
      (match meta_lookup rule name v with
	Ast.MetaDeclarerDecl(flex1,(rule,name)) when flex = flex1 -> ()
      | _ -> fail name)
  | Ast.MetaIteratorDecl(flex,(rule,name)) ->
      (match meta_lookup rule name v with
	Ast.MetaIteratorDecl(flex1,(rule,name)) when flex = flex1 -> ()
      | _ -> fail name)
  | _ ->
      raise
	(Semantic_cocci.Semantic ("arity not allowed on imported declaration"))

let check_meta m = check_meta_tyopt false m

let check_inherited_constraint meta_name check =
  match meta_name with
    (None,_) -> failwith "constraint must be an inherited variable"
  | (Some rule,name) ->
      let i = (rule,name) in
      check (lookup rule name);
      i

let create_metadec ar ispure kindfn ids current_rule =
  List.concat
    (List.map
       (function (rule,nm) ->
	 let (rule,checker) =
	   match rule with
	     None -> ((current_rule,nm),function x -> [Ast.LocalMV x])
	   | Some rule ->
	       ((rule,nm),
		function x -> check_meta x; [Ast.InheritedMV x]) in
	 kindfn ar rule ispure checker)
       ids)


let create_metadec_virt ar ispure kindfn ids current_rule =
  List.concat
    (List.map
       (function nm ->
	 let checker = function x -> [Ast.InheritedMV x] in
	 kindfn ar nm ispure checker !Flag.defined_virtual_env)
       ids)

let create_fresh_metadec kindfn ids current_rule =
  List.concat
    (List.map
       (function ((rule,nm),seed) ->
	 let (rule,checker) =
	   match rule with
	     None -> ((current_rule,nm),function x -> [Ast.LocalMV x])
	   | Some rule ->
	       ((rule,nm),
		function x -> check_meta x; [Ast.InheritedMV x]) in
	 kindfn rule checker seed)
       ids)

let create_metadec_with_constraints ar ispure kindfn ids current_rule =
  List.concat
    (List.map
       (function ((rule,nm),constraints) ->
	 let (rule,checker) =
	   match rule with
	       None -> ((current_rule,nm),function x -> [Ast.LocalMV x])
	     | Some rule ->
		 ((rule,nm),
		  function x -> check_meta x; [Ast.InheritedMV x]) in
	   kindfn ar rule ispure checker constraints)
       ids)

let create_metadec_ty ar ispure kindfn ids current_rule =
  List.concat
    (List.map
       (function ((rule,nm),constraints) ->
	 let (rule,checker) =
	   match rule with
	     None -> ((current_rule,nm),function x -> [Ast.LocalMV x])
	   | Some rule ->
	       ((rule,nm),
		function x -> check_meta x; [Ast.InheritedMV x]) in
	 kindfn ar rule ispure checker constraints)
       ids)

let create_len_metadec flex ispure kindfn lenid ids current_rule =
  let (lendec,lenname) =
    match lenid with
      Common.Left (lflex,lenid) ->
	let lendec =
	  create_metadec Ast.NONE Ast0.Impure
	    (fun _ name _ check_meta ->
	      check_meta(Ast.MetaListlenDecl(lflex,name)))
	    [lenid] current_rule in
	let lenname =
	  match lendec with
	    [Ast.LocalMV (Ast.MetaListlenDecl(flex,x))] ->
	      Ast.MetaLen(x,flex)
	  | [Ast.InheritedMV (Ast.MetaListlenDecl(flex,x))] ->
	      Ast.MetaLen(x,flex)
	  | _ -> failwith "unexpected length declaration" in
	(lendec,lenname)
    | Common.Right n -> ([],Ast.CstLen n) in
  lendec@(create_metadec flex ispure (kindfn lenname) ids current_rule)

(* ---------------------------------------------------------------------- *)

let str2inc s =
  let elements = Str.split (Str.regexp "/") s in
  List.map (function "..." -> Ast.IncDots | s -> Ast.IncPath s) elements

(* ---------------------------------------------------------------------- *)
(* declarations and statements *)

let meta_decl name =
  let (nm,pure,clt) = name in
  Ast0.wrap(Ast0.MetaDecl(clt2mcode nm clt,pure))

let meta_field name =
  let (nm,pure,clt) = name in
  Ast0.wrap(Ast0.MetaField(clt2mcode nm clt,pure))

let meta_field_list name =
  let (nm,lenname,pure,clt) = name in
  let lenname =
    match lenname with
      Ast.AnyLen -> Ast0.AnyListLen
    | Ast.MetaLen(nm,_) -> Ast0.MetaListLen(clt2mcode nm clt)
    | Ast.CstLen n -> Ast0.CstListLen n in
  Ast0.wrap(Ast0.MetaFieldList(clt2mcode nm clt,lenname,pure))

let meta_stm name =
  let (nm,pure,clt) = name in
  Ast0.wrap(Ast0.MetaStmt(clt2mcode nm clt,pure))

let meta_stm_list name =
  let (nm,lenname,pure,clt) = name in
  let lenname =
    match lenname with
      Ast.AnyLen -> Ast0.AnyListLen
    | Ast.MetaLen(nm,_) -> Ast0.MetaListLen(clt2mcode nm clt)
    | Ast.CstLen n -> Ast0.CstListLen n in
  Ast0.wrap(Ast0.MetaStmtList(clt2mcode nm clt,lenname,pure))

let exp_stm exp pv =
  Ast0.wrap(Ast0.ExprStatement (exp, clt2mcode ";" pv))

let make_fake_mcode _ = (Ast0.default_info(),Ast0.context_befaft(),-1)

let ifthen iff lp tst rp thn =
  Ast0.wrap(Ast0.IfThen(clt2mcode "if" iff,
    clt2mcode "(" lp,tst,clt2mcode ")" rp,thn,make_fake_mcode()))

let ifthenelse iff lp tst rp thn e els =
  Ast0.wrap(Ast0.IfThenElse(clt2mcode "if" iff,
    clt2mcode "(" lp,tst,clt2mcode ")" rp,thn,
    clt2mcode "else" e,els,make_fake_mcode()))

let forloop fr lp e1 sc1 e2 sc2 e3 rp s =
  Ast0.wrap(Ast0.For(clt2mcode "for" fr,clt2mcode "(" lp,
		     Ast0.wrap(Ast0.ForExp(e1,clt2mcode ";" sc1)),e2,
		     clt2mcode ";" sc2,e3,clt2mcode ")" rp,s,
		     make_fake_mcode()))

let forloop2 fr lp decl e2 sc2 e3 rp s =
  let bef = (Ast0.default_info(),Ast0.context_befaft()) in
  Ast0.wrap(Ast0.For(clt2mcode "for" fr,clt2mcode "(" lp,
		     Ast0.wrap(Ast0.ForDecl (bef,decl)),e2,
		     clt2mcode ";" sc2,e3,clt2mcode ")" rp,s,
		     make_fake_mcode()))

let whileloop w lp e rp s =
  Ast0.wrap(Ast0.While(clt2mcode "while" w,clt2mcode "(" lp,
		       e,clt2mcode ")" rp,s,make_fake_mcode()))

let doloop d s w lp e rp pv =
  Ast0.wrap(Ast0.Do(clt2mcode "do" d,s,clt2mcode "while" w,
		    clt2mcode "(" lp,e,clt2mcode ")" rp,
		    clt2mcode ";" pv))

let iterator i lp e rp s =
  Ast0.wrap(Ast0.Iterator(i,clt2mcode "(" lp,e,clt2mcode ")" rp,s,
			  make_fake_mcode()))

let switch s lp e rp lb d c rb =
  let d =
    List.map
      (function d ->
	Ast0.wrap(Ast0.Decl((Ast0.default_info(),Ast0.context_befaft()),d)))
      d in
  Ast0.wrap(Ast0.Switch(clt2mcode "switch" s,clt2mcode "(" lp,e,
			clt2mcode ")" rp,clt2mcode "{" lb,
			Ast0.wrap d,Ast0.wrap c,clt2mcode "}" rb))

let ret_exp r e pv =
  Ast0.wrap(Ast0.ReturnExpr(clt2mcode "return" r,e,clt2mcode ";" pv))

let ret r pv =
  Ast0.wrap(Ast0.Return(clt2mcode "return" r,clt2mcode ";" pv))

let break b pv =
  Ast0.wrap(Ast0.Break(clt2mcode "break" b,clt2mcode ";" pv))

let cont c pv =
  Ast0.wrap(Ast0.Continue(clt2mcode "continue" c,clt2mcode ";" pv))

let label i dd =
  Ast0.wrap(Ast0.Label(i,clt2mcode ":" dd))

let goto g i pv =
  Ast0.wrap(Ast0.Goto(clt2mcode "goto" g,i,clt2mcode ";" pv))

let seq lb s rb =
  Ast0.wrap(Ast0.Seq(clt2mcode "{" lb,s,clt2mcode "}" rb))

(* ---------------------------------------------------------------------- *)

let regbefore = Str.regexp "before_"
let regafter  = Str.regexp "after_"
    
let check_rule_name = function
    Some nm ->
      let n = id2name nm in
      (if not !Flag_parsing_cocci.getconstants &&
	(Str.string_match regbefore n 0 || Str.string_match regafter n 0)
      then
	raise
	  (Semantic_cocci.Semantic
	     "rule name cannot begine with before_ or after"));
      (try let _ =  Hashtbl.find Data.all_metadecls n in
      raise (Semantic_cocci.Semantic "repeated rule name")
      with Not_found -> Some n)
  | None -> None

let make_iso_rule_name_result n =
  (try let _ =  Hashtbl.find Data.all_metadecls n in
  raise (Semantic_cocci.Semantic ("repeated rule name"))
  with Not_found -> ());
  Ast.CocciRulename
    (Some n,Ast.NoDep,[],[],Ast.Undetermined,Ast.AnyP (*discarded*))

let fix_dependencies d =
  let rec loop inverted = function
      Ast0.Dep s when inverted -> Ast.AntiDep s
    | Ast0.Dep s -> Ast.Dep s
    | Ast0.AntiDep d -> loop (not inverted) d
    | Ast0.EverDep s when inverted -> Ast.NeverDep s
    | Ast0.EverDep s -> Ast.EverDep s
    | Ast0.NeverDep s when inverted -> Ast.EverDep s
    | Ast0.NeverDep s -> Ast.NeverDep s
    | Ast0.AndDep(d1,d2) when inverted ->
	Ast.OrDep(loop inverted d1,loop inverted d2)
    | Ast0.AndDep(d1,d2) ->
	Ast.AndDep(loop inverted d1,loop inverted d2)
    | Ast0.OrDep(d1,d2) when inverted ->
	Ast.AndDep(loop inverted d1,loop inverted d2)
    | Ast0.OrDep(d1,d2) ->
	Ast.OrDep(loop inverted d1,loop inverted d2)
    | Ast0.FileIn s when inverted -> Ast.NotFileIn s
    | Ast0.FileIn s -> Ast.FileIn s
    | Ast0.NoDep -> Ast.NoDep
    | Ast0.FailDep -> Ast.FailDep in
  loop false d

let make_cocci_rule_name_result nm d i a e ee =
  Ast.CocciRulename (check_rule_name nm,fix_dependencies d,i,a,e,ee)

let make_generated_rule_name_result nm d i a e ee =
  Ast.GeneratedRulename (check_rule_name nm,fix_dependencies d,i,a,e,ee)

let make_script_rule_name_result lang nm deps =
  let l = id2name lang in
  Ast.ScriptRulename (check_rule_name nm,l,fix_dependencies deps)

let make_initial_script_rule_name_result lang deps =
  let l = id2name lang in
  Ast.InitialScriptRulename(None,l,fix_dependencies deps)

let make_final_script_rule_name_result lang deps =
  let l = id2name lang in
  Ast.FinalScriptRulename(None,l,fix_dependencies deps)

(* ---------------------------------------------------------------------- *)
(* decide whether an init list is ordered or unordered *)

let struct_initializer initlist =
  let rec loop i =
    match Ast0.unwrap i with
      Ast0.InitGccExt _ -> true
    | Ast0.InitGccName _ -> true
    | Ast0.OptIni i -> loop i
    | Ast0.MetaInit _ | Ast0.MetaInitList _ -> false (* ambiguous... *)
    | _ -> false in
  let l = Ast0.unwrap initlist in
  (l = []) || (List.exists loop l)

let drop_dot_commas initlist =
  let rec loop after_comma = function
      [] -> []
    | x::xs ->
	(match Ast0.unwrap x with
	  Ast0.Idots(dots,whencode) -> x :: (loop true xs)
	| Ast0.IComma(comma) when after_comma -> (*drop*) loop false xs
	| _ -> x :: (loop false xs)) in
  Ast0.rewrap initlist (loop false (Ast0.unwrap initlist))

(* ----------------------------------------------------------------------- *)
(* strings *)

let not_format_string str clt =
  Ast0.wrap(Ast0.Constant (clt2mcode (Ast.String str) clt))

let parse_string str ((mc,b,c,d,e,f,g,h,i,_) as clt) =
  not_format_string str clt

(* -------------------------------------------------------------------- *)

let is_statement_dots s =
  match Ast0.unwrap s with
    Ast0.Dots _ -> true
  | _ -> false

let get_statement_dots_when s =
  match Ast0.unwrap s with
    Ast0.Dots(_,w) -> w
  | _ -> failwith "not dots"

let set_statement_dots_when s w =
  match Ast0.unwrap s with
    Ast0.Dots(d,_) -> Ast0.rewrap s (Ast0.Dots(d,w))
  | _ -> failwith "not dots"

let check_for_empty_dots middle isdots getwhen setwhen =
  let rec codeloop whenres = function
      [] -> (whenres,[])
    | x::xs ->
	if isdots x
	then dotloop [getwhen x] whenres x xs
	else
	  let (whenres,rest) = codeloop whenres xs in
	  (whenres, x :: rest)
  and dotloop accwhen whenres model = function
      [] -> failwith "not possible"
    | x :: xs ->
	if isdots x
	then dotloop ((getwhen x)::accwhen) whenres model xs
	else
	  let accwhen = List.concat (List.rev accwhen) in
	  let (whenres,rest) = codeloop (accwhen::whenres) xs in
	  (whenres, (setwhen model accwhen) :: x :: rest) in
  codeloop [] middle

let check_for_empty_nests middle whencode isdots getwhen setwhen =
  let rec remove_starting_dots accwhen = function
      [] -> (accwhen, [])
    | (x::xs) as all ->
	if isdots x
	then remove_starting_dots ((getwhen x) :: accwhen) xs
	else (accwhen, all) in
  let (rbeforewhen,middle) = remove_starting_dots [] middle in
  let beforewhen = List.rev rbeforewhen in
  let (afterwhen,middle) = remove_starting_dots [] (List.rev middle) in
  let (iwhen,middle) = check_for_empty_dots middle isdots getwhen setwhen in
  let beforewhen = List.fold_left Common.union_set whencode beforewhen in
  let afterwhen = List.fold_left Common.union_set whencode afterwhen in
  if Common.equal_set beforewhen afterwhen &&
    (middle = [] ||
     List.for_all (function x -> Common.include_set beforewhen x) iwhen)
  then (beforewhen,middle)
  else failwith "problem merging dots with beginning/end of nest"
