(*
 * This file is part of Prequel, lincensed under the terms of the GPL v2.
 * See copyright.txt in the Prequel source code for more information.
 * The Prequel source code can be obtained at
 * https://github.com/prequel-pql/prequel
 *)

val check_meta :
    string ->
    Ast_cocci.metavar list (* old metavariables *) ->
      Ast_cocci.metavar list (* explicitly inherited *) ->
	Ast_cocci.metavar list (* declared locally *) ->
	  Ast0_cocci.rule -> Ast0_cocci.rule -> unit
