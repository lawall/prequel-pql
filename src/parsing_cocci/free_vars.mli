(*
 * This file is part of Prequel, lincensed under the terms of the GPL v2.
 * See copyright.txt in the Prequel source code for more information.
 * The Prequel source code can be obtained at
 * https://github.com/prequel-pql/prequel
 *)

(* Used after things can only have one binding.  Positions can have many
bindings.  These are combined in ctlcocciintegration, ie after the CTL
generation. *)

val free_vars : Ast_cocci.rule list ->
  (((Ast_cocci.meta_name list) list) list) (*negated position vars*)
