(*
 * This file is part of Prequel, lincensed under the terms of the GPL v2.
 * See copyright.txt in the Prequel source code for more information.
 * The Prequel source code can be obtained at
 * https://github.com/prequel-pql/prequel
 *)

module Ast0 = Ast0_cocci
module V0 = Visitor_ast0
module VT0 = Visitor_ast0_types

(* Not the same as in Coccinelle.  Here only statement level dots break
adjacency.  This is only used in get_constants2, so only the adjacencies of
interesting tokens matter.  In particular, the adjacency of ... itself
doesn't matter.  Also the adjacency of fake nodes does matter, so simpler
than the Coccinelle case.  Finally, adjacency needs to be different between
rules, to account fxor for dependencies between rules*)

let rec ltrmap f = function (* guarantees left to right eval order *)
    [] -> []
  | x::xs ->
      let x = f x in
      x :: ltrmap f xs

let counter = ref 0

let compute_adjacency p =
  let mcode (a,b,c,d,e,_) = (a,b,c,d,e,!counter) in
  let statement r k s =
    (* a case for statement-level dots *)
    match Ast0.unwrap s with
      Ast0.Dots(dots,whn) ->
	counter := !counter + 1;
	k s
    | Ast0.Nest(lp,pat,rp,whn,multi) ->
	counter := !counter + 1;
	let s = k s in
	counter := !counter + 1;
	s
    | _ -> k s in
  let initlist r k i =
    (* increment before processing each entry and after the last one *)
    let res =
      Ast0.rewrap i
	(ltrmap
	   (function x ->
	     counter := !counter + 1;
	     r.VT0.rebuilder_rec_initialiser x)
	   (Ast0.unwrap i)) in
    counter := !counter + 1;
    res in
  let fn =
    V0.rebuilder
      {V0.rebuilder_functions with
	VT0.rebuilder_stmtfn = statement;
	VT0.rebuilder_dotsinitfn = initlist;
	VT0.rebuilder_meta_mcode = mcode;
        VT0.rebuilder_string_mcode = mcode;
        VT0.rebuilder_const_mcode = mcode;
        VT0.rebuilder_simpleAssign_mcode = mcode;
        VT0.rebuilder_opAssign_mcode = mcode;
        VT0.rebuilder_fix_mcode = mcode;
        VT0.rebuilder_unary_mcode = mcode;
        VT0.rebuilder_arithOp_mcode = mcode;
        VT0.rebuilder_logicalOp_mcode = mcode;
        VT0.rebuilder_cv_mcode = mcode;
        VT0.rebuilder_sign_mcode = mcode;
        VT0.rebuilder_struct_mcode = mcode;
	VT0.rebuilder_storage_mcode = mcode;
        VT0.rebuilder_inc_mcode = mcode;} in
  ltrmap
    (function x ->
      counter := !counter + 1;
      fn.VT0.rebuilder_rec_top_level x)
    p

let reinit _ = counter := 0
