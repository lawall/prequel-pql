;;; Usage

;; Add the following lines to your ~/.emacs or equivalent:
;;  (setq load-path (append '("path_to_prequel/scripts") load-path))
;;  (setq auto-mode-alist 
;;          (cons '("\\.porg$" . porg-mode) auto-mode-alist))
;;  (autoload 'porg-mode "porg" 
;;          "Major mode for viewing prequel output." t)

(require 'org)

(defface porg-plus-face
  '((((background light)) (:foreground "green4"))
    (((background dark)) (:foreground "SeaGreen3")))
  "Highlighting removed lines")

(defface porg-minus-face
  '((((background light)) (:foreground "red"))
    (((background dark)) (:foreground "salmon")))
  "Highlighting added lines")

(defface porg-special-face
  '((((background light)) (:foreground "blue"))
    (((background dark)) (:foreground "red")))
  "")

(defun porg-colors ()
   (add-hook 'org-mode-hook
          (lambda ()
	   (local-set-key "\C-ca" 'org-show-subtree)
	   (local-set-key "\C-cd" 'hide-subtree)
           (font-lock-add-keywords nil
            '(("^\\??\\+.*" . 'porg-plus-face)
	      ("^\\??-.*"   . 'porg-minus-face)
	      ("^\\??@@.*"   . 'porg-special-face))))))

(defvar porg-mode-hook nil
  "Hook called by  `porg-mode'")

(message "doing something")

;;;###autoload
(defun porg-mode ()
  "Major mode for editing porg code.
Special commands: \\{porg-mode-map}
Turning on porg-mode runs the hook `porg-mode-hook'."
  (message "running porg mode")
  (interactive)
  (porg-colors)
  (org-mode)
  (message "running porg mode2")
  (run-hooks 'porg-mode-hook)
)

;; Provide
(provide 'porg-mode)

;;; porg.el ends here
