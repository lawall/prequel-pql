@@
statement S;
expression list es;
@@

+ spin_lock(es);
S
- spin_lock(es);

@@
statement S;
expression list es;
@@

+ spin_lock_irqsave(es);
S
- spin_lock_irqsave(es);

@@
statement S;
expression list es;
@@

- spin_unlock(es);
S
+ spin_unlock(es);

@@
statement S;
expression list es;
@@

- spin_unlock_irqrestore(es);
S
+ spin_unlock_irqrestore(es);
