@depends on before || after@
expression e;
constant char[] c;
identifier f,print;
@@

e = f(...);
... when != e
if (<+...e...+>) {
  ... when any
- print(...,c,...);
  ... when any
}

@depends on before@
expression e;
constant char[] c;
identifier f,print;
statement S;
@@

e = f(...);
... when != e
if (<+...e...+>)
- {
- print(...,c,...);
  S
- }
