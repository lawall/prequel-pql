@r@
expression e;
identifier f1,f2;
@@

- e = f1(...);
+ e = f2(...);
  ...
- memset(e,0,...);

@script:ocaml@
f1 << r.f1;
f2 << r.f2;
@@

Printf.printf "%s -> %s" f1 f2
