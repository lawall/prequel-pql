@exists@
iterator name for_each_child_of_node;
expression e1,n;
@@

for_each_child_of_node(e1,n) {
  ... when any
+ of_node_put(n);
  ... when any
}
