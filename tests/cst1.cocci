@r1@
symbol a,x,y;
expression m;
@@

(
a(m)
|
- x(m)
+ y
)

@@
expression r1.m;
@@

(
f(m)
|
- g(m)
+ h(m)
)
