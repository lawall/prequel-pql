@@
expression res;
@@

- (res->end - res->start) + 1
+ resource_size(res)

@@
expression res;
@@

- res->end - res->start + 1
+ resource_size(res)
