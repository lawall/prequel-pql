@bad depends on before@
@@

(
devm_gpiod_get(...
 ,\(GPIOD_OUT_LOW\|GPIOD_OUT_HIGH\|GPIOD_IN\)
  )
|
devm_gpiod_get_optional(...
 ,\(GPIOD_OUT_LOW\|GPIOD_OUT_HIGH\|GPIOD_IN\)
  )
|
devm_gpiod_get_array_optional(...
 ,\(GPIOD_OUT_LOW\|GPIOD_OUT_HIGH\|GPIOD_IN\)
  )
|
devm_gpiod_get_index(...
 ,\(GPIOD_OUT_LOW\|GPIOD_OUT_HIGH\|GPIOD_IN\)
  )
)

@depends on !bad@
@@

devm_gpiod_get(...
+ ,\(GPIOD_OUT_LOW\|GPIOD_OUT_HIGH\|GPIOD_IN\)
  )

@depends on !bad@
@@

devm_gpiod_get_optional(...
+ ,\(GPIOD_OUT_LOW\|GPIOD_OUT_HIGH\|GPIOD_IN\)
  )

@depends on !bad@
@@

devm_gpiod_get_array_optional(...
+ ,\(GPIOD_OUT_LOW\|GPIOD_OUT_HIGH\|GPIOD_IN\)
  )

@depends on !bad@
@@

devm_gpiod_get_index(...
+ ,\(GPIOD_OUT_LOW\|GPIOD_OUT_HIGH\|GPIOD_IN\)
  )
