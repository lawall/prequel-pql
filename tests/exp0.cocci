virtual select
virtual select_rule1
virtual opportunities
virtual prequel

@rule1 depends on (!select || select_rule1)@
expression EXP0;
position __p;
@@
- EXP0->wr.fast_reg.iova_start@__p
+ EXP0->iova_start

@script:ocaml
 depends on (!select || select_rule1) && opportunities@
p << rule1.__p;
@@
Printf.printf "opportunity for rule1 in: %s:%s\n"
  (List.hd p).file (List.hd p).current_element

@prule1 depends on (!select || select_rule1) && prequel@
expression EXP0;
position __p;@@
- EXP0->wr.fast_reg.iova_start@__p
+ EXP0->iova_start
