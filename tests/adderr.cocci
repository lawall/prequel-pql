@@
statement S;
expression x;
identifier f,g;
@@

x = 
-f
+g
 (...);
...
if (
- x == NULL
+ IS_ERR(x)
  )
  S

@@
statement S1,S2;
expression x;
identifier f,g;
@@

x = 
-f
+g
 (...);
...
if (
- x == NULL
+ IS_ERR(x)
  )
  S1 else S2

@@
statement S;
expression x;
identifier f,g;
@@

x = 
-f
+g
 (...);
...
if (
- !x
+ IS_ERR(x)
  )
  S

@@
statement S1,S2;
expression x;
identifier f,g;
@@

x = 
-f
+g
 (...);
...
if (
- !x
+ IS_ERR(x)
  )
  S1 else S2



