@r@
expression x;
@@
x =
- pci_enable_msix
+ pci_enable_msix_exact
    (...)

@s@
expression x;
@@
x =
- pci_enable_msix
+ pci_enable_msix_range
    (...)
