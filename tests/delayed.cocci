@@
statement S;
@@

- if (<+...work_pending(...)...+>)
    S

@@
statement S;
@@

- if (<+...delayed_work_pending(...)...+>)
    S

@@
statement S,S2;
@@

- if (<+...work_pending(...)...+>)
    S else S2

@@
statement S,S2;
@@

- if (<+...delayed_work_pending(...)...+>)
    S else S2