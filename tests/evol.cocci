@bad1@
expression x;
identifier f;
position p;
@@

x =
- f@p
+ f
   (...)

@prebad2@
expression x;
identifier f,g;
position p != bad1.p;
@@

x =
- f@p
+ g
   (...)

@bad2a@
identifier prebad2.f;
@@

f(...) { ... }

@bad2b@
identifier prebad2.g;
@@

g(...) { ... }

@good depends on !bad2a && !bad2b@
expression x;
identifier f,g;
position prebad2.p;
@@

x =
- f@p
+ g
   (...)

@script:ocaml@
f << good.f;
g << good.g;
@@

Printf.printf "%s -> %s\n" f g
