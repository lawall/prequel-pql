@bad@
identifier virtual.f;
position p;
@@

- f@p(...)
+ f(...)

@depends on !bad@
identifier virtual.f;
position p != bad.p;
@@

- f@p(...)