@prerule depends on after@
identifier f;
expression *e;
statement S1;
@@

e = f(...);
... when != e
+ if (\(e == NULL\|NULL == e\|!e\))
 S1

@callstart depends on before || after @
position xp;
flexible expression e;
identifier prerule.f;
@@

e = f@xp(...);

@arule@
identifier prerule.f;
expression *e;
statement S1;
position callstart.xp;
@@

e = f@xp(...);
... when != e
+if (\(e == NULL\|NULL == e\|!e\)) S1
