@arule depends on before || after@
identifier f,fld;
expression *e;
statement S1,S2;
position xp;
@@

e = f@xp(...);
... when+ != e
    when- != if (e == NULL) S1 else S2
    when- != if (e != NULL) S1 else S2
+if (\(e == NULL\|NULL == e\|!e\)) S1
...
(
 *e
|
 e->fld
)
... when any