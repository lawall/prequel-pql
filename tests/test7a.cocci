@callstart depends on before || after @
position xp;
flexible expression e;
identifier f;
@@

e = f@xp(...);

@arule@
identifier callstart.f;
expression *e,e1;
statement S1;
position callstart.xp;
@@

e = f@xp(...);
... when != e = e1
+if (\(e == NULL\|NULL == e\|!e\)) S1
