@added depends on after@
declarer name DEVICE_ATTR;
declarer name DEVICE_ATTR_RO;
declarer name DEVICE_ATTR_WO;
declarer name DEVICE_ATTR_RW;
expression nm,mode,show,store;
@@

(
+DEVICE_ATTR(nm,mode,show,store);
|
+DEVICE_ATTR_RO(nm);
|
+DEVICE_ATTR_WO(nm);
|
+DEVICE_ATTR_RW(nm);
)

@bad depends on before@
declarer name DEVICE_ATTR;
declarer name DEVICE_ATTR_RO;
declarer name DEVICE_ATTR_WO;
declarer name DEVICE_ATTR_RW;
expression added.nm,added.mode,added.show,added.store;
@@

(
DEVICE_ATTR(nm,mode,show,store);
|
DEVICE_ATTR_RO(nm);
|
DEVICE_ATTR_WO(nm);
|
DEVICE_ATTR_RW(nm);
)

@added2 depends on after && bad@
declarer name DEVICE_ATTR;
declarer name DEVICE_ATTR_RO;
declarer name DEVICE_ATTR_WO;
declarer name DEVICE_ATTR_RW;
expression added.nm,mode,show,store;
@@

(
+DEVICE_ATTR(nm,mode,show,store);
|
+DEVICE_ATTR_RO(nm);
|
+DEVICE_ATTR_WO(nm);
|
+DEVICE_ATTR_RW(nm);
)

@script:ocaml depends on added2@
nm << added.nm;
c << virtual.commit;
@@

let linux = "/run/shm/linux-next" in

let checkon side nm commit =
  let info =
    Common.cmd_to_list
      (Printf.sprintf "cd %s; git show --name-only %s" linux commit) in
  let docabi s =
    try
      let _ = Str.search_forward (Str.regexp "Documentation/ABI/") s 0 in true
    with Not_found -> false in
  match List.filter docabi info with
    [] -> false
  | files ->
      List.exists
	(function file ->
	  List.length
	    (Common.cmd_to_list
	       (Printf.sprintf "cd %s ; git show %s | grep ^%s | grep -w %s"
		  linux commit side nm)) > 0)
	files in

let safehd = function x::xs -> Some x | _ -> None in

let neighbors =
  let author =
    let a =
      Common.cmd_to_list
	(Printf.sprintf "cd %s; git show --format=\"%%aN\" %s" linux c) in
    match a with [] -> "" | _ -> "--author=\""^(List.hd a)^"\"" in
  let before =
    safehd
      (Common.cmd_to_list (Printf.sprintf "cd %s; git describe %s" linux c)) in
  let after =
    safehd
      (Common.cmd_to_list
	 (Printf.sprintf "cd %s; git describe --contains %s" linux c)) in
  let simplify c =
    let pieces = Str.split (Str.regexp "[-~]") c in
    match pieces with
      ver::rc::rest when Str.string_match (Str.regexp "rc") rc 0 ->
	ver ^ "-" ^ rc
    | ver::rest -> ver
    | _ -> failwith "unexpected tag" in
  match (before,after) with
      (None,_) | (_,None) -> [c]
    | (Some b,Some a) ->
	let b = simplify b in
	let a = simplify a in
	let res =
	  Common.cmd_to_list
	    (Printf.sprintf
	       "cd %s; git log --oneline --format=\"%%h\" %s..%s %s Documentation"
	       linux b a author) in
	if not (List.mem c res) then c::res else res in
let doc =
  try Some(List.find (checkon "+" nm) neighbors)
  with Not_found -> None in
match doc with
  Some doc -> Printf.printf "%s added with doc in %s\n" nm doc
| None -> Printf.printf "%s added with no doc\n" nm

(* ------------------------------------------------------------------- *)

@rename1@
declarer name DEVICE_ATTR;
expression nm,nm1,mode,show,store;
@@

DEVICE_ATTR(
-nm
+nm1
  ,mode,show,store);

@script:ocaml@
nm << rename1.nm;
nm1 << rename1.nm1;
c << virtual.commit;
@@


let linux = "/run/shm/linux-next" in

let checkon side nm commit =
  let info =
    Common.cmd_to_list
      (Printf.sprintf "cd %s; git show --name-only %s" linux commit) in
  let docabi s =
    try
      let _ = Str.search_forward (Str.regexp "Documentation/ABI/") s 0 in true
    with Not_found -> false in
  match List.filter docabi info with
    [] -> false
  | files ->
      List.exists
	(function file ->
	  List.length
	    (Common.cmd_to_list
	       (Printf.sprintf "cd %s ; git show %s | grep ^%s | grep -w %s"
		  linux commit side nm)) > 0)
	files in

let safehd = function x::xs -> Some x | _ -> None in

let neighbors =
  let author =
    let a =
      Common.cmd_to_list
	(Printf.sprintf "cd %s; git show --format=\"%%aN\" %s" linux c) in
    match a with [] -> "" | _ -> "--author=\""^(List.hd a)^"\"" in
  let before =
    safehd
      (Common.cmd_to_list (Printf.sprintf "cd %s; git describe %s" linux c)) in
  let after =
    safehd
      (Common.cmd_to_list
	 (Printf.sprintf "cd %s; git describe --contains %s" linux c)) in
  let simplify c =
    let pieces = Str.split (Str.regexp "[-~]") c in
    match pieces with
      ver::rc::rest when Str.string_match (Str.regexp "rc") rc 0 ->
	ver ^ "-" ^ rc
    | ver::rest -> ver
    | _ -> failwith "unexpected tag" in
  match (before,after) with
      (None,_) | (_,None) -> [c]
    | (Some b,Some a) ->
	let b = simplify b in
	let a = simplify a in
	let res =
	  Common.cmd_to_list
	    (Printf.sprintf
	       "cd %s; git log --oneline --format=\"%%h\" %s..%s %s Documentation"
	       linux b a author) in
	if not (List.mem c res) then c::res else res in
let docm = List.filter (checkon "-" nm) neighbors in
let docp = List.filter (checkon "+" nm1) neighbors in
let both = List.filter (fun x -> List.mem x docp) docm in

match (both,docm,docp) with
  (_,[],_) | (_,_,[]) ->
    Printf.printf "%s changed to %s with no doc\n" nm nm1
| ([],_,_) ->
    Printf.printf "%s changed to %s with no doc, but see -%s +%s\n" nm nm1
      (String.concat " " docm) (String.concat " " docp)
| ([c],_,_) ->
    Printf.printf "%s changed to %s with doc in %s\n" nm nm1 c
| (c,_,_) ->
    Printf.printf "%s changed to %s with multi docs? %s\n" nm nm1
      (String.concat " " c)

@rename2@
declarer name DEVICE_ATTR_RO;
expression nm,nm1;
@@

DEVICE_ATTR_RO(
- nm
+ nm1
  );

@script:ocaml@
nm << rename2.nm;
nm1 << rename2.nm1;
c << virtual.commit;
@@


let linux = "/run/shm/linux-next" in

let checkon side nm commit =
  let info =
    Common.cmd_to_list
      (Printf.sprintf "cd %s; git show --name-only %s" linux commit) in
  let docabi s =
    try
      let _ = Str.search_forward (Str.regexp "Documentation/ABI/") s 0 in true
    with Not_found -> false in
  match List.filter docabi info with
    [] -> false
  | files ->
      List.exists
	(function file ->
	  List.length
	    (Common.cmd_to_list
	       (Printf.sprintf "cd %s ; git show %s | grep ^%s | grep -w %s"
		  linux commit side nm)) > 0)
	files in

let safehd = function x::xs -> Some x | _ -> None in

let neighbors =
  let author =
    let a =
      Common.cmd_to_list
	(Printf.sprintf "cd %s; git show --format=\"%%aN\" %s" linux c) in
    match a with [] -> "" | _ -> "--author=\""^(List.hd a)^"\"" in
  let before =
    safehd
      (Common.cmd_to_list (Printf.sprintf "cd %s; git describe %s" linux c)) in
  let after =
    safehd
      (Common.cmd_to_list
	 (Printf.sprintf "cd %s; git describe --contains %s" linux c)) in
  let simplify c =
    let pieces = Str.split (Str.regexp "[-~]") c in
    match pieces with
      ver::rc::rest when Str.string_match (Str.regexp "rc") rc 0 ->
	ver ^ "-" ^ rc
    | ver::rest -> ver
    | _ -> failwith "unexpected tag" in
  match (before,after) with
      (None,_) | (_,None) -> [c]
    | (Some b,Some a) ->
	let b = simplify b in
	let a = simplify a in
	let res =
	  Common.cmd_to_list
	    (Printf.sprintf
	       "cd %s; git log --oneline --format=\"%%h\" %s..%s %s Documentation"
	       linux b a author) in
	if not (List.mem c res) then c::res else res in
let docm = List.filter (checkon "-" nm) neighbors in
let docp = List.filter (checkon "+" nm1) neighbors in
let both = List.filter (fun x -> List.mem x docp) docm in

match (both,docm,docp) with
  (_,[],_) | (_,_,[]) ->
    Printf.printf "%s changed to %s with no doc\n" nm nm1
| ([],_,_) ->
    Printf.printf "%s changed to %s with no doc, but see -%s +%s\n" nm nm1
      (String.concat " " docm) (String.concat " " docp)
| ([c],_,_) ->
    Printf.printf "%s changed to %s with doc in %s\n" nm nm1 c
| (c,_,_) ->
    Printf.printf "%s changed to %s with multi docs? %s\n" nm nm1
      (String.concat " " c)

@rename3@
declarer name DEVICE_ATTR_WO;
expression nm,nm1;
@@

DEVICE_ATTR_WO(
- nm
+ nm1
  );

@script:ocaml@
nm << rename3.nm;
nm1 << rename3.nm1;
c << virtual.commit;
@@

let linux = "/run/shm/linux-next" in

let checkon side nm commit =
  let info =
    Common.cmd_to_list
      (Printf.sprintf "cd %s; git show --name-only %s" linux commit) in
  let docabi s =
    try
      let _ = Str.search_forward (Str.regexp "Documentation/ABI/") s 0 in true
    with Not_found -> false in
  match List.filter docabi info with
    [] -> false
  | files ->
      List.exists
	(function file ->
	  List.length
	    (Common.cmd_to_list
	       (Printf.sprintf "cd %s ; git show %s | grep ^%s | grep -w %s"
		  linux commit side nm)) > 0)
	files in

let safehd = function x::xs -> Some x | _ -> None in

let neighbors =
  let author =
    let a =
      Common.cmd_to_list
	(Printf.sprintf "cd %s; git show --format=\"%%aN\" %s" linux c) in
    match a with [] -> "" | _ -> "--author=\""^(List.hd a)^"\"" in
  let before =
    safehd
      (Common.cmd_to_list (Printf.sprintf "cd %s; git describe %s" linux c)) in
  let after =
    safehd
      (Common.cmd_to_list
	 (Printf.sprintf "cd %s; git describe --contains %s" linux c)) in
  let simplify c =
    let pieces = Str.split (Str.regexp "[-~]") c in
    match pieces with
      ver::rc::rest when Str.string_match (Str.regexp "rc") rc 0 ->
	ver ^ "-" ^ rc
    | ver::rest -> ver
    | _ -> failwith "unexpected tag" in
  match (before,after) with
      (None,_) | (_,None) -> [c]
    | (Some b,Some a) ->
	let b = simplify b in
	let a = simplify a in
	let res =
	  Common.cmd_to_list
	    (Printf.sprintf
	       "cd %s; git log --oneline --format=\"%%h\" %s..%s %s Documentation"
	       linux b a author) in
	if not (List.mem c res) then c::res else res in
let docm = List.filter (checkon "-" nm) neighbors in
let docp = List.filter (checkon "+" nm1) neighbors in
let both = List.filter (fun x -> List.mem x docp) docm in

match (both,docm,docp) with
  (_,[],_) | (_,_,[]) ->
    Printf.printf "%s changed to %s with no doc\n" nm nm1
| ([],_,_) ->
    Printf.printf "%s changed to %s with no doc, but see -%s +%s\n" nm nm1
      (String.concat " " docm) (String.concat " " docp)
| ([c],_,_) ->
    Printf.printf "%s changed to %s with doc in %s\n" nm nm1 c
| (c,_,_) ->
    Printf.printf "%s changed to %s with multi docs? %s\n" nm nm1
      (String.concat " " c)

@rename4@
declarer name DEVICE_ATTR_RW;
expression nm,nm1;
@@

DEVICE_ATTR_RW(
- nm
+ nm1
  );

@script:ocaml@
nm << rename4.nm;
nm1 << rename4.nm1;
c << virtual.commit;
@@

let linux = "/run/shm/linux-next" in

let checkon side nm commit =
  let info =
    Common.cmd_to_list
      (Printf.sprintf "cd %s; git show --name-only %s" linux commit) in
  let docabi s =
    try
      let _ = Str.search_forward (Str.regexp "Documentation/ABI/") s 0 in true
    with Not_found -> false in
  match List.filter docabi info with
    [] -> false
  | files ->
      List.exists
	(function file ->
	  List.length
	    (Common.cmd_to_list
	       (Printf.sprintf "cd %s ; git show %s | grep ^%s | grep -w %s"
		  linux commit side nm)) > 0)
	files in

let safehd = function x::xs -> Some x | _ -> None in

let neighbors =
  let author =
    let a =
      Common.cmd_to_list
	(Printf.sprintf "cd %s; git show --format=\"%%aN\" %s" linux c) in
    match a with [] -> "" | _ -> "--author=\""^(List.hd a)^"\"" in
  let before =
    safehd
      (Common.cmd_to_list (Printf.sprintf "cd %s; git describe %s" linux c)) in
  let after =
    safehd
      (Common.cmd_to_list
	 (Printf.sprintf "cd %s; git describe --contains %s" linux c)) in
  let simplify c =
    let pieces = Str.split (Str.regexp "[-~]") c in
    match pieces with
      ver::rc::rest when Str.string_match (Str.regexp "rc") rc 0 ->
	ver ^ "-" ^ rc
    | ver::rest -> ver
    | _ -> failwith "unexpected tag" in
  match (before,after) with
      (None,_) | (_,None) -> [c]
    | (Some b,Some a) ->
	let b = simplify b in
	let a = simplify a in
	let res =
	  Common.cmd_to_list
	    (Printf.sprintf
	       "cd %s; git log --oneline --format=\"%%h\" %s..%s %s Documentation"
	       linux b a author) in
	if not (List.mem c res) then c::res else res in
let docm = List.filter (checkon "-" nm) neighbors in
let docp = List.filter (checkon "+" nm1) neighbors in
let both = List.filter (fun x -> List.mem x docp) docm in

match (both,docm,docp) with
  (_,[],_) | (_,_,[]) ->
    Printf.printf "%s changed to %s with no doc\n" nm nm1
| ([],_,_) ->
    Printf.printf "%s changed to %s with no doc, but see -%s +%s\n" nm nm1
      (String.concat " " docm) (String.concat " " docp)
| ([c],_,_) ->
    Printf.printf "%s changed to %s with doc in %s\n" nm nm1 c
| (c,_,_) ->
    Printf.printf "%s changed to %s with multi docs? %s\n" nm nm1
      (String.concat " " c)

(* ------------------------------------------------------------------- *)

@remode1@
declarer name DEVICE_ATTR;
expression nm,mode,mode1;
@@

DEVICE_ATTR(nm,
-mode
+mode1
  ,...);

@script:ocaml@
nm << remode1.nm;
mode << remode1.mode;
mode1 << remode1.mode1;
c << virtual.commit;
@@

let linux = "/run/shm/linux-next" in

let checkon side nm commit =
  let info =
    Common.cmd_to_list
      (Printf.sprintf "cd %s; git show --name-only %s" linux commit) in
  let docabi s =
    try
      let _ = Str.search_forward (Str.regexp "Documentation/ABI/") s 0 in true
    with Not_found -> false in
  match List.filter docabi info with
    [] -> false
  | files ->
      List.exists
	(function file ->
	  List.length
	    (Common.cmd_to_list
	       (Printf.sprintf "cd %s ; git show %s | grep ^%s | grep -w %s"
		  linux commit side nm)) > 0)
	files in

let safehd = function x::xs -> Some x | _ -> None in

let neighbors =
  let author =
    let a =
      Common.cmd_to_list
	(Printf.sprintf "cd %s; git show --format=\"%%aN\" %s" linux c) in
    match a with [] -> "" | _ -> "--author=\""^(List.hd a)^"\"" in
  let before =
    safehd
      (Common.cmd_to_list (Printf.sprintf "cd %s; git describe %s" linux c)) in
  let after =
    safehd
      (Common.cmd_to_list
	 (Printf.sprintf "cd %s; git describe --contains %s" linux c)) in
  let simplify c =
    let pieces = Str.split (Str.regexp "[-~]") c in
    match pieces with
      ver::rc::rest when Str.string_match (Str.regexp "rc") rc 0 ->
	ver ^ "-" ^ rc
    | ver::rest -> ver
    | _ -> failwith "unexpected tag" in
  match (before,after) with
      (None,_) | (_,None) -> [c]
    | (Some b,Some a) ->
	let b = simplify b in
	let a = simplify a in
	let res =
	  Common.cmd_to_list
	    (Printf.sprintf
	       "cd %s; git log --oneline --format=\"%%h\" %s..%s %s Documentation"
	       linux b a author) in
	if not (List.mem c res) then c::res else res in
match (mode,mode1) with
  ("S_IRUGO","0444") | ("0444","S_IRUGO")
| ("S_IRUGO | S_IWUSR","0644") | ("0644","S_IRUGO | S_IWUSR") ->
    Printf.printf "no semantic change for %s\n" nm
| _ ->
    let doc =
      try Some(List.find (checkon "+" nm) neighbors)
      with Not_found -> None in
    match doc with
      Some doc ->
	Printf.printf "%s changed mode (%s -> %s) with doc in %s\n"
	  nm mode mode1 doc
    | None ->
	Printf.printf "%s changed mode (%s -> %s) with no doc\n" nm
	  mode mode1

@remode2@
declarer name DEVICE_ATTR_RO;
declarer name DEVICE_ATTR_WO;
expression nm;
@@

- DEVICE_ATTR_RO
+ DEVICE_ATTR_WO
   (nm);

@script:ocaml@
nm << remode2.nm;
c << virtual.commit;
@@

let linux = "/run/shm/linux-next" in

let checkon side nm commit =
  let info =
    Common.cmd_to_list
      (Printf.sprintf "cd %s; git show --name-only %s" linux commit) in
  let docabi s =
    try
      let _ = Str.search_forward (Str.regexp "Documentation/ABI/") s 0 in true
    with Not_found -> false in
  match List.filter docabi info with
    [] -> false
  | files ->
      List.exists
	(function file ->
	  List.length
	    (Common.cmd_to_list
	       (Printf.sprintf "cd %s ; git show %s | grep ^%s | grep -w %s"
		  linux commit side nm)) > 0)
	files in

let safehd = function x::xs -> Some x | _ -> None in

let neighbors =
  let author =
    let a =
      Common.cmd_to_list
	(Printf.sprintf "cd %s; git show --format=\"%%aN\" %s" linux c) in
    match a with [] -> "" | _ -> "--author=\""^(List.hd a)^"\"" in
  let before =
    safehd
      (Common.cmd_to_list (Printf.sprintf "cd %s; git describe %s" linux c)) in
  let after =
    safehd
      (Common.cmd_to_list
	 (Printf.sprintf "cd %s; git describe --contains %s" linux c)) in
  let simplify c =
    let pieces = Str.split (Str.regexp "[-~]") c in
    match pieces with
      ver::rc::rest when Str.string_match (Str.regexp "rc") rc 0 ->
	ver ^ "-" ^ rc
    | ver::rest -> ver
    | _ -> failwith "unexpected tag" in
  match (before,after) with
      (None,_) | (_,None) -> [c]
    | (Some b,Some a) ->
	let b = simplify b in
	let a = simplify a in
	let res =
	  Common.cmd_to_list
	    (Printf.sprintf
	       "cd %s; git log --oneline --format=\"%%h\" %s..%s %s Documentation"
	       linux b a author) in
	if not (List.mem c res) then c::res else res in
let doc =
  try Some(List.find (checkon "+" nm) neighbors)
  with Not_found -> None in
match doc with
  Some doc -> Printf.printf "%s changed mode with doc in %s\n" nm doc
| None -> Printf.printf "%s changed mode with no doc\n" nm


@remode3@
declarer name DEVICE_ATTR_RO;
declarer name DEVICE_ATTR_RW;
expression nm;
@@

- DEVICE_ATTR_RO
+ DEVICE_ATTR_RW
   (nm);

@script:ocaml@
nm << remode3.nm;
c << virtual.commit;
@@

let linux = "/run/shm/linux-next" in

let checkon side nm commit =
  let info =
    Common.cmd_to_list
      (Printf.sprintf "cd %s; git show --name-only %s" linux commit) in
  let docabi s =
    try
      let _ = Str.search_forward (Str.regexp "Documentation/ABI/") s 0 in true
    with Not_found -> false in
  match List.filter docabi info with
    [] -> false
  | files ->
      List.exists
	(function file ->
	  List.length
	    (Common.cmd_to_list
	       (Printf.sprintf "cd %s ; git show %s | grep ^%s | grep -w %s"
		  linux commit side nm)) > 0)
	files in

let safehd = function x::xs -> Some x | _ -> None in

let neighbors =
  let author =
    let a =
      Common.cmd_to_list
	(Printf.sprintf "cd %s; git show --format=\"%%aN\" %s" linux c) in
    match a with [] -> "" | _ -> "--author=\""^(List.hd a)^"\"" in
  let before =
    safehd
      (Common.cmd_to_list (Printf.sprintf "cd %s; git describe %s" linux c)) in
  let after =
    safehd
      (Common.cmd_to_list
	 (Printf.sprintf "cd %s; git describe --contains %s" linux c)) in
  let simplify c =
    let pieces = Str.split (Str.regexp "[-~]") c in
    match pieces with
      ver::rc::rest when Str.string_match (Str.regexp "rc") rc 0 ->
	ver ^ "-" ^ rc
    | ver::rest -> ver
    | _ -> failwith "unexpected tag" in
  match (before,after) with
      (None,_) | (_,None) -> [c]
    | (Some b,Some a) ->
	let b = simplify b in
	let a = simplify a in
	let res =
	  Common.cmd_to_list
	    (Printf.sprintf
	       "cd %s; git log --oneline --format=\"%%h\" %s..%s %s Documentation"
	       linux b a author) in
	if not (List.mem c res) then c::res else res in
let doc =
  try Some(List.find (checkon "+" nm) neighbors)
  with Not_found -> None in
match doc with
  Some doc -> Printf.printf "%s changed mode with doc in %s\n" nm doc
| None -> Printf.printf "%s changed mode with no doc\n" nm


@remode4@
declarer name DEVICE_ATTR_RO;
declarer name DEVICE_ATTR_WO;
expression nm;
@@

- DEVICE_ATTR_WO
+ DEVICE_ATTR_RO
   (nm);

@script:ocaml@
nm << remode4.nm;
c << virtual.commit;
@@

let linux = "/run/shm/linux-next" in

let checkon side nm commit =
  let info =
    Common.cmd_to_list
      (Printf.sprintf "cd %s; git show --name-only %s" linux commit) in
  let docabi s =
    try
      let _ = Str.search_forward (Str.regexp "Documentation/ABI/") s 0 in true
    with Not_found -> false in
  match List.filter docabi info with
    [] -> false
  | files ->
      List.exists
	(function file ->
	  List.length
	    (Common.cmd_to_list
	       (Printf.sprintf "cd %s ; git show %s | grep ^%s | grep -w %s"
		  linux commit side nm)) > 0)
	files in

let safehd = function x::xs -> Some x | _ -> None in

let neighbors =
  let author =
    let a =
      Common.cmd_to_list
	(Printf.sprintf "cd %s; git show --format=\"%%aN\" %s" linux c) in
    match a with [] -> "" | _ -> "--author=\""^(List.hd a)^"\"" in
  let before =
    safehd
      (Common.cmd_to_list (Printf.sprintf "cd %s; git describe %s" linux c)) in
  let after =
    safehd
      (Common.cmd_to_list
	 (Printf.sprintf "cd %s; git describe --contains %s" linux c)) in
  let simplify c =
    let pieces = Str.split (Str.regexp "[-~]") c in
    match pieces with
      ver::rc::rest when Str.string_match (Str.regexp "rc") rc 0 ->
	ver ^ "-" ^ rc
    | ver::rest -> ver
    | _ -> failwith "unexpected tag" in
  match (before,after) with
      (None,_) | (_,None) -> [c]
    | (Some b,Some a) ->
	let b = simplify b in
	let a = simplify a in
	let res =
	  Common.cmd_to_list
	    (Printf.sprintf
	       "cd %s; git log --oneline --format=\"%%h\" %s..%s %s Documentation"
	       linux b a author) in
	if not (List.mem c res) then c::res else res in
let doc =
  try Some(List.find (checkon "+" nm) neighbors)
  with Not_found -> None in
match doc with
  Some doc -> Printf.printf "%s changed mode with doc in %s\n" nm doc
| None -> Printf.printf "%s changed mode with no doc\n" nm


@remode5@
declarer name DEVICE_ATTR_RO;
declarer name DEVICE_ATTR_RW;
expression nm;
@@

- DEVICE_ATTR_WO
+ DEVICE_ATTR_RW
   (nm);

@script:ocaml@
nm << remode5.nm;
c << virtual.commit;
@@

let linux = "/run/shm/linux-next" in

let checkon side nm commit =
  let info =
    Common.cmd_to_list
      (Printf.sprintf "cd %s; git show --name-only %s" linux commit) in
  let docabi s =
    try
      let _ = Str.search_forward (Str.regexp "Documentation/ABI/") s 0 in true
    with Not_found -> false in
  match List.filter docabi info with
    [] -> false
  | files ->
      List.exists
	(function file ->
	  List.length
	    (Common.cmd_to_list
	       (Printf.sprintf "cd %s ; git show %s | grep ^%s | grep -w %s"
		  linux commit side nm)) > 0)
	files in

let safehd = function x::xs -> Some x | _ -> None in

let neighbors =
  let author =
    let a =
      Common.cmd_to_list
	(Printf.sprintf "cd %s; git show --format=\"%%aN\" %s" linux c) in
    match a with [] -> "" | _ -> "--author=\""^(List.hd a)^"\"" in
  let before =
    safehd
      (Common.cmd_to_list (Printf.sprintf "cd %s; git describe %s" linux c)) in
  let after =
    safehd
      (Common.cmd_to_list
	 (Printf.sprintf "cd %s; git describe --contains %s" linux c)) in
  let simplify c =
    let pieces = Str.split (Str.regexp "[-~]") c in
    match pieces with
      ver::rc::rest when Str.string_match (Str.regexp "rc") rc 0 ->
	ver ^ "-" ^ rc
    | ver::rest -> ver
    | _ -> failwith "unexpected tag" in
  match (before,after) with
      (None,_) | (_,None) -> [c]
    | (Some b,Some a) ->
	let b = simplify b in
	let a = simplify a in
	let res =
	  Common.cmd_to_list
	    (Printf.sprintf
	       "cd %s; git log --oneline --format=\"%%h\" %s..%s %s Documentation"
	       linux b a author) in
	if not (List.mem c res) then c::res else res in
let doc =
  try Some(List.find (checkon "+" nm) neighbors)
  with Not_found -> None in
match doc with
  Some doc -> Printf.printf "%s changed mode with doc in %s\n" nm doc
| None -> Printf.printf "%s changed mode with no doc\n" nm

@remode6@
declarer name DEVICE_ATTR_RO;
declarer name DEVICE_ATTR_WO;
expression nm;
@@

- DEVICE_ATTR_RW
+ DEVICE_ATTR_RO
   (nm);

@script:ocaml@
nm << remode6.nm;
c << virtual.commit;
@@

let linux = "/run/shm/linux-next" in

let checkon side nm commit =
  let info =
    Common.cmd_to_list
      (Printf.sprintf "cd %s; git show --name-only %s" linux commit) in
  let docabi s =
    try
      let _ = Str.search_forward (Str.regexp "Documentation/ABI/") s 0 in true
    with Not_found -> false in
  match List.filter docabi info with
    [] -> false
  | files ->
      List.exists
	(function file ->
	  List.length
	    (Common.cmd_to_list
	       (Printf.sprintf "cd %s ; git show %s | grep ^%s | grep -w %s"
		  linux commit side nm)) > 0)
	files in

let safehd = function x::xs -> Some x | _ -> None in

let neighbors =
  let author =
    let a =
      Common.cmd_to_list
	(Printf.sprintf "cd %s; git show --format=\"%%aN\" %s" linux c) in
    match a with [] -> "" | _ -> "--author=\""^(List.hd a)^"\"" in
  let before =
    safehd
      (Common.cmd_to_list (Printf.sprintf "cd %s; git describe %s" linux c)) in
  let after =
    safehd
      (Common.cmd_to_list
	 (Printf.sprintf "cd %s; git describe --contains %s" linux c)) in
  let simplify c =
    let pieces = Str.split (Str.regexp "[-~]") c in
    match pieces with
      ver::rc::rest when Str.string_match (Str.regexp "rc") rc 0 ->
	ver ^ "-" ^ rc
    | ver::rest -> ver
    | _ -> failwith "unexpected tag" in
  match (before,after) with
      (None,_) | (_,None) -> [c]
    | (Some b,Some a) ->
	let b = simplify b in
	let a = simplify a in
	let res =
	  Common.cmd_to_list
	    (Printf.sprintf
	       "cd %s; git log --oneline --format=\"%%h\" %s..%s %s Documentation"
	       linux b a author) in
	if not (List.mem c res) then c::res else res in
let doc =
  try Some(List.find (checkon "+" nm) neighbors)
  with Not_found -> None in
match doc with
  Some doc -> Printf.printf "%s changed mode with doc in %s\n" nm doc
| None -> Printf.printf "%s changed mode with no doc\n" nm

@remode7@
declarer name DEVICE_ATTR_RO;
declarer name DEVICE_ATTR_RW;
expression nm;
@@

- DEVICE_ATTR_RW
+ DEVICE_ATTR_WO
   (nm);

@script:ocaml@
nm << remode7.nm;
c << virtual.commit;
@@

let linux = "/run/shm/linux-next" in

let checkon side nm commit =
  let info =
    Common.cmd_to_list
      (Printf.sprintf "cd %s; git show --name-only %s" linux commit) in
  let docabi s =
    try
      let _ = Str.search_forward (Str.regexp "Documentation/ABI/") s 0 in true
    with Not_found -> false in
  match List.filter docabi info with
    [] -> false
  | files ->
      List.exists
	(function file ->
	  List.length
	    (Common.cmd_to_list
	       (Printf.sprintf "cd %s ; git show %s | grep ^%s | grep -w %s"
		  linux commit side nm)) > 0)
	files in

let safehd = function x::xs -> Some x | _ -> None in

let neighbors =
  let author =
    let a =
      Common.cmd_to_list
	(Printf.sprintf "cd %s; git show --format=\"%%aN\" %s" linux c) in
    match a with [] -> "" | _ -> "--author=\""^(List.hd a)^"\"" in
  let before =
    safehd
      (Common.cmd_to_list (Printf.sprintf "cd %s; git describe %s" linux c)) in
  let after =
    safehd
      (Common.cmd_to_list
	 (Printf.sprintf "cd %s; git describe --contains %s" linux c)) in
  let simplify c =
    let pieces = Str.split (Str.regexp "[-~]") c in
    match pieces with
      ver::rc::rest when Str.string_match (Str.regexp "rc") rc 0 ->
	ver ^ "-" ^ rc
    | ver::rest -> ver
    | _ -> failwith "unexpected tag" in
  match (before,after) with
      (None,_) | (_,None) -> [c]
    | (Some b,Some a) ->
	let b = simplify b in
	let a = simplify a in
	let res =
	  Common.cmd_to_list
	    (Printf.sprintf
	       "cd %s; git log --oneline --format=\"%%h\" %s..%s %s Documentation"
	       linux b a author) in
	if not (List.mem c res) then c::res else res in
let doc =
  try Some(List.find (checkon "+" nm) neighbors)
  with Not_found -> None in
match doc with
  Some doc -> Printf.printf "%s changed mode with doc in %s\n" nm doc
| None -> Printf.printf "%s changed mode with no doc\n" nm
