@bad depends on before || after@
flexible expression list[n] es;
@@

iio_get_time_ns(es)

@depends on !bad && (before || after)@
expression e;
@@

iio_get_time_ns(...,
+ e,
  ...)
