@initialize:ocaml @
hunk_file << virtual.hunk_file;
@@

type repl = REPLACEMENT | ADDREMOVE
let minus_hunks = Hashtbl.create 101
let plus_hunks = Hashtbl.create 101
let hunks_to_lines = Hashtbl.create 101

type data = string (*file*) * (int * int) (*start/end*) * int (*hunknum*)
type tree = Node of data * tree * tree | Leaf

let minus_tree = ref Leaf
let plus_tree = ref Leaf

type 'a tokty = Meta of 'a | Concrete of 'a

type start_end =
    Region of (Coccilib.pos list * Coccilib.pos list) tokty list
  | BeforeEmpty of Coccilib.pos list
  | AfterEmpty of Coccilib.pos list
  | AroundEmpty of Coccilib.pos list * Coccilib.pos list

(* ----------------------------------------------------------------------- *)

let rec iota n end_line =
  (if n > end_line
  then failwith (Printf.sprintf "bad arguments to iota: %d %d" n end_line));
  if n = end_line then [n] else n :: iota (n+1) end_line

let rec split n = function
    [] -> ([],[])
  | (x::xs) as all ->
      if n = 0
      then ([],all)
      else
        let (f,r) = split (n-1) xs in
        (x::f,r)

let rec build n = function
    [] -> Leaf
  | [x] -> Node(x,Leaf,Leaf)
  | l ->
      let n2 = n / 2 in
      let (f,r) = split n2 l in
      match r with
        [] -> failwith "not possible"
      | r::rs ->
          let rslen = if n2 * 2 = n then n2-1 else n2 in
          Node (r, build n2 f, build rslen rs)

let mktree l = build (List.length l) (List.sort compare l)

let search file line tree =
  let rec search tree parent =
    match tree with
      Node((fl,(s,e),n),lc,rc) ->
        if file = fl && s <= line && line <= e
        then tree
        else
          if fl > file || (fl = file && s > line)
          then search lc parent
          else search rc tree
    | Leaf -> parent in
  match search tree Leaf with
    Node((fl,(s,e),n),_,_) when file = fl -> Some n
  | _ -> None

(* ----------------------------------------------------------------------- *)

let ios = int_of_string

let fromto n1 n2 =
  let rec loop n1 = if n1 > n2 then [] else n1 :: loop (n1+1) in
  loop n1

let hunknum = fst

let _parse_hunk_data = (* executed during initialize *)
  let i = open_in hunk_file in
  let mhunks = ref [] in
  let phunks = ref [] in
  let rec loop _ =
    let l = input_line i in
    (match Str.split (Str.regexp "|") l with
    | ["Hunk";n;ty;file;mstarter;mender;pstarter;pender] ->
	let n = ios n in
	let ty = if ty = "R" then REPLACEMENT else ADDREMOVE in
	let mstarter = ios mstarter in
	let mender = ios mender in
	let pstarter = ios pstarter in
	let pender = ios pender in
	let infos = (ty,mstarter,mender,pstarter,pender) in
	Hashtbl.add hunks_to_lines n (file,infos);
	mhunks := (file,(mstarter,mender),n) :: !mhunks;
	phunks := (file,(pstarter,pender),n) :: !phunks;
	(if mstarter <= mender
	then
	  List.iter (function l -> Hashtbl.add minus_hunks (file,l) (n,infos))
	    (fromto mstarter mender));
	(if pstarter <= pender
	then
	  List.iter (function l -> Hashtbl.add plus_hunks (file,l) (n,infos))
	    (fromto pstarter pender))
    | _ -> failwith (Printf.sprintf "bad hunk record: %s" l));
    loop() in
  try loop()
  with End_of_file ->
    begin
      minus_tree := mktree !mhunks;
      plus_tree := mktree !phunks
    end

(* ----------------------------------------------------------------------- *)

let start_record _ =
  flush stdout; Printf.printf "start record\n"; flush stdout
let end_record _ =
  flush stdout; Printf.printf "end record\n"; flush stdout

let normalize_points compare_by_line starter ender =
  let (starter,ender) = (* probably not safe in obscure cases *)
    if List.length starter = List.length ender
    then (starter,ender)
    else
      match (starter,ender) with (* not sure this is useful *)
	([starter],_) -> (List.map (function _ -> starter) ender, ender)
      |	(_,[ender]) -> (starter, List.map (function _ -> ender) starter)
      | _ ->
	  failwith
	    "not able to accomodate position lists of different lengths" in
  let starter = List.sort compare_by_line starter in
  let ender = List.sort compare_by_line ender in
  (starter,ender)

let string_of_hunk (k,f,h,l,el) = Printf.sprintf "%s:%s:%d:%d:%d" k f h l el

let thefile = ref ""
let get_file key p =
  let drop_parmap fl =
    let res =
      String.concat "/" (List.tl (Str.split (Str.regexp "/") fl)) in
    (if not (res = !thefile) then thefile := res);
    res in
  let fl = p.Coccilib.file in
  match key with
      "M" ->
	(try drop_parmap(List.nth (Str.split (Str.regexp "/before/") fl) 1)
	with e -> !thefile (*normally file is standard.h, hope for the best*))
    | "P" ->
        (try drop_parmap(List.nth (Str.split (Str.regexp "/after/") fl) 1)
         with e -> !thefile)
    | _ -> failwith "bad key"

let get_contained (table : (string * int, int * 'a) Hashtbl.t) key starter =
  if starter = [] (* can at least occur in -+ case *)
  then None
  else
  try
    Some
      (List.map string_of_hunk
	 (Common.nub
	    (List.concat
               (List.map
		  (fun starter ->
		    let fl = get_file key starter in
		    let start_line = starter.Coccilib.line in
		    let end_line = starter.Coccilib.line_end in
		    let lines = iota start_line end_line in
		    let hunks =
		      List.concat
			(List.map
			   (function line ->
			     try
			       [(line,
				 hunknum(Hashtbl.find table (fl,line)))]
			     with Not_found -> [])
			   lines) in
		    if hunks = []
		    then raise Not_found
		    else
		      let rec merge = function
			  [] -> failwith "not possible"
			| [(line,num)] -> [((line,line),num)]
			| (line,num) :: rest ->
			    match merge rest with
			      [] -> failwith "not possible"
			    | ((_,line1),num1)::rest when num = num1 ->
				((line,line1),num1) :: rest
			    | rest -> ((line,line),num)::rest in
		      let infos = merge hunks in
		      match infos with
			[] -> failwith "not possible"
		      | _ ->
			  List.map
			    (function ((start_line,end_line),start_hunk) ->
			      (key,fl,start_hunk,start_line,end_line))
			    infos)
		  starter))))
  with Not_found -> None

(*Just map here, collect all the hunks between the starter and ender points. If the starter and ender as concrete these should be the same, if the starter and ender are meta these can be different.*)
let do_check_in_same_hunk table key region =
  let res =
    List.map
      (function starter_ender ->
	let (is_concrete,starter,ender) =
	  match starter_ender with
	    Concrete(starter,ender) -> (true,starter,ender)
	  | Meta(starter,ender) -> (false,starter,ender) in
	if starter = [] || ender = [] (* can at least occur in -+ case *)
	then None
	else
	  let compare_by_line p1 p2 =
	    compare p1.Coccilib.line p2.Coccilib.line_end in
	  let (starter,ender) =
	    normalize_points compare_by_line starter ender in
	  try
	    Some
	      (Common.nub
		 (List.concat
		    (List.map2
		       (fun starter ender ->
			 let fl = get_file key starter in
			 let start_line = starter.Coccilib.line in
			 let end_line = ender.Coccilib.line_end in
			 let lines = iota start_line end_line in
			 let hunks =
			   Common.nub
			     (List.map
				(fun ln -> hunknum (Hashtbl.find table (fl,ln)))
				lines) in
			 if not is_concrete || List.length hunks = 1
			 then
			   List.map (fun h -> (key,fl,h,start_line,end_line))
			     hunks
			 else raise Not_found)
		       starter ender)))
	  with Not_found -> None)
      region in
  if List.exists (fun x -> x = None) res
  then None
  else
    Some
      (List.fold_left
	 (fun prev ->
	   function
	       Some l -> List.append l prev
	     | None -> failwith "not possible")
	 [] res)

let check_in_same_hunk table key starter ender =
  match do_check_in_same_hunk table key [Concrete(starter,ender)] with
    Some l -> Some (List.map string_of_hunk l)
  | None -> None

let in_or_before hunk point choose =
  let end_line = point.Coccilib.line_end in
  try
    let (_,(_,_,mender,_,pender)) = Hashtbl.find hunks_to_lines hunk in
    end_line <= choose mender pender
  with Not_found -> false

let in_or_after hunk point choose =
  let line = point.Coccilib.line in
  try
    let (_,(_,mstarter,_,pstarter,_)) = Hashtbl.find hunks_to_lines hunk in
    line >= choose mstarter pstarter
  with Not_found -> false

let empty_check hunks other dir choose =
  match hunks with
    None -> None
  | Some hunks ->
      if List.length hunks = List.length other
      then
	try
	  Some
	    (Common.nub
	       (List.concat
		  (List.map2
		     (fun ((_,_,h,_,_) as hunk) p ->
		       if dir h p choose then [hunk] else raise Not_found)
		     hunks other)))
	with Not_found -> None
      else (Printf.eprintf "range-empty: lengths must be the same\n"; None)

let do_check_pair_in_same_hunk = function
    (Region(mstarter_mender),Region(pstarter_pender)) ->
      let mhunks = do_check_in_same_hunk minus_hunks "M" mstarter_mender in
      let phunks = do_check_in_same_hunk plus_hunks "P" pstarter_pender in
	(match (mhunks,phunks) with
	  (None,_) | (_,None) -> None
	| (Some mhunks,Some phunks) ->
	    (* not sure this is good enough... *)
	    let res =
	      try Some(normalize_points compare mhunks phunks)
	      with Failure _ -> None in
	    match res with
	      None -> None
	    | Some(mhunks,phunks) ->
		try
		  Some
		    (Common.nub
		       (List.concat
			  (List.map2
			     (fun
			       ((_,_,mh,_,_) as mhunk)
				 ((_,_,ph,_,_) as phunk) ->
				   if mh = ph
				   then [mhunk;phunk]
				   else raise Not_found)
			     mhunks phunks)))
		with Not_found -> None)
  | (Region(mstarter_mender),BeforeEmpty m) ->
      let mhunks = do_check_in_same_hunk minus_hunks "M" mstarter_mender in
      empty_check mhunks m in_or_before (fun m p -> p)
  | (Region(mstarter_mender),AfterEmpty m) ->
      let mhunks = do_check_in_same_hunk minus_hunks "M" mstarter_mender in
      empty_check mhunks m in_or_after (fun m p -> p)
  | (Region(mstarter_mender),AroundEmpty(m1,m2)) ->
      let mhunks = do_check_in_same_hunk minus_hunks "M" mstarter_mender in
      let res1 = empty_check mhunks m1 in_or_before (fun m p -> p) in
      let res2 = empty_check mhunks m2 in_or_after (fun m p -> p) in
      (match (res1,res2) with
	(Some(h1),Some(h2)) -> Some(Common.nub(List.append h1 h2))
      | _ -> None)
  | (BeforeEmpty m,Region(pstarter_pender)) ->
      let phunks = do_check_in_same_hunk plus_hunks "P" pstarter_pender in
      empty_check phunks m in_or_before (fun m p -> m)
  | (AfterEmpty m,Region(pstarter_pender)) ->
      let phunks = do_check_in_same_hunk plus_hunks "P" pstarter_pender in
      empty_check phunks m in_or_after (fun m p -> m)
  | (AroundEmpty(m1,m2),Region(pstarter_pender)) ->
      let phunks = do_check_in_same_hunk plus_hunks "P" pstarter_pender in
      let res1 = empty_check phunks m1 in_or_before (fun m p -> m) in
      let res2 = empty_check phunks m2 in_or_after (fun m p -> m) in
      (match (res1,res2) with
	(Some(h1),Some(h2)) -> Some(Common.nub(List.append h1 h2))
      | _ -> None)
  | _ -> failwith "not possible"

let check_pair_in_same_hunk minus plus =
  match do_check_pair_in_same_hunk (minus, plus) with
    Some l -> Some (List.map string_of_hunk l)
  | None -> None

let check_in_a_hunk(key,table,p) =
  let fl = get_file key (List.hd p) in
  List.exists
      (function p ->
	let start_line = p.Coccilib.line in
	try let _ = Hashtbl.find table (fl,start_line) in true
	with Not_found -> false)
      p

let check_intersects_a_hunk(key,table,p) =
  let fl = get_file key (List.hd p) in
  List.exists
    (function p ->
      let start_line = p.Coccilib.line in
      let end_line = p.Coccilib.line_end in
      List.exists
	(function ln ->
	  try let _ = Hashtbl.find table (fl,ln) in true
	  with Not_found -> false)
	(iota start_line end_line))
    p

let check_in_a_replacement_hunk(key,table,p) =
  let fl = get_file key (List.hd p) in
  List.exists
      (function p ->
	let start_line = p.Coccilib.line in
	try
	  let (_,(ty,_,_,_,_)) = Hashtbl.find table (fl,start_line) in
	  ty = REPLACEMENT
	with Not_found -> false)
      p

let check_intersects_a_replacement_hunk(key,table,p) =
  let fl = get_file key (List.hd p) in
  List.exists
    (function p ->
      let start_line = p.Coccilib.line in
      let end_line = p.Coccilib.line_end in
      List.exists
	(function ln ->
	  try
	    let (_,(ty,_,_,_,_)) = Hashtbl.find table (fl,ln) in
	    ty = REPLACEMENT
	  with Not_found -> false)
	(iota start_line end_line))
    p

(* pretty inefficient for the moment... *)
let find_pre_hunk tbl file line =
  let rec loop = function
      0 -> None
    | n ->
	let attempt =
	  try Some (Hashtbl.find tbl (file,line))
	  with Not_found -> None in
	match attempt with
	  Some hunk -> Some hunk
	| None -> loop (n-1) in
  loop line

(* Only concerned about success and failure, so just return boolean *)
let do_check_point_correspondence file bef_line bef_col aft_line aft_col =
  let bef_hunk =
    try Some(Hashtbl.find minus_hunks (file,bef_line))
    with Not_found -> None in
  let aft_hunk =
    try Some(Hashtbl.find plus_hunks (file,aft_line))
    with Not_found -> None in
  match (bef_hunk,aft_hunk) with
    (Some _,None) | (None,Some _) -> false
  | (Some bef_hunk, Some aft_hunk) -> fst bef_hunk = fst aft_hunk
  | (None,None) ->
      if bef_col = aft_col
      then
	begin
	  let bef_pre_hunk = search file bef_line !minus_tree in
	  match bef_pre_hunk with
	    None ->
	      let aft_pre_hunk = search file aft_line !plus_tree in
	      (match aft_pre_hunk with
		None -> bef_line = aft_line
	      | Some _ -> false)
	  | Some bef_hunk ->
	      let (_,(_,_,_,_,pender)) as hi =
		Hashtbl.find hunks_to_lines bef_hunk in
	      let hi = (* hunk_file must be ordered! *)
		try
		  let (fl,(_,_,_,pstarter,_)) =
		    Hashtbl.find hunks_to_lines (bef_hunk+1) in
		  if file = fl && pender <= aft_line && aft_line <= pstarter
		  then Some hi
		  else None
		with Not_found -> None in
	      match hi with
		Some (_,(_,mstarter,mender,pstarter,pender)) ->
		  let convert_bef_to_aft = bef_line - mender + pender in
		  convert_bef_to_aft = aft_line
	      | None -> false
	end
      else false (* cols do not match, so match impossible *)

let check_point_correspondence before after =
  (List.length before) = (List.length after) &&
  List.for_all2
   (fun bef aft ->
     let file =
       List.nth (Str.split (Str.regexp "/before/") bef.Coccilib.file) 1 in
     (do_check_point_correspondence file
	bef.Coccilib.line bef.Coccilib.col
	aft.Coccilib.line aft.Coccilib.col) &&
     (do_check_point_correspondence file
	bef.Coccilib.line_end bef.Coccilib.col_end
	aft.Coccilib.line_end aft.Coccilib.col_end))
  (List.sort compare before) (List.sort compare after)

@prerule depends on file in "/tmp/pi/after"@
position p__0__6__1 : script:ocaml() { check_in_a_hunk("P",plus_hunks,p__0__6__1) };
position p__0__6__2 : script:ocaml() { check_in_a_hunk("P",plus_hunks,p__0__6__2) };
position p__0__5__1 : script:ocaml() { check_intersects_a_hunk("P",plus_hunks,p__0__5__1) };
position p__0__4__1 : script:ocaml() { check_in_a_hunk("P",plus_hunks,p__0__4__1) };
position p__0__4__2 : script:ocaml() { check_in_a_hunk("P",plus_hunks,p__0__4__2) };
position p__0__3__1 : script:ocaml() { check_intersects_a_hunk("P",plus_hunks,p__0__3__1) };
position p__0__2__1 : script:ocaml() { check_in_a_hunk("P",plus_hunks,p__0__2__1) };
position p__0__1__1 : script:ocaml() { check_intersects_a_hunk("P",plus_hunks,p__0__1__1) };
position p__0__0__1 : script:ocaml() { check_in_a_hunk("P",plus_hunks,p__0__0__1) };
identifier f;
expression * e;
statement S1;
@@

e = f(...);
...   WHEN != e
if@p__0__6__1  (@p__0__6__2 
(
e@p__0__5__1  ==@p__0__4__1  NULL
|
NULL ==@p__0__4__2  e@p__0__3__1 
|
!@p__0__2__1 e@p__0__1__1 
)
 )@p__0__0__1  S1

@script:ocaml check_prerule depends on prerule@
p__0__0__1 << prerule.p__0__0__1 = [];
p__0__1__1 << prerule.p__0__1__1 = [];
p__0__2__1 << prerule.p__0__2__1 = [];
p__0__3__1 << prerule.p__0__3__1 = [];
p__0__4__1 << prerule.p__0__4__1 = [];
p__0__4__2 << prerule.p__0__4__2 = [];
p__0__5__1 << prerule.p__0__5__1 = [];
p__0__6__1 << prerule.p__0__6__1 = [];
p__0__6__2 << prerule.p__0__6__2 = [];
found_hunks;
@@

let res = ref [] in

let flag =
(let p__0__0__1 = check_in_same_hunk plus_hunks "P" p__0__0__1 p__0__0__1 in
match p__0__0__1 with
      None -> false
    | Some l -> (res := List.append l !res; true) &&2
let p__0__1__1 = get_contained plus_hunks "P" p__0__1__1 in
match p__0__1__1 with
      None -> false
    | Some l -> (res := List.append l !res; true) &&2
let p__0__2__1 = check_in_same_hunk plus_hunks "P" p__0__2__1 p__0__2__1 in
match p__0__2__1 with
      None -> false
    | Some l -> (res := List.append l !res; true) &&2
let p__0__3__1 = get_contained plus_hunks "P" p__0__3__1 in
match p__0__3__1 with
      None -> false
    | Some l -> (res := List.append l !res; true) &&2
let p__0__4__1 = check_in_same_hunk plus_hunks "P" p__0__4__1 p__0__4__2 in
match p__0__4__1 with
      None -> false
    | Some l -> (res := List.append l !res; true) &&2
let p__0__5__1 = get_contained plus_hunks "P" p__0__5__1 in
match p__0__5__1 with
      None -> false
    | Some l -> (res := List.append l !res; true) &&2
let p__0__6__1 = check_in_same_hunk plus_hunks "P" p__0__6__1 p__0__6__2 in
match p__0__6__1 with
      None -> false
    | Some l -> (res := List.append l !res; true)) in
if not flag
then Coccilib.include_match false
else
found_hunks := make_ident (String.concat "," !res)

@before_callstart depends on file in "/tmp/pi/before"@
position xp;
expression e;
identifier prerule.f;
@@

e = f@xp (...);

@after_callstart depends on file in "/tmp/pi/after" && before_callstart@
position xp;
expression e;
identifier prerule.f;
@@

e = f@xp (...);

@script:ocaml check_before_callstart_after_callstart depends on before_callstart && !after_callstart@
@@

Coccilib.include_match false

@script:ocaml local_check_after_callstart depends on after_callstart@
xp_1 << before_callstart.xp = [];
xp_2 << after_callstart.xp = [];
@@

if not (check_point_correspondence xp_1 xp_2)
then Coccilib.include_match false

@arule depends on file in "/tmp/pi/after"@
position p__0__7__1 : script:ocaml() { check_in_a_hunk("P",plus_hunks,p__0__7__1) };
position p__0__7__2 : script:ocaml() { check_in_a_hunk("P",plus_hunks,p__0__7__2) };
position p__0__6__1 : script:ocaml() { check_intersects_a_hunk("P",plus_hunks,p__0__6__1) };
position p__0__5__1 : script:ocaml() { check_in_a_hunk("P",plus_hunks,p__0__5__1) };
position p__0__5__2 : script:ocaml() { check_in_a_hunk("P",plus_hunks,p__0__5__2) };
position p__0__4__1 : script:ocaml() { check_intersects_a_hunk("P",plus_hunks,p__0__4__1) };
position p__0__3__1 : script:ocaml() { check_in_a_hunk("P",plus_hunks,p__0__3__1) };
position p__0__2__1 : script:ocaml() { check_intersects_a_hunk("P",plus_hunks,p__0__2__1) };
position p__0__1__1 : script:ocaml() { check_in_a_hunk("P",plus_hunks,p__0__1__1) };
position p__0__0__1 : script:ocaml() { check_intersects_a_hunk("P",plus_hunks,p__0__0__1) };
expression * e;
statement S1;
identifier prerule.f;
position after_callstart.xp;
@@

e = f@xp (...);
...   WHEN != e
if@p__0__7__1  (@p__0__7__2 
(
e@p__0__6__1  ==@p__0__5__1  NULL
|
NULL ==@p__0__5__2  e@p__0__4__1 
|
!@p__0__3__1 e@p__0__2__1 
)
 )@p__0__1__1  S1@p__0__0__1 

@script:ocaml check_arule depends on arule@
p__0__0__1 << arule.p__0__0__1 = [];
p__0__1__1 << arule.p__0__1__1 = [];
p__0__2__1 << arule.p__0__2__1 = [];
p__0__3__1 << arule.p__0__3__1 = [];
p__0__4__1 << arule.p__0__4__1 = [];
p__0__5__1 << arule.p__0__5__1 = [];
p__0__5__2 << arule.p__0__5__2 = [];
p__0__6__1 << arule.p__0__6__1 = [];
p__0__7__1 << arule.p__0__7__1 = [];
p__0__7__2 << arule.p__0__7__2 = [];
found_hunks;
@@

let res = ref [] in

let flag =
(let p__0__0__1 = get_contained plus_hunks "P" p__0__0__1 in
match p__0__0__1 with
      None -> false
    | Some l -> (res := List.append l !res; true) &&2
let p__0__1__1 = check_in_same_hunk plus_hunks "P" p__0__1__1 p__0__1__1 in
match p__0__1__1 with
      None -> false
    | Some l -> (res := List.append l !res; true) &&2
let p__0__2__1 = get_contained plus_hunks "P" p__0__2__1 in
match p__0__2__1 with
      None -> false
    | Some l -> (res := List.append l !res; true) &&2
let p__0__3__1 = check_in_same_hunk plus_hunks "P" p__0__3__1 p__0__3__1 in
match p__0__3__1 with
      None -> false
    | Some l -> (res := List.append l !res; true) &&2
let p__0__4__1 = get_contained plus_hunks "P" p__0__4__1 in
match p__0__4__1 with
      None -> false
    | Some l -> (res := List.append l !res; true) &&2
let p__0__5__1 = check_in_same_hunk plus_hunks "P" p__0__5__1 p__0__5__2 in
match p__0__5__1 with
      None -> false
    | Some l -> (res := List.append l !res; true) &&2
let p__0__6__1 = get_contained plus_hunks "P" p__0__6__1 in
match p__0__6__1 with
      None -> false
    | Some l -> (res := List.append l !res; true) &&2
let p__0__7__1 = check_in_same_hunk plus_hunks "P" p__0__7__1 p__0__7__2 in
match p__0__7__1 with
      None -> false
    | Some l -> (res := List.append l !res; true)) in
if not flag
then Coccilib.include_match false
else
found_hunks := make_ident (String.concat "," !res)

@script:ocaml commit_arule@
hunks_for_arule << check_arule.found_hunks;
hunks_for_prerule << check_prerule.found_hunks;
@@

start_record();
Printf.printf "%s\n" hunks_for_arule;
Printf.printf "%s\n" hunks_for_prerule;
end_record()

