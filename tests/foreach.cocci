@@
local idexpression n;
expression e1,e2;
iterator name for_each_node_by_name, for_each_node_by_type,
for_each_compatible_node, for_each_matching_node,
for_each_matching_node_and_match, for_each_child_of_node,
for_each_available_child_of_node, for_each_node_with_property;
statement S;
@@

(
+ for_each_node_by_name(n,e1)
  S
|
+ for_each_node_by_type(n,e1)
  S
|
+ for_each_compatible_node(n,e1,e2)
  S
|
+ for_each_matching_node(n,e1)
  S
|
+ for_each_matching_node_and_match(n,e1,e2)
  S
|
+ for_each_child_of_node(e1,n)
  S
|
+ for_each_available_child_of_node(e1,n)
  S
|
+ for_each_node_with_property(n,e1)
  S
)
