@exists@
iterator name list_for_each, list_for_each_entry;
@@

- list_for_each
+ list_for_each_entry
     (...) {
   ... when any
-  list_entry(...)
   ... when any
}

@exists@
iterator name list_for_each_safe, list_for_each_entry_safe;
@@

- list_for_each_safe
+ list_for_each_entry_safe
     (...) {
   ... when any
-  list_entry(...)
   ... when any
}