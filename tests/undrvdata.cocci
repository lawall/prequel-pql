@@
expression e;
@@
- ac97_set_drvdata(e,NULL);

@@
expression e;
@@
- amba_set_drvdata(e,NULL);

@@
expression e;
@@
- bcma_set_drvdata(e,NULL);

@@
expression e;
@@
- dev_set_drvdata(e,NULL);

@@
expression e;
@@
- dio_set_drvdata(e,NULL);

@@
expression e;
@@
- ecard_set_drvdata(e,NULL);

@@
expression e;
@@
- eisa_set_drvdata(e,NULL);

@@
expression e;
@@
- epc_set_drvdata(e,NULL);

@@
expression e;
@@
- epf_set_drvdata(e,NULL);

@@
expression e;
@@
- err_reset_drvdata(e,NULL);

@@
expression e;
@@
- gameport_set_drvdata(e,NULL);

@@
expression e;
@@
- gnss_set_drvdata(e,NULL);

@@
expression e;
@@
- greybus_set_drvdata(e,NULL);

@@
expression e;
@@
- hci_set_drvdata(e,NULL);

@@
expression e;
@@
- hid_set_drvdata(e,NULL);

@@
expression e;
@@
- hsi_client_set_drvdata(e,NULL);

@@
expression e;
@@
- hsi_controller_set_drvdata(e,NULL);

@@
expression e;
@@
- hsi_port_set_drvdata(e,NULL);

@@
expression e;
@@
- hv_set_drvdata(e,NULL);

@@
expression e;
@@
- iio_device_set_drvdata(e,NULL);

@@
expression e;
@@
- iio_trigger_set_drvdata(e,NULL);

@@
expression e;
@@
- input_set_drvdata(e,NULL);

@@
expression e;
@@
- lm_set_drvdata(e,NULL);

@@
expression e;
@@
- locomo_set_drvdata(e,NULL);

@@
expression e;
@@
- macio_set_drvdata(e,NULL);

@@
expression e;
@@
- maple_set_drvdata(e,NULL);

@@
expression e;
@@
- mcb_set_drvdata(e,NULL);

@@
expression e;
@@
- mcp_set_drvdata(e,NULL);

@@
expression e;
@@
- mdev_set_drvdata(e,NULL);

@@
expression e;
@@
- mei_cldev_set_drvdata(e,NULL);

@@
expression e;
@@
- memstick_set_drvdata(e,NULL);

@@
expression e;
@@
- mipi_dsi_set_drvdata(e,NULL);

@@
expression e;
@@
- mips_cdmm_set_drvdata(e,NULL);

@@
expression e;
@@
- nci_set_drvdata(e,NULL);

@@
expression e;
@@
- nfc_digital_set_drvdata(e,NULL);

@@
expression e;
@@
- nfc_set_drvdata(e,NULL);

@@
expression e;
@@
- nubus_set_drvdata(e,NULL);

@@
expression e;
@@
- parisc_set_drvdata(e,NULL);

@@
expression e;
@@
- pci_set_drvdata(e,NULL);

@@
expression e;
@@
- phy_set_drvdata(e,NULL);

@@
expression e;
@@
- platform_set_drvdata(e,NULL);

@@
expression e;
@@
- pnp_set_drvdata(e,NULL);

@@
expression e;
@@
- ps3_system_bus_set_drvdata(e,NULL);

@@
expression e;
@@
- regulator_set_drvdata(e,NULL);

@@
expression e;
@@
- reset_drvdata(e,NULL);

@@
expression e;
@@
- sa1111_set_drvdata(e,NULL);

@@
expression e;
@@
- sdio_set_drvdata(e,NULL);

@@
expression e;
@@
- serdev_controller_set_drvdata(e,NULL);

@@
expression e;
@@
- serdev_device_set_drvdata(e,NULL);

@@
expression e;
@@
- serio_set_drvdata(e,NULL);

@@
expression e;
@@
- snd_soc_card_set_drvdata(e,NULL);

@@
expression e;
@@
- snd_soc_codec_set_drvdata(e,NULL);

@@
expression e;
@@
- snd_soc_component_set_drvdata(e,NULL);

@@
expression e;
@@
- snd_soc_dai_set_drvdata(e,NULL);

@@
expression e;
@@
- spi_mem_set_drvdata(e,NULL);

@@
expression e;
@@
- spi_set_drvdata(e,NULL);

@@
expression e;
@@
- spmi_controller_set_drvdata(e,NULL);

@@
expression e;
@@
- ssb_set_drvdata(e,NULL);

@@
expression e;
@@
- sunxi_rsb_device_set_drvdata(e,NULL);

@@
expression e;
@@
- tb_service_set_drvdata(e,NULL);

@@
expression e;
@@
- tifm_set_drvdata(e,NULL);

@@
expression e;
@@
- typec_altmode_set_drvdata(e,NULL);

@@
expression e;
@@
- ulpi_set_drvdata(e,NULL);

@@
expression e;
@@
- umc_set_drvdata(e,NULL);

@@
expression e;
@@
- unset_drvdata(e,NULL);

@@
expression e;
@@
- video_set_drvdata(e,NULL);

@@
expression e;
@@
- vlynq_set_drvdata(e,NULL);

@@
expression e;
@@
- watchdog_set_drvdata(e,NULL);

@@
expression e;
@@
- zorro_set_drvdata(e,NULL);
