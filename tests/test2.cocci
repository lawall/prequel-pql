virtual before, after

@@
expression x,e1,e2,e3;
@@

(
- x = kzalloc(
+ x = devm_kzalloc(e1,
     e2,e3)
|
- x = kmalloc(
+ x = devm_kmalloc(e1,
     e2,e3)
)