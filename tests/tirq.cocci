@@
identifier fn,irq,desc;
@@

void fn(
-       unsigned int irq,
        struct irq_desc *desc) { ... }

@@
identifier fn,irq,irq2,desc;
@@

void fn(
        unsigned int
- irq
+ irq2
        ,
        struct irq_desc *desc) { ... }
