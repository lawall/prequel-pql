@r@
expression e;
@@

- init_timer(&e);

@added@
expression r.e;
@@

+ setup_timer(&e,...);

@depends on added@
expression r.e;
expression f;
@@

- e.function = f;

@depends on added@
expression r.e;
expression f;
@@

- e.data = f;

