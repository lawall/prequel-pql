 ** Compilation **

You must first install a recent version of
 - OCaml (at least 3.11),
   see http://caml.inria.fr/download.en.html
   At least, OCaml 3.12 is required to use the OCaml scripting
   feature in the SmPL code.
 - The Menhir parser generator (at least 20080912),
   see http://cristal.inria.fr/~fpottier/menhir/

Prequel has only been tested on Ubuntu.

On Debian/Ubuntu, install the following packages
 - pkg-config (optional, but strongly recommended)
 - ocaml-native-compilers (or alternatively ocaml)
 - ocaml-findlib
 - menhir and libmenhir-ocaml-dev

Then simply type
 make
in the src directory.

Make targets of interest:
opt           compiles just the optimized version, produces prequel.opt

No make target will produce the bytecode version, prequel

There is no install target.  Just copy prequel or prequel.opt wherever you like.


Note: The test subdirectory is intended for developers. In particular, you
      should not expect that every test will run or do something useful.
