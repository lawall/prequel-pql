let git = ref "/run/shm/linux-stable"
let prequel = ref "../src/prequel"
let pq = ref ""
let starter = ref "v3.0"
let ender = ref "v5.0"
let extra = ref "" (* extra arguments *)

(* ------------------------------------------------------------------- *)

let process_output_to_list2 = fun command ->
  let chan = Unix.open_process_in command in
  let res = ref ([] : string list) in
  let rec process_otl_aux () =
    let e = input_line chan in
    res := e::!res;
    process_otl_aux() in
  try process_otl_aux ()
  with End_of_file ->
    let stat = Unix.close_process_in chan in (List.rev !res,stat)
let cmd_to_list command =
  let (l,_) = process_output_to_list2 command in l
let process_output_to_list = cmd_to_list
let cmd_to_list_and_status = process_output_to_list2

let hashadd tbl k v =
  let cell =
    try Hashtbl.find tbl k
    with Not_found ->
      let cell = ref [] in
      Hashtbl.add tbl k cell;
      cell in
  if not (List.mem v !cell) then cell := v :: !cell

(* ------------------------------------------------------------------- *)

let get_ranges _ =
  let tags = cmd_to_list (Printf.sprintf "cd %s; git tag | grep -v rc" !git) in
  let parse s =
    let rest =
      Str.split (Str.regexp_string ".") (String.sub s 1 (String.length s - 1)) in
    try List.map int_of_string rest with _ -> (Printf.eprintf "failure on %s\n" s; []) in
  let starter = parse !starter in
  let ender = parse !ender in
  let tags = List.map parse tags in
  let tags = List.filter (fun x -> not(x = [])) tags in
  let rec bigger s b =
    match (s,b) with
      ([],b) -> true
    | (s,[]) -> false
    | (s1::s,b1::b) ->
	b1 > s1 || (b1 = s1 && bigger s b) in
  let tags =
    List.filter (function x -> bigger starter x && bigger x ender) tags in
  let tbl = Hashtbl.create 101 in
  List.iter
    (fun x -> hashadd tbl [List.hd x; List.hd (List.tl x)] (List.tl (List.tl x)))
    tags;
  let tags = Hashtbl.fold (fun k v r -> (k,!v)::r) tbl [] in
  let tags = List.sort compare tags in
  List.map
    (function (k,v) ->
      let v = List.sort (fun x y -> if bigger x y then -1 else 1) v in
      (k@List.hd v,k@List.hd (List.rev v)))
    tags

(* ------------------------------------------------------------------- *)

let options =
  ["--git", Arg.Set_string git, "  path to git repository";
    "--prequel", Arg.Set_string prequel, "  path to prequel";
    "--pq", Arg.Set_string pq, "  patch query";
    "--start", Arg.Set_string starter, "  start major version";
    "--end", Arg.Set_string ender, "  end major version"]

let anonymous s =
  Printf.eprintf "argument: |%s|\n" s;
  if Filename.check_suffix s ".cocci" then pq := s else extra := !extra ^ " --" ^ s;
  Printf.eprintf "pq |%s| extra |%s|\n" !pq !extra

let usage = ""

let _ =
  Arg.parse (Arg.align options) anonymous usage;
  let ranges = get_ranges() in
  let resdir = Filename.remove_extension !pq in
  (if not (Sys.file_exists resdir) then ignore(Sys.command ("mkdir "^resdir)));
  let cmd (vs,ve) =
    let vs = "v"^(String.concat "." (List.map string_of_int vs)) in
    let ve = "v"^(String.concat "." (List.map string_of_int ve)) in
    let cmd =
      Printf.sprintf "%s --sp %s --git %s --cores 40 --commits %s..%s --all-patches --line-diff -o %s/%s_%s_%s.porg %s"
	!prequel!pq !git vs ve resdir resdir vs ve !extra in
    Printf.eprintf "%s\n" cmd; flush stderr;
    ignore (Sys.command cmd) in
  List.iter cmd ranges
