#!/usr/bin/python3

from os import path, chdir, getcwd, mkdir
from shutil import rmtree
import subprocess
import argparse
import tempfile
from multiprocessing import Pool, cpu_count

DEFAULT_CHUNCK_SIZE=1000

class ParsingError(Exception):
    pass


def create_chunck(l, size):
    for i in range(0, len(l), size):
        yield l[i:i+size]


def get_hashes(commit_range):
    """Extract commit hashes in `commit_range`"""
    LOG_OPTIONS = [
        "--no-merges", 
        "--diff-filter=M", 
        "--pretty=format:%h",
        "--",
        "*.c",
        "*.h",
    ]

    result = subprocess.run(["git", "log", commit_range] + LOG_OPTIONS,
                            stdout=subprocess.PIPE, check=True)

    hashes = [hash.decode() for hash in result.stdout.splitlines()]
    return hashes


def extract_files(hashes, working_dir, show_options=[], write_filter=lambda x: True):
    """Extract files from commit in `hashes` to use in mkid database`"""
    with subprocess.Popen(["git", "show"] + show_options + hashes,
                          stdout=subprocess.PIPE,
                          universal_newlines=True, bufsize=1) as proc:
        current_commit = None
        current_file = None
        current_output = []

        for line in proc.stdout:
            if line.startswith("commit ") and len(line.rstrip()) == 47:
                if current_commit:
                    current_file.writelines(filter(write_filter, current_output))
                    current_file.close()
                    current_output = []

                current_commit = line[len("commit "):len("commit ") + 12]
                current_file = open("%s/%s.c" % (working_dir, current_commit), "w")
            if not current_commit:
                raise(ParsingError)

            current_output.append(line)

        current_file.close()

def extract_files_fileindex(hashes, working_dir):
    """Special extract files function for fileindex"""
    with subprocess.Popen(["git", "show", "--name-only", "--pretty=format:"] + hashes,
                          stdout=subprocess.PIPE,
                          universal_newlines=True, bufsize=1) as proc:
        hash_iter = iter(hashes)
        current_commit = next(hash_iter)
        current_file = open("%s/%s.c" % (working_dir, current_commit), "w")
        current_output = []

        for line in proc.stdout:
            if line.strip(): # Line is not empty
                replaced = line.replace("/", "QQ").replace("-", "FF").replace(".", "XX")
                current_output.append(replaced)
            else: # Empty line mean that we have a commit change
                current_file.writelines(current_output)
                current_file.close()
                current_output = []
                current_commit = next(hash_iter)
                current_file = open("%s/%s.txt" % (working_dir, current_commit), "w")

        current_file.close()


def _filter_true(line):
    return True


def _filter_plus(line):
    return line.startswith("+") and not line.startswith("+++")


def _filter_minus(line):
    return line.startswith("-") and not line.startswith("---")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("git", help="path to linux git")
    parser.add_argument("range", help="commit range used for indexes construction")
    parser.add_argument("-o", "--output", default=getcwd(),
                        help="directory to store indexes (default current_dir)")
    parser.add_argument("-j", "--jobs", type=int, default=cpu_count(),
                        help="number of threads")
    args = parser.parse_args()

    git = path.abspath(args.git)
    output_dir = path.abspath(args.output)
    working_dir = tempfile.mkdtemp()
    
    chdir(git)
    hashes = get_hashes(args.range)

    PASSES = (
            ("U0", ["-U0"], _filter_true),
            ("U3", ["-U3"], _filter_true),
            ("plus_U0", ["-U0"], _filter_plus),
            ("minus_U0", ["-U0"], _filter_minus),
            ("plus_word_U0", ["-U0", "--word-diff=porcelain"], _filter_plus),
            ("minus_word_U0", ["-U0", "--word-diff=porcelain"], _filter_minus),
    )

    # Split passes between available cores
    for current_pass in PASSES:
        name, options, write_filter = current_pass
        pass_dir = "%s/%s" % (working_dir, name)
        mkdir(pass_dir)
        with Pool(args.jobs) as pool:
            for chunck in create_chunck(hashes, DEFAULT_CHUNCK_SIZE):
                pool.apply_async(extract_files, (chunck, pass_dir, options, write_filter))

            pool.close()
            pool.join()

        # Create mkid database
        subprocess.run(["mkid", "-i", "C", "-o", "%s/linux_idutils_%s.index" % (output_dir, name), pass_dir],
                       check=True)

    # Fileindex case
    fileindex_dir = "%s/files" % working_dir
    mkdir(fileindex_dir)
    with Pool(args.jobs) as pool:
        for chunck in create_chunck(hashes, DEFAULT_CHUNCK_SIZE):
            pool.apply_async(extract_files_fileindex, (chunck, fileindex_dir))

        pool.close()
        pool.join()

    subprocess.run(["mkid", "-i", "text", "-o", "%s/linux_idutils_files.index" % output_dir, fileindex_dir],
                   check=True)

    rmtree(working_dir)
